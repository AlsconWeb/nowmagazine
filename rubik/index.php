<?php get_header();?>
<?php
$rubik_option = rubik_core::bk_get_global_var('rubik_option');
$archiveLayout = 'masonry-nosb';
$bk_post_icon = 'show';
?>
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts('post_type=post&post_status=publish&paged=' . $paged);
?>
<div id="body-wrapper" class="wp-page">
    <div class="bkwrapper container">		
        <div class="row">			
            <div class="bk-blog-content bkpage-content <?php if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3'))): echo 'col-md-8 has-sb'; else: echo 'col-md-12 fullwidth';  endif;?>">
                <div class="row">
                    <div id="main-content" class="clear-fix" role="main">
                  		<div class="page-title-wrapper col-md-12">
                    		<div class="module-title">
                                <h2 class="heading"><?php esc_html_e( 'Latest Articles', 'rubik' );?></h2> 
                            </div>
                        </div>
                    <?php
                        if (have_posts()) {
                            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
                            $bk_contentout = new bk_contentout2;
                
                            $custom_var = array (
                                'thumbnail'     => 'rubik-masonry-size',
                                'post-icon'     => $bk_post_icon,
                                'except_length' => 20,
                            );
                            $bk_no_thumb = '';
                            
                            echo '<div class="content-wrap bk-masonry">';
                            echo '<ul class="bk-masonry-content clearfix">';
                                while (have_posts()): the_post();
                                    if(is_sticky(get_the_ID())) {
                                        $stickyClass = 'sticky-rubik-post';
                                    }else {
                                        $stickyClass = '';
                                    }
                                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                                        $bk_no_thumb = 'bk_no_thumb';
                                    }else {
                                        $bk_no_thumb = '';
                                    }
                                    echo '<li class="col-md-4 col-sm-6 item '.$stickyClass.'">';
                                    echo '<div class="row-type content_out '.$bk_no_thumb.'">';
                                    echo rubik_core::bk_render_html_string($bk_contentout->render($custom_var));
                                    echo '</div>';
                                    echo '</li>';
                                endwhile;
                            echo '</ul>';
                            echo '</div>';
                            if (function_exists("rubik_paginate")) {
                                echo '<div class="rubik_pagination buttons-align-center col-md-12">';
                                rubik_paginate();
                                echo '</div>';
                            }
                        }
                    ?>
            
    	            </div> <!-- end #main -->
                </div>
            </div> <!-- end #bk-content -->
            <?php
                if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3'))) {?>
                    <div class="sidebar col-md-4">
                        <div class="sidebar-wrap <?php if($rubik_option['category-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-category-sidebar">
                            <?php
                                if (isset($cat_opt[$cur_cat_id]) && is_array($cat_opt[$cur_cat_id]) && array_key_exists('sb_category',$cat_opt[$cur_cat_id])) { 
                                    $bkcatSidebar = $cat_opt[$cur_cat_id]['sb_category'];
                                    if ((strlen($bkcatSidebar) != 0)&&($bkcatSidebar != 'global')) { 
                                        dynamic_sidebar($bkcatSidebar);
                                    }else {
                                        get_sidebar();
                                    }
                                }else {
                                    get_sidebar();
                                }
                            ?>
                        </div>
                    </div>
                <?php }
            ?>
        </div>
    </div>
</div>   
<?php get_footer(); ?>