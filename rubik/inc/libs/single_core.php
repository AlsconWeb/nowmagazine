<?php
if (!class_exists('single_core')) {
    class single_core {
        static function bk_get_post_share($bkPostId, $social_share, $share_items) { 
            $share_box ='';
            if ( function_exists( 'bk_share_box_no_cnt' ) ) {
                $share_box .= bk_share_box_no_cnt($bkPostId, $social_share, $share_items);
            }
            return $share_box;
        }    
        
    /**
     * Video Post Format
     */
        static function bk_get_video_postFormat($postFormat) { 
            $videoFormat = '';
            if ($postFormat['url'] != null) {
                if ($postFormat['popup'] == 1) {
                    $videoFormat .= '<div class="icon-play"><a class="video-popup-link" href="'.esc_url($postFormat['url']).'"><i class="fa fa-play"></i></a></div>';
                }else {
                    $videoFormat .= '<div class="bk-embed-video">';
                    $videoFormat .= '<div class="bk-frame-wrap">';
                    $videoFormat .= $postFormat['iframe'];
                    $videoFormat .= '</div></div> <!-- End embed-video -->';
                }
            }else {
                $videoFormat .= '';
            }
            return $videoFormat;
        }
    /**
     * Audio Post Format
     */
        static function bk_get_audio_postFormat($bkPostId, $postFormat, $audioType) { 
            $audioFormat = '';
            if ($postFormat['url'] != null) {
                preg_match('/src="([^"]+)"/', wp_oembed_get( $postFormat['url'] ), $match);
                if(isset($match[1])) {
                    $bkNewURL = $match[1];
                }else {
                    return null;
                }
                if ($postFormat['popup'] == 1) {
                    $audioFormat .= '<div class="icon-play"><a class="img-popup-link mfp-iframe audio" href="'.esc_url($bkNewURL).'"><i class="fa fa-volume-up"></i></a></div>';
                }else {
                    $audioFormat .= '<div class="bk-embed-audio"><div class="bk-frame-wrap">';
                    $audioFormat .= wp_oembed_get( $postFormat['url'] );
                    $audioFormat .= '</div></div>';
                }
            }else {
                $audioFormat .= '';
            }
            return $audioFormat;
        }
     /**
     * Gallery Post Format
     */
        static function bk_get_gallery_postFormat($galleryImages) { 
            $galleryFormat = '';
            $galleryFormat .= '<div class="gallery-wrap">';
            $galleryFormat .= '<div id="bk-gallery-slider" class="flexslider">';
            $galleryFormat .= '<ul class="slides">';
            foreach ( $galleryImages as $image ){
                $attachment_url = wp_get_attachment_image_src($image['ID'], 'full', true);
                $attachment = get_post($image['ID']);
                $caption = apply_filters('the_title', $attachment->post_excerpt);
                $galleryFormat .= '<li class="bk-gallery-item">';
                    $galleryFormat .= '<a class="zoomer" title="'.esc_attr($caption).'" data-source="'.$attachment_url[0].'" href="'.$attachment_url[0].'">'.wp_get_attachment_image($attachment->ID, 'rubik-1000-540').'</a>';
                    if (strlen($caption) > 0) {
                        $galleryFormat .= '<div class="caption">'.esc_attr($caption).'</div>';
                    }
                $galleryFormat .= '</li>';
            }
            $galleryFormat .= '</ul></div>';
            
            $galleryFormat .= '<div id="bk-gallery-carousel" class="flexslider">';
            $galleryFormat .= '<ul class="slides">';
            
            foreach ( $galleryImages as $image ){
                $attachment_url = wp_get_attachment_image_src($image['ID'], 'full', true);
                $attachment = get_post($image['ID']);
                $caption = apply_filters('the_title', $attachment->post_excerpt);
                $galleryFormat .= '<li class="bk-gallery-item">';
                    $galleryFormat .= wp_get_attachment_image($attachment->ID, 'rubik-150-105');
                $galleryFormat .= '</li>';
            }
            
            $galleryFormat .= '</ul></div>';
            
            $galleryFormat .= '</div><!-- Close gallery-wrap -->';
            return $galleryFormat; 
        }
     /**
     * Image Post Format
     */
        static function bk_get_image_postFormat($bkPostId) { 
            $attachmentID = get_post_meta($bkPostId, 'bk_image_upload', true );
            $thumb_url = wp_get_attachment_image_src($attachmentID, true);
            $imageFormat = '';
            $imageFormat .= '<div class="icon-play zoomer"><a class="img-popup-link" href="'.$thumb_url[0].'"><i class="fa fa-camera"></i></a></div>'; 
            return $imageFormat;
        }
    /**
     * Standard Post Format
     */
        static function bk_get_standard_postFormat($bkPostId, $imageSize = 'full') { 
            $imageFormat = '';
            $imageCaption = get_the_post_thumbnail_caption($bkPostId);
            $imageCaptionOutput = '';
            if($imageCaption != '') {
                $imageCaptionOutput = '<p class="wp-caption-text featured-image-caption">'.$imageCaption.'</p>';
            }else {
                $imageCaptionOutput = '';
            }
            $imageFormat .= '<div class="s-feat-img">'.get_the_post_thumbnail($bkPostId, $imageSize).$imageCaptionOutput.'</div>';
            return $imageFormat;        
        }
    
    
    /**
     * Post Format Detect
     */
        static function bk_post_format_detect($bkPostId) { 
            $bk_format = array();
    /** Video Post Format **/
            if(function_exists('has_post_format') && ( get_post_format( $bkPostId ) == 'video')){
                $bkURL = get_post_meta($bkPostId, 'bk_media_embed_code_post', true);
                $bkPopup = get_post_meta(get_the_ID(),'bk_popup_frame',true);
                $bkUrlParse = parse_url($bkURL);
                $bk_format['format'] = 'video';
                $bk_format['popup'] = $bkPopup;
                if (isset($bkUrlParse['host']) && (($bkUrlParse['host'] == 'www.youtube.com')||($bkUrlParse['host'] == 'youtube.com'))) { 
                    $video_id = rubik_core::parse_youtube($bkURL);
                    $bk_format['iframe'] = '<iframe width="1050" height="591" src="//www.youtube.com/embed/'.$video_id.'" allowFullScreen ></iframe>';
                    $bk_format['url'] = $bkURL;
                }else if (isset($bkUrlParse['host']) && (($bkUrlParse['host'] == 'www.vimeo.com')||($bkUrlParse['host'] == 'vimeo.com'))) {
                    $video_id = rubik_core::parse_vimeo($bkURL);
                    $bk_format['iframe'] = '<iframe src="//player.vimeo.com/video/'.$video_id.'?api=1&amp;title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff"></iframe>';
                    $bk_format['url'] = $bkURL;
                }else {
                    $bk_format['iframe'] = null;
                    $bk_format['url'] = null;
                    $bk_format['notice'] = esc_html__('please put youtube or vimeo video link to the video post format section', 'rubik');
                }
            }
    /** Audio Post Format **/        
            else if(function_exists('has_post_format') && (has_post_format('audio'))) {
                $bkURL = get_post_meta($bkPostId, 'bk_media_embed_code_post', true);
                $bkPopup = get_post_meta(get_the_ID(),'bk_popup_frame',true);
                $bkUrlParse = parse_url($bkURL);
                $bk_format['format'] = 'audio';
                $bk_format['popup'] = $bkPopup;
                if (isset($bkUrlParse['host']) && (($bkUrlParse['host'] == 'www.soundcloud.com')||($bkUrlParse['host'] == 'soundcloud.com'))) { 
                    $bk_format['url'] = $bkURL;
                }else {
                    $bk_format['url'] = null;
                }
            }
    /** Gallery post format **/
            else if( function_exists('has_post_format') && has_post_format( 'gallery') ) {
                $bk_format['popup'] = null;
                $bk_format['format'] = 'gallery';
            }
    /** Image Post Format **/
            else if( function_exists('has_post_format') && has_post_format( 'image') ) {
                $bk_format['popup'] = null;
                $bk_format['format'] = 'image';
            }
    /** Standard Post **/
            else {
                $bk_format['popup'] = null;
                $bk_format['format'] = 'standard';
            }
            return $bk_format;
            
        }
    /**
     * ************* Display Post format *****************
     *---------------------------------------------------
     */ 
    /**
     * BK WP Native Gallery
     */
         static function bk_native_gallery($bkPostId, $attachment_ids){
            global $rubik_justified_ids;
            $uid = rand();
            $rubik_justified_ids[]=$uid;
            wp_localize_script( 'rubik-customjs', 'justified_ids', $rubik_justified_ids );
            $ret = '';
            
            $ret .= '<div class="zoom-gallery justifiedgall_'.$uid.' justified-gallery" style="margin: 0px 0px 1.5em;">';
    						if ($attachment_ids) :					
    						foreach ($attachment_ids as $id) :
    							$attachment_url = wp_get_attachment_image_src( $id, 'full' );
                                $attachment = get_post($id);
    							$caption = apply_filters('the_title', $attachment->post_excerpt);
    					
                                $ret .= '<a class="zoomer" title="'.$caption.'" data-source="'.$attachment_url[0].'" href="'.$attachment_url[0].'">'.wp_get_attachment_image($attachment->ID, 'full').'</a>';
    
    						endforeach;
    						endif;
    			$ret .= '</div>';
                return $ret;
         }
        static function bk_post_format_display($bkPostId, $imageSize) { 
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $postFormat = array();
            $postFormat = self::bk_post_format_detect($bkPostId);
            $postFormatOut = '';
            
    /** Video **/
            if($postFormat['format'] == 'video') {
                $postFormatOut .= self::bk_get_video_postFormat($postFormat);
            }
    /** Audio **/
            else if($postFormat['format'] == 'audio') {
                $postFormatOut .= self::bk_get_audio_postFormat($bkPostId, $postFormat, null);
            }
    /** Gallery **/
            else if($postFormat['format'] == 'gallery') {
                $galleryImages = array();
                if(function_exists('rwmb_meta')):
                    $galleryImages = rwmb_meta( 'bk_gallery_content', $args = array('type' => 'image'), $bkPostId );
                else:
                    return null;
                endif;
                $galleryLength = count($galleryImages); 
                if ($galleryLength == 0) {
                    return null;
                }else {
                    $postFormatOut .= self::bk_get_gallery_postFormat($galleryImages);
                }
            }
    /** Standard **/
            else if($postFormat['format'] == 'standard') {
                $postFormatOut .= self::bk_get_standard_postFormat($bkPostId, $imageSize);
            }else {
                $postFormatOut .= '';
            }
            return $postFormatOut;
            
        }
    /**
     * Canvas box 
     */
         static function bk_canvas_box($bk_final_score){
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_best_rating = '10';
            $bk_review_final_score = floatval($bk_final_score);  
            $arc_percent = $bk_final_score*10;
            $bk_canvas_ret = '';                                       
            $score_circle = '<canvas class="rating-canvas" data-rating="'.$arc_percent.'"></canvas>';           
            $bk_canvas_ret .= '<div class="bk-score-box" itemprop="reviewRating" itemscope itemtype="https://schema.org/Rating">'.$score_circle.'<meta itemprop="worstRating" content="1"><meta itemprop="bestRating" content="'. $bk_best_rating .'"><span class="score" itemprop="ratingValue">'.$bk_review_final_score.'</span></div><!--close canvas-->';
            return $bk_canvas_ret;
        }
    /**
     * User Review
     */
        static function bk_user_review($bk_post_id){
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_user_rating = get_post_meta($bk_post_id, 'bk_user_rating', true );
            if($bk_user_rating) {
                $bk_user_review = '';
                $bk_number_rates = get_post_meta($bk_post_id, "bk_rates", true);
                $bk_user_score = get_post_meta($bk_post_id, "bk_user_score_output", true); 
                if ($bk_number_rates == NULL) {$bk_number_rates = 0;}
                if ($bk_user_score == NULL) {$bk_user_score = 0;}
                $bk_average_score = '<span class="bk-criteria-score bk-average-score">'.  floatval($bk_user_score) .'</span>'; 
                if(isset($_COOKIE["bk_user_rating"])) { $bk_current_rates = $_COOKIE["bk_user_rating"]; } else { $bk_current_rates = NULL; }
                if(isset($_COOKIE["bk_score_rating"])) { $bk_current_score = $_COOKIE["bk_score_rating"]; } else { $bk_current_score = NULL; }
    
                if ( preg_match('/\b' .$bk_post_id . '\b/', $bk_current_rates) ) {
                     $bk_class = " bk-rated"; 
                     $bk_tip_class = ' bk-tipper-bottom'; 
                     $bk_tip_title = ' data-title="'. esc_attr__("You have rated ", 'rubik').$bk_current_score. esc_attr__(" points for this post", 'rubik') .'"'; 
                } else {
                     $bk_class = $bk_tip_title = $bk_tip_class = NULL; 
                }
    
                if ( $bk_number_rates == '1' ) {
                    $bk_rate_str = esc_html__("Rate", 'rubik');
                }  else {
                    $bk_rate_str = esc_html__("Rates", 'rubik');
                }             
                $bk_user_review .= '<div class="bk-bar bk-user-rating clearfix"><div id="bk-rate" class="bg bk-criteria-wrap clearfix '. $bk_class .'"><span class="bk-criteria">'. esc_html__("Reader Rating", 'rubik'). ': (<span>'. $bk_number_rates .'</span> '. $bk_rate_str .')</span>';
                
                $bk_user_review .= $bk_average_score. '<div class="bk-overlay'. $bk_tip_class .'"'. $bk_tip_title .'><span style="width:'. $bk_user_score*10 .'%"></span></div></div></div>';
    
                 if ( function_exists('wp_nonce_field') ) { $bk_user_review .= wp_nonce_field('rating_nonce', 'rating_nonce', true, false); } 
                 
                 return $bk_user_review;
            }
        }
    /**
    * ************* Display post review box ********
    *---------------------------------------------------
    */
        static function bk_post_review_boxes($bk_post_id, $reviewPos){
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            if (isset($rubik_option)){
                $primary_color = $rubik_option['bk-primary-color'];
            }
            $bk_custom_fields = get_post_custom();
            $bk_review_checkbox = get_post_meta($bk_post_id, 'bk_review_checkbox', true );
    
            if ( $bk_review_checkbox == '1' ) {
                 $bk_review_checkbox = 'on'; 
            } else {
                 $bk_review_checkbox = 'off';
            }
            if ($bk_review_checkbox == 'on') {
                $bk_summary = get_post_meta($bk_post_id, 'bk_summary', true );                
                $bk_final_score = get_post_meta($bk_post_id, 'bk_final_score', true );        
                $criteria_val = rwmb_meta( 'reviewcriterias' );
    
                for ($i = 0; $i < count($criteria_val); $i++) {
                    ${"bk_rating_".$i."_title"} = $criteria_val[$i]['bk_ct'];
                    ${"bk_rating_".$i."_score"} = $criteria_val[$i]['bk_cs'];
                }
                
                $bk_ratings = array();  
            
                for( $i = 0; $i < count($criteria_val); $i++ ) {
                     if ( isset(${"bk_rating_".$i."_score"}) ) { $bk_ratings[] =  ${"bk_rating_".$i."_score"};}
                }
                $bk_review_ret = '<div class="bk-review-box '.$reviewPos.' clearfix"><div class="bk-detail-rating clearfix">'; 
                for( $j = 0; $j < count($criteria_val); $j++ ) {
                     if ((isset(${"bk_rating_".$j."_title"})) && (isset(${"bk_rating_".$j."_score"})) ) {                       
                            $bk_review_ret .= '<div class="bk-criteria-wrap"><span class="bk-criteria">'. ${"bk_rating_".$j."_title"}.'</span><span class="bk-criteria-score">'. $bk_ratings[$j].'</span>';                                     
                            $bk_review_ret .= '<div class="bk-bar clearfix"><span class="bk-overlay"><span class="bk-zero-trigger" style="width:'. ( ${"bk_rating_".$j."_score"}*10).'%"></span></span></div></div>';
                     }
                }
                $bk_review_ret .= '</div>';
                $bk_review_ret .= '<div class="summary-wrap clearfix">';
                $bk_review_ret .= self::bk_canvas_box($bk_final_score);
                if ( $bk_summary != NULL ) { $bk_review_ret .= '<div class="bk-summary"><div id="bk-conclusion" itemprop="description">'.$bk_summary.'</div></div>'; }                                    
                $bk_review_ret .= '</div><!-- /bk-author-review-box -->';
                $bk_review_ret .= self::bk_user_review($bk_post_id);
                $bk_review_ret .= '</div> <!--bk-review-box close-->';
                return $bk_review_ret;
            }

        }
    /**
     * Single Tags
     */
        static function bk_single_tags($tags) {
            $single_tags = '';
            $single_tags .= '<div class="s-tags">';
            $single_tags .= '<span><i class="fa fa-tags"></i>'.esc_html__('Tags', 'rubik').'</span>';
        		foreach ($tags as $tag):
        			$single_tags .= '<a href="'. get_tag_link($tag->term_id) .'" title="'. esc_attr(sprintf(__("View all posts tagged %s",'rubik'), $tag->name)) .'">'. $tag->name.'</a>';
        		endforeach;
            $single_tags .= '</div>';
            return $single_tags;
        }
    /**
     * Post Navigation
     */
        static function bk_single_post_nav($next_post, $prev_post){
            $post_nav = '';
            $bk_next_no_thumb = $bk_prev_no_thumb = '';
            $post_nav .= '<div class="s-post-nav">';  
            if (!empty($prev_post)):
                if(! rubik_core::bk_check_has_post_thumbnail( $prev_post->ID )) {
                    $bk_prev_no_thumb = 'bk_no_thumb';
                }
                $post_nav .= '<div class="nav-btn hide-nav nav-prev">';
                $post_nav .= '<a href="'.get_permalink( $prev_post->ID ).'">';
                $post_nav .= '<span class="icon">';
                $post_nav .= '<i class="fa fa-long-arrow-left"></i>';
                $post_nav .= '</span>';
                $post_nav .= '</a>';
                $post_nav .= '<div class="nav-c-wrap '.$bk_prev_no_thumb.'">';
                if(rubik_core::bk_check_has_post_thumbnail( $prev_post->ID )) {
                    $post_nav .= '<div class="thumb">';
                    $post_nav .= get_the_post_thumbnail($prev_post->ID, 'rubik-150-105');
                    $post_nav .= '</div>';
                }
        		$post_nav .= '<div class="nav-title">';
                $post_nav .= '<span>'.esc_html__("Previous Article",'rubik').'</span>';
                $post_nav .= '<h3><a href="'.get_permalink( $prev_post->ID ).'">';
                $post_nav .= rubik_core::the_excerpt_limit_by_word(get_the_title($prev_post->ID),7);
                $post_nav .= '</a></h3>';
        		$post_nav .= '</div>';
                $post_nav .= '</div>';
                $post_nav .= '</div>';
    		endif;
            if (!empty($next_post)):
                if(! rubik_core::bk_check_has_post_thumbnail( $next_post->ID )) {
                    $bk_next_no_thumb = 'bk_no_thumb';
                }
                $post_nav .= '<div class="nav-btn hide-nav nav-next">';
                $post_nav .= '<a href="'.get_permalink( $next_post->ID ).'">';
                $post_nav .= '<span class="icon">';
                $post_nav .= '<i class="fa fa-long-arrow-right"></i>';
                $post_nav .= '</span>';
                $post_nav .= '</a>';
                $post_nav .= '<div class="nav-c-wrap '.$bk_next_no_thumb.'">';
                if(rubik_core::bk_check_has_post_thumbnail( $next_post->ID )) {
                    $post_nav .= '<div class="thumb">';
                    $post_nav .= get_the_post_thumbnail($next_post->ID, 'rubik-150-105');
                    $post_nav .= '</div>';
                }
                $post_nav .= '<div class="nav-title">';
                $post_nav .= '<span>'.esc_html__("Next Article",'rubik').'</span>';
                $post_nav .= '<h3><a href="'.get_permalink( $prev_post->ID ).'">';
                $post_nav .= rubik_core::the_excerpt_limit_by_word(get_the_title($prev_post->ID),7);
                $post_nav .= '</a></h3>';
                $post_nav .= '</div>';
                $post_nav .= '</div>';
                $post_nav .= '</div>';
            endif;                
            $post_nav .= '</div>';
            return $post_nav;
        }
        
        static function bk_single_static_post_nav($next_post, $prev_post){
            $post_nav = '';
            $post_nav .= '<div class="s-static-post-nav clearfix">';  
            if (!empty($prev_post)):
                $post_nav .= '<div class="nav-btn hide-nav nav-prev">';
                $post_nav .= '<div class="nav-c-wrap">';
        		$post_nav .= '<div class="nav-title">';
                $post_nav .= '<span class="nav-icon">';
                $post_nav .= '<i class="fa fa-long-arrow-left"></i>';
                $post_nav .= '</span>';
                $post_nav .= '<span>'.esc_html__("Previous Article",'rubik').'</span>';
                $post_nav .= '<h3><a href="'.get_permalink( $prev_post->ID ).'">';
                $post_nav .= rubik_core::the_excerpt_limit_by_word(get_the_title($prev_post->ID),12);
                $post_nav .= '</a></h3>';
        		$post_nav .= '</div>';
                $post_nav .= '</div>';
                $post_nav .= '</div>';
    		endif;
            if (!empty($next_post)):
                $post_nav .= '<div class="nav-btn hide-nav nav-next">';
                $post_nav .= '<div class="nav-c-wrap">';
                $post_nav .= '<div class="nav-title">';
                $post_nav .= '<span>'.esc_html__("Next Article",'rubik').'</span>';
                $post_nav .= '<span class="nav-icon">';
                $post_nav .= '<i class="fa fa-long-arrow-right"></i>';
                $post_nav .= '</span>';
                $post_nav .= '<h3><a href="'.get_permalink( $next_post->ID ).'">';
                $post_nav .= rubik_core::the_excerpt_limit_by_word(get_the_title($next_post->ID),12);
                $post_nav .= '</a></h3>';
                $post_nav .= '</div>';
                $post_nav .= '</div>';
                $post_nav .= '</div>';
            endif;                
            $post_nav .= '</div>';
            return $post_nav;
        }
        
        static function bk_related_posts_layout($the_query) {
            $bk_related_output = '';
            $bk_related_output .= '<ul class="related-posts row clearfix">';                                            
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $postLink = get_permalink(get_the_ID());
                    $postTitle = get_the_title(get_the_ID());
                    $postLinkTitle = '<a href="'.$postLink.'">'.$postTitle.'</a>' ;  
                    $bk_related_output .= '<li class="item row-type content_out col-md-4 col-sm-4">';
                    $bk_related_output .= rubik_core::bk_get_thumbnail(get_the_ID(), 'rubik-360-240');
                    $bk_related_output .= '<div class="post-c-wrap">';
                    $bk_related_output .= '<h4>'.$postLinkTitle.'</h4>';
                    $bk_related_output .= '<div class="meta">'.rubik_core::bk_meta_cases('date').'</div>';
                    $bk_related_output .= '</div>';
                    $bk_related_output .= '</li>';
                endwhile;
            $bk_related_output .= '</ul>';
            return $bk_related_output;
        }
        static function bk_get_heading_related_posts($the_query) {
            $bk_related_output = '';
            $bk_related_output .= '<ul class="heading-related-posts clearfix liststyle-none">';                                            
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $postLink = get_permalink(get_the_ID());
                    $postTitle = get_the_title(get_the_ID());
                    $postLinkTitle = '<a href="'.$postLink.'">'.$postTitle.'</a>' ;  
                    $bk_related_output .= '<li class="item">';
                    $bk_related_output .= '<h4>'.$postLinkTitle.'</h4>';
                    $bk_related_output .= '</li>';
                endwhile;
            $bk_related_output .= '</ul>';
            return $bk_related_output;
        }
    /**
     * ************* Related Post *****************
     *---------------------------------------------------
     */            
        static function bk_heading_related_posts($bk_number_related) {
            global $post;
            $bkPostId = $post->ID;
            if (is_attachment() && ($post->post_parent)) { $bkPostId = $post->post_parent; };
            $i = 1;
            $excludeid = array();
            array_push($excludeid, $bkPostId);
    
            $bk_tags = wp_get_post_tags($bkPostId);
            $tag_length = sizeof($bk_tags);                               
            $bk_tag_check = $bk_all_cats = array();
            
            $bk_categories = get_the_category($bkPostId);  
            $category_length = sizeof($bk_categories);
            if ($category_length > 0){       
                foreach ( $bk_categories as $category_key => $bk_category ) { $bk_all_cats[$category_key] = $bk_category->term_id; }
            }
            $bk_related_output = '';
     
     // Get tag post
            foreach ( $bk_tags as $tag_key => $bk_tag ) { $bk_tag_check[$tag_key] = $bk_tag->term_id; }        
                $args = array(  'posts_per_page' => $bk_number_related,
                                'post__not_in' => $excludeid,
                                'post_status' => 'publish',
                                'post_type' => 'post',
                    			'ignore_sticky_posts' => 1,
                                'tax_query' => array(
                                    'relation' => 'OR',
                                    array(
                                        'taxonomy' => 'category',
                                        'field' => 'term_id',
                                        'terms' => $bk_all_cats,
                                        'include_children' => false,
                                    ),
                                    array(
                                        'taxonomy' => 'post_tag',
                                        'field' => 'term_id',
                                        'terms' => $bk_tag_check,
                                    )
                                )
                        );
            $the_query = new WP_Query( $args );
            
            if ( $the_query != NULL ) {
                $bk_related_output .= '<div class="rubik-heading-related-posts">';
                
                $bk_related_output .= self::bk_get_heading_related_posts($the_query);
                
                $bk_related_output .= '</div>';
                wp_reset_postdata();    
                return $bk_related_output;
            }  
        }
        static function bk_related_posts($bk_number_related) {
            global $post;
            $bkPostId = $post->ID;
            if (is_attachment() && ($post->post_parent)) { $bkPostId = $post->post_parent; };
            $i = 1;
            $excludeid = array();
            array_push($excludeid, $bkPostId);
    
            $bk_tags = wp_get_post_tags($bkPostId);
            $tag_length = sizeof($bk_tags);                               
            $bk_tag_check = $bk_all_cats = array();
            
            $bk_categories = get_the_category($bkPostId);  
            $category_length = sizeof($bk_categories);
            if ($category_length > 0){       
                foreach ( $bk_categories as $category_key => $bk_category ) { $bk_all_cats[$category_key] = $bk_category->term_id; }
            }
            $bk_related_output = '';
     
     // Get tag post
            foreach ( $bk_tags as $tag_key => $bk_tag ) { $bk_tag_check[$tag_key] = $bk_tag->term_id; }        
                $args = array(  'posts_per_page' => $bk_number_related,
                                'post__not_in' => $excludeid,
                                'post_status' => 'publish',
                                'post_type' => 'post',
                    			'ignore_sticky_posts' => 1,
                                'tax_query' => array(
                                    'relation' => 'OR',
                                    array(
                                        'taxonomy' => 'category',
                                        'field' => 'term_id',
                                        'terms' => $bk_all_cats,
                                        'include_children' => false,
                                    ),
                                    array(
                                        'taxonomy' => 'post_tag',
                                        'field' => 'term_id',
                                        'terms' => $bk_tag_check,
                                    )
                                )
                        );
                        
            rubik_section_parent::$rubik_ajax_c['s-related-articles']['args'] = $args;
            
            $related_author_args = array(  
                                'posts_per_page' => $bk_number_related,
                                'post__not_in' => $excludeid,
                                'post_status' => 'publish',
                                'post_type' => 'post',
                    			'ignore_sticky_posts' => 1,
                                'author'                => $post->post_author
                                
                        );
            
            rubik_section_parent::$rubik_ajax_c['s-author-articles']['args'] = $related_author_args;
            
            $related_category_args = array(  
                                'posts_per_page' => $bk_number_related,
                                'post__not_in' => $excludeid,
                                'post_status' => 'publish',
                                'post_type' => 'post',
                    			'ignore_sticky_posts' => 1,
                                'category__in'          => rubik_section_parent::$rubik_ajax_c['s-cat-IDs'],
                                
                        );
            
            rubik_section_parent::$rubik_ajax_c['s-category-articles']['args'] = $related_category_args;
            
            $the_query = new WP_Query( $args );
            
            if ( $the_query != NULL ) {
                $bk_related_output .= '<div class="bk-related-posts">';
                
                $bk_related_output .= self::bk_related_posts_layout($the_query);
                
                $bk_related_output .= '</div>';
                wp_reset_postdata();    
                return $bk_related_output;
            }    
        }  
        static function bk_get_article_info($bkPostId){  
            global $rubik_option, $post;
            $article_info = '';
            $bk_logo = array();
            $bk_logo_url = '';
            if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
                $bk_logo = $rubik_option['bk-logo'];
                if (($bk_logo != null) && (array_key_exists('url',$bk_logo))) {
                    $bk_logo_url = $bk_logo['url'];
                }
            }

            $bk_publisher_name = get_bloginfo('name');
            if (empty($bk_publisher_name)){
                $bk_publisher_name = esc_attr(get_the_author_meta('display_name', $post->post_author));
            }
            $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $bkPostId ), 'full' );
            $article_info .= '<meta itemprop="author" content="'.$post->post_author.'">';    
            $article_info .= '<span style="display: none;" itemprop="author" itemscope itemtype="https://schema.org/Person">' ;
            $article_info .= '<meta itemprop="name" content="' . esc_attr(get_the_author_meta('display_name', $post->post_author)) . '">' ;
            $article_info .= '</span>' ;    
            $article_info .= '<meta itemprop="headline " content="'.get_the_title($bkPostId).'">';
            $article_info .= '<meta itemprop="datePublished" content="'.date(DATE_W3C, get_the_time('U', $bkPostId)).'">';
            $article_info .= '<meta itemprop="dateModified" content="' . the_modified_date('c', '', '', false) . '">';
            $article_info .= '<meta itemscope itemprop="mainEntityOfPage" content="" itemType="https://schema.org/WebPage" itemid="' . get_permalink($bkPostId) .'"/>';
            $article_info .= '<span style="display: none;" itemprop="publisher" itemscope itemtype="https://schema.org/Organization">';
            $article_info .= '<span style="display: none;" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">';
            $article_info .= '<meta itemprop="url" content="' . $bk_logo_url . '">';
            $article_info .= '</span>';
            $article_info .= '<meta itemprop="name" content="' . $bk_publisher_name . '">';
            $article_info .= '</span>';
            $article_info .= '<span style="display: none;" itemprop="image" itemscope itemtype="https://schema.org/ImageObject">';
            $article_info .= '<meta itemprop="url" content="' . $thumbnail_src[0] . '">';
            $article_info .= '<meta itemprop="width" content="' . $thumbnail_src[1] . '">';
            $article_info .= '<meta itemprop="height" content="' . $thumbnail_src[2] . '">';
            $article_info .= '</span>';
            $article_info .= '<meta itemprop="interactionCount" content="UserComments:' . get_comments_number($bkPostId) . '"/>';
            return $article_info;
        }
    /**
     *  Single Content
     */    
        static function bk_single_content($bkPostId){
            $the_content = '';
            return the_content($bkPostId);
            $the_content = apply_filters( 'the_content', get_the_content($bkPostId) );
            $the_content = str_replace( ']]>', ']]&gt;', $the_content );
    
            $post_content_str = get_the_content($bkPostId);
            $gallery_flag = 0;
            $i = 0;
            $ids = null;
            for ($i=0; $i < 10; $i++) {
                preg_match('/<style(.+(\n))+.*?<\/div>/', $the_content, $match);
                    
                preg_match('/\[gallery.*ids=.(.*).\]/', $post_content_str, $ids);             
                
                if ($ids != null) {
                    $the_content = str_replace($match[0], $ids[0] ,$the_content);          
                       
                    $attachment_ids = explode(",", $ids[1]);
                    $post_content_str = str_replace($ids[0], self::bk_native_gallery ($bkPostId, $attachment_ids),$post_content_str);
                    $the_content = str_replace($ids[0], self::bk_native_gallery ($bkPostId, $attachment_ids),$the_content);
                    $gallery_flag = 1;
                }
            }
            if($gallery_flag == 1) {
                return $the_content;
            }else {
                return the_content($bkPostId);
            }
        }
    /**
     *  BK Get Single Header Layout 0
     */
        static function bk_get_single_layout_0($bkPostId, $imageSize = 'rubik-900-613') {
            $postFormat = self::bk_post_format_detect($bkPostId);
            $bk_feat_img = '';            

            if((rubik_core::bk_check_has_post_thumbnail($bkPostId)) || ($postFormat['format'] == 'video')) {
                $bk_feat_img .= '<div id="bk-normal-feat" class="clearfix">'; 
            }else {
                $bk_feat_img = null;
            }
            
            if($postFormat['format'] != 'gallery') {
                if ($bk_feat_img != null) {
                    if ($postFormat['popup'] == 1) {
                        $bk_feat_img .= '<div class="s-feat-img">'.get_the_post_thumbnail($bkPostId, $imageSize).'</div>';
                    }
                    $bk_feat_img .= self::bk_post_format_display($bkPostId, $imageSize);
                    if((rubik_core::bk_check_has_post_thumbnail($bkPostId)) || ($postFormat['format'] == 'video')) {
                        $bk_feat_img .= '</div>';
                    }
                }
            }else {
                $bk_feat_img .= self::bk_post_format_display($bkPostId, $imageSize);
                if((rubik_core::bk_check_has_post_thumbnail($bkPostId)) || ($postFormat['format'] == 'video')) {
                    $bk_feat_img .= '</div>';
                }
            }
            
            return $bk_feat_img;
        }
    /**
     *  BK Get Single Header Layout 1 (FW)
     */
        static function bk_get_single_layout_1($bkPostId, $imageSize = 'full') {
            $postFormat = self::bk_post_format_detect($bkPostId);
            $bk_feat_img = '';
                                    
            if((rubik_core::bk_check_has_post_thumbnail($bkPostId)) || ($postFormat['format'] == 'video')) {
                $bk_feat_img .= '<div class="post-head clearfix">'; 
            }else {
                $bk_feat_img = null;
            }
            
            if($postFormat['format'] != 'gallery') {
                if ($bk_feat_img != null) {
                    if ($postFormat['popup'] == 1) {
                        $bk_feat_img .= '<div class="s-feat-img">'.get_the_post_thumbnail($bkPostId, $imageSize).'</div>';
                    }
                    $bk_feat_img .= self::bk_post_format_display($bkPostId, $imageSize);
                    if((rubik_core::bk_check_has_post_thumbnail($bkPostId)) || ($postFormat['format'] == 'video')) {
                        $bk_feat_img .= '</div>';
                    }
                }
            }else {
                $bk_feat_img .= self::bk_post_format_display($bkPostId, $imageSize);
                if((rubik_core::bk_check_has_post_thumbnail($bkPostId)) || ($postFormat['format'] == 'video')) {
                    $bk_feat_img .= '</div>';
                }
            }
            
            return $bk_feat_img;
        }    
        static function bk_count_twitter( $user ) {
    		//check options
    		if ( empty( $user ) ) {
    			return false;
    		}
    
    		$params = array(
    			'timeout'   => 120,
    			'sslverify' => false
    		);
    
    		$filter   = array(
    			'start_1' => 'ProfileNav-item--followers',
    			'start_2' => 'title',
    			'end'     => '>'
    		);
    		$response = wp_remote_get( 'https://twitter.com/' . $user, $params );
    		//check & return
    		if ( is_wp_error( $response ) || empty( $response['response']['code'] ) || '200' != $response['response']['code'] ) {
    			return false;
    		}
    		//get content
    		$response = wp_remote_retrieve_body( $response );
    
    		if ( ! empty( $response ) && $response !== false ) {
    			foreach ( $filter as $key => $value ) {
    
    				$key = explode( '_', $key );
    				$key = $key[0];
    
    				if ( $key == 'start' ) {
    					$key = false;
    				} else if ( $value == 'end' ) {
    					$key = true;
    				}
    				$key = (bool) $key;
    
    				$index = strpos( $response, $value );

    				if ( $index === false ) {
    					return false;
    				}
    				if ( $key ) {
    					$response = substr( $response, 0, $index );
    				} else {
    					$response = substr( $response, $index + strlen( $value ) );
    				}
    			}
    
    			if ( strlen( $response ) > 100 ) {
    				return false;
    			}
    			$count = self::bk_extract_one_number( $response );
    
    			if ( ! is_numeric( $count ) || strlen( number_format( $count ) ) > 15 ) {
    				return false;
    			}
    
    			$count = intval( $count );
    
    			return $count;
    		} else {
    			return false;
    		}
    
    	}
    } // Close rubik_core class
}
