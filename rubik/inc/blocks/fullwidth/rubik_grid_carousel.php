<?php
if (!class_exists('rubik_grid_carousel')) {
    class rubik_grid_carousel extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            
            $block_str = '';
            
            $uid = uniqid('grid_carousel-');
            
            $moduleStyle = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_style', true ); 
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $moduleHasbg = '';                                    
            $moduleHasbg = $rubik_dynamic_css[$uid]['moduleHasbg'];
            
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_grid_carousel'], $page_info);    //get block config
    
            $the_query = bk_get_query::query($module_cfg);//get query
            
            if($the_query->post_count > 3) {
                $flexClass = ' flexslider';
            }else {
                $flexClass = '';
            }
            
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-grid-carousel '.$moduleStyle.' '.$moduleHasbg.'">';
            $block_str .= '<div class="container bkwrapper">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
            endif;
            $block_str .= '<div class="bk-carousel-wrap'.$flexClass.'">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon);
            $block_str .= '</div></div></div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $bk_post_icon = ''){
            $render_modules = '';
            $bk_contentin3 = new bk_contentin3;
            $custom_var = array (
                    'thumbnail'     => 'rubik-620-420',
                    'meta'          => array('author', 'date'),
                    'cat'           => 'on',
                    'post-icon'     => $bk_post_icon,
                );          
            $bk_counter = 1;
            if ( $the_query->have_posts() ) :
                $render_modules .= '<ul class="slides clearfix">';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    if (($bk_counter <= $the_query->post_count) && ($bk_counter%3 == 1)) {
                        $render_modules .= '<li class="bk-post-title-large content_in carousel-single-item">';
                        $render_modules .= '<div class="content_in_wrapper">';
                        $render_modules .= $bk_contentin3->render($custom_var);
                        $render_modules .= '</div></li>';
                        $bk_counter++;
                    }else {
                        $render_modules .= '<li class="bk-post-title-small content_in carousel-double-item">';
                        if (($bk_counter <= $the_query->post_count) && ($bk_counter%3 == 2)) {
                            $render_modules .= '<div class="content_in_wrapper">';
                            $render_modules .= $bk_contentin3->render($custom_var);
                            $render_modules .= '</div>';
                            $bk_counter++;
                            if( $bk_counter <= $the_query->post_count ) {
                                $the_query->the_post();
                                $render_modules .= '<div class="content_in_wrapper">';
                                $render_modules .= $bk_contentin3->render($custom_var);
                                $render_modules .= '</div>';
                                $bk_counter++;
                            }
                        }
                        $render_modules .= '</li>';
                    }
                endwhile;
                $render_modules .= '</ul> <!-- Close render slider -->';
            endif;
            return $render_modules;
        }
        
    }
}