<?php
    $rubik_justified_ids = rubik_core::bk_get_global_var('rubik_justified_ids');
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    
    $rubik_tabs_more = '<div class="bk-tabs-dropdown"><div class="bk-tabs-more"><span>'.esc_html__('More', 'rubik').'</span><i class="fa fa-caret-down" aria-hidden="true"></i></div><div class="bk-tabs-pull-list clearfix"></div></div>';

    if (isset($rubik_option)):
        $fixed_nav = $rubik_option['bk-sticky-nav-switch'];            
        wp_localize_script( 'rubik-customjs', 'fixed_nav', $fixed_nav );
        
        $bk_allow_html = array(
                                'a' => array(
                                    'href' => array(),
                                    'title' => array()
                                ),
                                'br' => array(),
                                'em' => array(),
                                'strong' => array(),
                            );
        $cr_text = $rubik_option['cr-text'];
    endif;
?>
<?php if (is_active_sidebar('footer_sidebar_1') 
            || is_active_sidebar('footer_sidebar_2')
            || is_active_sidebar('footer_sidebar_3')
            || has_nav_menu('menu-footer')
            || ($cr_text != '')) { ?>
<div class="footer">
       
    <?php if (is_active_sidebar('footer_sidebar_1') 
            || is_active_sidebar('footer_sidebar_2')
            || is_active_sidebar('footer_sidebar_3')) { ?>
    <div class="footer-content bkwrapper clearfix container">
        <div class="row">
            <div class="footer-sidebar col-md-4">
                <?php dynamic_sidebar( 'footer_sidebar_1' ); ?>
            </div>
            <div class="footer-sidebar col-md-4">
                <?php dynamic_sidebar( 'footer_sidebar_2' ); ?>
            </div>
            <div class="footer-sidebar col-md-4">
                <?php dynamic_sidebar( 'footer_sidebar_3' ); ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (is_active_sidebar('footer_d3_1') 
            || is_active_sidebar('footer_d3_2')
            || is_active_sidebar('footer_d3_3')) { ?>
    <div class="footer-content bkwrapper clearfix container">
        <div class="row">
            <div id="footer_d3_1" class="footer-sidebar">
                <?php dynamic_sidebar( 'footer_d3_1' ); ?>
            </div>
            <div id="footer_d3_2" class="footer-sidebar">
                <?php dynamic_sidebar( 'footer_d3_2' ); ?>
            </div>
            <div id="footer_d3_3" class="footer-sidebar">
                <?php dynamic_sidebar( 'footer_d3_3' ); ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (is_active_sidebar('footer_d2_1') 
            || is_active_sidebar('footer_d2_2')) { ?>
    <div class="footer-content bkwrapper clearfix container">
        <div class="row">
            <div id="footer_d2_1" class="footer-sidebar">
                <?php dynamic_sidebar( 'footer_d2_1' ); ?>
            </div>
            <div id="footer_d2_2" class="footer-sidebar">
                <?php dynamic_sidebar( 'footer_d2_2' ); ?>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <?php if (is_active_sidebar('footer_d1')) { ?>
    <div class="footer-content bkwrapper clearfix container">
        <div class="row">
            <div id="footer_d1" class="footer-sidebar">
                <?php dynamic_sidebar( 'footer_d1' ); ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if (is_active_sidebar('footer_fullwidth')) { ?>
    <div class="footer-content bkwrapper clearfix container">
        <div class="row">
            <div class="footer-sidebar footer-fw col-md-12">
                <?php dynamic_sidebar( 'footer_fullwidth' ); ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="footer-lower">
        <div class="container">
            <div class="footer-inner clearfix <?php if ( !has_nav_menu('menu-footer') ) { echo 'bk-align-center';}?>">
                <?php if ( has_nav_menu('menu-footer') ) {?> 
                    <?php wp_nav_menu(array('theme_location' => 'menu-footer', 'depth' => '1', 'container_id' => 'footer-menu'));?>  
                <?php }?>  
                <div class="bk-copyright"><?php echo wp_kses($cr_text, $bk_allow_html);?></div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
    wp_localize_script( 'rubik-customjs', 'rubik_tabs_more', $rubik_tabs_more );
    
    wp_localize_script( 'rubik-customjs', 'justified_ids', $rubik_justified_ids );
    
    wp_localize_script( 'rubik-module-load-post', 'ajax_c', rubik_section_parent::$rubik_ajax_c );
?>
