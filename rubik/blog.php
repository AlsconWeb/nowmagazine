<?php
/*
Template Name: Blog
*/
 ?> 
<?php get_header();?>
<?php
$rubik_option = rubik_core::bk_get_global_var('rubik_option');
$bk_post_icon = 'hide';
$sidebar_option_class = '';
$archiveLayout = '';
if (isset($rubik_option) && ($rubik_option != '')): 
    $archiveLayout = $rubik_option['bk-blog-layout'];
    if(isset($rubik_option['blog_post_icon'])) {
        $bk_post_icon= $rubik_option['blog_post_icon'];
    }else {
        $bk_post_icon = 'hide';
    }
    $sidebarPosition = $rubik_option['blog-page-sidebar-position'];
    if($sidebarPosition == 'left'){
        $sidebar_option_class = 'has-left-sidebar';
    }else {
        $sidebar_option_class = '';
    }
endif;
?>
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts('post_type=post&post_status=publish&paged=' . $paged);
?>
<div id="body-wrapper" class="wp-page bk-blog-template-page">
    <div class="bkwrapper container <?php if($sidebar_option_class != '') echo esc_attr($sidebar_option_class);?> <?php if(($archiveLayout == 'large-blog-nosb') || ($archiveLayout == 'large-blog-2-nosb')){echo ('page-large-blog-nosb');}?>">		
        <div class="row bksection">			
            <div class="blog-page-content bkpage-content <?php if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3')) && (!($archiveLayout == 'large-blog-nosb')) && (!($archiveLayout == 'large-blog-2-nosb')) && (!($archiveLayout == 'row-nosb'))): echo 'col-md-8 has-sb'; else: echo 'col-md-12 fullwidth';  endif;?>">
                <div class="row">
                    <div id="main-content" class="bk-blog-content clear-fix" role="main">
                		<div class="page-title-wrapper col-md-12">
                    		<div class="module-title">
                                <h2 class="heading"><?php esc_html_e( 'Blog', 'rubik' );?></h2>
                            </div>
                        </div>	
                        <?php
                            if (have_posts()) {
                                echo rubik_archive::archive_content_area($archiveLayout, $bk_post_icon);
                            }
                        ?>
            
    	            </div> <!-- end #main -->
                </div>
            </div> <!-- end #bk-content -->
            <?php
                if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3')) && (!($archiveLayout == 'large-blog-nosb')) && (!($archiveLayout == 'large-blog-2-nosb')) && (!($archiveLayout == 'row-nosb'))) {?>
                    <div class="sidebar col-md-4">
                        <div class="sidebar-wrap <?php if($rubik_option['blog-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-blog-sidebar">
                            <?php
                                if((isset($rubik_option['blog-page-sidebar'])) && ($rubik_option['blog-page-sidebar'] != '')){
                                    $sidebar = $rubik_option['blog-page-sidebar'];
                                }else {
                                    $sidebar = '';
                                }
                                if (strlen($sidebar)) {
                                    dynamic_sidebar($sidebar);
                                }else {
                                    dynamic_sidebar();
                                }
                            ?>
                        </div>
                    </div>
                <?php }
            ?>
        </div>
    </div>
</div>   
<?php get_footer(); ?>