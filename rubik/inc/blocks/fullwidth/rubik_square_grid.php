<?php
if (!class_exists('rubik_square_grid')) {
    class rubik_square_grid extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('square_grid-');
            
            $moduleAlign = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_align', true ); 
            $moduleStyle = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_style', true ); 
            $module_layout = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_layout', true ); 
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_square_grid'], $page_info);    //get block config
            $bk_ajax_button = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_button', true );
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            
            $sec = '';
            $has_bkwrapper = '';
            $columns = "";
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $has_bkwrapper = 'square-grid-2';
                $sec = "has_sb";   
                parent::$rubik_ajax_c[$uid]['sec'] = 'has_sb';            
            }else {
                $has_bkwrapper = 'bkwrapper container square-grid-3';  
                $sec = "fw";   
                parent::$rubik_ajax_c[$uid]['sec'] = 'fw';    
            }
    // prepare ajax vars 
            $ajax_load_number = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_load_number', true );
            
            parent::$rubik_ajax_c[$uid]['entries'] = $ajax_load_number;
            
            parent::$rubik_ajax_c[$uid]['offset'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_offset', true );
            
    ///////////////////////
    // locallize ajax vars 
    
            //wp_localize_script( 'rubik-module-load-post', 'ajax_c', parent::$rubik_ajax_c );
            
            /** Tabs **/
            $rubik_tabs = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_categorytabs', true );
            
            $rubikTabsToArray = explode(",",$rubik_tabs);
            
            if ($module_cfg['category_id'] != 0) {
                $catTabArray = explode(",",$module_cfg['category_id']);
            }else {
                $catTabArray = 0;
            }
            
            $tabs_amount = intval(count($rubikTabsToArray));
            
            parent::$rubik_ajax_c[$uid]['tabfirst']['cat'] = $catTabArray;
                
            parent::$rubik_ajax_c[$uid]['tabfirst']['content'] = '';
            
            parent::$rubik_ajax_c[$uid]['post_icon'] = $bk_post_icon;
            
            if($tabs_amount > 0) {
                
                for ($i=0; $i< $tabs_amount; $i++) {
                    parent::$rubik_ajax_c[$uid]['tab'.$i]['cat'] = $rubikTabsToArray[$i];
                    parent::$rubik_ajax_c[$uid]['tab'.$i]['content'] = '';
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);           //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule '.$has_bkwrapper.' module-square-grid '.$moduleAlign.' '.$moduleStyle.' '.$module_layout.'">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info, $rubikTabsToArray);  //render block title
            endif;
            $block_str .= '<div class="bk-square-grid-wrap">';
            $block_str .= '<div class="row clearfix">';
            $block_str .= '<ul class="bk-square-grid-content bk-module-inner clearfix">';
            $block_str .= $this->render_modules($the_query, $sec, $bk_post_icon);
            $block_str .= '</ul></div></div>';
            //Loadmore button 
            if($bk_ajax_button !== 'disable') {
                $block_str .= '<div class="square-grid-ajax loadmore">';
                $block_str .= rubik_core::bk_get_load_more_buttom();
                $block_str .= '</div><!-- End Loadmore -->';
            }
            $block_str .= '</div>';
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $sec, $bk_post_icon){
            $render_modules = '';
            
            $custom_var = array (
                    'cat'           => 'on',
                    'post-icon'     => $bk_post_icon,
                    'meta'          => array('author', 'date'),
                    'thumbnail'     => 'rubik-620-420',
            );
            
            if($sec == 'has_sb') {
                $col_width = 'col-md-6 col-sm-6';
            }else {
                $col_width = 'col-md-4 col-sm-6';
            }
            $bk_contentin3 = new bk_contentin3;
            if ( $the_query->have_posts() ) :
                
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="item content_in '.$col_width.'">';
                    $render_modules .= '<div class="content_in_wrapper">';
                    $render_modules .= $bk_contentin3->render($custom_var);
                    $render_modules .= '</div></li><!-- end post item -->';
                endwhile;
    
            endif;
            return $render_modules;
        }
        
    }
}