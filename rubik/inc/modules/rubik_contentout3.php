<?php
if (!class_exists('bk_contentout3')) {
    class bk_contentout3 {
        
        function render($custom_var) {
            ob_start();
            $bk_final_score = get_post_meta(get_the_ID(), 'bk_final_score', true );
            ?>
            <?php if(rubik_core::bk_check_has_post_thumbnail( get_the_ID() ) && isset($custom_var['thumbnail']) && ($custom_var['thumbnail'] != 'off')) {?>
            <div class="small-thumbnail-wrapper">
                <?php 
                echo rubik_core::get_feature_image($custom_var['thumbnail'], true);
                ?>
            </div>
            <?php }?>
            <div class="post-c-wrap">
                <?php
                    if (isset($custom_var['cat']) && ($custom_var['cat'] != 'off')) :
                        echo rubik_core::bk_meta_cases('cat');
                    endif;
                ?>
                
                <?php echo rubik_core::bk_get_post_title(get_the_ID(), 15);?>
                <?php
                if (isset($custom_var['meta']) && ($custom_var['meta'] != 'off')) :
                    echo rubik_core::bk_get_post_meta($custom_var['meta']);
                endif;
                ?> 
            </div>
            <?php return ob_get_clean();
        }
        
    }
}