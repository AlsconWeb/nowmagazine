<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */
?>
<?php
get_header(); 
$rubik_option = rubik_core::bk_get_global_var('rubik_option');
$cur_tag_id = $wp_query->get_queried_object_id();
$archive_opt = get_option('bk_cat_opt');

$archiveLayout = '';
$bk_post_icon = 'hide';
$sidebarPosition = '';

if (isset($archive_opt[$cur_tag_id]) && is_array($archive_opt[$cur_tag_id]) && array_key_exists('archive-sidebar-position',$archive_opt[$cur_tag_id])) { $sidebarPosition = $archive_opt[$cur_tag_id]['archive-sidebar-position'];};
if($sidebarPosition == 'left'){
    $sidebar_option_class = 'has-left-sidebar';
}else {
    $sidebar_option_class = '';
}

if (isset($rubik_option) && ($rubik_option != '')): 
    $archiveLayout = $rubik_option['bk-archive-layout'];
    if(isset($rubik_option['archive_post_icon'])) {
        $bk_post_icon= $rubik_option['archive_post_icon'];
    }else {
        $bk_post_icon = 'hide';
    }
    $cat_feat = '';    
    $tag_layout = 0;    
    
    if(($sidebarPosition == 'global') || ($sidebarPosition == '')) :
        $sidebarPosition = $rubik_option['archive-page-sidebar-position'];
        if($sidebarPosition == 'left'){
            $sidebar_option_class = 'has-left-sidebar';
        }else {
            $sidebar_option_class = '';
        }
    endif;
    
endif;
if (isset($archive_opt[$cur_tag_id]) && is_array($archive_opt[$cur_tag_id]) && array_key_exists('cat_layout',$archive_opt[$cur_tag_id])) { $tag_layout = $archive_opt[$cur_tag_id]['cat_layout'];};
if (isset($archive_opt[$cur_tag_id]) && is_array($archive_opt[$cur_tag_id]) && array_key_exists('cat_feat',$archive_opt[$cur_tag_id])) { $cat_feat = $archive_opt[$cur_tag_id]['cat_feat'];};
if ($cat_feat == '') { $cat_feat = 0;};
if ((strlen($tag_layout) != 0)&&($tag_layout != 'global')) { $archiveLayout = $tag_layout;};
if ($archiveLayout == '') {
    $archiveLayout = 'classic-blog';
}
?>
<div id="body-wrapper" class="wp-page bk-cat-arc-page-<?php echo esc_attr($cur_tag_id);?>">
    <div class="bkwrapper container <?php if($sidebar_option_class != '') echo esc_attr($sidebar_option_class);?> <?php if(($archiveLayout == 'large-blog-nosb') || ($archiveLayout == 'large-blog-2-nosb')){echo ('page-large-blog-nosb');}?>">		
        <div class="row bksection">			
            <div class="bk-archive-content bk-cat-arc-content bkpage-content <?php if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3')) && (!($archiveLayout == 'large-blog-nosb')) && (!($archiveLayout == 'large-blog-2-nosb')) && (!($archiveLayout == 'row-nosb'))): echo 'col-md-8 has-sb'; else: echo 'col-md-12 fullwidth';  endif;?>">
                <div class="row">
                    <div id="main-content" class="clear-fix" role="main">
                		
                        <div class="page-title-wrapper col-md-12">
                    		<div class="module-title">
                        		<h2 class="heading">
                                    <?php
                                    	/* Queue the first post, that way we know
                                    	 * what date we're dealing with (if that is the case).
                                    	 *
                                    	 * We reset this later so we can run the loop
                                    	 * properly with a call to rewind_posts().
                                    	 */
                                    	if ( have_posts() )
                                            the_post();
                                     ?>
                                    <?php
                        
                        				$var = get_query_var('post_format');
                        				// POST FORMATS
                        				if ($var == 'post-format-image') :
                        					esc_html_e('Image ', 'rubik');
                        				elseif ($var == 'post-format-gallery') :
                        					esc_html_e('Gallery ', 'rubik');
                        				elseif ($var == 'post-format-video') :
                        					esc_html_e('Video ', 'rubik');
                        				elseif ($var == 'post-format-audio') :
                        					esc_html_e('Audio ', 'rubik');
                        				endif;
                        
                        				the_archive_title();
                        			?>
                                </h2>
                            </div>
                            
                            <div class="sub-title"><p><?php the_archive_description()?></p></div>
                        </div>
                        <?php
                        	/* Since we called the_post() above, we need to
                        	 * rewind the loop back to the beginning that way
                        	 * we can run the loop properly, in full.
                        	 */
                        	rewind_posts();
                        ?>
                        <?php
                            if (have_posts()) {
                                echo rubik_archive::archive_content_area($archiveLayout, $bk_post_icon);
                            }
                        ?>
            
    	            </div> <!-- end #main -->
                </div>
            </div> <!-- end #bk-content -->
            <?php
                if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3')) && (!($archiveLayout == 'large-blog-nosb')) && (!($archiveLayout == 'large-blog-2-nosb')) && (!($archiveLayout == 'row-nosb'))) {?>
                    <div class="sidebar col-md-4">
                        <div class="sidebar-wrap <?php if($rubik_option['archive-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-archive-sidebar">
                            <?php
                                if (isset($archive_opt[$cur_tag_id]) && is_array($archive_opt[$cur_tag_id]) && array_key_exists('sb_category',$archive_opt[$cur_tag_id])) { 
                                    $bkarchiveSidebar = $archive_opt[$cur_tag_id]['sb_category'];
                                    if ((strlen($bkarchiveSidebar) != 0)&&($bkarchiveSidebar != 'global')) { 
                                        dynamic_sidebar($bkarchiveSidebar);
                                    }else {
                                        get_sidebar();
                                    }
                                }else {
                                    get_sidebar();
                                }
                            ?>
                        </div>
                    </div>
                <?php }
            ?>
        </div>
    </div>
</div>   
<?php get_footer(); ?>