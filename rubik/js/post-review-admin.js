(function($){"use strict";
    jQuery(document).ready(function($){
        $('#bk_final_score').attr('readonly', true);
        $('#bk_review .inside .rwmb-meta-box > div:gt(0)').wrapAll('<div class="bk-enabled-review">');
        $('.bk-enabled-review > div:first-child').prev().addBack().wrapAll($('<div/>',{'class': 'bk-criterias'}));
        
        var bkReviewCheckbox = $('#bk_review_checkbox');
        var bkReviewBox = $('.bk-enabled-review');
    
        if ( bkReviewCheckbox.is(":checked") ) {
                bkReviewBox.show();
            }
            
        bkReviewCheckbox.on('click', function() {
            bkReviewBox.slideToggle('slow');
        });
        function bkAvrScore() { 
            setTimeout(function(){
                var bk_criteria_length = $('.bk-criterias > div > div').children('.rwmb-clone').length;
                var bk_total_score = 0;
                $('.bk-criterias > div > div').children('.rwmb-clone').each(function(){
                    bk_total_score += parseFloat($(this).find('.rwmb-slider-wrapper').find('input').val());
                })
                var bkFinalScore = Math.round((bk_total_score / bk_criteria_length)*10)/10;
                $("#bk_final_score").val(bkFinalScore);
                if ( isNaN(bkFinalScore) ) { $("#bk_final_score").val(''); }
            }, 300);
        }
        $('.rwmb-slider').on('slidechange', bkAvrScore);
        $('.add-clone').on('click', function() {
            setTimeout(function(){
                $('.rwmb-slider').on('slidechange', bkAvrScore);
            }, 1000);
            bkAvrScore();
        });
        $(".remove-clone").live('click', function(){
            setTimeout(function(){
                $('.rwmb-slider').on('slidechange', bkAvrScore);
            }, 1000);
            bkAvrScore();
        });
        bkAvrScore();
    });
})(jQuery);