<?php
if (!class_exists('rubik_archive')) {
    class rubik_archive {
        static function the_query__sticky($catID, $posts_per_page){
            $feat_tag = '';                            

            $args = array(
				'post__in'  => get_option( 'sticky_posts' ),
                'cat' => $catID,
				'post_status' => 'publish',
				'ignore_sticky_posts' => 1,
				'posts_per_page' => $posts_per_page,
            );
            
            $the_query = new WP_Query( $args );
            wp_reset_postdata();
            return $the_query;
        }
    /**
     * ************* Get Option *****************
     *---------------------------------------------------
     */
        static function bk_get_archive_option($termID, $theoption = '') {
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $output = '';
            
            if($theoption != '') :
                $output  = rubik_core::bk_rwmb_meta( $theoption, array( 'object_type' => 'term' ), $termID );  
                if (isset($output) && ($output == 'global_settings')): 
                    //$output = $rubik_option[$theoption];
                endif;
            endif;
            
            return $output;
        }
        static function archive_content_area($archiveLayout, $bk_post_icon){  
            echo '<div class="bkmodule">';
            switch( $archiveLayout ) {
                case 'masonry':
                    echo self::masonry__render($bk_post_icon);
                    break;
                case 'masonry-nosb':
                    echo self::masonry_nosb__render($bk_post_icon);
                    break;
                case 'classic-blog':
                    echo self::classic_blog__render($bk_post_icon);
                    break;
                case 'large-blog':
                    echo self::large_blog__render($bk_post_icon);
                    break;
                case 'large-blog-nosb':
                    echo self::large_blog__render($bk_post_icon);
                    break;
                case 'large-blog-2':
                    echo self::large_blog_2__render($bk_post_icon);
                    break;
                case 'large-blog-2-nosb':
                    echo self::large_blog_2__render($bk_post_icon);
                    break;
                case 'row':
                    echo self::row__render($bk_post_icon);
                    break;
                case 'row-nosb':
                    echo self::row_nosb__render($bk_post_icon);
                    break;
                case 'square-grid-3-s1':
                    echo self::square_grid_3_s1__render($bk_post_icon);
                    break;
                case 'square-grid-2-s1':
                    echo self::square_grid_2_s1__render($bk_post_icon);
                    break;
                default:
                    echo self::classic_blog__render($bk_post_icon);
                    break;
            }
            echo '</div>';
        }
        static function square_grid_2_s1__render($bk_post_icon){  
            $bk_contentin3 = new bk_contentin3;
            $custom_var = array (
                'cat'           => 'on',
                'thumbnail'     => 'rubik-620-420',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('author', 'date'),
            );
            
            echo '<div class="content-wrap square-grid-2 module-square-grid module_style1">';
            echo '<ul class="bk-blog-content clearfix">';
                while (have_posts()): the_post();
                    echo '<li class="content_in col-md-6 col-sm-6">';
                    echo '<div class="content_in_wrapper">';
                    echo rubik_core::bk_render_html_string($bk_contentin3->render($custom_var));
                    echo '</div>';
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        static function square_grid_3_s1__render($bk_post_icon){  
            $bk_contentin3 = new bk_contentin3;
            $custom_var = array (
                'cat'           => 'on',
                'thumbnail'     => 'rubik-620-420',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('author', 'date'),
            );
            
            echo '<div class="content-wrap square-grid-3 module-square-grid module_style1">';
            echo '<ul class="bk-blog-content clearfix">';
                while (have_posts()): the_post();
                    echo '<li class="content_in col-md-4 col-sm-6">';
                    echo '<div class="content_in_wrapper">';
                    echo rubik_core::bk_render_html_string($bk_contentin3->render($custom_var));
                    echo '</div>';
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="rubik_pagination buttons-align-center col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        static function large_blog__render($bk_post_icon){  
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_contentout = new bk_contentout2;
            
            $custom_var = array (
                'thumbnail'     => 'rubik-600-315',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('author', 'date'),
                'except_length' => 50,
            );
            $bk_no_thumb = '';
            
            echo '<div class="content-wrap module-large-blog module-blog">';
            echo '<ul class="bk-blog-content clearfix">';
                while (have_posts()): the_post();
                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                        $bk_no_thumb = 'bk_no_thumb';
                    }else {
                        $bk_no_thumb = '';
                    }
                    echo '<li class="item col-md-12 row-type">';
                    echo '<div class="content_out clearfix '.$bk_no_thumb.'">';
                    echo rubik_core::bk_render_html_string($bk_contentout->render($custom_var));
                    echo '</div>';
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="rubik_pagination col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        static function large_blog_2__render($bk_post_icon){  
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_contentout = new bk_contentout2;
            
            $custom_var = array (
                'thumbnail'     => 'rubik-730-405',
                'post-icon'     => $bk_post_icon,
                'except_length' => 32,
            );
            $bk_no_thumb = '';
            
            echo '<div class="content-wrap module-large-blog layout-2 module-blog">';
            echo '<ul class="bk-blog-content clearfix">';
                while (have_posts()): the_post();
                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                        $bk_no_thumb = 'bk_no_thumb';
                    }else {
                        $bk_no_thumb = '';
                    }
                    echo '<li class="item col-md-12 row-type">';
                    echo '<div class="content_out clearfix '.$bk_no_thumb.'">';
                    echo rubik_core::bk_render_html_string($bk_contentout->render($custom_var));
                    echo '</div>';
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="rubik_pagination col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        static function row__render($bk_post_icon){  
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_contentout = new bk_contentout2;
            
            $custom_var = array (
                'thumbnail'    => 'rubik-360-180',
                'post-icon'     => $bk_post_icon,
                'except_length' => 20,
            );
            $bk_no_thumb = '';
            
            echo '<div class="content-wrap module-row wsb-module layout-1 module-blog">';
            echo '<ul class="bk-row-wrap bk-module-inner clearfix">';
                while (have_posts()): the_post();
                    echo '<li class="row-type content_out col-md-6 col-sm-6">';
                    echo rubik_core::bk_render_html_string($bk_contentout->render($custom_var));
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="rubik_pagination col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        static function row_nosb__render($bk_post_icon){  
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_contentout = new bk_contentout2;
            
            $custom_var = array (
                'thumbnail'    => 'rubik-360-180',
                'post-icon'     => $bk_post_icon,
                'except_length' => 20,
            );
            $bk_no_thumb = '';
            
            echo '<div class="content-wrap module-row fw-module layout-1 module-blog">';
            echo '<ul class="bk-row-wrap bk-module-inner clearfix">';
                while (have_posts()): the_post();
                    echo '<li class="row-type content_out col-md-4 col-sm-6">';
                    echo rubik_core::bk_render_html_string($bk_contentout->render($custom_var));
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="rubik_pagination col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        static function classic_blog__render($bk_post_icon){  
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_contentout = new bk_contentout5;
            
            $custom_var = array (
                'thumbnail'     => 'rubik-310-280',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('author', 'date'),
                'except_length' => 22,
                'rm_btn'        => true,
            );
            $bk_no_thumb = '';
            echo '<div class="content-wrap module-classic-blog module-blog">';
            echo '<ul class="bk-blog-content clearfix">';
                while (have_posts()): the_post();
                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                        $bk_no_thumb = 'bk_no_thumb';
                    }else {
                        $bk_no_thumb = '';
                    }
                    echo '<li class="item col-md-12">';
                    echo '<div class="content_out clearfix '.$bk_no_thumb.'">';
                    echo rubik_core::bk_render_html_string($bk_contentout->render($custom_var));
                    echo '</div>';
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="rubik_pagination col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        static function masonry_nosb__render($bk_post_icon){  
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_contentout = new bk_contentout2;

            $custom_var = array (
                'thumbnail'     => 'rubik-masonry-size',
                'post-icon'     => $bk_post_icon,
                'except_length' => 20,
            );
            $bk_no_thumb = '';
            
            echo '<div class="content-wrap bk-masonry">';
            echo '<ul class="bk-masonry-content clearfix">';
                while (have_posts()): the_post();
                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                        $bk_no_thumb = 'bk_no_thumb';
                    }else {
                        $bk_no_thumb = '';
                    }
                    echo '<li class="col-md-4 col-sm-6 item">';
                    echo '<div class="row-type content_out '.$bk_no_thumb.'">';
                    echo rubik_core::bk_render_html_string($bk_contentout->render($custom_var));
                    echo '</div>';
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="rubik_pagination buttons-align-center col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        static function masonry__render($bk_post_icon){  
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $bk_contentout = new bk_contentout2;
            
            $custom_var = array (
                'thumbnail'     => 'rubik-masonry-size',
                'post-icon'     => $bk_post_icon,
                'except_length' => 20,
            );
            $bk_no_thumb = '';
            
            echo '<div class="content-wrap bk-masonry">';
            echo '<ul class="bk-masonry-content clearfix">';
                while (have_posts()): the_post();
                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                        $bk_no_thumb = 'bk_no_thumb';
                    }else {
                        $bk_no_thumb = '';
                    }
                    echo '<li class="col-md-6 col-sm-6 item">';
                    echo '<div class="row-type content_out '.$bk_no_thumb.'">';
                    echo rubik_core::bk_render_html_string($bk_contentout->render($custom_var));
                    echo '</div>';
                    echo '</li>';
                endwhile;
            echo '</ul>';
            echo '</div>';
            if (function_exists("rubik_paginate")) {
                echo '<div class="rubik_pagination col-md-12">';
                rubik_paginate();
                echo '</div>';
            }
        }
        
    } // Close rubik_archive class
    
}
