<?php
if (!class_exists('rubik_feature1')) {
    class rubik_feature1 extends rubik_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            global $rubik_dynamic_css;
            $uid = uniqid('feature1-');
                       
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_feature1'], $page_info);
            $module_cfg['limit'] = 3;
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $moduleHasbg = '';                                    
            $moduleHasbg = $rubik_dynamic_css[$uid]['moduleHasbg'];
            
            /** Tabs **/
            $rubik_tabs = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_categorytabs', true );
            
            $rubikTabsToArray = explode(",",$rubik_tabs);
            
            $tabs_amount = intval(count($rubikTabsToArray));
            
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['content'] = '';
            rubik_section_parent::$rubik_ajax_c[$uid]['post_icon'] = $bk_post_icon;
            
            if($tabs_amount > 0) {
                for ($i=0; $i< $tabs_amount; $i++) {
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['cat'] = $rubikTabsToArray[$i];
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['content'] = '';
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);              //get query
    
            $block_str .= '<div id='.$uid.' class="bkmodule module-feature1">';
            $block_str .= '<div class="container bkwrapper">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info, $rubikTabsToArray);  //render block title
            endif;
            $block_str .= '<div class="bk-module-inner">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon);
            $block_str .= '</div>';
            $block_str .= '</div>';
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $bk_post_icon){
            $render_modules = '';
            $custom_var_in3 = array (
                    'cat'           => 'on',
                    'post-icon'     => $bk_post_icon,
                    'meta'          => array('author', 'date'),
                    'thumbnail'     => 'rubik-620-420',
            );
            
            $custom_var_out2 = array (
                    'thumbnail'    => 'rubik-620-420',
                    'post-icon'     => $bk_post_icon,
                    'except_length' => 35,
                );
            $bk_contentout2 = new bk_contentout2;
            $bk_contentin3 = new bk_contentin3;
            if ( $the_query->have_posts() ) : $the_query->the_post();
                $render_modules .= '<div class="row clearfix">';
                $render_modules .= '<div class="col-md-7 col-sm-6 large-post row-type content_out">';
                $render_modules .= $bk_contentout2->render($custom_var_out2);
                $render_modules .= '</div>  <!-- end post -->';
                
                $render_modules .= '<div class="col-md-5 col-sm-6"><ul>';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="sub-post content_in bk-post-title-small">';
                    $render_modules .= '<div class="post-wrap">';
                    $render_modules .= $bk_contentin3->render($custom_var_in3);
                    $render_modules .= '</div>';
                    $render_modules .= '</li>  <!-- end post -->';
                endwhile;
                $render_modules .= '</div>';
                $render_modules .= '</ul></div> <!-- Close render modules -->';
            endif;
            return $render_modules;
        }
        
    }
}