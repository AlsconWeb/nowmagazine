<?php if (is_active_sidebar('footer_sidebar_1') 
        || is_active_sidebar('footer_sidebar_2')
        || is_active_sidebar('footer_sidebar_3')) { ?>
<div class="footer-content footer_1_3 bkwrapper clearfix container">
    <div class="row">
        <div class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_sidebar_1' ); ?>
        </div>
        <div class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_sidebar_2' ); ?>
        </div>
        <div class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_sidebar_3' ); ?>
        </div>
    </div>
</div>
<?php } ?>