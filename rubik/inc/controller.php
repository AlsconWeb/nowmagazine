<?php
// Section
    require_once(get_template_directory()."/inc/blocks/rubik_parent.php");
    
// Block Fullwidth
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_feature3.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_feature4.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_promo_box.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_featured_slider.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_feature1.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_feature2.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_boxed_slider.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_row_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_row_tall_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_hero.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_fw_slider.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_masonry.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_masonry_2.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_masonry_3.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_carousel.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_carousel_type2.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_carousel.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_tech_grid.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_pyramid_grid.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_square_grid.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_ads.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_ads_2cols.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_ads_3cols.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_adsense.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_custom_html.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_short_code.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_mailchimp_shortcode.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_block_2_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_youtube_playlist_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_youtube_playlist_fw_2.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_youtube_playlist_fw_3.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_1.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_2.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_3.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_4.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_5.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_6.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_7.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_8.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_9.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_10.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_11.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_12.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_13.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_grid_14.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_block_1_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_block_3_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_block_4_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_block_5_fw.php");
    
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_row_2_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_row_3_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_row_tall_2_fw.php");
    require_once(get_template_directory()."/inc/blocks/fullwidth/rubik_row_tall_3_fw.php");
// Block has sidebar
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_1.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_2.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_3.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_4.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_5.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_6.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_7.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_8.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_9.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_10.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_11.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_block_12.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_row.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_row_2.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_row_3.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_row_4.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_row_tall.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_row_tall_2.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_row_tall_3.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_classic_blog.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_large_blog.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_large_blog_2.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_youtube_playlist_2.php");
    require_once(get_template_directory()."/inc/blocks/has_sb/rubik_youtube_playlist_3.php");
        
// Modules    
    require_once(get_template_directory()."/inc/modules/rubik_contentin1.php");
    require_once(get_template_directory()."/inc/modules/rubik_contentin2.php");
    require_once(get_template_directory()."/inc/modules/rubik_contentin3.php");
    require_once(get_template_directory()."/inc/modules/rubik_contentin4.php"); //FW slider
    require_once(get_template_directory()."/inc/modules/rubik_contentin5.php"); //Featured slider
    require_once(get_template_directory()."/inc/modules/rubik_contentout1.php"); // feature1
    require_once(get_template_directory()."/inc/modules/rubik_contentout2.php"); //row
    require_once(get_template_directory()."/inc/modules/rubik_contentout3.php"); //hero
    require_once(get_template_directory()."/inc/modules/rubik_contentout4.php"); //masonry
    require_once(get_template_directory()."/inc/modules/rubik_contentout5.php"); //classic blog
    require_once(get_template_directory()."/inc/modules/rubik_contentout6.php"); //large blog
    require_once(get_template_directory()."/inc/modules/rubik_contentout7.php"); //large blog
    require_once(get_template_directory()."/inc/modules/rubik_contentout8.php"); //block 5
    require_once(get_template_directory()."/inc/modules/rubik_contentout9.php"); //Row 2
    require_once(get_template_directory()."/inc/modules/rubik_contentout10.php"); //Row 3
//Libs        
    require_once(get_template_directory()."/inc/libs/rubik_get_configs.php");
    require_once(get_template_directory()."/inc/libs/rubik_core.php");
    require_once(get_template_directory()."/inc/libs/single_core.php");
    require_once(get_template_directory()."/inc/libs/rubik_query.php");
    require_once(get_template_directory()."/inc/libs/rubik_cache.php");
    require_once(get_template_directory()."/inc/libs/rubik_youtube.php");
    require_once(get_template_directory()."/inc/libs/rubik_archive.php");