<?php
if (!class_exists('rubik_boxed_slider')) {
    class rubik_boxed_slider extends rubik_section_parent  {
        
        static $pageInfo = '';
        
        public function render( $page_info ) {
            
            self::$pageInfo = $page_info;
            
            global $rubik_dynamic_css;
            $uid = uniqid('boxed_slider-');
            
            $moduleAlign = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_align', true ); 
            $moduleStyle = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_style', true ); 
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $moduleHasbg = '';                                    
            $moduleHasbg = $rubik_dynamic_css[$uid]['moduleHasbg'];
            
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $sec = '';
            $has_bkwrapper = '';
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $has_bkwrapper = '';                 
            }else {
                $has_bkwrapper = 'bkwrapper container';   
            }
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_boxed_slider'], $page_info);
            
            $the_query = bk_get_query::query($module_cfg);
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-boxed-slider bk-slider-module '.$moduleAlign.' '.$moduleStyle.' '.$moduleHasbg.' ">';
            $block_str .= '<div class="bkmodule-slider-wrapper '.$has_bkwrapper.'">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
            endif;
            $block_str .= $this->render_modules($the_query, $bk_post_icon);
            $block_str .= '</div>';
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $bk_post_icon=''){
            $render_modules = '';
            $bk_contentin3 = new bk_contentin3;
            
            $custom_var = array (
                'thumbnail'     => 'rubik-900-613',
                'meta'          => array('author', 'date'),
                'cat'           => 'on',
                'post-icon'     => $bk_post_icon,
            ); 
            
            $meta_items = get_post_meta( self::$pageInfo['page_id'], self::$pageInfo['block_prefix'].'_meta_items', true ); 
            
            if($meta_items == 'hide-cat-meta') {
                $custom_var['meta'] = 'off';
                $custom_var['cat'] = 'off';
            }else if($meta_items == 'hide-cat') {
                $custom_var['cat'] = 'off';
            }else if($meta_items == 'hide-meta') {
                $custom_var['meta'] = 'off';
            }
            
            if ( $the_query->have_posts() ) :            
                $render_modules .= '<div class="flexslider">';
                $render_modules .= '<ul class="col-md-12 slides">';
                
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="item content_in">';
                    $render_modules .= $bk_contentin3->render($custom_var);
                    $render_modules .= '</li>';
                endwhile;
                
                $render_modules .= '</ul></div> <!-- Close render modules -->';
                
            endif;
            return $render_modules;
        }       
    }
}