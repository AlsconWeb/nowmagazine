<?php
if (!class_exists('bk_contentin5')) {
    class bk_contentin5 {
        
        function render($custom_var) {
            ob_start();?>
            
            <div class="bk-article-wrapper" itemscope itemtype="https://schema.org/Article">
                <div class="bk-mask">
                    <?php 
                        $bkPostId = get_the_ID();
                        $feat_img = '';
                        $feat_img .= '<div class="thumb hide-thumb">';
                        $feat_img .= '<a class="link-overlap" href="'.esc_url(get_permalink($bkPostId)).'"></a>'; 
                            
                        $bkThumbId = get_post_thumbnail_id( $bkPostId );
                        $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, $custom_var['thumbnail'] );
                        $feat_img .= '<div class="bk-slick-thumbnail" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div>';
                        
                        $review_checkbox = get_post_meta($bkPostId, 'bk_review_checkbox', true );
                        if((isset($custom_var['post-icon']) && ($custom_var['post-icon'] != 'hide')) || ($review_checkbox == 1)) {
                            echo '<div class="rubik-post-icon-wrap">';
                            if (isset($custom_var['post-icon']) && ($custom_var['post-icon'] == 'show')) {
                                echo rubik_core::bk_get_post_icon($bkPostId);
                            }else if (isset($custom_var['post-icon']) && ($custom_var['post-icon'] == 'video-only')) {
                                echo rubik_core::bk_get_post_icon_video_only($bkPostId);
                            }
                            if($review_checkbox == 1) {
                                $bk_final_score = get_post_meta($bkPostId, 'bk_final_score', true );
                                if($bk_final_score != 0) {
                                    echo rubik_core::bk_get_review_score($bkPostId, $bk_final_score);
                                }
                            }
                            echo '</div>';
                        }
                        
                        $feat_img .= '</div> <!-- close thumb -->';
                    ?>
                    <?php echo rubik_core::bk_render_html_string($feat_img);?>
                </div>
                <div class="post-c-wrap">
                    <div class="inner">
                        <div class="inner-cell"> 
                            <div class="post-c-inner">
                                <?php
                                    if (!(isset($custom_var['cat'])) || (isset($custom_var['cat']) && ($custom_var['cat'] !== 'off'))) :
                                        echo rubik_core::bk_meta_cases('cat');
                                    endif;
                                ?>
    
                                <?php 
                                    if (isset($custom_var['title_length']) && ($custom_var['title_length'] > 0)) {
                                        echo rubik_core::bk_get_post_title($bkPostId, $post_args['title_length']);
                                    }else {
                                        echo rubik_core::bk_get_post_title($bkPostId, '');
                                    }
                                ?>
                                <?php
                                    if (isset($custom_var['meta']) && ($custom_var['meta'] != 'off')) :
                                        echo rubik_core::bk_get_post_meta($custom_var['meta']);
                                    endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php return ob_get_clean();
        }
        
    }
}