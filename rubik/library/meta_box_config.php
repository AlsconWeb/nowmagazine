<?php
/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
add_filter( 'rwmb_meta_boxes', 'bk_register_meta_boxes' );
function bk_register_meta_boxes( $meta_boxes ) {
        
    // Better has an underscore as last sign
    $prefix = 'bk_';
    
    global $meta_boxes;
    
    $prefix = 'bk_';
    $bk_sidebar = array();
    foreach ( $GLOBALS['wp_registered_sidebars'] as $value => $label ) {
        $bk_sidebar[$value] = ucwords( $label['name'] );
    }
    $bk_sidebar['disable'] = esc_html__( 'Disable Sidebar', 'rubik' );
    $bk_sidebar['global'] = esc_html__( 'From Theme Options', 'rubik' );
    
    // 2nd meta box
    $meta_boxes[] = array(
        'id' => "{$prefix}subtitle",
        'title' => esc_html__( 'BK SubTitle', 'rubik' ),
        'pages' => array( 'post' ),
        'context' => 'normal',
        'priority' => 'high',
    	'fields' => array(        
            //Video
            array(
                'name' => esc_html__( 'SubTitle', 'rubik' ),
                'desc' => esc_html__('Insert the SubTitle for this post', 'rubik'),
                'id' => "{$prefix}visual_subtitle",
                'type' => 'textarea',
                'placeholder' => esc_attr__('SubTitle ...', 'rubik'),
                'std' => ''
            ),
        )
    );
    // Post Layout Options
    $meta_boxes[] = array(
        'id' => "{$prefix}post_ops",
        'title' => esc_html__( 'BK Post Option', 'rubik' ),
        'pages' => array( 'post' ),
        'context' => 'normal',
        'priority' => 'low',
    
        'fields' => array(
            // Enable Review
            array(
    			'id' => "{$prefix}post_layout",
                'class' => 'post-layout-options',
                'name' => esc_html__( 'Post Layout Option', 'rubik' ),
                'type' => 'image_select', 
    			'options'  => array(
                                'global_settings' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_global.png',
            					'single_template_1' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_1.png',
                                'single_template_2' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_2.png',
                                'single_template_3' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_3.png',
                                'single_template_4' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_4.png',
                                'single_template_5' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_5.png',
                                'single_template_6' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_6.png',
                                'single_template_7' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_7.png',
                                'single_template_8' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_8.png',
                                'single_template_9' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_9.png',
                                'single_template_10' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_10.png',
                                'single_template_11' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_11.png',
                                'single_template_12' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_12.png',
                                'single_template_13' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_13.png',
                                'single_template_14' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_14.png',
                                'single_template_15' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_15.png',
                                'single_template_16' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_16.png',
                                'single_template_17' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_17.png',
        				    ),
    			'multiple'    => false,
    			'std'         => 'global_settings',
    		),
            array(
                'name' => esc_html__( 'Heading Related Posts', 'rubik' ),
                'id' => "{$prefix}heading_related_posts",
                'type' => 'select', 
    			'options'  => array(
                                'enable'    => esc_html__( 'Enable', 'rubik' ),
                                'disable'   => esc_html__( 'Disable', 'rubik' ),
                                'from-themeoptions' => esc_html__( 'From Theme Options', 'rubik' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'from-themeoptions',
            ),
            // Post Nav Section Style
            array(
                'name' => esc_html__( 'Post Nav Section Style', 'rubik' ),
                'id' => "{$prefix}post_nav_style",
                'type' => 'select', 
    			'options'  => array(
                                'under-content' => esc_html__( 'Under Post Content', 'rubik' ),
                                'fixed-position' => esc_html__( 'Fixed Position on Left and Right Side', 'rubik' ),
                                'from-themeoptions' => esc_html__( 'From Theme Options', 'rubik' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'from-themeoptions',
            ),
            // Sidebar Select
            array(
                'name' => esc_html__( 'Choose a sidebar for this post', 'rubik' ),
                'id' => "{$prefix}post_sb_select",
                'type' => 'select',
                'options'  => $bk_sidebar,
                'desc' => esc_html__( 'Sidebar Select', 'rubik' ),
                'std'  => 'global',
            ),
            // Sidebar Position
            array(
                'name' => esc_html__( 'Sidebar Position', 'rubik' ),
                'id' => "{$prefix}sidebar_position",
                'type' => 'select', 
    			'options'  => array(
                                'right' => esc_html__( 'Right', 'rubik' ),
                                'left' => esc_html__( 'Left', 'rubik' ),
                                'from-themeoptions' => esc_html__( 'From Theme Options', 'rubik' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'from-themeoptions',
            ),
        )
    );
    
    
    $meta_boxes[] = array(
        'id' => "bk_video_post_format",
        'title' => esc_html__( 'BK Video Post Format', 'rubik' ),
        'pages' => array( 'post' ),
        'context' => 'normal',
        'priority' => 'high',
        'visible' => array( 'post_format', 'in', array('video', 'audio')),
    	'fields' => array(        
            //Video
            array(
                'name' => esc_html__( 'Format Options: Video, Audio', 'rubik' ),
                'desc' => esc_html__('Support Youtube, Vimeo, SoundCloud, DailyMotion Link', 'rubik'),
                'id' => "{$prefix}media_embed_code_post",
                'type'  => 'oembed',
                'placeholder' => esc_attr__('Link ...', 'rubik'),
                'std' => ''
            ),
            array(
                'name' => esc_html__( 'Popup Frame', 'rubik' ),
                'id' => "{$prefix}popup_frame",
                'type' => 'checkbox',
                'desc' => esc_html__( 'Enable Popup Frame', 'rubik' ),
                'std'  => 0,
            ),
        )
    );
    $meta_boxes[] = array(
        'id' => "bk_gallery_post_format",
        'title' => esc_html__( 'BK Gallery Post Format', 'rubik' ),
        'pages' => array( 'post' ),
        'context' => 'normal',
        'priority' => 'high',
        'visible' => array( 'post_format', 'in', array('gallery')),
    	'fields' => array(  
            //Gallery
            array(
                'name' => esc_html__( 'Format Options: Gallery', 'rubik' ),
                'desc' => esc_html__('Gallery Images', 'rubik'),
                'id' => "{$prefix}gallery_content",
                'type' => 'image_advanced',
                'std' => ''
            ),
        )
    );
    
    // Post Review Options
    $meta_boxes[] = array(
        'id' => "{$prefix}review",
        'title' => esc_html__( 'BK Review System', 'rubik' ),
        'pages' => array( 'post' ),
        'context' => 'normal',
        'priority' => 'high',
    
        'fields' => array(
            // Enable Review
            array(
                'name' => esc_html__( 'Include Review Box', 'rubik' ),
                'id' => "{$prefix}review_checkbox",
                'type' => 'checkbox',
                'desc' => esc_html__( 'Enable Review On This Post', 'rubik' ),
                'std'  => 0,
            ),
            array(
				'id'     => 'reviewcriterias',
				'type'   => 'group',

				'fields' => array(
					// Criteria 1 Text & Score
                    array(
                        'name'  => esc_html__( 'Criteria Title', 'rubik' ),
                        'id'    => "{$prefix}ct",
                        'type'  => 'text',
                    ),
                    array(
                        'name' => esc_html__( 'Criteria Score', 'rubik' ),
                        'id' => "{$prefix}cs",
                        'type' => 'slider',
                        'js_options' => array(
                            'min'   => 0,
                            'max'   => 10.05,
                            'step'  => .1,
                        ),
                    ),
				),

				// Clone whole group?
				'clone'  => true,
			),
            // Summary
            array(
                'name' => esc_html__( 'Summary', 'rubik' ),
                'id'   => "{$prefix}summary",
                'type' => 'textarea',
                'cols' => 20,
                'rows' => 4,
            ),
            
            // Final average
            array(
                'name'  => esc_html__('Final Average Score','rubik'),
                'id'    => "{$prefix}final_score",
                'type'  => 'text',
            ),
            array(
                'name' => esc_html__( 'User Rating', 'rubik' ),
                'id' => "{$prefix}user_rating",
                'type' => 'checkbox',
                'desc' => esc_html__( 'Enable User Rating On This Post', 'rubik' ),
                'std'  => 0,
            ),
            array(
    			'id' => "{$prefix}review_box_position",
                'name' => esc_html__( 'Review Box Position', 'rubik' ),
    			'desc' => esc_html__('Setup review post position [left-content, right-content, above-content, below-content]', 'rubik'),
                'type' => 'select', 
    			'options'  => array(
            					'left' => esc_html__( 'Align Left', 'rubik' ),
            					'right' => esc_html__( 'Align Right ', 'rubik' ),
                                'above' => esc_html__( 'Above The Content', 'rubik' ),
                                'below' => esc_html__( 'Under The Content', 'rubik' ),
        				    ),
    			// Select multiple values, optional. Default is false.
    			'multiple'    => false,
    			'std'         => 'below',
    		),
    
        )
    );  
    return $meta_boxes;
}