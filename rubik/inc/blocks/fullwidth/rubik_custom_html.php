<?php
if (!class_exists('rubik_custom_html')) {
    class rubik_custom_html extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $block_str = '';
            $uid = uniqid('customhtml-');
            $custom_html = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_custom_html', true );
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $bkwrapper_container = '';                         
            }else {
                $bkwrapper_container = 'bkwrapper container';            
            }
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-custom-html '.$bkwrapper_container.' clearfix">';
            $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
            $block_str .= $custom_html;
            $block_str .= '</div>';
            wp_reset_postdata();
            return $block_str;
    	}
        
    }
}