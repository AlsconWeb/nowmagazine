<?php
if (!class_exists('rubik_featured_slider')) {
    class rubik_featured_slider extends rubik_section_parent  {
        
        static $pageInfo = '';
        
        public function render( $page_info ) {
            
            self::$pageInfo = $page_info;
            
            global $rubik_dynamic_css;
                        
            $uid = uniqid('featured_slider_');
            
            $moduleAlign = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_align', true ); 
            $moduleStyle = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_style', true );
            
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            
            $rubik_dynamic_css[$uid]['title_font_size'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_font_size', true ); 
            $rubik_dynamic_css[$uid]['title_font_weight'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_font_weight', true ); 
            $rubik_dynamic_css[$uid]['title_letter_spacing'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_letter_spacing', true ); 

            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_featured_slider'], $page_info);    //get block config
            
            $the_query = bk_get_query::query($module_cfg);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-featured-slider background-preload '.$moduleAlign.' '.$moduleStyle.'"><div class="featured-slider-wrapper featured-slider-runner opacity-zero">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon);            //render modules
            $block_str .= '</div><div class="bk-preload"></div></div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $bk_post_icon = ''){
            $render_modules = '';
            $bk_contentin5 = new bk_contentin5;
            
            $meta_items = get_post_meta( self::$pageInfo['page_id'], self::$pageInfo['block_prefix'].'_meta_items', true ); 
            
            if ( $the_query->have_posts() ) :
                $custom_var = array (
                    'thumbnail'     => 'rubik-900-613',
                    'meta'          => array('author', 'date'),
                    'cat'           => true,
                    'post-icon'     => $bk_post_icon,
                    'except_length' => '',
                    'rm_btn'        => true,
                );
                
                if($meta_items == 'hide-cat-meta') {
                    $custom_var['meta'] = 'off';
                    $custom_var['cat'] = 'off';
                }else if($meta_items == 'hide-cat') {
                    $custom_var['cat'] = 'off';
                }else if($meta_items == 'hide-meta') {
                    $custom_var['meta'] = 'off';
                }
                
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<div class="item content_in">';
                    $render_modules .= $bk_contentin5->render($custom_var);
                    $render_modules .= '</div>';
                endwhile;
                
            endif;
            return $render_modules;
        }
        
    }
}