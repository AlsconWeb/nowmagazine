<?php
if (!class_exists('rubik_breaking_carousel')) {
    class rubik_breaking_carousel extends rubik_section_parent  {
        
        public function render( $page_info ) {
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_breaking_carousel'], $page_info);    //get block config
            
            $the_query = bk_get_query::query($module_cfg);              //get query
    
            $block_str .= '<div class="bkmodule container bkwrapper module-breaking-carousel hide">';
            $block_str .= $this->render_modules($the_query);
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentout3 = new bk_contentout3;
            $custom_var_out3 = array (
                'thumbnail'    => 'rubik-90-65',
                'meta'         => array('date'),
            );
            if ( $the_query->have_posts() ) :
                $render_modules .= '<div class="bk-carousel-wrap">';
                $render_modules .= '<ul class="slides clearfix">';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="small-post content_out clearfix">';
                        $render_modules .= $bk_contentout3->render($custom_var_out3);
                    $render_modules .= '</li>';
                endwhile;
                $render_modules .= '</ul> <!-- Close render slider -->';
                $render_modules .= '</div>';
            endif;
            return $render_modules;
        }
        
    }
}