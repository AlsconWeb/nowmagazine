<?php
if (!class_exists('bk_contentin4')) {
    class bk_contentin4 {
        
        function render($custom_var = null) {
            ob_start();
            $postID = get_the_ID();
            $category = get_the_category($postID); 
            $bkThumbId = get_post_thumbnail_id( $postID );
            $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, $custom_var['thumbnail'] );
            echo '<div class="bk-thumb-wrap term-'.$category[0]->term_id.'"><a class="link-overlap" href="'.esc_url(get_permalink($postID)).'"></a><div class="thumb hide-thumb" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div></div>';
            ?>
            <div class="slider-content-wrap container bkwrapper">   
                <div class="slider-content-wrap-inner">   
                    <div class="post-c-wrap">
                        <div class="inner">
                            <div class="inner-cell">
                                <div class="post-c-inner innerwrap">
                                    <?php 
                                        if (isset($custom_var['cat']) && ($custom_var['cat'] != 'off')) :
                                            echo rubik_core::bk_meta_cases('cat');
                                        endif;
                                    ?> 
                                    <?php echo rubik_core::bk_get_post_title($postID, 15);?>
                                    <?php
                                        if (isset($custom_var['meta']) && ($custom_var['meta'] != 'off')) :
                                            echo rubik_core::bk_get_post_meta($custom_var['meta']);
                                        endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                    
            </div>
            <?php return ob_get_clean();
        }
        
    }
}