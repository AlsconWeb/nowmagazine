<?php
if (!class_exists('bk_contentout1')) {
    class bk_contentout1 {
        
        function render($custom_var) {
            ob_start();
            ?>
            <div class="col-md-7 col-sm-12 feat-img">
                <?php echo rubik_core::get_feature_image($custom_var['thumbnail'], true, $custom_var['post-icon']);?>
            </div>
            <div class="post-c-wrap col-md-5 col-sm-12">     
                <?php 
                    if (!(isset($custom_var['cat'])) || (isset($custom_var['cat']) && ($custom_var['cat'] != 'off'))) :
                        echo rubik_core::bk_meta_cases('cat');
                    endif;
                ?>
                <?php echo rubik_core::bk_get_post_title(get_the_ID(), 15);?>
                <?php echo rubik_core::bk_get_post_excerpt($custom_var['except_length']);?>
                <div class="meta-wrap clearfix">
                    <?php
                    if (isset($custom_var['meta']) && ($custom_var['meta'] != 'off')) :
                        echo rubik_core::bk_get_post_meta($custom_var['meta']);
                    endif;
                    ?>                                        
                    <?php 
                    if (isset($custom_var['rm_btn']) && ($custom_var['rm_btn'] != 'off')) :
                        echo rubik_core::bk_readmore_btn(get_the_ID());
                    endif;
                    ?>
                </div>
            </div>
            
            <?php return ob_get_clean();
        }
        
    }
}