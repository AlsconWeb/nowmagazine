<?php
if (!class_exists('bk_contentin3')) {
    class bk_contentin3 {
        
        function render($custom_var = null) {
            ob_start();
            $bkPostId = get_the_ID();
            $bkThumbId = get_post_thumbnail_id( $bkPostId );
            
            $category = get_the_category($bkPostId); 
            $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, $custom_var['thumbnail'] );
            
            echo '<div class="bk-thumb-wrap term-'.$category[0]->term_id.'">';
            echo '<a class="link-overlap" href="'.esc_url(get_permalink($bkPostId)).'"></a>'; 
            echo '<div class="thumb hide-thumb" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div>';
            
            $review_checkbox = get_post_meta($bkPostId, 'bk_review_checkbox', true );
            if((isset($custom_var['post-icon']) && ($custom_var['post-icon'] != 'hide')) || ($review_checkbox == 1)) {
                echo '<div class="rubik-post-icon-wrap">';
                if (isset($custom_var['post-icon']) && ($custom_var['post-icon'] == 'show')) {
                    echo rubik_core::bk_get_post_icon($bkPostId);
                }else if (isset($custom_var['post-icon']) && ($custom_var['post-icon'] == 'video-only')) {
                    echo rubik_core::bk_get_post_icon_video_only($bkPostId);
                }
                if($review_checkbox == 1) {
                    $bk_final_score = get_post_meta($bkPostId, 'bk_final_score', true );
                    if($bk_final_score != 0) {
                        echo rubik_core::bk_get_review_score($bkPostId, $bk_final_score);
                    }
                }
                echo '</div>';
            }
            echo '</div>';
            ?>
            <div class="post-c-wrap">
                <div class="inner">
                    <div class="inner-cell">
                        <div class="post-c-inner innerwrap">
                            <?php 
                                if (!(isset($custom_var['cat'])) || (isset($custom_var['cat']) && ($custom_var['cat'] != 'off'))) :
                                    echo rubik_core::bk_meta_cases('cat');
                                endif;
                            ?> 
                            <?php echo rubik_core::bk_get_post_title($bkPostId, 15);?>
                            <?php
                                if (isset($custom_var['meta']) && ($custom_var['meta'] != 'off')) :
                                    echo rubik_core::bk_get_post_meta($custom_var['meta']);
                                endif;
                            ?>
                        </div>                                             
                    </div>
                </div>
            </div>
            <?php return ob_get_clean();
        }
        
    }
}