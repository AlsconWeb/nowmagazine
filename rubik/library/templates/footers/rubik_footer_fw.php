<?php if (is_active_sidebar('footer_d1')) { ?>
<div class="footer-content bkwrapper clearfix container">
    <div class="row">
        <div id="footer_d1" class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_d1' ); ?>
        </div>
    </div>
</div>
<?php } ?>