<?php
require_once (get_template_directory().'/framework/page-builder/controller/bk_pd_template.php');
require_once (get_template_directory().'/framework/page-builder/controller/bk_pd_cfg.php');
require_once (get_template_directory().'/framework/page-builder/controller/bk_pd_save.php');
require_once (get_template_directory().'/framework/page-builder/controller/bk_pd_del.php');
require_once (get_template_directory().'/framework/page-builder/controller/render-sections.php');
require_once (get_template_directory().'/framework/page-builder/bk_pd_start.php');
require_once (get_template_directory().'/plugins/sitelinks-search-box.php');

require_once (get_template_directory().'/library/taxonomy-meta-config.php');

/**
 * Load the TGM Plugin Activator class to notify the user
 * to install the Envato WordPress Toolkit Plugin
 */
require_once( get_template_directory() . '/inc/class-tgm-plugin-activation.php' );
function rubik_tgmpa_register_toolkit() {
    // Specify the Envato Toolkit plugin
    $plugins = array(
        array(
            'name' => esc_html__('Sidebar Generator', 'rubik'),
            'slug' => 'rubik-sidebar-generator',
            'img' => get_template_directory_uri() . '/images/plugins/sidebar-generator.jpg',
            'source' => get_template_directory() . '/plugins/rubik-sidebar-generator.zip',
            'required' => '',
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        
        array(
            'name' => esc_html__('Redux Framework', 'rubik'),
            'slug' => 'redux-framework',
            'img' => get_template_directory_uri() . '/images/plugins/Redux.jpg',
            'required' => true,
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Meta Box', 'rubik'),
            'slug' => 'meta-box',
            'img' => get_template_directory_uri() . '/images/plugins/meta-box.jpg',
            'required' => true,
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),      
        array(
            'name' => esc_html__('Meta Box Conditional Logic', 'rubik'),
            'slug' => 'meta-box-conditional-logic',
            'img' => get_template_directory_uri() . '/images/plugins/Conditional-Logic.jpg',
            'source' => get_template_directory() . '/plugins/meta-box-conditional-logic.zip',
            'required' => '',
            'version' => '1.6.7',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Rubik Admin Panel', 'rubik'),
            'slug' => esc_html__('rubik-admin-panel', 'rubik'),
            'source' => get_template_directory() . '/plugins/rubik-admin-panel.zip',
            'required' => true,
            'version' => '2.0',
            'external_url' => '',
        ),        
        array(
            'name' => esc_html__('Rubik Extension', 'rubik'),
            'slug' => esc_html__('rubik-extension', 'rubik'),
            'img' => get_template_directory_uri() . '/images/plugins/extension.jpg',
            'source' => get_template_directory() . '/plugins/rubik-extension.zip',
            'required' => true,
            'version' => '1.1',
            'external_url' => '',
        ),  
        array(
            'name' => esc_html__('Rubik Shortcode', 'rubik'),
            'slug' => esc_html__('rubik-shortcode', 'rubik'),
            'img' => get_template_directory_uri() . '/images/plugins/shortcode.jpg',
            'source' => get_template_directory() . '/plugins/rubik-shortcode.zip',
            'required' => false,
            'version' => '1.2',
            'external_url' => '',
        ),  
        array(
            'name' => esc_html__('Taxonomy Meta', 'rubik'),
            'slug' => esc_html__('taxonomy-meta', 'rubik'),
            'img' => get_template_directory_uri() . '/images/plugins/taxonomy.jpg',
            'required' => false,
        ),
        array(
            'name' => esc_html__('MailChimp for WordPress', 'rubik'),
            'slug' => 'mailchimp-for-wp',
            'img' => get_template_directory_uri() . '/images/plugins/mailchimp.jpg',
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Contact Form 7', 'rubik'),
            'slug' => 'contact-form-7',
            'title' => esc_html__('Contact Form 7', 'rubik'),
            'img' => get_template_directory_uri() . '/images/plugins/contact-form-7.jpg',
            'required' => false,
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
                'name' => esc_html__('Classic Editor', 'rubik'),
                'slug' => 'classic-editor',
                'title' => esc_html__('Classic Editor - Optional', 'rubik'),
                'force_activation' => false,
                'force_deactivation' => false,
                'external_url' => '',
            ),
    );
     
    // Configuration of TGM
    $config = array(
        'domain'           => 'rubik',
        'default_path'     => '',
        'menu'             => 'install-required-plugins',
        'has_notices'      => true,
        'is_automatic'     => true,
        'message'          => '',
        'strings'          => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'rubik' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'rubik' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'rubik' ),
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'rubik' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'rubik' ),
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'rubik' ),
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'rubik' ),
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'rubik' ),
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'rubik' ),
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'rubik' ),
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'rubik' ),
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'rubik' ),
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'rubik' ),
            'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'rubik' ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'rubik' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'rubik' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'rubik' ),
            'nag_type'                        => 'updated'
        )
    );
    tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'rubik_tgmpa_register_toolkit' );

$rubik_justified_ids = array();
$rubik_ajax_c = array();
$rubik_ajax_btnstr = array();
$rubik_ajax_btnstr['loadmore'] = esc_html__('Load More', 'rubik');
$rubik_ajax_btnstr['nomore'] = esc_html__('No More Posts', 'rubik');
$rubik_dynamic_css = array();
/**
 * http://codex.wordpress.org/Content_Width
 */
if ( ! isset($content_width)) {
	$content_width = 1200;
}

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function rubik_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'rubik_pingback_header' );

/**-------------------------------------------------------------------------------------------------------------------------
 * register ajax
 */
if ( ! function_exists( 'rubik_enqueue_ajax_url' ) ) {
	function rubik_enqueue_ajax_url() {
        echo '<script>var ajaxurl = "' . esc_url(admin_url( 'admin-ajax.php' )) . '"</script>';
	}

	add_action( 'wp_enqueue_scripts', 'rubik_enqueue_ajax_url' );
    add_action( 'admin_enqueue_scripts', 'rubik_enqueue_ajax_url' );
}

if ( ! function_exists( 'rubik_scripts_method' ) ) {
    function rubik_scripts_method() {
        $rubik_option = rubik_core::bk_get_global_var('rubik_option');
        $rubik_container_w = isset($rubik_option['bk-site-container-width']) ? $rubik_option['bk-site-container-width'] : 1110;
        
        wp_enqueue_script('jquery-ui-widget');
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-accordion');
        wp_enqueue_script('imagesLoaded');
        wp_enqueue_script('jquery-masonry', array( 'imagesLoaded' ),'', true);
        
        wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/framework/bootstrap/css/bootstrap.css', array(), '' );        

        wp_enqueue_style('fa', get_template_directory_uri() . '/css/fonts/awesome-fonts/css/font-awesome.min.css');
        
        wp_enqueue_style('rubik-external', get_template_directory_uri() . '/css/external.css');
        
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        } 
        
        wp_enqueue_style('rubik-style', get_template_directory_uri() . '/css/bkstyle.css');
        if($rubik_container_w == 1110) {
            wp_enqueue_style('rubik-1110', get_template_directory_uri() . '/css/css_w/css_1110.css');
        }else if($rubik_container_w == 1140) {
            wp_enqueue_style('rubik-1140', get_template_directory_uri() . '/css/css_w/css_1140.css');
        }else if($rubik_container_w == 1170) {
            wp_enqueue_style('rubik-1170', get_template_directory_uri() . '/css/css_w/css_1170.css');
        }else if($rubik_container_w == 1200) {
            wp_enqueue_style('rubik-1200', get_template_directory_uri() . '/css/css_w/css_1200.css');
        }else {
            wp_enqueue_style('rubik-1110', get_template_directory_uri() . '/css/css_w/css_1110.css');
        }
        wp_enqueue_style('rubik-responsive', get_template_directory_uri() . '/css/responsive.css');
        
        wp_enqueue_script( 'cookie',  get_template_directory_uri() . '/js/cookie.min.js', array( 'jquery' ),'', true);
        wp_enqueue_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'),'', true);
        wp_enqueue_script('froogaloop2', get_template_directory_uri() . '/js/froogaloop2.min.js', array('jquery'),'', true);
        wp_enqueue_script( 'modernizr', get_template_directory_uri().'/js/modernizr.js', array( 'jquery' ), false, true );
        wp_enqueue_script( 'justifiedGallery', get_template_directory_uri().'/js/justifiedGallery.js', array( 'jquery' ), false, true );
        wp_enqueue_script( 'justifiedlightbox', get_template_directory_uri().'/js/jquery.magnific-popup.min.js', array( 'jquery' ), false, true );
        wp_enqueue_script( 'fitvids',get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'),false,true );
        wp_enqueue_script( 'tipper', get_template_directory_uri().'/js/jquery.fs.tipper.js', array( 'jquery' ), false, true );        
          
        wp_enqueue_script( 'Slick', get_template_directory_uri().'/js/slick.js', array( 'jquery' ), false, true );   
        
        wp_enqueue_script( 'newsticker',  get_template_directory_uri() . '/js/jquery-news-ticker.js', array( 'jquery' ),'', true);
        
        if((isset($rubik_option['bk-footer-instagram-access-token']) && ($rubik_option['bk-footer-instagram-access-token'] != '')) || is_active_widget('','','rubik_instagram')) {
            wp_enqueue_script('gridrotator', get_template_directory_uri() . '/js/jquery.gridrotator.js', array( 'jquery' ), '', true);
        }
        
        wp_enqueue_script( 'rubik-onviewport', get_template_directory_uri().'/js/onviewport.js', array( 'jquery' ), false, true );
        wp_enqueue_script('rubik-module-load-post', get_template_directory_uri() . '/js/module-load-post.js', array('jquery'),false,true); 
        
        wp_enqueue_script( 'rubik-menu', get_template_directory_uri().'/js/menu.js', array( 'jquery' ), false, true );
        
        wp_enqueue_script( 'rubik-playlist', get_template_directory_uri().'/js/playlist.js', array( 'jquery' ), false, true );
        
        wp_enqueue_script( 'rubik-customjs', get_template_directory_uri().'/js/customjs.js', array( 'jquery' ), false, true );             
     }
}

add_action('wp_enqueue_scripts', 'rubik_scripts_method');

if ( ! function_exists( 'rubik_post_admin_scripts_and_styles' ) ) {
    function rubik_post_admin_scripts_and_styles($hook) {        
    	if( $hook == 'post.php' || $hook == 'post-new.php' ) {
            wp_enqueue_script( 'bootstrap-admin', get_template_directory_uri().'/framework/bootstrap-admin/bootstrap.js', array(), '', true );
            wp_enqueue_style( 'bootstrap-admin', get_template_directory_uri().'/framework/bootstrap-admin/bootstrap.css', array(), '' );
            wp_register_script( 'rubik-post-review-admin',  get_template_directory_uri() . '/js/post-review-admin.js', array(), '', true);
            wp_enqueue_script( 'rubik-post-review-admin' ); // enqueue it
   		}
        
        wp_enqueue_script( 'colorpickerjs', get_template_directory_uri().'/js/bootstrap-colorpicker.min.js', array(), '', true );
        
        wp_enqueue_style( 'colorpicker', get_template_directory_uri().'/css/bootstrap-colorpicker.min.css', array(), '' );
        
        wp_enqueue_style( 'rubik-admin', get_template_directory_uri().'/css/admin.css', array(), '' );
        
        add_editor_style('css/editorstyle.css');
        
        wp_enqueue_style('fa-admin', get_template_directory_uri() . '/css/fonts/awesome-fonts/css/font-awesome.min.css');
        
        wp_enqueue_style( 'rwmb-group', get_template_directory_uri() .'/framework/meta-box-group/group.css');                
        
        wp_enqueue_script( 'rubik-autosize', get_template_directory_uri().'/js/jquery.autosize.min.js', array(), '', true );
        
        wp_enqueue_script( 'rubik-admin', get_template_directory_uri().'/js/admin.js', array('jquery-ui-sortable'), '', true );

        
    }
}
add_action('admin_enqueue_scripts', 'rubik_post_admin_scripts_and_styles');
 if ( ! function_exists( 'rubik_page_builder_script' ) ) {
    function rubik_page_builder_script($hook) {
        if( $hook == 'post.php' || $hook == 'post-new.php' ) {
            wp_enqueue_script( 'rubik-page-builder-script', get_template_directory_uri().'/framework/page-builder/controller/js/page-builder.js', array( 'jquery' ), null, true );
            wp_localize_script( 'rubik-page-builder-script', 'bkpb_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
  		}
    }
 }
 add_action('admin_enqueue_scripts', 'rubik_page_builder_script', 9);

/**
 * Add php library file.
 */
require_once (get_template_directory().'/library/core.php');
require_once (get_template_directory().'/library/mega_menu.php');
require_once (get_template_directory().'/library/custom_css.php');
require_once (get_template_directory().'/library/translation.php');
require_once (get_template_directory().'/inc/controller.php');


/**
 * Integrate Meta box plugin
 */
require_once (get_template_directory() . '/framework/meta-box-group/meta-box-group.php');
require_once (get_template_directory() . '/library/meta_box_config.php');

add_theme_support('title-tag');

/**
 * Add support for the featured images (also known as post thumbnails).
 */
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
}

add_action( 'after_setup_theme', 'rubik_thumbnail_setup' );
if ( ! function_exists( 'rubik_thumbnail_setup' ) ){

    function rubik_thumbnail_setup() {
        add_image_size( 'rubik-1000-540', 1000, 540, true );
        add_image_size( 'rubik-900-613', 905, 613, true );
        add_image_size( 'rubik-600-315', 600, 315, true );
        add_image_size( 'rubik-310-280', 310, 280, true );
        add_image_size( 'rubik-620-420', 620, 420, true );
        add_image_size( 'rubik-450-380', 450, 380, true );        
        add_image_size( 'rubik-730-400', 730, 400, true ); 
        add_image_size( 'rubik-360-240', 360, 240, true );
        add_image_size( 'rubik-360-180', 360, 180, true );
        add_image_size( 'rubik-210-140', 210, 140, true );
        add_image_size( 'rubik-225-110', 225, 110, true );
        add_image_size( 'rubik-150-105', 150, 105, true );
        add_image_size( 'rubik-90-65', 90, 65, true );
        add_image_size( 'rubik-masonry-size', 400, 99999, false );
    }
}
/**
 * Post Format 
 */
 add_action('after_setup_theme', 'rubik_add_theme_format', 11);
function rubik_add_theme_format() {
    add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio' ) );
}
/**
 * Add support for the featured images (also known as post thumbnails).
 */
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
}

/**
 * Register menu locations
 *---------------------------------------------------
 */
if ( ! function_exists( 'rubik_register_menu' ) ) {
    function rubik_register_menu() {
        register_nav_menu('main-menu', esc_html__( 'Main Menu', 'rubik' ));
        register_nav_menu('menu-top', esc_html__( 'Top Menu', 'rubik' ));
        register_nav_menu('canvas-menu', esc_html__( 'Offcanvas Menu', 'rubik' ));
        register_nav_menu('menu-footer', esc_html__( 'Footer Menu', 'rubik' )); 
        register_nav_menu('menu-footer-lower', esc_html__( 'Footer Lower Menu', 'rubik' )); 
    }
}
add_action( 'init', 'rubik_register_menu' );

function rubik_category_nav_class( $classes, $item ){
    if (isset($item->bkmegamenu) && isset($item->bkmegamenu[0]) && ($item->bkmegamenu[0])) {
        $classes[] = 'menu-category-megamenu';
    }
    
    if(isset($item->object) && ( 'category' == $item->object )){
        $classes[] = 'menu-category-' . $item->object_id;
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'rubik_category_nav_class', 10, 4 );

function rubik_custom_excerpt_length( $length ) {
	return 100;
}
add_filter( 'excerpt_length', 'rubik_custom_excerpt_length', 999 );

/**
 * Limit number of tags in widget tag cloud
 */
if ( !function_exists('rubik_tag_widget_limit') ) {
  function rubik_tag_widget_limit($args){

    //Check if taxonomy option inside widget is set to tags
    if(isset($args['taxonomy']) && $args['taxonomy'] == 'post_tag'){
      $args['number'] = 16; //Limit number of tags
      $args['smallest'] = 14; //Size of lowest count tags
      $args['largest'] = 14; //Size of largest count tags
      $args['unit'] = 'px'; //Unit of font size
      $args['orderby'] = 'count'; //Order by counts
      $args['order'] = 'DESC';
    }

    return $args;
  }
}
add_filter('widget_tag_cloud_args', 'rubik_tag_widget_limit');

/**
 * ReduxFramework
 */
if ( !isset( $rubik_option ) && file_exists( get_template_directory() . '/library/theme-option.php' ) ) {
    require_once( get_template_directory() . '/library/theme-option.php' );
}
/**
 * Register sidebars and widgetized areas.
 *---------------------------------------------------
 */
 if ( ! function_exists( 'rubik_widgets_init' ) ) {
    function rubik_widgets_init() {
        $rubik_option = rubik_core::bk_get_global_var('rubik_option');
        
        $sidebarSetupStatus = get_option( 'rubik_sidebar_setup');
        
        
        register_sidebar( array(
    		'name' => esc_html__('Home Sidebar', 'rubik'),
    		'id' => 'home_sidebar',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Second Home Sidebar', 'rubik'),
    		'id' => 'second_home_sidebar',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Single Sidebar', 'rubik'),
    		'id' => 'single_sidebar',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Footer Sidebar 1', 'rubik'),
    		'id' => 'footer_sidebar_1',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        
        register_sidebar( array(
    		'name' => esc_html__('Footer Sidebar 2', 'rubik'),
    		'id' => 'footer_sidebar_2',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        
        register_sidebar( array(
    		'name' => esc_html__('Footer Sidebar 3', 'rubik'),
    		'id' => 'footer_sidebar_3',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Footer 40 30 30 - 1', 'rubik'),
    		'id' => 'footer_d3_1',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        
        register_sidebar( array(
    		'name' => esc_html__('Footer 40 30 30 - 2', 'rubik'),
    		'id' => 'footer_d3_2',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        
        register_sidebar( array(
    		'name' => esc_html__('Footer 40 30 30 - 3', 'rubik'),
    		'id' => 'footer_d3_3',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Footer 60 40 - 1', 'rubik'),
    		'id' => 'footer_d2_1',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        
        register_sidebar( array(
    		'name' => esc_html__('Footer 60 40 - 2', 'rubik'),
    		'id' => 'footer_d2_2',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
        register_sidebar( array(
    		'name' => esc_html__('Footer Full Width', 'rubik'),
    		'id' => 'footer_d1',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="bk-header"><div class="widget-title hide"><h3>',
    		'after_title' => '</h3></div></div>',
    	) );
    }
}
add_action( 'widgets_init', 'rubik_widgets_init' );