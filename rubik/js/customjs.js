(function($) {
  "use strict";
    $=jQuery;
    jQuery(document).ready(function($){
        $('h4.title').css('opacity','1');
        $('.module-title').removeClass('hide');
        $('.widget-title').removeClass('hide');
        $('.bk-shortcode').removeClass('hide');
        
        /** Menu Overflow **/
        $(".main-menu li").on('mouseenter mouseleave', function (e) {
            if ($('ul', this).length) {
                var elm = $('ul:first', this);
                var off = elm.offset();
                var l = off.left;
                var w = elm.width();
                var docW = $("#page-wrap").width();
                var isEntirelyVisible = (l + w <= docW);
    
                if (!isEntirelyVisible) {
                    $(this).addClass('atbs-submenu-to-left');
                } else {
                    $(this).removeClass('atbs-submenu-to-left');
                }
                
                if(l<0) {
                    $(this).addClass('atbs-submenu-to-right');
                } else {
                    $(this).removeClass('atbs-submenu-to-right');
                }
            }
        }); 
        /** Ticker **/
        $('#'+ajax_c['tickerid']).ticker({
            titleText: ajax_c['tickertitle'],
            direction: 'ltr',
            controls: true,
        });
        setTimeout(function() {
            var tk_titleW = $('.ticker-title').find('span').width();
            $('.ticker-title').css('width', tk_titleW); 
            $('.bk-ticker-wrapper').css('opacity', '1');  
        },1000);
        
        /** Dynamic Tabs **/
        bkArrangeModuleTabs();
        $('.bk-tabs-wrapper').removeClass('bk-hide-tabs');
        
        $(window).resize(function(){
            bkArrangeModuleTabs();
        });
        
        $('.bk-links-remember').on('click', function(e) {
            e.preventDefault();
            $(this).parents('.bk-form-wrapper').hide();
            $(this).parents('.bk-form-wrapper').siblings('.bk-remember-form-wrapper').fadeIn(500);
        });
        $('.bk-back-login').on('click', function(e) {
            e.preventDefault();
            if ($(this).parents('.bk-form-wrapper').hasClass('bk-remember-form-wrapper')) {
                $(this).parents('.bk-form-wrapper').siblings('.bk-register-form-wrapper').hide();
            }else if ($(this).parents('.bk-form-wrapper').hasClass('bk-register-form-wrapper')) {
                $(this).parents('.bk-form-wrapper').siblings('.bk-remember-form-wrapper').hide();  
            }
            $(this).parents('.bk-form-wrapper').hide();
            $(this).parents('.bk-form-wrapper').siblings('.bk-login-form-wrapper').fadeIn(500);
        });
        
        $('.bk-links-register-inline').on('click', function(e) {
            e.preventDefault();
            $(this).parents('.bk-form-wrapper').hide();
            $(this).parents('.bk-form-wrapper').siblings('.bk-register-form-wrapper').fadeIn(500);
        });
        
        $(".price_slider_amount").css("opacity", "1");
        $("#main-canvas-menu").css("display", "block");    
        $('#canvas-menu ul li.menu-item-has-children').prepend('<div class="expand"><i class="fa fa-chevron-down"></i></div>');
        $('.expand').on('click', function() {
            $(this).siblings('.sub-menu').toggle(300); 
        });
    // display breaking module
        $('.module-breaking-carousel').removeClass('hide');
        $('.module-carousel').removeClass('hide');
        $('.single .article-content').fitVids();
        $('.sidebar .rubik-video-wrap').fitVids();
        $('.video-wrap-sc').fitVids();
        var bkWindowWidth = $(window).width(),
            bkWindowHeight = $(window).height();
            
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
    
    // Breaking margin
        if($('#page-content-wrap').children('.bksection:first-child').find('.bkmodule:first-child').hasClass('module-breaking-carousel') == true) {
            $('#page-content-wrap').css('margin-top', '0px');
        }
        $('.img-popup-link').magnificPopup({
    		type: 'image',
            removalDelay: 300,
            mainClass: 'mfp-fade',
    		closeOnContentClick: true,
    		closeBtnInside: false,
    		fixedContentPos: true,		
    		image: {
    			verticalFit: true
    		}
    	});
        $('.video-popup-link').magnificPopup({
    		closeBtnInside: false,
    		fixedContentPos: true,
    		disableOn: 700,
    		type: 'iframe',
            removalDelay: 300,
            mainClass: 'mfp-fade',
    		preloader: false,
    	});
        if (typeof justified_ids !== 'undefined') {
            $.each( justified_ids, function( index, justified_id ) {
            	$(".justifiedgall_"+justified_id).justifiedGallery({
            		rowHeight: 200,
            		fixedHeight: false,
            		lastRow: 'justify',
            		margins: 4,
            		randomize: false,
                    sizeRangeSuffixes: {lt100:"",lt240:"",lt320:"",lt500:"",lt640:"",lt1024:""},
            	});
            });        
        }
        $('.zoom-gallery').each(function() { // the containers for all your galleries
            $(this).magnificPopup({
        		delegate: 'a.zoomer',
        		type: 'image',
        		closeOnContentClick: false,
        		closeBtnInside: false,
        		mainClass: 'mfp-with-zoom mfp-img-mobile',
        		image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        			verticalFit: true,
        			titleSrc: function(item) {
        				return ' <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">'+ item.el.attr('title') + '</a>';
        			}
        		},
        		gallery: {
        			enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1]
        		},
                zoom: {
        			enabled: true,
        			duration: 600, // duration of the effect, in milliseconds
                    easing: 'ease', // CSS transition easing function
        			opener: function(element) {
        				return element.find('img');
        			}
        		}
        	});	
    	}); 
         $('#bk-gallery-slider').each(function() { // the containers for all your galleries
            $(this).magnificPopup({
        		delegate: 'a.zoomer',
        		type: 'image',
        		closeOnContentClick: false,
        		closeBtnInside: false,
                removalDelay: 300,
        		//mainClass: 'mfp-with-zoom mfp-img-mobile',
                mainClass: 'mfp-fade',
        		image: {
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        			verticalFit: true,
        			titleSrc: function(item) {
        				return ' <a class="image-source-link" href="'+item.src+'" target="_blank">'+ item.el.attr('title') + '</a>';
        			}
        		},
        		gallery: {
        			enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1]
        		}
        	});	
    	});  
    // tiny helper function to add breakpoints
        function getGridSize() {
            return  (window.innerWidth < 500)  ? 1 :
                    (window.innerWidth < 1000) ? 2 :
                    (window.innerWidth < 1170) ? 3 : 4;
        }
        $(window).resize(function() {
            if (typeof flexslider !== 'undefined') {
                var gridSize = getGridSize();
                flexslider.vars.minItems = gridSize;
                flexslider.vars.maxItems = gridSize;
            }
        });
        var prev_arrow = '<div class="bk-slider-prev bk-control-nav"><i class="fa fa-angle-left"></i></div>';
        var next_arrow = '<div class="bk-slider-next bk-control-nav"><i class="fa fa-angle-right"></i></div>';
        $('.featured-slider-runner').slick({
            speed: 500,
            centerMode: true,
            infinite: true,
            centerPadding: '220px',
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            prevArrow: prev_arrow,
            nextArrow: next_arrow,
            responsive:[
                {
                    breakpoint:1200,
                    settings:{
                            centerMode:!0,
                            centerPadding:"145px",
                            slidesToShow:1
                    }
                },
                {
                    breakpoint:992,
                    settings:{
                        centerMode:!0,
                        centerPadding:"90px",
                        slidesToShow:1
                    }
                },
                {
                    breakpoint:768,
                    settings:{
                        centerMode:!0,
                        centerPadding:"0",
                        slidesToShow:1
                    }                        
                },
                {
                    breakpoint:510,
                    settings:{
                        centerMode:!0,
                        centerPadding:"0",
                        slidesToShow:1                        
                    }                        
                },                
            ]
        });
        $('.module-featured-slider').imagesLoaded(function(){
            setTimeout(function() {
                $('.featured-slider-wrapper').siblings('.bk-preload').addClass('hide');
                $('.featured-slider-wrapper').removeClass('opacity-zero');
                $('.module-featured-slider').removeClass('background-preload');
                $('.module-featured-slider').find('.thumb').removeClass('hide-thumb');
            }, 300);
        });
    //FW Slider	
        $('.flexslider').imagesLoaded(function(){		    
            $('.bk-slider-module .flexslider').flexslider({
                animation: 'fade',
                controlNav: false,
                animationLoop: true,
                slideshow: true,
                pauseOnHover: true,
                slideshowSpeed: 10000,
                animationSpeed: 600,
                smoothHeight: true,
                directionNav: true,
                prevText: '',
                nextText: '',
            });
    //Feature 2 slider 
            $('.module-feature2 .flexslider').flexslider({
                animation: 'fade',
                controlNav: false,
                animationLoop: true,
                slideshow: true,
                pauseOnHover: true,
                slideshowSpeed: 15000,
                animationSpeed: 600,
                smoothHeight: true,
                directionNav: true,
                prevText: '',
                nextText: '',
            });
    //Widget Comment 
            $('.widget_comment').flexslider({
                animation: 'slide',
                controlNav: false,
                animationLoop: true,
                slideshow: true,
                pauseOnHover: true,
                slideshowSpeed: 10000,
                animationSpeed: 600,
                smoothHeight: true,
                directionNav: true,
                prevText: '',
                nextText: '',
            });
    // Twitter 
            $('.twitter-slider').flexslider({
                animation: 'slide',
                controlNav: false,
                animationLoop: true,
                slideshow: true,
                pauseOnHover: true,
                slideshowSpeed: 10000,
                animationSpeed: 600,
                smoothHeight: true,
                directionNav: true,
                prevText: '',
                nextText: '',
            });
    // Widget Slider 
            $('.widget_slider .flexslider').flexslider({
                animation: 'fade',
                controlNav: false,
                animationLoop: true,
                slideshow: true,
                pauseOnHover: true,
                slideshowSpeed: 8000,
                animationSpeed: 600,
                smoothHeight: true,
                directionNav: true,
                prevText: '',
                nextText: '',
            });
    // Gallery Slider
            $('#bk-gallery-slider').flexslider({
                animation: 'slide',
                pauseOnHover: true,
                slideshowSpeed: 5000,
                animationSpeed: 600,
                smoothHeight: true,
                directionNav: true,
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                prevText: '',
                nextText: '',
                sync: "#bk-gallery-carousel"
            }); 
            
            $('#bk-gallery-carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 105,
                itemMargin: 5,
                asNavFor: '#bk-gallery-slider'
              });
              
            if($('.product.flexslider ul li').length > 3) {
                $('.product.flexslider').flexslider({
                    animation: "slide",
                    animationLoop: false,
                    directionNav: true,
                    controlNav: false,
                    itemWidth: 50,
                    minItems: 1,
                    maxItems: 3,
                    prevText: '',
                    nextText: '',
                });
            }else {
                $('.product').removeClass('flexslider');
            }
        });
        waitForGallerySlider();
    // Breaking Slider 
        $('.module-breaking-carousel .bk-carousel-wrap').flexslider({
            animation: "slide",
            controlNav: false,
            itemWidth: 210,
            columnWidth: 1,
            pauseOnHover: true,
            move: 1,
            animationLoop: true,
            prevText: '',
            nextText: '',
            minItems: getGridSize(), // use function to pull in initial value
            maxItems: getGridSize(), // use function to pull in initial value
            start: function(slider){
                if (typeof flexslider !== 'undefined') {
                    flexslider = slider;
                }
            }
        });
    // Module carousel
        $('.carousel_2 .bk-carousel-wrap').flexslider({
            animation: "slide",
            controlNav: false,
            itemWidth: 320,
            columnWidth: 1,
            pauseOnHover: true,
            animationSpeed: 600,
            move: 1,
            animationLoop: true,
            prevText: '',
            nextText: '',
            minItems: 1, // use function to pull in initial value
            maxItems: 2, // use function to pull in initial value
        });
        $('.carousel_3 .bk-carousel-wrap').flexslider({
            animation: "slide",
            controlNav: false,
            itemWidth: 320,
            columnWidth: 1,
            pauseOnHover: true,
            animationSpeed: 600,
            move: 1,
            animationLoop: true,
            prevText: '',
            nextText: '',
            minItems: 1, // use function to pull in initial value
            maxItems: 3, // use function to pull in initial value
        });
        $('.carousel_4 .bk-carousel-wrap').flexslider({
            animation: "slide",
            controlNav: false,
            itemWidth: 260,
            columnWidth: 1,
            pauseOnHover: true,
            animationSpeed: 600,
            move: 1,
            animationLoop: true,
            prevText: '',
            nextText: '',
            minItems: 1, // use function to pull in initial value
            maxItems: 4, // use function to pull in initial value
        });
        $('.module-grid-carousel .bk-carousel-wrap').flexslider({
            animation: "slide",
            controlNav: false,
            itemWidth: 320,
            columnWidth: 1,
            pauseOnHover: true,
            animationSpeed: 600,
            move: 1,
            animationLoop: true,
            prevText: '',
            nextText: '',
            minItems: 1, // use function to pull in initial value
            maxItems: 2, // use function to pull in initial value
        });
    // Masonry Module Init
        $('#page-wrap').imagesLoaded(function(){
            setTimeout(function() {
                if($('.bk-masonry-content').find('.item').length > 2){
                    $('.bk-masonry-content').masonry({
                        itemSelector: '.item',
                        columnWidth: 1,
                        isAnimated: true,
                        isFitWidth: true,
                     });
                }
                $('.ajax-load-btn').addClass('active');
                $('.bk-masonry-content').find('.post-c-wrap').removeClass('sink');
                $('.bk-masonry-content').find('.post-category').removeClass('sink');
             },500);
        });
        
        $('.bk_share_expand_btn').on('click', function() {
            $(this).siblings('li').removeClass('hide');
            $(this).siblings('li').show();
            $(this).hide();
            
        });
        
        $('.bk_share_close_btn').on('click', function() {
            $(this).hide();
            $(this).siblings('.item-hide').hide();
            $(this).siblings('.bk_share_expand_btn').show();
        });        
        // Back top
    	$(window).scroll(function () {
    		if ($(this).scrollTop() > 500) {
    			$('#back-top').css('bottom','0');
    		} else {
    			$('#back-top').css('bottom','-34px');
    		}
    	});
        
    	// scroll body to top on click
    	$('#back-top').on('click', function() {
    		$('body,html').animate({
    			scrollTop: 0,
    		}, 1300);
    		return false;
    	});
        if ((typeof fixed_nav !== 'undefined') && (fixed_nav == 2)) {
            var stickyheader = $('.rubik-sticky-header');
            var headerHeight = $('.bk-page-header').outerHeight();
            var nav = $('nav.main-nav');
            var d = nav.offset().top;
            var lastScrollTop = 0;
            var lastScrollDown = 0;
            $(window).scroll(function () {
                if ($(this).scrollTop() > (d + 500)) {
                    var st = $(this).scrollTop();
                    if (st > lastScrollTop){
                       // downscroll code
                        stickyheader.removeClass('rubik-shown-sticky');
                        stickyheader.css('margin-top','0');
                        lastScrollDown = st;
                    } else {
                      // upscroll code
                        if(lastScrollTop < (lastScrollDown - 150)) {                      
                            stickyheader.addClass('rubik-shown-sticky');
                            var ad_bar = $('#wpadminbar');
                            if(ad_bar.length != 0) {
                                stickyheader.css('margin-top',ad_bar.height());
                            }                
                        }                        
                    }
                    lastScrollTop = st;
               }else {
                    stickyheader.removeClass('rubik-shown-sticky');
                    stickyheader.css('margin-top','0');
                    stickyheader.find('.ajax-search-result').hide();
                }
            });
        }
        // Single Parallax
        var bkParallaxWrap = $('#bk-parallax-feat'),
            bkParallaxFeatImg = bkParallaxWrap.find('.s-feat-img');
        if ( bkParallaxFeatImg.length !== 0 ) {
            $(window).scroll(function() {
            var bkBgy_p = -( ($(window).scrollTop()) / 3.5),
                bkBgPos = '50% ' + bkBgy_p + 'px';
            
            bkParallaxFeatImg.css( "background-position", bkBgPos );
            
            });
        }
        //Rating canvas
        var canvasArray  = $('.rating-canvas');
        $.each(canvasArray, function(i,canvas){
            var percent = $(canvas).data('rating');
            var ctx     = canvas.getContext('2d');
    
            canvas.width  = $(canvas).parent().width();
            canvas.height = $(canvas).parent().height();
    
            var x = (canvas.width) / 2;
            var y = (canvas.height) / 2;
            if ($(canvas).parents().hasClass('rating-wrap')) {
                var radius = (canvas.width - 6) / 2;
                var lineWidth = 2;
            } else {
                var radius = (canvas.width - 10) / 2;
                var lineWidth = 4;
            }
                    
            var endAngle = (Math.PI * percent * 2 / 100);
            ctx.beginPath();
            ctx.arc(x, y, radius, -(Math.PI/180*90), endAngle - (Math.PI/180*90), false);   
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = "#fff";
            ctx.stroke();  
        });
        $(".bk-tipper-bottom").tipper({
            direction: "bottom"
        });
    /* Sidebar stick */    
        var win, tick, curscroll, nextscroll; 
        win = $(window);
        var width = $('.sidebar-wrap').width();
        tick = function() {
            nextscroll = win.scrollTop();
            $(".sidebar-wrap.stick").each(function(){
                var bottom_compare, top_compare, screen_scroll, parent_top, parent_h, parent_bottom, scroll_status = 0, topab; 
                var sbID = "#"+$(this).attr(("id"));
                bottom_compare = $(sbID).offset().top + $(sbID).outerHeight(true);
                screen_scroll = win.scrollTop() + win.height();
                parent_top = $(sbID).parents('.bksection').offset().top;
                parent_h = $(sbID).parents('.bksection').height();
                parent_bottom = parent_top + parent_h;
                if($(sbID).parents('.bksection').hasClass('bk-in-single-page')) {
                    topab =  parent_h - $(sbID).outerHeight(true) - 36;
                }else {
                    topab =  parent_h - $(sbID).outerHeight(true);                            
                }
                
                if(window.innerWidth > 991) {
                    if(parent_h > $(sbID).outerHeight(true)) {
                        if(win.scrollTop() < parent_top) {
                            scroll_status = 0;
                        }else if((win.scrollTop() >= parent_top) && (screen_scroll < parent_bottom)) {
                            if(curscroll <= nextscroll) {
                                scroll_status = 1;
                            }else if(curscroll > nextscroll) {
                                scroll_status = 3;
                            }
                        }else if(screen_scroll >= parent_bottom) {
                            scroll_status = 2;
                        } 
                        if(scroll_status == 0) {
                            $(sbID).css({
                                "position"  : "static",
                                "top"       : "auto",
                                "bottom"    : "auto"
                            });
                        }else if (scroll_status == 1) {
                            if(win.height() > $(sbID).outerHeight(true)) {
                                var ad_bar = $('#wpadminbar');
                                if ((typeof fixed_nav !== 'undefined') && (fixed_nav == 2)) {
                                    if(ad_bar.length != 0) {
                                        var sb_height_fixed = 16 + ad_bar.height() + $('.main-nav').height() + 'px';
                                    }
                                    else {
                                        var sb_height_fixed = 16 + $('.main-nav').height() + 'px';
                                    }

                                }else {
                                    if(ad_bar.length != 0) {
                                        var sb_height_fixed = 16 + ad_bar.height() + 'px';
                                    }else {
                                        var sb_height_fixed = 16 + 'px';
                                    }
                                }
                                $(sbID).css({
                                    "position"  : "fixed",
                                    "top"       : sb_height_fixed,
                                    "bottom"    : "auto",
                                    "width"     : width
                                });
                            }else {
                                if (screen_scroll < bottom_compare) {
                                    if($(sbID).parents('.bksection').hasClass('bk-in-single-page')) {
                                        topab = $(sbID).offset().top - parent_top - 36;  
                                    }else {
                                        topab = $(sbID).offset().top - parent_top;                            
                                    }
                                    $(sbID).css({
                                        "position"  : "absolute",
                                        "top"       : topab,
                                        "bottom"    : "auto",
                                        "width"     : width
                                    });
                                }else {
                                    $(sbID).css({
                                        "position"  : "fixed",
                                        "top"       : "auto",
                                        "bottom"    : "16px",
                                        "width"     : width
                                    });
                                }
                            }
                        }else if (scroll_status == 3) {
                            if (win.scrollTop() > ($(sbID).offset().top)) {
                                if($(sbID).parents('.bksection').hasClass('bk-in-single-page')) {
                                    topab = $(sbID).offset().top - parent_top - 36;  
                                }else {
                                    topab = $(sbID).offset().top - parent_top;                            
                                }
                                $(sbID).css({
                                    "position"  : "absolute",
                                    "top"       : topab,
                                    "bottom"    : "auto",
                                    "width"     : width
                                });
                            }else {
                                var ad_bar = $('#wpadminbar');
                                if ((typeof fixed_nav !== 'undefined') && (fixed_nav == 2)) {
                                    if(ad_bar.length != 0) {
                                        var sb_height_fixed = 16 + ad_bar.height() + $('.main-nav').height() + 'px';
                                    }
                                    else {
                                        var sb_height_fixed = 16 + $('.main-nav').height() + 'px';
                                    }

                                }else {
                                    if(ad_bar.length != 0) {
                                        var sb_height_fixed = 16 + ad_bar.height() + 'px';
                                    }else {
                                        var sb_height_fixed = 16 + 'px';
                                    }
                                }
                                $(sbID).css({
                                    "position"  : "fixed",
                                    "top"       : sb_height_fixed,
                                    "bottom"    : "auto",
                                    "width"     : width
                                });
                            }
                        }else if(scroll_status == 2) {
                            if(win.height() > $(sbID).outerHeight(true)) {
                                var status2_inner = 0;
                                if(curscroll <= nextscroll) {
                                    status2_inner = 1;
                                }else if(curscroll > nextscroll) {
                                    status2_inner = 2;
                                }
                                if(((status2_inner == 1) && (bottom_compare < parent_bottom)) || ((status2_inner == 2) && (win.scrollTop() < $(sbID).offset().top))){
                                    var ad_bar = $('#wpadminbar');
                                    if ((typeof fixed_nav !== 'undefined') && (fixed_nav == 2)) {
                                        if(ad_bar.length != 0) {
                                            var sb_height_fixed = 16 + ad_bar.height() + $('.main-nav').height() + 'px';
                                        }
                                        else {
                                            var sb_height_fixed = 16 + $('.main-nav').height() + 'px';
                                        }
    
                                    }else {
                                        if(ad_bar.length != 0) {
                                            var sb_height_fixed = 16 + ad_bar.height() + 'px';
                                        }else {
                                            var sb_height_fixed = 16 + 'px';
                                        }
                                    }
                                    $(sbID).css({
                                        "position"  : "fixed",
                                        "top"       : sb_height_fixed,
                                        "bottom"    : "auto",
                                        "width"     : width
                                    });
                                }else {
                                    $(sbID).css({
                                        "position"  : "absolute",
                                        "top"       : topab,
                                        "bottom"    : "auto",
                                        "width"     : width
                                    });
                                }
                            }else {
                                $(sbID).css({
                                    "position"  : "absolute",
                                    "top"       : topab,
                                    "bottom"    : "auto",
                                    "width"     : width
                                });
                            }
                        }      
                    }
                }      
                $(sbID).parent().css("height", $(sbID).height());
            });
            curscroll = nextscroll;
        }
        $(".sidebar-wrap.stick").each(function(){
            $(this).wrap("<div class='bk-sticksb-wrapper'></div>");
        });
        setTimeout(function() {
            win.on("scroll", tick);
        }, 2000);
        win.resize(function(){
            $(".sidebar-wrap.stick").each(function(){
                var sbID = "#"+$(this).attr(("id"));
                if(window.innerWidth > 991) {
                    if($(this).parent().hasClass('bk-sticksb-wrapper')){
                        width = $('.bk-sticksb-wrapper').width();
                        $(sbID).css({
                            "width"     : width
                        });
                    }
                }else {
                    $(sbID).css({
                        "position"  : "static",
                        "top"       : "auto",
                        "bottom"    : "auto"
                    });
                }  
            });
        });
        //Short Code 
        $('.module-shortcode').fitVids();
        $('.shortcode-widget-content').fitVids();
        $('.bk_accordions').each(function(){
            var accordions_id=$(this).attr('id');
            if(accordions_id){
                $('#'+accordions_id).accordion({
                    icons:{'header':'ui-icon-plus sprites','activeHeader':'ui-icon-minus sprites'},
                    collapsible:true
                });
            }
        });
        $('.bk_tabs').each(function(){
            var tabs_id=$(this).attr('id');
            if(tabs_id){
                $('#'+tabs_id).tabs();
            }
        });
            
        // Parallax
        // Single Parallax
        var bkscParallax = $('.bkparallaxsc');
        var bkscParallaxImg = new Array();
        $.each( bkscParallax, function( index, value ) {       
            bkscParallaxImg[index] = $(this).find('.parallaximage');
        });
        $(window).scroll(function() {
            $.each( bkscParallaxImg, function( index, value ) {       
                if ( bkscParallaxImg[index].length !== 0 ) {
                    var bkBgy_p = -( ($(window).scrollTop() - bkscParallaxImg[index].offset().top) / 3.5),
                        bkBgPos = '50% ' + bkBgy_p + 'px';
                    bkscParallaxImg[index].css( "background-position", bkBgPos );
                }
            });
        });
        
        var find_riGrid;
        find_riGrid = ($('body').find('.ri-grid'));
        if (find_riGrid.length > 0){
            $('.rubik-instagram-footer .ri-grid').gridrotator( {
                rows		: 1,
        		columns		: rubik_instagram_col[0],
        		animType	: 'random',
        		animSpeed	: 1000,
        		interval	: 3000,
                preventClick : false,
        		step		: 'random',
                w1400			: {
                	rows	: 1,
                	columns	: rubik_instagram_col[0]
                },
        		w1024			: {
                	rows	: 1,
                	columns	: 5
                },
                
                w768			: {
                	rows	: 1,
                	columns	: 4
                },
                
                w480			: {
                	rows	: 1,
                	columns	: 3
                },
                
                w320			: {
                	rows	: 1,
                	columns	: 2
                },
                
                w240			: {
                	rows	: 1,
                	columns	: 1
                },
        
        	} );
            var sidebarInstagramGrids = $('.sidebar ').find('.ri-grid');
            var instagram_cols = 3;
            var instagram_rows = 3;
            if($('.sidebar ').find('.ri-grid').hasClass('ri-grid-col-2')) {
                instagram_cols = 2;
            }else {
                instagram_cols = 3;
            }
            if($('.sidebar ').find('.ri-grid').hasClass('ri-grid-row-1')) {
                instagram_rows = 1;
            }else if($('.sidebar ').find('.ri-grid').hasClass('ri-grid-row-2')) {
                instagram_rows = 2;
            }else if($('.sidebar ').find('.ri-grid').hasClass('ri-grid-row-3')) {
                instagram_rows = 3;
            }else if($('.sidebar ').find('.ri-grid').hasClass('ri-grid-row-4')) {
                instagram_rows = 4;
            }else {
                instagram_rows = 3;
            }
            if(sidebarInstagramGrids.length > 0) {
                $.each(sidebarInstagramGrids, function() {
                    $(this).gridrotator( {
                        rows		: instagram_rows,
                		columns		: instagram_cols,
                		animType	: 'random',
                		animSpeed	: 1000,
                		interval	: 3000,
                        preventClick : false,
                		step		: 'random',
                        w1024		: {
                        	rows	: instagram_rows,
                        	columns	: instagram_cols
                        },
                        
                        w768			: {
                        	rows	: instagram_rows,
                        	columns	: instagram_cols
                        },
                        
                        w480			: {
                        	rows	: instagram_rows,
                        	columns	: instagram_cols
                        },
                        
                        w320			: {
                        	rows	: instagram_rows,
                        	columns	: 2
                        },
                        
                        w240			: {
                        	rows	: instagram_rows,
                        	columns	: 2
                        },
                	} );
                });
            }
        }
    });
    
    function bkgetFeatureImage() { 
    	var metas = document.getElementsByTagName('meta'); 
        var i;
    	for (i=0; i<metas.length; i++) { 
    	   if (metas[i].getAttribute("property") == "og:image") { 
    		  return metas[i].getAttribute("content"); 
    	   } else if (metas[i].getAttribute("property") == "image") { 
    		  return metas[i].getAttribute("content");  
    	   } else if (metas[i].getAttribute("property") == "twitter:image:src") { 
    		  return metas[i].getAttribute("content");  
    	   }
    	}
    
    	return "";
    }
    
    function waitForGallerySlider() {
     	// if slider is loaded
    	if ($("#bk-gallery-slider").children('div:first').hasClass('flex-viewport')) {
            $(".bk-gallery-item").each(function(){
                if($(this).hasClass('clone')){
                    $(this).children('a').removeClass('zoomer');
                }
            });
            return false;
    	}	
    	else
    	{
    		// Wait another 0,5 seconds
    		setTimeout(waitForGallerySlider, 500); 
    	}	
    }
    function bkArrangeModuleTabs () {
        var timeout;
        $('.module-title').each(function(){
            var moduleTitleWidth = $(this).outerWidth();
            var bkMainTitle = $(this).find('.bk-tab-original').outerWidth();
            var tabWrapper = $(this).find('.bk-tabs-wrapper').outerWidth();
            var last_menu_item = '';
            if ((moduleTitleWidth - (bkMainTitle + tabWrapper)) <  0) {
                if (! $(this).find('.bk-tabs-wrapper').children().hasClass('bk-tabs-dropdown')) {
                    $(this).find('.bk-tabs-wrapper').append(rubik_tabs_more);
                }
                
                $(this).find('.bk-tabs:last-child').appendTo($(this).find('.bk-tabs-pull-list'));
                
                moduleTitleWidth = $(this).outerWidth();
                tabWrapper = $(this).find('.bk-tabs-wrapper').outerWidth();
                
                timeout = 5;
                while(( moduleTitleWidth - (bkMainTitle + tabWrapper) < 0 ) && (timeout > 0)) {
                    $(this).find('.bk-tabs:last-child').appendTo($(this).find('.bk-tabs-pull-list'));
                    moduleTitleWidth = $(this).outerWidth();
                    tabWrapper = $(this).find('.bk-tabs-wrapper').outerWidth();
                    timeout = timeout - 1;
                }
            }else if (moduleTitleWidth - (bkMainTitle + tabWrapper) > 130) {
                
                if($(this).find('.bk-tabs-wrapper').children().hasClass('bk-tabs-dropdown')) {
                    if ($(this).find('.bk-tabs-pull-list').children().length > 0) {
                        $(this).find('.bk-tabs-pull-list').find('.bk-tabs:first-child').appendTo($(this).find('.bk-module-tabs'));
                        
                        moduleTitleWidth = $(this).outerWidth();
                        tabWrapper = $(this).find('.bk-tabs-wrapper').outerWidth();
                        
                        timeout = 5;
                        while((moduleTitleWidth - (bkMainTitle + tabWrapper) > 130) && (timeout > 0)) {
                            $(this).find('.bk-tabs-pull-list').find('.bk-tabs:first-child').appendTo($(this).find('.bk-module-tabs'));
                            moduleTitleWidth = $(this).outerWidth();
                            tabWrapper = $(this).find('.bk-tabs-wrapper').outerWidth();
                            timeout = timeout - 1;
                        }
                        if ($(this).find('.bk-tabs-pull-list').children().length == 0) {
                            $(this).find('.bk-tabs-dropdown').remove();
                        }
                    }else {
                        $(this).find('.bk-tabs-dropdown').remove();
                    }
                }
            }
        });
    }
})(jQuery);