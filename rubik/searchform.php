<form action="<?php echo esc_url(home_url('/')); ?>/" id="searchform" method="get">
    <div class="searchform-wrap">
        <input type="text" placeholder="<?php esc_attr_e('Search...', 'rubik'); ?>" name="s" id="s" />
    <div class="search-icon">
        <i class="fa fa-search"></i>
    </div>
    </div>
</form>