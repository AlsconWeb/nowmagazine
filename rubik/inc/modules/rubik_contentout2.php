<?php
if (!class_exists('bk_contentout2')) {
    class bk_contentout2 {
        
        function render($custom_var) {
            ob_start();
            $meta_ar = array('author', 'date');
            $meta2_ar = array('postview', 'postcomment');
            $show_meta = 1;
            if(isset($custom_var['meta_items']) && ($custom_var['meta_items'] == 'off')){
                $show_meta = 0;
            }else {
                $show_meta = 1;
            }
            ?>
            <?php if(rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {?>
            <div class="bk-mask">
                <?php echo rubik_core::bk_meta_cases('cat');?>
                <?php echo rubik_core::get_feature_image($custom_var['thumbnail'], true, $custom_var['post-icon']);?>
            </div>
            <?php }?>
            <div class="post-c-wrap">
                <?php echo rubik_core::bk_get_post_title(get_the_ID(), 15);?>
                <?php if($show_meta == 1) {?>
                    <?php if ($custom_var['except_length'] != 0) {?>
                        <div class="meta-wrap">
                            <?php echo rubik_core::bk_get_post_meta($meta_ar);?>
                            <?php echo rubik_core::bk_get_post_meta($meta2_ar);?>
                        </div>
                    <?php }else {?>
                        <div class="meta-wrap">
                            <?php echo rubik_core::bk_get_post_meta($meta_ar);?>
                        </div>
                    <?php }?>
                <?php }?>
                <?php if ($custom_var['except_length'] != 0) echo rubik_core::bk_get_post_excerpt($custom_var['except_length']);?>                
            </div>     
            
            <?php return ob_get_clean();
        }
        
    }
}