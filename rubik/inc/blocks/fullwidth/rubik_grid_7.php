<?php
if (!class_exists('rubik_grid_7')) {
    class rubik_grid_7 extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('grid_7-');
            $moduleAlign = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_align', true ); 
            
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $moduleHasbg = '';                                    
            $moduleHasbg = $rubik_dynamic_css[$uid]['moduleHasbg'];
            
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_grid_7'], $page_info);    //get block config
            
            $module_cfg['limit'] = 5;
            
            /** Tabs **/
            $rubik_tabs = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_categorytabs', true );
            
            $rubikTabsToArray = explode(",",$rubik_tabs);
            
            $tabs_amount = intval(count($rubikTabsToArray));
            
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['content'] = '';
            rubik_section_parent::$rubik_ajax_c[$uid]['post_icon'] = $bk_post_icon;
            
            if($tabs_amount > 0) {
                for ($i=0; $i< $tabs_amount; $i++) {
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['cat'] = $rubikTabsToArray[$i];
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['content'] = '';
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-grid-7 bk-grid-general '.$moduleAlign.' '.$moduleHasbg.' clearfix">';
            $block_str .= '<div class="container bkwrapper">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info, $rubikTabsToArray);  //render block title
            endif;
            
            $block_str .= '<div class="bk-grid-7-wrap bk-module-inner clearfix">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon);            //render modules
            $block_str .= '</div><!-- Close bk-grid-7-wrap -->';
            $block_str .= '</div>';
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        public function render_modules ($the_query, $bk_post_icon){
            $render_modules = '';
            $custom_var = array();
            $bk_contentin3 = new bk_contentin3;
            $bk_contentout8 = new bk_contentout8;
            $custom_var = array (
                'cat'           => 'on',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('author', 'date'),
                'thumbnail'     => 'rubik-620-420',
            );
            
            $custom_var_out8 = array (
                'cat'           => 'on',
                'thumbnail'     => 'rubik-210-140',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('date'),
            );
            if ( $the_query->have_posts() ) :            
                
                $render_modules .= '<ul class="grid-7-post-list">';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $bk_current_post = $the_query->current_post;
                    if($bk_current_post == 0) {
                        $bk_open_col = '<li class="grid7-col grid7-col-1 clearfix">';
                    }else if(($bk_current_post == 1) || ($bk_current_post == 2)) {
                        $bk_open_col = '<li class="grid7-col grid7-col-2 clearfix">';
                    }else if(($bk_current_post == 3) || ($bk_current_post == 4)) {
                        $bk_open_col = '<li class="grid7-col grid7-col-3 clearfix">';
                    }
                    $bk_close_col = '</li>';
                    if (($bk_current_post == 0)) {
                        $custom_var['cat'] = 'on';
                        $custom_var['meta'] = array('author', 'date');
                    }else {
                        $custom_var['cat'] = 'off';
                        $custom_var['meta'] = 'off';
                    }
                    if(($bk_current_post == 0) || ($bk_current_post == 1) || ($bk_current_post == 3)) {
                        $render_modules .= $bk_open_col;
                    } 
                    if($bk_current_post == 0) {
                        $render_modules .= '<div class="content_in">';
                        $render_modules .= '<div class="post-wrap">';
                        $render_modules .= $bk_contentin3->render($custom_var);
                        $render_modules .= '</div>';
                        $render_modules .= '</div><!-- End post -->';     
                    }else {
                        $render_modules .= '<div class="content_out clearfix bk-post-title-small">';
                        $render_modules .= $bk_contentout8->render($custom_var_out8);
                        $render_modules .= '</div><!-- End post -->';    
                    }
                    if(($bk_current_post == 0) || ($bk_current_post == 2) || ($bk_current_post == 4)) {
                        $render_modules .= $bk_close_col;
                    }  
                endwhile;
                $render_modules .= '</ul> <!-- End list-post -->';
                
            endif;
            return $render_modules;
        }
        
    }
}