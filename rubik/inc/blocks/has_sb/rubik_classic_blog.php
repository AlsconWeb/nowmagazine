<?php
if (!class_exists('rubik_classic_blog')) {
    class rubik_classic_blog extends rubik_section_parent  {
    
        public function render( $page_info ) {
            $block_str = '';
            global $rubik_dynamic_css;
            $uid = uniqid('classic_blog-');
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $cfg_ops = array();
            $bk_ajax_button = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_button', true );
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            $excerpt_length = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_excerpt_length', true );
            $moduleLayout = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_layout', true );
            $cfg_ops = $this->cfg_options(); 
            
    // prepare ajax vars 
            $ajax_load_number = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_load_number', true );
            
            parent::$rubik_ajax_c[$uid]['entries'] = $ajax_load_number;
            parent::$rubik_ajax_c[$uid]['sec'] = ''; 
            parent::$rubik_ajax_c[$uid]['offset'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_offset', true );
            parent::$rubik_ajax_c[$uid]['post_icon'] = $bk_post_icon;
    ///////////////////////
                    
            $module_cfg = bk_get_cfg::configs($cfg_ops['has_sb']['bk_classic_blog'], $page_info);    //get block config
            
            /** Tabs **/
            $rubik_tabs = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_categorytabs', true );
            
            $rubikTabsToArray = explode(",",$rubik_tabs);
            
            if ($module_cfg['category_id'] != 0) {
                $catTabArray = explode(",",$module_cfg['category_id']);
            }else {
                $catTabArray = 0;
            }
            $tabs_amount = intval(count($rubikTabsToArray));
            
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['cat'] = $catTabArray;
                
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['content'] = '';
            
            rubik_section_parent::$rubik_ajax_c[$uid]['post_icon'] = $bk_post_icon;
            rubik_section_parent::$rubik_ajax_c[$uid]['excerpt_length'] = $excerpt_length;
                
            if($tabs_amount > 0) {
                
                for ($i=0; $i< $tabs_amount; $i++) {
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['cat'] = $rubikTabsToArray[$i];
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['content'] = '';
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);           //get query
    
    // locallize ajax vars 
            //wp_localize_script( 'rubik-module-load-post', 'ajax_c', parent::$rubik_ajax_c );
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-classic-blog module-blog '.$moduleLayout.'">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info, $rubikTabsToArray);  //render block title
            endif;
            $block_str .= '<div class="bk-module-inner row clearfix">';
            $block_str .= '<ul class="bk-blog-content clearfix">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon, $excerpt_length);            //render modules
            $block_str .= '</ul></div>';
            //Loadmore button 
            if($bk_ajax_button !== 'disable') {
                $block_str .= '<div class="blog-ajax loadmore">';
                $block_str .= rubik_core::bk_get_load_more_buttom();
                $block_str .= '</div><!-- End Loadmore -->';
            }
            
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $bk_post_icon, $excerpt_length){
            $render_modules = '';
            $bk_contentout = new bk_contentout5;
            $custom_var = array (
                'thumbnail'     => 'rubik-310-280',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('author', 'date'),
                'except_length' => $excerpt_length,
                'rm_btn'        => true,
            );
            $bk_no_thumb = '';
            if ( $the_query->have_posts() ) :            
              
                while ( $the_query->have_posts() ): $the_query->the_post();
                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                        $bk_no_thumb = 'bk_no_thumb';
                    }else {
                        $bk_no_thumb = '';
                    }
                    $render_modules .= '<li class="item col-md-12">';
                    $render_modules .= '<div class="content_out clearfix '.$bk_no_thumb.'">';
                    $render_modules .= $bk_contentout->render($custom_var);
                    $render_modules .= '</div></li><!-- end post item -->';
                endwhile;
                
            endif;
            return $render_modules;
        }
    }
}