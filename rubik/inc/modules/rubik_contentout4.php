<?php
if (!class_exists('bk_contentout4')) {
    class bk_contentout4 {
        
        function render($custom_var) {
            ob_start();
            ?>
            <?php if(rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {?>
            <div class="bk-mask">
                <?php echo rubik_core::bk_meta_cases('cat');?>
                <?php echo rubik_core::get_feature_image($custom_var['thumbnail'], true, $custom_var['post-icon']);?>
            </div>
            <?php }?>
            <div class="post-c-wrap">
                <?php echo rubik_core::bk_get_post_title(get_the_ID(), 15);?>
                <?php echo rubik_core::bk_get_post_excerpt($custom_var['except_length']);?>
                <div class="meta-wrap">
                    <?php
                    if (isset($custom_var['meta']) && ($custom_var['meta'] != 'off')) :
                        echo rubik_core::bk_get_post_meta($custom_var['meta']);
                    endif;
                    ?>
                    <?php
                    if (isset($custom_var['rm_btn']) && ($custom_var['rm_btn'] != 'off')) :
                        echo rubik_core::bk_readmore_btn(get_the_ID());
                    endif;
                    ?>
                </div>
            </div>    
            <?php return ob_get_clean();
        }
        
    }
}