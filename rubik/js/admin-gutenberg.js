/* -----------------------------------------------------------------------------
 * Page Template Meta-box
 * -------------------------------------------------------------------------- */
;(function( $, window, document, undefined ){
	"use strict";
	
	$( document ).ready( function () {
        function selectAndScrollToOption(select) {
            var $select;
            var $selectedOptions
            $select = $(select);
            $select.each(function(index,el){
                // Store the currently selected options
                $selectedOptions = $(el).find("option[selected]");
                       
                // Re-select the original options
                $selectedOptions.prop("selected", true);
            });
        }
        var select = $('.bk-category-field');
        var selecttabs = $('.bk-categorytabs-field');

        selectAndScrollToOption(selecttabs);
        selectAndScrollToOption(select);
        
        /** Option Tabs **/
        $('.bk-module-tabs a').on('click', function(e) {
            e.preventDefault();
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            if($(this).hasClass('bk-tab-1')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-1').show();
            }else if($(this).hasClass('bk-tab-2')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-2').show();
            }else if($(this).hasClass('bk-tab-3')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-3').show();
            }else if($(this).hasClass('bk-tab-4')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-4').show();
            }else if($(this).hasClass('bk-tab-5')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-5').show();
            }
            
        });
        $("body").on('DOMSubtreeModified', "#bk-container", function() {
            $('.bk-module-tabs a').on('click', function(e) {
                e.preventDefault();
                $(this).addClass('active');
                $(this).siblings().removeClass('active');
                if($(this).hasClass('bk-tab-1')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-1').show();
                }else if($(this).hasClass('bk-tab-2')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-2').show();
                }else if($(this).hasClass('bk-tab-3')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-3').show();
                }else if($(this).hasClass('bk-tab-4')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-4').show();
                }else if($(this).hasClass('bk-tab-5')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-5').show();
                }
                
            });
        });

        $(function() {
            if ($('input[name=post_format]:checked', '#post-formats-select').val() == 0) {
                $("#bk_format_options").hide();
                $('.rwmb-image-select:nth-child(7)').css("display", "block");
                $('.rwmb-image-select:nth-child(5)').css("display", "block");
                $('.rwmb-image-select:nth-child(6)').css("display", "block");
            }else {
                var value = $('input[name=post_format]:checked', '#post-formats-select').val(); 
                $("#bk_format_options").show();
                if (value == "gallery"){
                    $("#bk_media_embed_code_post-description").parents(".rwmb-field").css("display", "none");
                    //$("#bk_media_video_gif_post-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_image_upload-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_gallery_content-description").parents(".rwmb-field").css("display", "block");
                    $("#bk_popup_frame_description").parents(".rwmb-field").css("display", "none");
                    $('.rwmb-image-select:nth-child(7)').css("display", "none");
                    $('.rwmb-image-select:nth-child(5)').css("display", "none");
                    $('.rwmb-image-select:nth-child(6)').css("display", "none");
                }else if ((value == "video")||(value == "audio")){
                    $("#bk_media_embed_code_post-description").parents(".rwmb-field").css("display", "block");
                    $("#bk_image_upload-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_gallery_content-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_popup_frame_description").parents(".rwmb-field").css("display", "block");
                    $('.rwmb-image-select:nth-child(7)').css("display", "none");
                    $('.rwmb-image-select:nth-child(5)').css("display", "none");
                    $('.rwmb-image-select:nth-child(6)').css("display", "none");
                    if(value == "video"){
                        //$("#bk_media_video_gif_post-description").parents(".rwmb-field").css("display", "block");
                    }else {
                        //$("#bk_media_video_gif_post-description").parents(".rwmb-field").css("display", "none");
                    }
                }else if (value == "image"){
                    $("#bk_media_embed_code_post-description").parents(".rwmb-field").css("display", "none");
                    //$("#bk_media_video_gif_post-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_image_upload-description").parents(".rwmb-field").css("display", "block");
                    $("#bk_gallery_content-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_popup_frame_description").parents(".rwmb-field").css("display", "none");
                    $('.rwmb-image-select:nth-child(7)').css("display", "block");
                    $('.rwmb-image-select:nth-child(5)').css("display", "block");
                    $('.rwmb-image-select:nth-child(6)').css("display", "block");
                }
            }
            $('#post-formats-select input').on('change', function() { 
                var value = $('input[name=post_format]:checked', '#post-formats-select').val(); 
                if (value == 0){
                    $("#bk_format_options").hide();
                    $('.rwmb-image-select:nth-child(7)').css("display", "block");
                    $('.rwmb-image-select:nth-child(5)').css("display", "block");
                    $('.rwmb-image-select:nth-child(6)').css("display", "block");
                }else {
                    $("#bk_format_options").show();
                } 
                if (value == "gallery"){
                    $("#bk_media_embed_code_post-description").parents(".rwmb-field").css("display", "none");
                    //$("#bk_media_video_gif_post-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_image_upload-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_gallery_content-description").parents(".rwmb-field").css("display", "block");
                    $("#bk_popup_frame_description").parents(".rwmb-field").css("display", "none");
                    $('.rwmb-image-select:nth-child(7)').css("display", "none");
                    $('.rwmb-image-select:nth-child(5)').css("display", "none");
                    $('.rwmb-image-select:nth-child(6)').css("display", "none");
                }else if ((value == "video")||(value == "audio")){
                    $("#bk_media_embed_code_post-description").parents(".rwmb-field").css("display", "block");
                    $("#bk_image_upload-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_gallery_content-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_popup_frame_description").parents(".rwmb-field").css("display", "block");
                    $('.rwmb-image-select:nth-child(7)').css("display", "none");
                    $('.rwmb-image-select:nth-child(5)').css("display", "none");
                    $('.rwmb-image-select:nth-child(6)').css("display", "none");
                    if(value == "video"){
                        //$("#bk_media_video_gif_post-description").parents(".rwmb-field").css("display", "block");
                    }else {
                        //$("#bk_media_video_gif_post-description").parents(".rwmb-field").css("display", "none");
                    }
                }else if (value == "image"){
                    $("#bk_media_embed_code_post-description").parents(".rwmb-field").css("display", "none");
                    //$("#bk_media_video_gif_post-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_image_upload-description").parents(".rwmb-field").css("display", "block");
                    $("#bk_gallery_content-description").parents(".rwmb-field").css("display", "none");
                    $("#bk_popup_frame_description").parents(".rwmb-field").css("display", "none");
                    $('.rwmb-image-select:nth-child(7)').css("display", "block");
                    $('.rwmb-image-select:nth-child(5)').css("display", "block");
                    $('.rwmb-image-select:nth-child(6)').css("display", "block");
                }
            });
        });

        // Pagebuilder Tabs
        
	} );
})( jQuery, window , document );
/* -----------------------------------------------------------------------------
 * UUID
 * https://github.com/eburtsev/jquery-uuid/blob/master/jquery-uuid.js
 * -------------------------------------------------------------------------- */
;(function( $, window, document, undefined ){
	$.uuid = function() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
			return v.toString(16);
		});
	};
})( jQuery, window , document );
