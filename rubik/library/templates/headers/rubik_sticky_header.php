<?php
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    $logo = array();
    if ((isset($rubik_option['bk-sticky-nav-logo'])) && (($rubik_option['bk-sticky-nav-logo']) != NULL)){ 
        $logo = $rubik_option['bk-sticky-nav-logo'];
        if (($logo != null) && (array_key_exists('url',$logo))) {
            if ($logo['url'] == '') {
                if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
                    $logo = $rubik_option['bk-logo'];
                }
            }
        }
    }else {
        if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
            $logo = $rubik_option['bk-logo'];
        }
    }
?> 
<div class="rubik-sticky-header header-wrap">
    <nav class="main-nav">
        <div class="main-nav-inner">
            <div class="main-nav-container clearfix">
                <div class="main-nav-wrap">
                    <div class="canvas-menu-wrap show-on-desktop">
                        <?php echo rubik_core::bk_canvas_button();?>
                    </div>
                    
                    <div class="header-logo-wrapper">
                        
                        <!-- logo open -->
                        <?php if (($logo != null) && (array_key_exists('url',$logo))) {
                                if ($logo['url'] != '') {
                            ?>
            			<div class="logo">
                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                <img src="<?php echo esc_url($logo['url']);?>" alt="<?php esc_attr_e( 'Logo', 'rubik' ); ?>"/>
                            </a>
            			</div>
            			<!-- logo close -->
                        <?php } else {?> 
                        <div class="logo logo-text">
                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                <?php echo esc_attr(bloginfo( 'name' ));?>
                            </a>
            			</div>
                        <?php }
                        } else {?> 
                        <div class="logo logo-text">
                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                <?php echo esc_attr(bloginfo( 'name' ));?>
                            </a>
            			</div>
                        <?php } ?>  
                    </div>  
                    
                    <?php if ( has_nav_menu( 'main-menu' ) ) { 
                        wp_nav_menu( array( 
                            'theme_location' => 'main-menu',
                            'container_class' => 'main-menu',
                            'walker' => new BK_Walker,
                            'depth' => '5' ) );}?>
                </div>
                <?php 
                    echo rubik_ajax_form_search();
                ?> 
            </div>    
        </div><!-- main-nav-inner -->       
        <div class="rubik-ajax-search-bg"></div>   
	</nav>
</div>