<?php
if ( ! function_exists( 'rubik_review_score' ) ) {
    function rubik_review_score ($post_id) {
        $ret = '';
        if (strlen(get_post_meta($post_id, 'bk_final_score', true)) > 0) {
            $ret .= '<div class="review-score">';
            $ret .= '<span><i class="fa fa-star-o"></i></span>';
            $ret .= get_post_meta($post_id, 'bk_final_score', true);
            $ret .= '</div>';
        }
        return $ret;
    }
}
/**
 * ********* Get Post Category ************
 *---------------------------------------------------
 */ 
if ( ! function_exists('rubik_get_category_link')){
    function rubik_get_category_link($postid){ 
        $html = '';
        $category = get_the_category($postid); 
        if(isset($category[0]) && $category[0]){
            foreach ($category as $key => $value) {
                $html.= '<a class="term-'.$value->term_id.'" href="'.get_category_link($value->term_id ).'">'.$value->cat_name.'</a>';  
            }
        						
        }
        return $html;
    }
}
/**
 * ********* Get Header ************
 *---------------------------------------------------
 */
if ( ! function_exists( 'rubik_get_header' ) ) {
    function rubik_get_header () {
        $rubik_option = rubik_core::bk_get_global_var('rubik_option');
        $bkHeaderType = '';
        if ((isset($rubik_option['bk-header-type'])) && (($rubik_option['bk-header-type']) != NULL)){ 
            $bkHeaderType = $rubik_option['bk-header-type'];
        }else {
            $bkHeaderType == 'header1';
        }
        
        if ($bkHeaderType == 'header2') {
            if ((isset($rubik_option['bk-header-2-options'])) && (($rubik_option['bk-header-2-options']) != NULL)){ 
                $headerStyle = $rubik_option['bk-header-2-options'];
                if($headerStyle == 'style-1') {
                    $bkHeaderType = 'header2';
                }elseif($headerStyle == 'style-2') {
                    $bkHeaderType = 'header5';
                }elseif($headerStyle == 'style-3') {
                    $bkHeaderType = 'header6';
                }elseif($headerStyle == 'style-4') {
                    $bkHeaderType = 'header8';
                }elseif($headerStyle == 'style-5') {
                    $bkHeaderType = 'header9';
                }elseif($headerStyle == 'style-6') {
                    $bkHeaderType = 'header14';
                }
            }
        }else if ($bkHeaderType == 'header12') {
            if ((isset($rubik_option['bk-header-12-options'])) && (($rubik_option['bk-header-12-options']) != NULL)){ 
                $headerStyle = $rubik_option['bk-header-12-options'];
                if($headerStyle == 'style-1') {
                    $bkHeaderType = 'header12';
                }elseif($headerStyle == 'style-2') {
                    $bkHeaderType = 'header13';
                }else {
                    $bkHeaderType = 'header12';
                }
            }
        }
        
        if ($bkHeaderType == 'header1') {
            get_template_part( 'library/templates/headers/rubik_header_1' );
        }
        elseif ($bkHeaderType == 'header2') {
            get_template_part( 'library/templates/headers/rubik_header_2' );
        }
        elseif ($bkHeaderType == 'header3') {
            get_template_part( 'library/templates/headers/rubik_header_3' );
        }
        elseif ($bkHeaderType == 'header4') {
            get_template_part( 'library/templates/headers/rubik_header_4' );
        }
        elseif ($bkHeaderType == 'header5') {
            get_template_part( 'library/templates/headers/rubik_header_5' );
        }
        elseif ($bkHeaderType == 'header6') {
            get_template_part( 'library/templates/headers/rubik_header_6' );
        }
        elseif ($bkHeaderType == 'header7') {
            get_template_part( 'library/templates/headers/rubik_header_7' );
        }
        elseif ($bkHeaderType == 'header8') {
            get_template_part( 'library/templates/headers/rubik_header_8' );
        }
        elseif ($bkHeaderType == 'header9') {
            get_template_part( 'library/templates/headers/rubik_header_9' );
        }
        elseif ($bkHeaderType == 'header10') {
            get_template_part( 'library/templates/headers/rubik_header_10' );
        }
        elseif ($bkHeaderType == 'header11') {
            get_template_part( 'library/templates/headers/rubik_header_11' );
        }
        elseif ($bkHeaderType == 'header12') {
            get_template_part( 'library/templates/headers/rubik_header_12' );
        }
        elseif ($bkHeaderType == 'header13') {
            get_template_part( 'library/templates/headers/rubik_header_13' );
        }
        elseif ($bkHeaderType == 'header14') {
            get_template_part( 'library/templates/headers/rubik_header_14' );
        }
        else {
            get_template_part( 'library/templates/headers/rubik_header_1' );
        }
    }
}
/**
 * ********* Get Footer ************
 *---------------------------------------------------
 */
if ( ! function_exists( 'rubik_get_footer' ) ) {
    function rubik_get_footer () {
        $rubik_option = rubik_core::bk_get_global_var('rubik_option');
        $rubik_justified_ids = rubik_core::bk_get_global_var('rubik_justified_ids');
        
        $rubik_instagram_col[0] = '';
        $footerStyle = 'dark';
        
        if(isset($rubik_option['bk-footer-instagram-access-token']) && ($rubik_option['bk-footer-instagram-access-token'] != NULL)) {
            $rubik_instagram_col[0] = $rubik_option['bk-footer-instagram-columns'];
        }else {
            $rubik_instagram_col[0] = '';
        }        
        if($rubik_instagram_col[0] == '') {
            $rubik_instagram_col[0] = 7;
        }
        if(isset($rubik_option['bk-footer-content-style']) && ($rubik_option['bk-footer-content-style'] != 'bk-footer-dark')) {
            $footerStyle = 'bk-footer-light';
        }else {
            $footerStyle = 'bk-footer-dark';
        }
        $rubik_tabs_more = '<div class="bk-tabs-dropdown"><div class="bk-tabs-more"><span>'.esc_html__('More', 'rubik').'</span><i class="fa fa-caret-down" aria-hidden="true"></i></div><div class="bk-tabs-pull-list clearfix"></div></div>';

        if (isset($rubik_option)):
            $fixed_nav = $rubik_option['bk-sticky-nav-switch'];            
            wp_localize_script( 'rubik-customjs', 'fixed_nav', $fixed_nav );
            
            $bk_allow_html = array(
                                    'a' => array(
                                        'href' => array(),
                                        'title' => array()
                                    ),
                                    'br' => array(),
                                    'em' => array(),
                                    'strong' => array(),
                                );
            $cr_text = $rubik_option['cr-text'];
        endif;
        
        if(isset($rubik_option['bk-footer-instagram-access-token']) && ($rubik_option['bk-footer-instagram-access-token'] != '')) {
            echo rubik_core::rubik_footer_instagram();
        }
        
        if (is_active_sidebar('footer_sidebar_1') || is_active_sidebar('footer_sidebar_2') || is_active_sidebar('footer_sidebar_3')
            || is_active_sidebar('footer_d3_1') || is_active_sidebar('footer_d3_2') || is_active_sidebar('footer_d3_3')
            || is_active_sidebar('footer_d2_1') || is_active_sidebar('footer_d2_2')
            || is_active_sidebar('footer_d1')
            || has_nav_menu('menu-footer')
            || ($cr_text != '')) { ?>
            
            <div class="footer <?php echo esc_attr($footerStyle);?>">
                <?php if ( isset($rubik_option ['bk-footer-menu-social-position']) && ($rubik_option ['bk-footer-menu-social-position'] == 'top') ){ ?>
                    <div class="container">
                        <?php if ( has_nav_menu('menu-footer') ) {?> 
                            <div class="footer-menu row">
                            <?php wp_nav_menu(array('theme_location' => 'menu-footer', 'depth' => '1', 'container_id' => 'footer-menu'));?>  
                            </div>
                        <?php }?>  
                        <?php if ( isset($rubik_option ['bk-social-footer-switch']) && ($rubik_option ['bk-social-footer-switch'] == 1) ){ ?>
        				<div class="footer-social row">
        					<ul class="clearfix">
        						<?php if ($rubik_option['bk-social-footer']['fb']){ ?>
        							<li class="social-icon fb"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['fb']); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['twitter']){ ?>
        							<li class="social-icon twitter"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['twitter']); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['gplus']){ ?>
        							<li class="social-icon gplus"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['gplus']); ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['linkedin']){ ?>
        							<li class="social-icon linkedin"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['linkedin']); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['pinterest']){ ?>
        							<li class="social-icon pinterest"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['pinterest']); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['instagram']){ ?>
        							<li class="social-icon instagram"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['instagram']); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['dribbble']){ ?>
        							<li class="social-icon dribbble"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['dribbble']); ?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['youtube']){ ?>
        							<li class="social-icon youtube"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['youtube']); ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
        						<?php } ?>      							
        						                                    
                                <?php if ($rubik_option['bk-social-footer']['vimeo']){ ?>
        							<li class="social-icon vimeo"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['vimeo']); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
        						<?php } ?>
                                
                                <?php if ($rubik_option['bk-social-footer']['vk']){ ?>
        							<li class="social-icon vk"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['vk']); ?>" target="_blank"><i class="fa fa-vk"></i></a></li>
        						<?php } ?>
                                <?php if ($rubik_option['bk-social-footer']['vine']){ ?>
        							<li class="social-icon vine"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['vine']); ?>" target="_blank"><i class="fa fa-vine"></i></a></li>
        						<?php } ?>
                                <?php if ($rubik_option['bk-social-footer']['snapchat']){ ?>
        							<li class="social-icon snapchat"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['snapchat']); ?>" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>
        						<?php } ?> 
                                <?php if ($rubik_option['bk-social-footer']['telegram']){ ?>
        							<li class="social-icon telegram"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['telegram']); ?>" target="_blank"><i class="fa fa-telegram"></i></a></li>
        						<?php } ?>                                        
                                <?php if ($rubik_option['bk-social-footer']['rss']){ ?>
        							<li class="social-icon rss"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['rss']); ?>" target="_blank"><i class="fa fa-rss"></i></a></li>
        						<?php } ?>                    						
        					</ul>
        				</div>
                        <?php } ?>  
                    </div>
                <?php }?>  
                <?php 
                foreach ($rubik_option['footer-sortable'] as $key => $value):
                    if(($key == '1') && ($value == '1')) { // 1/3 1/3 1/3
                        get_template_part( 'library/templates/footers/rubik_footer_default' );
                    }
                    elseif(($key == '2') && ($value == '1')) { // Dynamic Width 3 columns
                        get_template_part( 'library/templates/footers/rubik_footer_d3');
                    }
                    elseif(($key == '3') && ($value == '1')) { // Dynamic Width 2 columns
                        get_template_part( 'library/templates/footers/rubik_footer_d2');
                    }
                    elseif(($key == '4') && ($value == '1')) { // Full Width
                        get_template_part( 'library/templates/footers/rubik_footer_fw');
                    }
                endforeach;
                ?>
                
                <?php if ( isset($rubik_option ['bk-footer-menu-social-position']) && ($rubik_option ['bk-footer-menu-social-position'] == 'bottom') ){ ?>
                        <div class="footer-content bkwrapper clearfix container">    
                            <?php if ( has_nav_menu('menu-footer') ) {?>                 
                            <div class="footer-menu row">
                            <?php wp_nav_menu(array('theme_location' => 'menu-footer', 'depth' => '1', 'container_id' => 'footer-menu'));?>  
                            </div>
                            <?php }?>  
                        <?php if ( isset($rubik_option ['bk-social-footer-switch']) && ($rubik_option ['bk-social-footer-switch'] == 1) ){ ?>
        				<div class="footer-social row">
        					<ul class="clearfix">
        						<?php if ($rubik_option['bk-social-footer']['fb']){ ?>
        							<li class="social-icon fb"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['fb']); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['twitter']){ ?>
        							<li class="social-icon twitter"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['twitter']); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['gplus']){ ?>
        							<li class="social-icon gplus"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['gplus']); ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['linkedin']){ ?>
        							<li class="social-icon linkedin"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['linkedin']); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['pinterest']){ ?>
        							<li class="social-icon pinterest"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['pinterest']); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['instagram']){ ?>
        							<li class="social-icon instagram"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['instagram']); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['dribbble']){ ?>
        							<li class="social-icon dribbble"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['dribbble']); ?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
        						<?php } ?>
        						
        						<?php if ($rubik_option['bk-social-footer']['youtube']){ ?>
        							<li class="social-icon youtube"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['youtube']); ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
        						<?php } ?>      							
        						                                    
                                <?php if ($rubik_option['bk-social-footer']['vimeo']){ ?>
        							<li class="social-icon vimeo"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['vimeo']); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
        						<?php } ?>
                                
                                <?php if ($rubik_option['bk-social-footer']['vk']){ ?>
        							<li class="social-icon vk"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['vk']); ?>" target="_blank"><i class="fa fa-vk"></i></a></li>
        						<?php } ?>
                                <?php if ($rubik_option['bk-social-footer']['vine']){ ?>
        							<li class="social-icon vine"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['vine']); ?>" target="_blank"><i class="fa fa-vine"></i></a></li>
        						<?php } ?>
                                <?php if ($rubik_option['bk-social-footer']['snapchat']){ ?>
        							<li class="social-icon snapchat"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['snapchat']); ?>" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>
        						<?php } ?>
                                <?php if ($rubik_option['bk-social-footer']['telegram']){ ?>
        							<li class="social-icon telegram"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['telegram']); ?>" target="_blank"><i class="fa fa-telegram"></i></a></li>
        						<?php } ?>  
                                <?php if ($rubik_option['bk-social-footer']['rss']){ ?>
        							<li class="social-icon rss"><a href="<?php echo esc_url($rubik_option['bk-social-footer']['rss']); ?>" target="_blank"><i class="fa fa-rss"></i></a></li>
        						<?php } ?>                    						
        					</ul>
        				</div>
                        <?php } ?>  
                    </div>
                <?php }?>  
                
                <div class="footer-lower">
                    <div class="container">
                        <div class="footer-inner clearfix">
                            <div class="footer-lower-left">
                                <div class="bk-copyright"><?php echo wp_kses($cr_text, $bk_allow_html);?></div>
                            </div>
                            <div class="footer-lower-right">
                                <?php 
                                if(isset($rubik_option['bk-right-side-footer-lower']) && ($rubik_option['bk-right-side-footer-lower'] == 'footer-lower-menu')) {
                                    if ( has_nav_menu('menu-footer-lower') ) {
                                        wp_nav_menu(array('theme_location' => 'menu-footer-lower', 'depth' => '1', 'container_id' => 'footer-menu-lower'));   
                                    }
                                }else if(isset($rubik_option['bk-right-side-footer-lower']) && ($rubik_option['bk-right-side-footer-lower'] == 'social-media')) {?>
                                    <ul class="clearfix">
                						<?php if ($rubik_option['bk-footer-lower-social']['fb']){ ?>
                							<li class="social-icon fb"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['fb']); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                						<?php } ?>
                						
                						<?php if ($rubik_option['bk-footer-lower-social']['twitter']){ ?>
                							<li class="social-icon twitter"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['twitter']); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                						<?php } ?>
                						
                						<?php if ($rubik_option['bk-footer-lower-social']['gplus']){ ?>
                							<li class="social-icon gplus"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['gplus']); ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                						<?php } ?>
                						
                						<?php if ($rubik_option['bk-footer-lower-social']['linkedin']){ ?>
                							<li class="social-icon linkedin"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['linkedin']); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                						<?php } ?>
                						
                						<?php if ($rubik_option['bk-footer-lower-social']['pinterest']){ ?>
                							<li class="social-icon pinterest"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['pinterest']); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                						<?php } ?>
                						
                						<?php if ($rubik_option['bk-footer-lower-social']['instagram']){ ?>
                							<li class="social-icon instagram"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['instagram']); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                						<?php } ?>
                						
                						<?php if ($rubik_option['bk-footer-lower-social']['dribbble']){ ?>
                							<li class="social-icon dribbble"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['dribbble']); ?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
                						<?php } ?>
                						
                						<?php if ($rubik_option['bk-footer-lower-social']['youtube']){ ?>
                							<li class="social-icon youtube"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['youtube']); ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
                						<?php } ?>      							
                						                                    
                                        <?php if ($rubik_option['bk-footer-lower-social']['vimeo']){ ?>
                							<li class="social-icon vimeo"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['vimeo']); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
                						<?php } ?>
                                        
                                        <?php if ($rubik_option['bk-footer-lower-social']['vk']){ ?>
                							<li class="social-icon vk"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['vk']); ?>" target="_blank"><i class="fa fa-vk"></i></a></li>
                						<?php } ?>
                                        <?php if ($rubik_option['bk-footer-lower-social']['vine']){ ?>
                							<li class="social-icon vine"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['vine']); ?>" target="_blank"><i class="fa fa-vine"></i></a></li>
                						<?php } ?>
                                        <?php if ($rubik_option['bk-footer-lower-social']['snapchat']){ ?>
                							<li class="social-icon snapchat"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['snapchat']); ?>" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>
                						<?php } ?>    
                                        <?php if ($rubik_option['bk-footer-lower-social']['telegram']){ ?>
                							<li class="social-icon telegram"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['telegram']); ?>" target="_blank"><i class="fa fa-telegram"></i></a></li>
                						<?php } ?>                                     
                                        <?php if ($rubik_option['bk-footer-lower-social']['rss']){ ?>
                							<li class="social-icon rss"><a href="<?php echo esc_url($rubik_option['bk-footer-lower-social']['rss']); ?>" target="_blank"><i class="fa fa-rss"></i></a></li>
                						<?php } ?>                    						
                					</ul>
                                <?php    
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        <?php }
        
        wp_localize_script( 'rubik-customjs', 'rubik_instagram_col', $rubik_instagram_col );
        
        wp_localize_script( 'rubik-customjs', 'rubik_tabs_more', $rubik_tabs_more );
        
        wp_localize_script( 'rubik-customjs', 'justified_ids', $rubik_justified_ids );
        
        wp_localize_script( 'rubik-module-load-post', 'ajax_c', rubik_section_parent::$rubik_ajax_c );
    }
}
// Square Grid
add_action( 'wp_ajax_square_grid_load', 'rubik_ajax_square_grid_load' );
add_action('wp_ajax_nopriv_square_grid_load', 'rubik_ajax_square_grid_load');
if ( ! function_exists( 'rubik_ajax_square_grid_load' ) ) {
    function rubik_ajax_square_grid_load() {
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        $entries = isset( $_POST['entries'] ) ? $_POST['entries'] : 0;
        $sec = isset( $_POST['sec'] ) ? $_POST['sec'] : 0;
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : 0;
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0; 
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;    
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args['category__in'] = $currentCatID;        
        $the_query = new WP_Query( $args );
        echo rubik_square_grid::render_modules($the_query, $sec, $post_icon);
        die();
    }
}
// Row Load More
add_action( 'wp_ajax_row_fw_load', 'rubik_ajax_row_fw_load' );
add_action('wp_ajax_nopriv_row_fw_load', 'rubik_ajax_row_fw_load');
if ( ! function_exists( 'rubik_ajax_row_fw_load' ) ) {
    function rubik_ajax_row_fw_load() {
        $bk_ajax_c = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        
        $entries = $bk_ajax_c[$blockID]['entries'];
        $postIcon = $bk_ajax_c[$blockID]['post_icon'];
        $excerpt_length = $bk_ajax_c[$blockID]['excerpt_length'];
        
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : 0;
        
        $module_row_type = isset( $_POST['module_row_type'] ) ? $_POST['module_row_type'] : 0;
        
        $module_row_section = isset( $_POST['module_row_section'] ) ? $_POST['module_row_section'] : 0;
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args['category__in'] = $currentCatID;
        
        $the_query = new WP_Query( $args );
        
        if($module_row_section == 'fullwidth') {
            if($module_row_type == 'tall-layout-1') {
                echo rubik_row_tall_fw::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'tall-layout-2') {
                echo rubik_row_tall_2_fw::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'tall-layout-3') {
                echo rubik_row_tall_3_fw::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'normal-layout-1') {
                echo rubik_row_fw::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'normal-layout-2') {
                echo rubik_row_2_fw::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'normal-layout-3') {
                echo rubik_row_3_fw::render_modules($the_query, $postIcon, $excerpt_length);
            }else {
                echo rubik_row_fw::render_modules($the_query, $postIcon, $excerpt_length);
            }
        }else {
            if($module_row_type == 'tall-layout-1') {
                echo rubik_row_tall::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'tall-layout-2') {
                echo rubik_row_tall_2::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'tall-layout-3') {
                echo rubik_row_tall_3::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'normal-layout-1') {
                echo rubik_row::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'normal-layout-2') {
                echo rubik_row_2::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'normal-layout-3') {
                echo rubik_row_3::render_modules($the_query, $postIcon, $excerpt_length);
            }else if($module_row_type == 'normal-layout-4') {
                echo rubik_row_4::render_modules($the_query, $postIcon, $excerpt_length);
            }else {
                echo rubik_row::render_modules($the_query, $postIcon, $excerpt_length);
            }
        }
        
        die();
    }
}
// Classic Blog
add_action( 'wp_ajax_blog_load', 'rubik_ajax_blog_load' );
add_action('wp_ajax_nopriv_blog_load', 'rubik_ajax_blog_load');
if ( ! function_exists( 'rubik_ajax_blog_load' ) ) {
    function rubik_ajax_blog_load() {
        
        $bk_ajax_c = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        
        $entries = $bk_ajax_c[$blockID]['entries'];
        $post_icon = $bk_ajax_c[$blockID]['post_icon'];
        $excerpt_length = $bk_ajax_c[$blockID]['excerpt_length'];
        
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : 0;
        
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;    
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args['category__in'] = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo rubik_classic_blog::render_modules($the_query, $post_icon, $excerpt_length);
        die();
    }
}
// Latge Blog
add_action( 'wp_ajax_large_blog_load', 'rubik_ajax_large_blog_load' );
add_action('wp_ajax_nopriv_large_blog_load', 'rubik_ajax_large_blog_load');
if ( ! function_exists( 'rubik_ajax_large_blog_load' ) ) {
    function rubik_ajax_large_blog_load() {
        $bk_ajax_c = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        
        $entries = $bk_ajax_c[$blockID]['entries'];
        $post_icon = $bk_ajax_c[$blockID]['post_icon'];
        $excerpt_length = $bk_ajax_c[$blockID]['excerpt_length'];
        
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : 0;
           
        $columns_class = isset( $_POST['columns_class'] ) ? $_POST['columns_class'] : 0;
        
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;    
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args['category__in'] = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo rubik_large_blog::render_modules($the_query, $post_icon, $excerpt_length, $columns_class);
        die();
    }
}
// Latge Blog
add_action( 'wp_ajax_large_blog_2_load', 'rubik_ajax_large_blog_2_load' );
add_action('wp_ajax_nopriv_large_blog_2_load', 'rubik_ajax_large_blog_2_load');
if ( ! function_exists( 'rubik_ajax_large_blog_2_load' ) ) {
    function rubik_ajax_large_blog_2_load() {
        $bk_ajax_c = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        
        $entries = $bk_ajax_c[$blockID]['entries'];
        $post_icon = $bk_ajax_c[$blockID]['post_icon'];
        $excerpt_length = $bk_ajax_c[$blockID]['excerpt_length'];
        
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : 0;
           
        $columns_class = isset( $_POST['columns_class'] ) ? $_POST['columns_class'] : 0;
        
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;    
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args['category__in'] = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo rubik_large_blog_2::render_modules($the_query, $post_icon, $excerpt_length, $columns_class);
        die();
    }
}
// Masonry
add_action( 'wp_ajax_masonry_load', 'rubik_ajax_masonry_load' );
add_action('wp_ajax_nopriv_masonry_load', 'rubik_ajax_masonry_load');
if ( ! function_exists( 'rubik_ajax_masonry_load' ) ) {
    function rubik_ajax_masonry_load() {
        $bk_ajax_c = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        
        $entries = $bk_ajax_c[$blockID]['entries'];
        $sec = $bk_ajax_c[$blockID]['sec'];
        $post_icon = $bk_ajax_c[$blockID]['post_icon'];
        $excerpt_length = $bk_ajax_c[$blockID]['excerpt_length'];
        
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : 0;
        
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;    
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args['category__in'] = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo rubik_masonry::render_modules($the_query, $sec, $post_icon, $excerpt_length);
        die();
    }
}
// Masonry
add_action( 'wp_ajax_masonry_2_load', 'rubik_ajax_masonry_2_load' );
add_action('wp_ajax_nopriv_masonry_2_load', 'rubik_ajax_masonry_2_load');
if ( ! function_exists( 'rubik_ajax_masonry_2_load' ) ) {
    function rubik_ajax_masonry_2_load() {
        $bk_ajax_c = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        
        $entries = $bk_ajax_c[$blockID]['entries'];
        $sec = $bk_ajax_c[$blockID]['sec'];
        $post_icon = $bk_ajax_c[$blockID]['post_icon'];
        $excerpt_length = $bk_ajax_c[$blockID]['excerpt_length'];
        
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : 0;
        
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;    
        $args[ 'posts_per_page' ]   = $entries;
        $args[ 'offset' ]           = $post_offset;
        $args['category__in']       = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo rubik_masonry_2::render_modules($the_query, $sec, $post_icon, $excerpt_length);
        die();
    }
}
// Masonry
add_action( 'wp_ajax_masonry_3_load', 'rubik_ajax_masonry_3_load' );
add_action('wp_ajax_nopriv_masonry_3_load', 'rubik_ajax_masonry_3_load');
if ( ! function_exists( 'rubik_ajax_masonry_3_load' ) ) {
    function rubik_ajax_masonry_3_load() {
        $bk_ajax_c = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        
        $post_offset = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : 0;
        
        $entries = $bk_ajax_c[$blockID]['entries'];
        $sec = $bk_ajax_c[$blockID]['sec'];
        $post_icon = $bk_ajax_c[$blockID]['post_icon'];
        $excerpt_length = $bk_ajax_c[$blockID]['excerpt_length'];
        
        $currentCatID = isset( $_POST['currentCatID'] ) ? $_POST['currentCatID'] : 0;
        
        $args = isset( $_POST['args'] ) ? $_POST['args'] : null;    
        $args[ 'posts_per_page' ] = $entries;
        $args[ 'offset' ] = $post_offset;
        $args['category__in'] = $currentCatID;
                
        $the_query = new WP_Query( $args );
        echo rubik_masonry_3::render_modules($the_query, $sec, $post_icon, $excerpt_length);
        die();
    }
}
/**
 * Ajax Tab
 */
// Tech Grid
add_action( 'wp_ajax_tech_grid', 'rubik_ajax_tech_grid' );
add_action('wp_ajax_nopriv_tech_grid', 'rubik_ajax_tech_grid');
if ( ! function_exists( 'rubik_ajax_tech_grid' ) ) {
    function rubik_ajax_tech_grid() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = $bk_ajax_c[$blockID]['post_icon'];
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_tech_grid::render_modules($the_query, $postIcon);
        die();
    }
}
// Pyramid Grid
add_action( 'wp_ajax_pyramid_grid', 'rubik_ajax_pyramid_grid' );
add_action('wp_ajax_nopriv_pyramid_grid', 'rubik_ajax_pyramid_grid');
if ( ! function_exists( 'rubik_ajax_pyramid_grid' ) ) {
    function rubik_ajax_pyramid_grid() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = $bk_ajax_c[$blockID]['post_icon'];
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_pyramid_grid::render_modules($the_query, $postIcon);
        die();
    }
}
// Grid 1
add_action( 'wp_ajax_grid_1', 'rubik_ajax_grid_1' );
add_action('wp_ajax_nopriv_grid_1', 'rubik_ajax_grid_1');
if ( ! function_exists( 'rubik_ajax_grid_1' ) ) {
    function rubik_ajax_grid_1() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_1::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 2
add_action( 'wp_ajax_grid_2', 'rubik_ajax_grid_2' );
add_action('wp_ajax_nopriv_grid_2', 'rubik_ajax_grid_2');
if ( ! function_exists( 'rubik_ajax_grid_2' ) ) {
    function rubik_ajax_grid_2() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_2::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 3
add_action( 'wp_ajax_grid_3', 'rubik_ajax_grid_3' );
add_action('wp_ajax_nopriv_grid_3', 'rubik_ajax_grid_3');
if ( ! function_exists( 'rubik_ajax_grid_3' ) ) {
    function rubik_ajax_grid_3() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_3::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 4
add_action( 'wp_ajax_grid_4', 'rubik_ajax_grid_4' );
add_action('wp_ajax_nopriv_grid_4', 'rubik_ajax_grid_4');
if ( ! function_exists( 'rubik_ajax_grid_4' ) ) {
    function rubik_ajax_grid_4() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_4::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 5
add_action( 'wp_ajax_grid_5', 'rubik_ajax_grid_5' );
add_action('wp_ajax_nopriv_grid_5', 'rubik_ajax_grid_5');
if ( ! function_exists( 'rubik_ajax_grid_5' ) ) {
    function rubik_ajax_grid_5() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null; 
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_5::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 6
add_action( 'wp_ajax_grid_6', 'rubik_ajax_grid_6' );
add_action('wp_ajax_nopriv_grid_6', 'rubik_ajax_grid_6');
if ( ! function_exists( 'rubik_ajax_grid_6' ) ) {
    function rubik_ajax_grid_6() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_6::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 7
add_action( 'wp_ajax_grid_7', 'rubik_ajax_grid_7' );
add_action('wp_ajax_nopriv_grid_7', 'rubik_ajax_grid_7');
if ( ! function_exists( 'rubik_ajax_grid_7' ) ) {
    function rubik_ajax_grid_7() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_7::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 8
add_action( 'wp_ajax_grid_8', 'rubik_ajax_grid_8' );
add_action('wp_ajax_nopriv_grid_8', 'rubik_ajax_grid_8');
if ( ! function_exists( 'rubik_ajax_grid_8' ) ) {
    function rubik_ajax_grid_8() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_8::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 9
add_action( 'wp_ajax_grid_9', 'rubik_ajax_grid_9' );
add_action('wp_ajax_nopriv_grid_9', 'rubik_ajax_grid_9');
if ( ! function_exists( 'rubik_ajax_grid_9' ) ) {
    function rubik_ajax_grid_9() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_9::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 10
add_action( 'wp_ajax_grid_10', 'rubik_ajax_grid_10' );
add_action('wp_ajax_nopriv_grid_10', 'rubik_ajax_grid_10');
if ( ! function_exists( 'rubik_ajax_grid_10' ) ) {
    function rubik_ajax_grid_10() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_10::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 11
add_action( 'wp_ajax_grid_11', 'rubik_ajax_grid_11' );
add_action('wp_ajax_nopriv_grid_11', 'rubik_ajax_grid_11');
if ( ! function_exists( 'rubik_ajax_grid_11' ) ) {
    function rubik_ajax_grid_11() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_11::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 12
add_action( 'wp_ajax_grid_12', 'rubik_ajax_grid_12' );
add_action('wp_ajax_nopriv_grid_12', 'rubik_ajax_grid_12');
if ( ! function_exists( 'rubik_ajax_grid_12' ) ) {
    function rubik_ajax_grid_12() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_12::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 13
add_action( 'wp_ajax_grid_13', 'rubik_ajax_grid_13' );
add_action('wp_ajax_nopriv_grid_13', 'rubik_ajax_grid_13');
if ( ! function_exists( 'rubik_ajax_grid_13' ) ) {
    function rubik_ajax_grid_13() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_13::render_modules($the_query, $post_icon);
        die();
    }
}
// Grid 14
add_action( 'wp_ajax_grid_14', 'rubik_ajax_grid_14' );
add_action('wp_ajax_nopriv_grid_14', 'rubik_ajax_grid_14');
if ( ! function_exists( 'rubik_ajax_grid_14' ) ) {
    function rubik_ajax_grid_14() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;  
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_grid_14::render_modules($the_query, $post_icon);
        die();
    }
}
/*post_block_1_fw*/
add_action( 'wp_ajax_block_1_fw', 'rubik_ajax_block_1_fw' );
add_action('wp_ajax_nopriv_block_1_fw', 'rubik_ajax_block_1_fw');
if ( ! function_exists( 'rubik_ajax_block_1_fw' ) ) {
    function rubik_ajax_block_1_fw() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_1_fw::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
/*post_block_3_fw*/
add_action( 'wp_ajax_block_3_fw', 'rubik_ajax_block_3_fw' );
add_action('wp_ajax_nopriv_block_3_fw', 'rubik_ajax_block_3_fw');
if ( ! function_exists( 'rubik_ajax_block_3_fw' ) ) {
    function rubik_ajax_block_3_fw() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_3_fw::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
/*post_block_4_fw*/
add_action( 'wp_ajax_block_4_fw', 'rubik_ajax_block_4_fw' );
add_action('wp_ajax_nopriv_block_4_fw', 'rubik_ajax_block_4_fw');
if ( ! function_exists( 'rubik_ajax_block_4_fw' ) ) {
    function rubik_ajax_block_4_fw() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null; 
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_4_fw::render_modules($the_query, $postIcon);
        die();
    }
}
/*post_block_5 FW*/
add_action( 'wp_ajax_block_5_fw', 'rubik_ajax_block_5_fw' );
add_action('wp_ajax_nopriv_block_5_fw', 'rubik_ajax_block_5_fw');
if ( ! function_exists( 'rubik_ajax_block_5_fw' ) ) {
    function rubik_ajax_block_5_fw() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_5_fw::render_modules($the_query, $postIcon);
        die();
    }
}
// Feature 1
add_action( 'wp_ajax_feature1', 'rubik_ajax_feature1' );
add_action('wp_ajax_nopriv_feature1', 'rubik_ajax_feature1');
if ( ! function_exists( 'rubik_ajax_feature1' ) ) {
    function rubik_ajax_feature1() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;  
         
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_feature1::render_modules($the_query, $postIcon);
        die();
    }
}
// Row
add_action( 'wp_ajax_row', 'rubik_ajax_row' );
add_action('wp_ajax_nopriv_row', 'rubik_ajax_row');
if ( ! function_exists( 'rubik_ajax_row' ) ) {
    function rubik_ajax_row() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null; 
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row 2
add_action( 'wp_ajax_row_2', 'rubik_ajax_row_2' );
add_action('wp_ajax_nopriv_row_2', 'rubik_ajax_row_2');
if ( ! function_exists( 'rubik_ajax_row_2' ) ) {
    function rubik_ajax_row_2() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_2::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row 2 FW
add_action( 'wp_ajax_row_2_fw', 'rubik_ajax_row_2_fw' );
add_action('wp_ajax_nopriv_row_2_fw', 'rubik_ajax_row_2_fw');
if ( ! function_exists( 'rubik_ajax_row_2_fw' ) ) {
    function rubik_ajax_row_2_fw() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_2_fw::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
add_action( 'wp_ajax_row_3', 'rubik_ajax_row_3' );
add_action('wp_ajax_nopriv_row_3', 'rubik_ajax_row_3');
if ( ! function_exists( 'rubik_ajax_row_3' ) ) {
    function rubik_ajax_row_3() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_3::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row 3 FW
add_action( 'wp_ajax_row_3_fw', 'rubik_ajax_row_3_fw' );
add_action('wp_ajax_nopriv_row_3_fw', 'rubik_ajax_row_3_fw');
if ( ! function_exists( 'rubik_ajax_row_3_fw' ) ) {
    function rubik_ajax_row_3_fw() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_3_fw::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row 4
add_action( 'wp_ajax_row_4', 'rubik_ajax_row_4' );
add_action('wp_ajax_nopriv_row_4', 'rubik_ajax_row_4');
if ( ! function_exists( 'rubik_ajax_row_4' ) ) {
    function rubik_ajax_row_4() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null; 
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_4::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row FW
add_action( 'wp_ajax_row_fw', 'rubik_ajax_row_fw' );
add_action('wp_ajax_nopriv_row_fw', 'rubik_ajax_row_fw');
if ( ! function_exists( 'rubik_ajax_row_fw' ) ) {
    function rubik_ajax_row_fw() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_fw::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row Tall
add_action( 'wp_ajax_row_tall', 'rubik_ajax_row_tall' );
add_action('wp_ajax_nopriv_row_tall', 'rubik_ajax_row_tall');
if ( ! function_exists( 'rubik_ajax_row_tall' ) ) {
    function rubik_ajax_row_tall() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_tall::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row Tall 2
add_action( 'wp_ajax_row_tall_2', 'rubik_ajax_row_tall_2' );
add_action('wp_ajax_nopriv_row_tall_2', 'rubik_ajax_row_tall_2');
if ( ! function_exists( 'rubik_ajax_row_tall_2' ) ) {
    function rubik_ajax_row_tall_2() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_tall_2::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row Tall 2 FW
add_action( 'wp_ajax_row_tall_2_fw', 'rubik_ajax_row_tall_2_fw' );
add_action('wp_ajax_nopriv_row_tall_2_fw', 'rubik_ajax_row_tall_2_fw');
if ( ! function_exists( 'rubik_ajax_row_tall_2_fw' ) ) {
    function rubik_ajax_row_tall_2_fw() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_tall_2_fw::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row Tall 3
add_action( 'wp_ajax_row_tall_3', 'rubik_ajax_row_tall_3' );
add_action('wp_ajax_nopriv_row_tall_3', 'rubik_ajax_row_tall_3');
if ( ! function_exists( 'rubik_ajax_row_tall_3' ) ) {
    function rubik_ajax_row_tall_3() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_tall_3::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row Tall 3 FW
add_action( 'wp_ajax_row_tall_3_fw', 'rubik_ajax_row_tall_3_fw' );
add_action('wp_ajax_nopriv_row_tall_3_fw', 'rubik_ajax_row_tall_3_fw');
if ( ! function_exists( 'rubik_ajax_row_tall_3_fw' ) ) {
    function rubik_ajax_row_tall_3_fw() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_tall_3_fw::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Row Tall FW
add_action( 'wp_ajax_row_tall_fw', 'rubik_ajax_row_tall_fw' );
add_action('wp_ajax_nopriv_row_tall_fw', 'rubik_ajax_row_tall_fw');
if ( ! function_exists( 'rubik_ajax_row_tall_fw' ) ) {
    function rubik_ajax_row_tall_fw() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_row_tall_fw::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Hero
add_action( 'wp_ajax_hero', 'rubik_ajax_hero' );
add_action('wp_ajax_nopriv_hero', 'rubik_ajax_hero');
if ( ! function_exists( 'rubik_ajax_hero' ) ) {
    function rubik_ajax_hero() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null; 
        $postIcon   = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;    
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_hero::render_modules($the_query, $postIcon);
        die();
    }
}
// Masonry
add_action( 'wp_ajax_masonry', 'rubik_ajax_masonry' );
add_action('wp_ajax_nopriv_masonry', 'rubik_ajax_masonry');
if ( ! function_exists( 'rubik_ajax_masonry' ) ) {
    function rubik_ajax_masonry() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;
        
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $sec = $bk_ajax_c[$blockID]['sec'];
        
        $the_query = new WP_Query( $args );
        echo rubik_masonry::render_modules($the_query, $sec, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Masonry 2
add_action( 'wp_ajax_masonry_2', 'rubik_ajax_masonry_2' );
add_action('wp_ajax_nopriv_masonry_2', 'rubik_ajax_masonry_2');
if ( ! function_exists( 'rubik_ajax_masonry_2' ) ) {
    function rubik_ajax_masonry_2() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;
        
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $sec = $bk_ajax_c[$blockID]['sec'];
        
        $the_query = new WP_Query( $args );
        echo rubik_masonry_2::render_modules($the_query, $sec, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Masonry 3
add_action( 'wp_ajax_masonry_3', 'rubik_ajax_masonry_3' );
add_action('wp_ajax_nopriv_masonry_3', 'rubik_ajax_masonry_3');
if ( ! function_exists( 'rubik_ajax_masonry_3' ) ) {
    function rubik_ajax_masonry_3() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;
        
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $sec = $bk_ajax_c[$blockID]['sec'];
        
        $the_query = new WP_Query( $args );
        echo rubik_masonry_3::render_modules($the_query, $sec, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Square Grid
add_action( 'wp_ajax_square_grid', 'rubik_ajax_square_grid' );
add_action('wp_ajax_nopriv_square_grid', 'rubik_ajax_square_grid');
if ( ! function_exists( 'rubik_ajax_square_grid' ) ) {
    function rubik_ajax_square_grid() {
        $bk_ajax_c= isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID= isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked= isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $post_icon = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : 0;
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $sec = $bk_ajax_c[$blockID]['sec'];
        $the_query = new WP_Query( $args );
        echo rubik_square_grid::render_modules($the_query, $sec, $post_icon);
        die();
    }
}
// Large Blog
add_action( 'wp_ajax_large_blog', 'rubik_ajax_large_blog' );
add_action('wp_ajax_nopriv_large_blog', 'rubik_ajax_large_blog');
if ( ! function_exists( 'rubik_ajax_large_blog' ) ) {
    function rubik_ajax_large_blog() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $columnClass    = isset( $_POST['columnClass'] ) ? $_POST['columnClass'] : null;   
        $excerptLength  = isset( $_POST['excerptLength'] ) ? $_POST['excerptLength'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_large_blog::render_modules($the_query, $postIcon, $excerptLength, $columnClass);
        die();
    }
}
// Large Blog 2
add_action( 'wp_ajax_large_blog_2', 'rubik_ajax_large_blog_2' );
add_action('wp_ajax_nopriv_large_blog_2', 'rubik_ajax_large_blog_2');
if ( ! function_exists( 'rubik_ajax_large_blog_2' ) ) {
    function rubik_ajax_large_blog_2() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;   
        $columnClass    = isset( $_POST['columnClass'] ) ? $_POST['columnClass'] : null;   
        $excerptLength  = isset( $_POST['excerptLength'] ) ? $_POST['excerptLength'] : null;   
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_large_blog_2::render_modules($the_query, $postIcon, $excerptLength, $columnClass);
        die();
    }
}
// Classic Blog
add_action( 'wp_ajax_classic_blog', 'rubik_ajax_classic_blog' );
add_action('wp_ajax_nopriv_classic_blog', 'rubik_ajax_classic_blog');
if ( ! function_exists( 'rubik_ajax_classic_blog' ) ) {
    function rubik_ajax_classic_blog() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = 0;
        $the_query = new WP_Query( $args );
        echo rubik_classic_blog::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
/*block_2_fw*/
add_action( 'wp_ajax_block_2_fw', 'rubik_ajax_block_2_fw' );
add_action('wp_ajax_nopriv_block_2_fw', 'rubik_ajax_block_2_fw');
if ( ! function_exists( 'rubik_ajax_block_2_fw' ) ) {
    function rubik_ajax_block_2_fw() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        $args[ 'offset' ] = $offset;
        $the_query = new WP_Query( $args );
        echo rubik_block_2_fw::render_modules($the_query, $postIcon);
        die();
    }
}
/*post_block_1*/
add_action( 'wp_ajax_block_1', 'rubik_ajax_block_1' );
add_action('wp_ajax_nopriv_block_1', 'rubik_ajax_block_1');
if ( ! function_exists( 'rubik_ajax_block_1' ) ) {
    function rubik_ajax_block_1() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_1::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
/*post_block_2*/
add_action( 'wp_ajax_block_2', 'rubik_ajax_block_2' );
add_action('wp_ajax_nopriv_block_2', 'rubik_ajax_block_2');
if ( ! function_exists( 'rubik_ajax_block_2' ) ) {
    function rubik_ajax_block_2() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_2::render_modules($the_query, $postIcon);
        die();
    }
}
/*post_block_3*/
add_action( 'wp_ajax_block_3', 'rubik_ajax_block_3' );
add_action('wp_ajax_nopriv_block_3', 'rubik_ajax_block_3');
if ( ! function_exists( 'rubik_ajax_block_3' ) ) {
    function rubik_ajax_block_3() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;   
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_3::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
/*post_block_4*/
add_action( 'wp_ajax_block_4', 'rubik_ajax_block_4' );
add_action('wp_ajax_nopriv_block_4', 'rubik_ajax_block_4');
if ( ! function_exists( 'rubik_ajax_block_4' ) ) {
    function rubik_ajax_block_4() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null; 
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_4::render_modules($the_query, $postIcon);
        die();
    }
}
/*post_block_5*/
add_action( 'wp_ajax_block_5', 'rubik_ajax_block_5' );
add_action('wp_ajax_nopriv_block_5', 'rubik_ajax_block_5');
if ( ! function_exists( 'rubik_ajax_block_5' ) ) {
    function rubik_ajax_block_5() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_5::render_modules($the_query, $postIcon);
        die();
    }
}
/*post_block_6*/
add_action( 'wp_ajax_block_6', 'rubik_ajax_block_6' );
add_action('wp_ajax_nopriv_block_6', 'rubik_ajax_block_6');
if ( ! function_exists( 'rubik_ajax_block_6' ) ) {
    function rubik_ajax_block_6() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_6::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
/*post_block_7*/
add_action( 'wp_ajax_block_7', 'rubik_ajax_block_7' );
add_action('wp_ajax_nopriv_block_7', 'rubik_ajax_block_7');
if ( ! function_exists( 'rubik_ajax_block_7' ) ) {
    function rubik_ajax_block_7() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_7::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
/*post_block_8*/
add_action( 'wp_ajax_block_8', 'rubik_ajax_block_8' );
add_action('wp_ajax_nopriv_block_8', 'rubik_ajax_block_8');
if ( ! function_exists( 'rubik_ajax_block_8' ) ) {
    function rubik_ajax_block_8() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_8::render_modules($the_query, $postIcon, $bk_ajax_c[$blockID]['excerpt_length']);
        die();
    }
}
// Block 9, 10 use the ajax of 7, 6 

/*post_block_11*/
add_action( 'wp_ajax_block_11', 'rubik_ajax_block_11' );
add_action('wp_ajax_nopriv_block_11', 'rubik_ajax_block_11');
if ( ! function_exists( 'rubik_ajax_block_11' ) ) {
    function rubik_ajax_block_11() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_11::render_modules($the_query, $postIcon);
        die();
    }
}
/*post_block_12*/
add_action( 'wp_ajax_block_12', 'rubik_ajax_block_12' );
add_action('wp_ajax_nopriv_block_12', 'rubik_ajax_block_12');
if ( ! function_exists( 'rubik_ajax_block_12' ) ) {
    function rubik_ajax_block_12() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;    
        $tabClicked     = isset( $_POST['tabClicked'] ) ? $_POST['tabClicked'] : null;
        $postIcon       = isset( $_POST['postIcon'] ) ? $_POST['postIcon'] : null;   
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        if($tabClicked != 'tabfirst') {
            $args['category__in'] = $bk_ajax_c[$blockID]['tab'.$tabClicked]['cat'];
        }
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_block_12::render_modules($the_query, $postIcon);
        die();
    }
}
/*widget: latest 1*/
add_action( 'wp_ajax_latest_1', 'rubik_ajax_latest_1' );
add_action('wp_ajax_nopriv_latest_1', 'rubik_ajax_latest_1');
if ( ! function_exists( 'rubik_ajax_latest_1' ) ) {
    function rubik_ajax_latest_1() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_core::bk_widget_latest_post_1_render($the_query);
        die();
    }
}
/*widget: latest 2*/
add_action( 'wp_ajax_latest_2', 'rubik_ajax_latest_2' );
add_action('wp_ajax_nopriv_latest_2', 'rubik_ajax_latest_2');
if ( ! function_exists( 'rubik_ajax_latest_2' ) ) {
    function rubik_ajax_latest_2() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        
        $return_string = '';
        $return_string .= rubik_core::bk_widget_latest_post_2_render($the_query);
        
        die($return_string);
    }
}
/*widget: latest 3*/
add_action( 'wp_ajax_latest_3', 'rubik_ajax_latest_3' );
add_action('wp_ajax_nopriv_latest_3', 'rubik_ajax_latest_3');
if ( ! function_exists( 'rubik_ajax_latest_3' ) ) {
    function rubik_ajax_latest_3() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        
        $return_string = '';
        $return_string .= rubik_core::bk_widget_latest_post_3_render($the_query);
        
        die($return_string);
    }
}
/*widget: latest 4*/
add_action( 'wp_ajax_latest_4', 'rubik_ajax_latest_4' );
add_action('wp_ajax_nopriv_latest_4', 'rubik_ajax_latest_4');
if ( ! function_exists( 'rubik_ajax_latest_4' ) ) {
    function rubik_ajax_latest_4() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        
        $return_string = '';
        $return_string .= rubik_core::bk_widget_latest_post_4_render($the_query);
        
        die($return_string);
    }
}
/*widget: latest 5*/
add_action( 'wp_ajax_latest_5', 'rubik_ajax_latest_5' );
add_action('wp_ajax_nopriv_latest_5', 'rubik_ajax_latest_5');
if ( ! function_exists( 'rubik_ajax_latest_5' ) ) {
    function rubik_ajax_latest_5() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        
        $return_string = '';
        $return_string .= rubik_core::bk_widget_latest_post_5_render($the_query);
        
        die($return_string);
    }
}
/*widget: latest 6*/
add_action( 'wp_ajax_latest_6', 'rubik_ajax_latest_6' );
add_action('wp_ajax_nopriv_latest_6', 'rubik_ajax_latest_6');
if ( ! function_exists( 'rubik_ajax_latest_6' ) ) {
    function rubik_ajax_latest_6() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        
        $return_string = '';
        $return_string .= rubik_core::bk_widget_latest_post_6_render($the_query);
        
        die($return_string);
    }
}
/*widget: latest 7*/
add_action( 'wp_ajax_latest_7', 'rubik_ajax_latest_7' );
add_action('wp_ajax_nopriv_latest_7', 'rubik_ajax_latest_7');
if ( ! function_exists( 'rubik_ajax_latest_7' ) ) {
    function rubik_ajax_latest_7() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        
        $return_string = '';
        $return_string .= rubik_core::bk_widget_latest_post_7_render($the_query);
        
        die($return_string);
    }
}
/*widget: latest 8*/
add_action( 'wp_ajax_latest_8', 'rubik_ajax_latest_8' );
add_action('wp_ajax_nopriv_latest_8', 'rubik_ajax_latest_8');
if ( ! function_exists( 'rubik_ajax_latest_8' ) ) {
    function rubik_ajax_latest_8() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        
        $return_string = '';
        $return_string .= rubik_core::bk_widget_latest_post_8_render($the_query);
        
        die($return_string);
    }
}
/*widget: latest 9*/
add_action( 'wp_ajax_latest_9', 'rubik_ajax_latest_9' );
add_action('wp_ajax_nopriv_latest_9', 'rubik_ajax_latest_9');
if ( ! function_exists( 'rubik_ajax_latest_9' ) ) {
    function rubik_ajax_latest_9() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;     
        
        $args =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        
        $return_string = '';
        $return_string .= rubik_core::bk_widget_latest_post_9_render($the_query);
        
        die($return_string);
    }
}
/*Mega Menu Ajax*/
add_action( 'wp_ajax_megamenu_ajax', 'rubik_ajax_megamenu_ajax' );
add_action('wp_ajax_nopriv_megamenu_ajax', 'rubik_ajax_megamenu_ajax');
if ( ! function_exists( 'rubik_ajax_megamenu_ajax' ) ) {
    function rubik_ajax_megamenu_ajax() {
        $bk_cat_id    = isset( $_POST['bk_cat_id'] ) ? $_POST['bk_cat_id'] : null;
        $post_offset  = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;
        $entries      = isset( $_POST['entries'] ) ? $_POST['entries'] : null;
        
        $args = array( 
                        'post_type' => 'post',  
                        'post_status' => 'publish', 
                        'ignore_sticky_posts' => 1,  
                        'posts_per_page' => $entries
                );
        
        $args[ 'cat' ] = $bk_cat_id;
        
        $args[ 'offset' ] = $post_offset;
        
        $the_query = new WP_Query( $args );
        echo rubik_core::bk_get_megamenu_posts($the_query, $entries); 
        die();
    }
}
// Single Related Section
add_action( 'wp_ajax_related_author_posts_load', 'rubik_ajax_related_author_posts_load' );
add_action('wp_ajax_nopriv_related_author_posts_load', 'rubik_ajax_related_author_posts_load');
if ( ! function_exists( 'rubik_ajax_related_author_posts_load' ) ) {
    function rubik_ajax_related_author_posts_load() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;
        $args           =  $bk_ajax_c['s-author-articles']['args'];
                
        $the_query = new WP_Query( $args );
        echo single_core::bk_related_posts_layout($the_query);
        die();
    }
}
add_action( 'wp_ajax_related_category_posts_load', 'rubik_ajax_related_category_posts_load' );
add_action('wp_ajax_nopriv_related_category_posts_load', 'rubik_ajax_related_category_posts_load');
if ( ! function_exists( 'rubik_ajax_related_category_posts_load' ) ) {
    function rubik_ajax_related_category_posts_load() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;  
        $args           =  $bk_ajax_c['s-category-articles']['args'];
                        
        $the_query = new WP_Query( $args );
        echo single_core::bk_related_posts_layout($the_query);
        die();
    }
}
add_action( 'wp_ajax_related_posts_load', 'rubik_ajax_related_posts_load' );
add_action('wp_ajax_nopriv_related_posts_load', 'rubik_ajax_related_posts_load');
if ( ! function_exists( 'rubik_ajax_related_posts_load' ) ) {
    function rubik_ajax_related_posts_load() {
        $bk_ajax_c      = isset( $_POST['ajax_c'] ) ? $_POST['ajax_c'] : null;    
        $blockID        = isset( $_POST['blockID'] ) ? $_POST['blockID'] : null;  
        $offset         = isset( $_POST['post_offset'] ) ? $_POST['post_offset'] : null;
        $args           =  $bk_ajax_c[$blockID]['args'];
        
        $args[ 'offset' ] = $offset;
        
        $the_query = new WP_Query( $args );
        echo single_core::bk_related_posts_layout($the_query);
        die();
    }
}
add_action('wp_ajax_nopriv_bk_ajax_search', 'rubik_ajax_search');
add_action('wp_ajax_bk_ajax_search', 'rubik_ajax_search');
if (!function_exists('rubik_ajax_search')) {
    function rubik_ajax_search()
    {
        $html_render = new bk_contentout3;
        $custom_var_out3 = array (
            'thumbnail'    => 'rubik-90-65',
            'meta'         => array('date'),
        );
        $post_cnt = 0;
        $str = '';
        if (!empty($_POST['s'])) {
            $s = ($_POST['s']);
        } else {
            $s = '';
        }
        $the_query = rubik_query_search($s);
        if ( $the_query->have_posts() ) {
            if ($the_query->post_count >= 12) {
                $post_cnt = 12;
            }else {
                $post_cnt = $the_query->post_count;
            }
            $str .= '<ul class="s-list row">';
            foreach( range( 1, $post_cnt) as $i ):
                $the_query->the_post();
                $str .= '<li class="small-post content_out col-md-4 col-sm-6 clearfix">';
                $str .= $html_render->render($custom_var_out3);
                $str .= '</li><!-- End post -->';        
            endforeach;
            $str .= '</ul>';
            $str .= '<div class="result-msg"><a href="' . get_search_link($s) . '">' . esc_html__('View all results', 'rubik') . '</a></div>';
        } else $str .= '<div class="ajax-not-found">'.esc_html__('Not found', 'rubik').'</div>';
        $data = array(
            'content' => $str,
        );
        die(json_encode($data));
    }
}
add_action('wp_ajax_nopriv_bk_ajax_share', 'rubik_ajax_share');
add_action('wp_ajax_bk_ajax_share', 'rubik_ajax_share');
if (!function_exists('rubik_ajax_share')) {
    function rubik_ajax_share()
    {

    }
}
//Search Query
if (!function_exists('rubik_query_search')) {
    function rubik_query_search($search_data)
    {
        $args = array(
            's' => esc_sql($search_data),
            'post_type' => array('post'),
            'post_status' => 'publish',
        );

        $bk_query = new WP_Query($args);
        return $bk_query;
    }
};
//render search form
if (!function_exists('rubik_ajax_form_search')) {
    function rubik_ajax_form_search($moreClass = '', $topBar = 0)
    {
        $rubik_option = rubik_core::bk_get_global_var('rubik_option');
        $inputSearchPlaceholder = '';
        if($topBar == 1) {
            if (isset($rubik_option['bk-input-search-placeholder-top-bar'])) :
                $inputSearchPlaceholder = $rubik_option['bk-input-search-placeholder-top-bar'];
            endif;
        }else {
            if (isset($rubik_option['bk-input-search-placeholder-main-nav'])) :
                $inputSearchPlaceholder = $rubik_option['bk-input-search-placeholder-main-nav'];
            endif;
        }
        $str = '';
        $str .= '<div class="ajax-search-wrap '.$inputSearchPlaceholder.'">';
        $str .= '<div class="ajax-form-search ajax-search-icon '.$moreClass.'"><i class="fa fa-search"></i><i class="fa fa-times"></i></div>';
        $str .= '<form class="ajax-form" method="get" action="' . esc_url(home_url('/')) . '">';
        $str .= '<fieldset>';
        $str .= '<input type="text" class="field search-form-text" name="s" autocomplete="off" value="" placeholder="' . esc_attr__('Search and hit enter..', 'rubik') . '">';
        $str .= '</fieldset>';
        $str .= '</form>';
        $str .= ' <div class="ajax-search-result"></div></div>';
        return $str;
    }
}

/**
 * ************* update image feature for image post  *****************
 *---------------------------------------------------
 */ 
if ( ! function_exists( 'rubik_set_image_post_as_featured_image') ) {
    
    function rubik_set_image_post_as_featured_image($bkPostId) {
        $format = get_post_format( $bkPostId );
        if(($format == 'image') && (!rubik_core::bk_check_has_post_thumbnail($bkPostId))){
            $attachment_id = get_post_meta($bkPostId, 'bk_image_upload', true );
            set_post_thumbnail( $bkPostId, $attachment_id );
        }
    }
}    
add_action('save_post', 'rubik_set_image_post_as_featured_image', 100);
/**
 * BK Comments
 */
if ( ! function_exists( 'rubik_comments') ) {
    function rubik_comments($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment; ?>
		<li <?php comment_class(); ?>>
			<article id="comment-<?php comment_ID(); ?>" class="comment-article  media">
                <header class="comment-author clear-fix">
                    <div class="comment-avatar">
                        <?php echo get_avatar( $comment, 60 ); ?>  
                    </div>
                        <?php printf('<span class="comment-author-name">%s</span>', get_comment_author_link()) ?>
    					          <span class="comment-time" datetime="<?php comment_time('c'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>" class="comment-timestamp"><?php comment_time(__('j F, Y \a\t H:i', 'rubik')); ?> </a></span>
                        <span class="comment-links">
                            <?php
                                edit_comment_link(__('Edit', 'rubik'),'  ','');
                                comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'])));
                            ?>
                        </span>
                    </header><!-- .comment-meta -->
                
                <div class="comment-text">
                    				
    				<?php if ($comment->comment_approved == '0') : ?>
    				<div class="alert info">
    					<p><?php esc_html_e('Your comment is awaiting moderation.', 'rubik') ?></p>
    				</div>
    				<?php endif; ?>
    				<section class="comment-content">
    					<?php comment_text() ?>
    				</section>
                </div>
			</article>
		<!-- </li> is added by WordPress automatically -->
		<?php
    }
}
add_action('wp_footer', 'rubik_user_rating');

// User Rating System
if ( ! function_exists( 'rubik_user_rating' ) ) {
    function rubik_user_rating() {
        if (is_single()) {
            global $wp_query;
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $post = $wp_query->post;
            $bk_review_checkbox = get_post_meta( $post->ID, 'bk_review_checkbox', true );
            if ($bk_review_checkbox == 1) {
                $user_rating_script = "";            
                $user_rating_script.= " <script type='text/javascript'>
                var bkExistingOverlay=0, bkWidthDivider = 1, old_val=0, new_val=1;
                old_val = jQuery('#bk-rate').find('.bk-overlay').width();
                jQuery(window).resize(function(){
                    x = jQuery('#bk-rate').find('.bk-overlay').find('span').width();
                    y = jQuery('#bk-rate').find('.bk-overlay').width();
                    new_val = y;
                    if (new_val != old_val) {
                        bkExistingOverlay = ((x/old_val)*y).toFixed(0)+'px';
                        old_val = new_val;
                    }
                    bkWidthDivider = jQuery('#bk-rate').width() / 100;
                    jQuery('#bk-rate').find('.bk-overlay').find('span').css( {'width': bkExistingOverlay} );
                });
                (function ($) {'use strict';
                    var bkRate = $('#bk-rate'), 
                        bkCriteriaAverage = $('.bk-criteria-score.bk-average-score'),
                        bkRateCriteria = bkRate.find('.bk-criteria'),
                        bkRateOverlay = bkRate.find('.bk-overlay');
                            
                        var bkExistingOverlaySpan = bkRateOverlay.find('span'),
                            bkNotRated = bkRate.not('.bk-rated').find('.bk-overlay');
                            
                        bkExistingOverlay = bkExistingOverlaySpan.css('width');
                        bkExistingOverlaySpan.addClass('bk-zero-trigger');
                        
                    var bkExistingScore =  $(bkCriteriaAverage).text(),
                        bkExistingRateLine = $(bkRateCriteria).html(),
                        bkRateAmount  = $(bkRate).find('.bk-criteria span').text();
                        bkWidthDivider = ($(bkRate).width() / 100);
                        
                    if ( typeof bkExistingRateLine !== 'undefined' ) {
                        var bkExistingRatedLine = bkExistingRateLine.substr(0, bkExistingRateLine.length-1) + ')'; 
                    }
                    var bk_newRateAmount = parseInt(bkRateAmount) + 1;
                    if ( (bkRateAmount) === '0' ) {
                        var bkRatedLineChanged = '". esc_html__('Reader Rating', 'rubik') .": (' + (bk_newRateAmount) + ' ". esc_html__('Rate', 'rubik') .")';
                    } else {
                        var bkRatedLineChanged = '". esc_html__('Reader Rating', 'rubik') .": (' + (bk_newRateAmount) + ' ". esc_html__('Rates', 'rubik') .")';      
                    }
    
                    if (bkRate.hasClass('bk-rated')) {
                        bkRate.find('.bk-criteria').html(bkExistingRatedLine); 
                    }
    
                    bkNotRated.on('mousemove click mouseleave mouseenter', function(e) {
                        var bkParentOffset = $(this).parent().offset();  
                        ";

                        $user_rating_script.= "
                        var bkBaseX = Math.ceil((e.pageX - bkParentOffset.left) / bkWidthDivider);";

                        $user_rating_script.= "
                        var bkFinalX = (bkBaseX / 10).toFixed(1);
                        bkCriteriaAverage.text(bkFinalX);
                        
                        bkExistingOverlaySpan.css( 'width', (bkBaseX +'%') );
     
                        if ( e.type == 'mouseleave' ) {
                            bkExistingOverlaySpan.animate( {'width': bkExistingOverlay}, 300); 
                            bkCriteriaAverage.text(bkExistingScore); 
                        }
                        
                        if ( e.type == 'click' ) {
                                var bkFinalX = bkFinalX;
                                bkRateCriteria.fadeOut(550, function () {  $(this).fadeIn(550).html(bkRatedLineChanged);  });
                                var bkParentOffset = $(this).parent().offset(),
                                    nonce = $('input#rating_nonce').val(),
                                    bk_data_rates = { 
                                            action  : 'bk_rate_counter', 
                                            nonce   : nonce, 
                                            postid  : '". $post->ID ."' 
                                    },
                                    bk_data_score = { 
                                            action: 'bk_add_user_score', 
                                            nonce: nonce, 
                                            bkCurrentRates: bkRateAmount, 
                                            bkNewScore: bkFinalX, 
                                            postid: '". $post->ID ."' 
                                    };
                                
                                bkRateOverlay.off('mousemove click mouseleave mouseenter'); 
                                        
                                $.post('". admin_url('admin-ajax.php'). "', bk_data_rates, function(bk_rates) {
                                    if ( bk_rates !== '-1' ) {
                                        
                                        var bk_checker = cookie.get('bk_user_rating'); 
                                       
                                        if (!bk_checker) {
                                            var bk_rating_c = '" . $post->ID . "';
                                        } else {
                                            var bk_rating_c = bk_checker + '," . $post->ID . "';
                                        }
                                       cookie.set('bk_user_rating', bk_rating_c, { expires: 1, }); 
                                    } 
                                });
                                        
                                $.post('". admin_url('admin-ajax.php') ."', bk_data_score, function(bk_score) {
                                        var res = bk_score.split(' ');
                                        if ( ( res[0] !== '-1' ) && ( res[0] !=='null' ) ) {
                                            
                                                var bkScoreOverlay = (res[0]*10);
                                                var latestScore = res[1];
                                                var bkScore_display = (parseFloat(res[0])).toFixed(1);
                                                var overlay_w = jQuery('#bk-rate').find('.bk-overlay').width();
                                                
                                                var bkScoreOverlay_px = (bkScoreOverlay*overlay_w)/100;
                                                
                                                old_val = overlay_w;
                                                
                                                bkCriteriaAverage.html( bkScore_display ); 
                                               
                                                bkExistingOverlaySpan.css( 'width', bkScoreOverlay_px +'px' );
                                                bkRate.addClass('bk-rated');
                                                bkRateOverlay.addClass('bk-tipper-bottom').attr('data-title', '". esc_attr__('You have rated ', 'rubik') . "' + latestScore + ' points for this post');
                                                bkRate.off('click');
                                        } 
                                });
                                cookie.set('bk_score_rating', bkFinalX, { expires: 1, }); 
                                
                                return false;
                       }
                    });
                })(jQuery);
                </script>";
                echo rubik_core::bk_render_html_string($user_rating_script);
            }
        }
    }
}
if ( ! function_exists( 'rubik_rate_counter' ) ) {
    function rubik_rate_counter() {
        if ( ! wp_verify_nonce($_POST['nonce'], 'rating_nonce') ) { return; }
    
        $bk_post_id = $_POST['postid'];   
        $bk_current_rates = get_post_meta($bk_post_id, "bk_rates", true); 
        
        if ($bk_current_rates == NULL) {
             $bk_current_rates = 0; 
        }
        
        $bk_current_rates = intval($bk_current_rates);       
        $bk_new_rates = $bk_current_rates + 1;
        
        update_post_meta($bk_post_id, 'bk_rates', $bk_new_rates);
            
        die(0);
    }
}
add_action('wp_ajax_bk_rate_counter', 'rubik_rate_counter');
add_action('wp_ajax_nopriv_bk_rate_counter', 'rubik_rate_counter');

if ( ! function_exists( 'rubik_add_user_score' ) ) {
    function rubik_add_user_score() {
        
        if ( ! wp_verify_nonce($_POST['nonce'], 'rating_nonce')) { return; }

        $bk_post_id = $_POST['postid'];
        $bk_latest_score = floatval($_POST['bkNewScore']);
        $bk_current_rates = floatval($_POST['bkCurrentRates']);   
        
        $current_score = get_post_meta($bk_post_id, "bk_user_score_output", true);    

        if ($bk_current_rates == NULL) {
            $bk_current_rates = 0; 
        }

        if ($bk_current_rates == 0) {
            $bk_new_score =  $bk_latest_score ;
        }
        
        if ($bk_current_rates == 1) {
            $bk_new_score = round(floatval(( $current_score + $bk_latest_score  ) / 2),1);
        }
        if ($bk_current_rates > 1) {
            $current_score_total = ($current_score * $bk_current_rates );
            $bk_new_score = round(floatval(($current_score_total + $bk_latest_score) / ($bk_current_rates + 1)),1) ;
        }

        update_post_meta($bk_post_id, 'bk_user_score_output', $bk_new_score);
        $score_return = array();
        $score_return['bk_new_score'] = $bk_new_score;
        $score_return['bk_latest_score'] = $bk_latest_score;                 
        echo implode(" ",$score_return);
        die();
    }
}
add_action('wp_ajax_bk_add_user_score', 'rubik_add_user_score');
add_action('wp_ajax_nopriv_bk_add_user_score', 'rubik_add_user_score');


/**
 * wp_get_attachment
 * -------------------------------------------------
 */
if ( ! function_exists( 'wp_get_attachment' ) ) {
    function wp_get_attachment( $attachment_id ) {
    
        $attachment = get_post( $attachment_id );
        return array(
        	'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        	'caption' => $attachment->post_excerpt,
        	'description' => $attachment->post_content,
        	'href' => get_permalink( $attachment->ID ),
        	'src' => $attachment->guid,
        	'title' => $attachment->post_title
        );
    }
}

/**
 * ************* Pagination *****************
 *---------------------------------------------------
 */ 
if ( ! function_exists( 'rubik_paginate') ) {
    function rubik_paginate(){  
        global $wp_query, $wp_rewrite, $rubik_option; 
        if ( $wp_query->max_num_pages > 1 ) : ?>
        <div id="pagination" class="clear-fix">
        	<?php
        		$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
                
        		$pagination = array(
        			'base' => esc_url(add_query_arg( 'paged','%#%' )),
        			'format' => '',
        			'total' => $wp_query->max_num_pages,
        			'current' => $current,
        			'prev_text' => '<i class="fa fa-long-arrow-left"></i>',
        			'next_text' => '<i class="fa fa-long-arrow-right"></i>',
        			'type' => 'plain'
        		);
        		
        		if( $wp_rewrite->using_permalinks() )
        			$pagination['base'] = user_trailingslashit( trailingslashit( esc_url(remove_query_arg( 's', get_pagenum_link( 1 ) )) ) . 'page/%#%/', 'paged' );
        
        		if( !empty( $wp_query->query_vars['s'] ) )
        			$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
        
        		echo paginate_links( $pagination );

        	?>
        </div>
<?php
    endif;
    }
}
/**
 * ************* Module Pagination *****************
 *---------------------------------------------------
 */ 
if ( ! function_exists( 'rubik_module_paginate') ) {
    function rubik_module_paginate(){  
        global $wp_query, $wp_rewrite, $rubik_option; 
        $module_pagination = '';
        if ( $wp_query->max_num_pages > 1 ) :

        $module_pagination .= '<div id="pagination" class="clear-fix">';

        		$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

        		$pagination = array(
        			'base' => esc_url(add_query_arg( 'paged','%#%' )),
        			'format' => '',
        			'total' => $wp_query->max_num_pages,
        			'current' => $current,
        			'prev_text' => '<i class="fa fa-long-arrow-left"></i>',
        			'next_text' => '<i class="fa fa-long-arrow-right"></i>',
        			'type' => 'plain'
        		);
        		
        		if( $wp_rewrite->using_permalinks() )
        			$pagination['base'] = user_trailingslashit( trailingslashit( esc_url(remove_query_arg( 's', get_pagenum_link( 1 ) )) ) . 'page/%#%/', 'paged' );
        
        		if( !empty( $wp_query->query_vars['s'] ) )
        			$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
        
        		$module_pagination .=  paginate_links( $pagination );

        $module_pagination .= '</div>';
        
    endif;
    }
}
//Lets add Open Graph Meta Info
function insert_fb_in_head() {
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    if($rubik_option['bk-og-tag']) {
    	global $post;
    	if ( !is_single()) //if it is not a post or a page
    		return;
            echo '<meta property="og:title" content="' . get_the_title() . '"/>';
            echo '<meta property="og:type" content="article"/>';
            echo '<meta property="og:url" content="' . get_permalink() . '"/>';
            echo '<meta property="og:site_name" content="' . get_bloginfo("name") . '"/>';
    	if(!rubik_core::bk_check_has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
    		echo '<meta property="og:image" content=""/>';
    	}
    	else{
    		$bk_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
            if (!empty($bk_image[0])) {
                echo '<meta property="og:image" content="' .  $bk_image[0] . '" />';
            }
    	}
    	echo "
    ";
    }
}
add_action( 'wp_head', 'insert_fb_in_head', 1 );
/* Convert hexdec color string to rgb(a) string */
if ( ! function_exists( 'rubik_hex2rgba') ) {
    function rubik_hex2rgba($color, $opacity = false) {
    
    	$default = 'rgb(0,0,0)';
    
    	//Return default if no color provided
    	if(empty($color))
              return $default; 
    
    	//Sanitize $color if "#" is provided 
            if ($color[0] == '#' ) {
            	$color = substr( $color, 1 );
            }
    
            //Check if color has 6 or 3 characters and get values
            if (strlen($color) == 6) {
                    $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
            } elseif ( strlen( $color ) == 3 ) {
                    $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
            } else {
                    return $default;
            }
    
            //Convert hexadec to rgb
            $rgb =  array_map('hexdec', $hex);
    
            //Check if opacity is set(rgba or rgb)
            if($opacity){
            	if(abs($opacity) > 1)
            		$opacity = 1.0;
            	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
            } else {
            	$output = 'rgb('.implode(",",$rgb).')';
            }
    
            //Return rgb(a) color string
            return $output;
    }
}

function rubik_searchfilter($query) {
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    if(isset($rubik_option['search-result-setting']) && ($rubik_option['search-result-setting'] != 'all')) :
        if ($query->is_search && !is_admin() ) {
            $query->set('post_type',array('post'));
        }
    endif;
    return $query;
}
 
add_filter('pre_get_posts','rubik_searchfilter');