<?php
/**
 * The template for displaying Author archive pages
 *
 */
 ?> 
<?php get_header();?>
<?php
$rubik_option = rubik_core::bk_get_global_var('rubik_option');
$bkauthor = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
$bk_author_id = $bkauthor->ID;
$bk_author_name = get_the_author_meta('display_name', $bk_author_id); 
$sidebar_option_class = '';
$archiveLayout = '';
$bk_post_icon = 'hide';
if (isset($rubik_option) && ($rubik_option != '')): 
    $archiveLayout = $rubik_option['bk-author-layout'];
    if(isset($rubik_option['author_post_icon'])) {
        $bk_post_icon= $rubik_option['author_post_icon'];
    }else {
        $bk_post_icon = 'hide';
    }
    $sidebarPosition = $rubik_option['author-page-sidebar-position'];
    if($sidebarPosition == 'left'){
        $sidebar_option_class = 'has-left-sidebar';
    }else {
        $sidebar_option_class = '';
    }
endif;
?>
<div id="body-wrapper" class="wp-page bk-author-page">	
    <div class="bkwrapper container <?php if($sidebar_option_class != '') echo esc_attr($sidebar_option_class);?> <?php if(($archiveLayout == 'large-blog-nosb') || ($archiveLayout == 'large-blog-2-nosb')){echo ('page-large-blog-nosb');}?>">
        <div class="row bksection">			
            <div class="bk-author-content bkpage-content <?php if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3')) && (!($archiveLayout == 'large-blog-nosb')) && (!($archiveLayout == 'large-blog-2-nosb')) && (!($archiveLayout == 'row-nosb'))): echo 'col-md-8 has-sb'; else: echo 'col-md-12 fullwidth';  endif;?>">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo rubik_core::bk_author_details($bk_author_id); ?>
                    </div>
                    <div class="page-title-wrapper col-md-12">
                		<div class="module-title">
                            <h2 class="heading"><?php esc_html_e('BY:','rubik');?> <?php echo get_the_author_meta('display_name', $bk_author_id);?></h2>
                        </div>
                    </div>
                    <div id="main-content" class="clear-fix" role="main">
                		
                    <?php
                        if (have_posts()) {
                            echo rubik_archive::archive_content_area($archiveLayout, $bk_post_icon);
                        } 
                    ?>
            
    	            </div> <!-- end #main -->
                </div>
            </div> <!-- end #bk-content -->
            <?php
                if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3')) && (!($archiveLayout == 'large-blog-nosb')) && (!($archiveLayout == 'large-blog-2-nosb')) && (!($archiveLayout == 'row-nosb'))) {?>
                    <div class="sidebar col-md-4">
                        <div class="sidebar-wrap <?php if($rubik_option['author-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-author-sidebar">
                            <?php
                                if((isset($rubik_option['author-page-sidebar'])) && ($rubik_option['author-page-sidebar'] != '')){
                                    $sidebar = $rubik_option['author-page-sidebar'];
                                }else {
                                    $sidebar = '';
                                }
                                if (strlen($sidebar)) {
                                    dynamic_sidebar($sidebar);
                                }else {
                                    dynamic_sidebar();
                                }
                            ?>
                        </div>
                    </div>
                <?php }
            ?>
        </div>
    </div>
</div>   
<?php get_footer(); ?>