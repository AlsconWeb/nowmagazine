jQuery(document).ready(function ($) {
    $('.bk-current-iframe').fitVids();
    $(".bk-playlist-wrap").on('click', '.bk-playlist-items a', function (e) {

        e.preventDefault();

        var $this = $(this),
            $playlist_wrapper = $this.closest('.bk-playlist-wrap'),
            $player_wrapper = $this.closest('.bk-playlist-wrap').find('.bk-current-iframe'),
            ID = $(this).data('video-id'),
            URL = $player_wrapper.data('frame-url').replace('{video-id}',ID);

        //append video player
        $player_wrapper.html('<iframe \n        src="'+URL+'"\n        allowfullscreen="allowfullscreen"></iframe>');
        
        $player_wrapper.fitVids();
        
        //set current class for active li
        $this.closest('li')
            .addClass('bk-playing')
            .siblings('li')
            .removeClass('bk-playing');
        
        $thisVideoTitle = $this.find('.bk-video-title').text();
        $thisVideoTime = $this.find('.bk-video-time').text();
        
        $this.parents('.bk-playlist-videos').find('.bk-current-video-title').empty();
        $this.parents('.bk-playlist-videos').find('.bk-current-video-time').empty();
        
        $this.parents('.bk-playlist-videos').find('.bk-current-video-title').append($thisVideoTitle);
        $this.parents('.bk-playlist-videos').find('.bk-current-video-time').append($thisVideoTime);
    });


    (function fix_element_query(){

        if (typeof elementQuery != 'function') {
            return;
        }

        elementQuery({
            ".bk-playlist-wrap": {
                "max-width": ["480px","680px","780px"]
            }
        });

    })();

});