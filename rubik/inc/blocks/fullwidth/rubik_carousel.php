<?php
if (!class_exists('rubik_carousel')) {
    class rubik_carousel extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $block_str = '';
            $uid = uniqid('carousel-');
            $cfg_ops = array();
            
            $moduleAlign = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_align', true ); 
            $moduleStyle = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_style', true ); 
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            $moduleLayout = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_layout', true ); 
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $moduleHasbg = '';                                    
            $moduleHasbg = $rubik_dynamic_css[$uid]['moduleHasbg'];
            
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_carousel'], $page_info);    //get block config                        
            $sec = '';
            $has_bkwrapper = '';
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $has_bkwrapper = 'carousel_2';                 
            }else {
                $bk_layout = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_layout', true );
                if ($bk_layout == 'carousel-2') {
                    $bk_layout = 'carousel_2';
                }else if ($bk_layout == 'carousel-3'){
                    $bk_layout = 'carousel_3';
                }else if ($bk_layout == 'carousel-4'){
                    $bk_layout = 'carousel_4';
                }else if ($bk_layout == 'carousel-2-tall'){
                    $bk_layout = 'carousel_2 module-tall';
                }else if ($bk_layout == 'carousel-3-tall'){
                    $bk_layout = 'carousel_3 module-tall';
                }else if ($bk_layout == 'carousel-4-tall'){
                    $bk_layout = 'carousel_4 module-tall';
                }   
                
                if($moduleLayout == 'bkmodule-fw') {
                    $has_bkwrapper = $bk_layout;   
                }else {
                    $has_bkwrapper = 'bkwrapper container '.$bk_layout;   
                }
            }
            
            $the_query = bk_get_query::query($module_cfg);//get query

            if($the_query->post_count > 3) {
                $flexClass = ' flexslider';
            }else {
                $flexClass = '';
            }
            
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-carousel module-carousel-type1 hide '.$moduleAlign.' '.$moduleStyle.' '.$moduleHasbg.'">';
            $block_str .= '<div class="bkmodule-carousel-wrapper '.$has_bkwrapper.'">';
            if ( $the_query->have_posts() ) :
                if($moduleLayout == 'bkmodule-fw') {
                    $block_str .= '<div class="container bkwrapper">';
                    $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
                    $block_str .= '</div>';
                }else {
                    $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
                }
            endif;
            $block_str .= '<div class="bk-carousel-wrap'.$flexClass.'">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon);
            $block_str .= '</div></div></div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $bk_post_icon){
            $render_modules = '';
            $bk_contentin3 = new bk_contentin3;
            
            $custom_var = array (
                    'cat'           => 'on',
                    'post-icon'     => $bk_post_icon,
                    'meta'          => array('author', 'date'),
                    'thumbnail'     => 'rubik-730-400',
            );
            if ( $the_query->have_posts() ) :
                $render_modules .= '<ul class="slides clearfix">';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="content_in">';
                    $render_modules .= '<div class="content_in_wrapper">';
                    $render_modules .= $bk_contentin3->render($custom_var);
                    $render_modules .= '</div></li>';
                endwhile;
                $render_modules .= '</ul> <!-- Close render slider -->';
            endif;
            return $render_modules;
        }
        
    }
}