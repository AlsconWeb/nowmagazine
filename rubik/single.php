<?php    
    if(function_exists('rubik_single_save_cookie')) {
        $bkcookied = rubik_single_save_cookie();
    }else {
        $bkcookied = 1;
    }
    
    get_header();
    
    $bk_single_template = 'single_template_1';
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    if (isset($rubik_option) && ($rubik_option != '')): 
        $bk_single_template = $rubik_option['bk-single-template'];
    endif;
    
    if ( have_posts() ) : while ( have_posts() ) : the_post(); 
    
    $bkPostID = get_the_ID();  
    
    if ($bkcookied == 1) {
        rubik_core::bk_setPostViews($bkPostID);
    }

    $bkPostLayout = get_post_meta($bkPostID,'bk_post_layout',true);
    $postFormat = single_core::bk_post_format_detect($bkPostID);
    
    if(($bkPostLayout == 'global_settings') && (($bk_single_template == 'single_template_4') || ($bk_single_template == 'single_template_5') || ($bk_single_template == 'single_template_6'))) {
        if($postFormat['format'] != 'standard') {
            get_template_part( '/library/templates/single/single_template_1' );
        }
    }
    
    if($bkPostLayout == 'global_settings') {
        get_template_part( '/library/templates/single/'.$bk_single_template );
    }else if($bkPostLayout == 'single_template_1') {
        get_template_part( '/library/templates/single/single_template_1' );
    }else if($bkPostLayout == 'single_template_2') {
        get_template_part( '/library/templates/single/single_template_2' );
    }else if($bkPostLayout == 'single_template_3') {
        get_template_part( '/library/templates/single/single_template_3' );
    }else if($bkPostLayout == 'single_template_4') {
        get_template_part( '/library/templates/single/single_template_4' );
    }else if($bkPostLayout == 'single_template_5') {
        get_template_part( '/library/templates/single/single_template_5' );
    }else if($bkPostLayout == 'single_template_6') {
        get_template_part( '/library/templates/single/single_template_6' );
    }else if($bkPostLayout == 'single_template_7') {
        get_template_part( '/library/templates/single/single_template_7' );
    }else if($bkPostLayout == 'single_template_8') {
        get_template_part( '/library/templates/single/single_template_8' );
    }else if($bkPostLayout == 'single_template_9') {
        get_template_part( '/library/templates/single/single_template_9' );
    }else if($bkPostLayout == 'single_template_10') {
        get_template_part( '/library/templates/single/single_template_10' );
    }else if($bkPostLayout == 'single_template_11') {
        get_template_part( '/library/templates/single/single_template_11' );
    }else if($bkPostLayout == 'single_template_12') {
        get_template_part( '/library/templates/single/single_template_12' );
    }else if($bkPostLayout == 'single_template_13') {
        get_template_part( '/library/templates/single/single_template_13' );
    }else if($bkPostLayout == 'single_template_14') {
        get_template_part( '/library/templates/single/single_template_14' );
    }else if($bkPostLayout == 'single_template_15') {
        get_template_part( '/library/templates/single/single_template_15' );
    }else if($bkPostLayout == 'single_template_16') {
        get_template_part( '/library/templates/single/single_template_16' );
    }else if($bkPostLayout == 'single_template_17') {
        get_template_part( '/library/templates/single/single_template_17' );
    }else {
        get_template_part( '/library/templates/single/'.$bk_single_template );
    }
    
    endwhile; endif;
    
    get_footer(); 
?>