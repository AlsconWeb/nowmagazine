(function($) {
  "use strict";
    $=jQuery;
    jQuery(document).ready(function(){
        var module_id, offset;
        
        jQuery('.masonry-ajax').on('click','.ajax-load-btn.active',function(){
            var $this = jQuery(this);
            if($this.hasClass('nomore')){
                $this.text(ajax_btn_str['nomore']);
                return;
            }
            var module_id = $this.parents('.module-masonry').attr('id');
            var moduleName = module_id.split("-")[0];
            var entries = ajax_c[module_id]['entries'];
            var args =  ajax_c[module_id]['args'];
            var sec = ajax_c[module_id]['sec'];
            var post_icon = ajax_c[module_id]['post_icon'];
            
            jQuery('.ajax-load-btn').removeClass('active');
            $this.css("display", "none");
            $this.siblings('.loading-animation').css("display", "inline-block")
            
            if (($this.parents('.module-masonry').find('.bk-tabs').length > 0) && ($this.parents('.module-masonry').find('.bk-tabs').hasClass('active'))) {
                var tabActive = $this.parents('.module-masonry').find('.bk-tabs.active').attr('class').split(" ")[0].split("-")[2];
                var currentCatID = ajax_c[module_id]['tab'+tabActive]['cat'];
            }else {
                var tabActive = 'first';
                var currentCatID = ajax_c[module_id]['tab'+tabActive]['cat'];
            }

            var $container = $this.parent('.masonry-ajax').siblings('.bk-masonry-wrap').children().children('.bk-masonry-content');  
            
            if (tabActive == 'first') {
                var offset = parseInt($container.find('.item').length)+ parseInt(ajax_c[module_id]['offset']);
            }else {
                var offset = parseInt($container.find('.item').length);
            } 
            
            var ajaxAction = '';
            if(moduleName == 'masonry') {
                ajaxAction = 'masonry_load';
            }else if (moduleName == 'masonry_2') {
                ajaxAction = 'masonry_2_load';
            }else if (moduleName == 'masonry_3') {
                ajaxAction = 'masonry_3_load';
            }else {
                return;
            }
            
            var data = {
    				action			: ajaxAction,
                    ajax_c          : ajax_c,
                    blockID         : module_id,
                    post_offset     : offset,
                    args            : args,
                    currentCatID    : currentCatID,
    			};
    		jQuery.post( ajaxurl, data, function( respond ){
                var el = $(respond).hide().fadeIn('1500');
                var respond_length = el.find('.bk-mask').length;
                $container.append(el).masonry( 'appended', el );
                $container.imagesLoaded(function(){
                    setTimeout(function() {
            			var postionscroll = jQuery(window).scrollTop();
                            $container.masonry('destroy');
                            $container.masonry({
                                itemSelector: '.item',
                                columnWidth: 1,
                                isAnimated: true,
                                isFitWidth: true,
                             });
            			window.scrollTo(0,postionscroll);
                        jQuery($container).find('.post-c-wrap').removeClass('sink');
                        jQuery($container).find('.post-category').removeClass('sink');
                        jQuery($container).find('.thumb').removeClass('hide-thumb');
                        jQuery('.ajax-load-btn').addClass('active');
                        $this.find('.ajax-load-btn').text(ajax_btn_str['loadmore']);
                        
                        if(respond_length < entries){
                            $this.text(ajax_btn_str['nomore']);
                            $this.addClass('no-more');
                            $this.removeClass('active');
                        } 
                        $this.css("display", "inline-block");
                        $this.siblings('.loading-animation').css("display", "none");
                    }, 500);
                });
                run_review_canvas_script($container);
            });
        });
    // Blog Load Post
        jQuery('.blog-ajax').on('click','.ajax-load-btn.active',function(){
            var $this = jQuery(this);
            if($this.hasClass('nomore')){
                $this.text(ajax_btn_str['nomore']);
                return;
            }
            var module_id = $this.parents('.module-blog').attr('id');
            var entries = ajax_c[module_id]['entries'];
            var args =  ajax_c[module_id]['args'];
            var post_icon = ajax_c[module_id]['post_icon'];
            
            if (($this.parents('.module-blog').find('.bk-tabs').length > 0) && ($this.parents('.module-blog').find('.bk-tabs').hasClass('active'))) {
                var tabActive = $this.parents('.module-blog').find('.bk-tabs.active').attr('class').split(" ")[0].split("-")[2];
                var currentCatID = ajax_c[module_id]['tab'+tabActive]['cat'];
            }else {
                var tabActive = 'first';
                var currentCatID = ajax_c[module_id]['tab'+tabActive]['cat'];
            }
                    
            jQuery('.ajax-load-btn').removeClass('active');
            $this.css("display", "none");
            $this.siblings('.loading-animation').css("display", "inline-block")
             
            var $container = $this.parent('.blog-ajax').siblings('.row').children('.bk-blog-content');  
            if ($this.parent('.blog-ajax').hasClass('large-blog')){
                var bk_ajax_action = 'large_blog_load';
                var excerpt_length = ajax_c[module_id]['excerpt_length'];
                var columns_class = ajax_c[module_id]['columns_class'];
            }else if ($this.parent('.blog-ajax').hasClass('large-blog-2')){
                var bk_ajax_action = 'large_blog_2_load';
                var excerpt_length = ajax_c[module_id]['excerpt_length'];
                var columns_class = ajax_c[module_id]['columns_class'];
            }else {
                var bk_ajax_action = 'blog_load';
                var excerpt_length = '';
                var columns_number = '';
            }  
            
            if (tabActive == 'first') {
                var offset = parseInt($container.find('.item').length)+ parseInt(ajax_c[module_id]['offset']);
            }else {
                var offset = parseInt($container.find('.item').length);
            } 
             
            var data = {
                    action			: bk_ajax_action,
                    ajax_c          : ajax_c,
                    blockID         : module_id,
                    post_offset     : offset,
                    args            : args,
                    currentCatID    : currentCatID,         
                    columns_class   : columns_class,                            
    			};
    		jQuery.post( ajaxurl, data, function( respond ){
                var el = $(respond).hide().fadeIn('1500');
                var respond_length = el.find('.content_out').length;
                $container.append(el);
                $container.imagesLoaded(function(){
                    setTimeout(function() {
                        jQuery($container).find('.thumb').removeClass('hide-thumb');
                        jQuery('.ajax-load-btn').addClass('active');
                        $this.find('.ajax-load-btn').text(ajax_btn_str['loadmore']);
                        if(respond_length < entries){
                            $this.text(ajax_btn_str['nomore']);
                            $this.addClass('no-more');
                            $this.removeClass('active');
                        } 
                        $this.css("display", "inline-block");
                        $this.siblings('.loading-animation').css("display", "none");
                    }, 500);
                });
                run_review_canvas_script($container);
            });
        });
        // square-grid Load Post
        jQuery('.square-grid-ajax').on('click','.ajax-load-btn.active',function(){
            var $this = jQuery(this);
            if($this.hasClass('nomore')){
                $this.text(ajax_btn_str['nomore']);
                return;
            }
            var module_id = $this.parents('.module-square-grid').attr('id');
            var entries = ajax_c[module_id]['entries'];
            var sec = ajax_c[module_id]['sec'];
            var args =  ajax_c[module_id]['args'];
            var post_icon = ajax_c[module_id]['post_icon'];
            
            jQuery('.ajax-load-btn').removeClass('active');
            $this.css("display", "none");
            $this.siblings('.loading-animation').css("display", "inline-block")
             
            if (($this.parents('.module-square-grid').find('.bk-tabs').length > 0) && ($this.parents('.module-square-grid').find('.bk-tabs').hasClass('active'))) {
                var tabActive = $this.parents('.module-square-grid').find('.bk-tabs.active').attr('class').split(" ")[0].split("-")[2];
                var currentCatID = ajax_c[module_id]['tab'+tabActive]['cat'];
            }else {
                var tabActive = 'first';
                var currentCatID = ajax_c[module_id]['tab'+tabActive]['cat'];
            }
            
            var $container = $this.parent('.square-grid-ajax').siblings('.bk-square-grid-wrap').children().children('.bk-square-grid-content');     
            
            if (tabActive == 'first') {
                var offset = parseInt($container.find('.content_in_wrapper').length)+ parseInt(ajax_c[module_id]['offset']); 
            }else {
                var offset = parseInt($container.find('.item').length);
            } 

            var data = {
    				action			: 'square_grid_load',
                    post_offset     : offset,
                    entries         : entries,
                    sec             : sec,
                    postIcon       : post_icon,
                    args            : args,
                    currentCatID    : currentCatID,
    			};
    		jQuery.post( ajaxurl, data, function( respond ){
                var el = $(respond).hide().fadeIn('1500');
                var respond_length = el.find('.content_in_wrapper').length;
                $container.append(el);
                $container.imagesLoaded(function(){
                    setTimeout(function() {
                        jQuery($container).find('.thumb').removeClass('hide-thumb');
                        jQuery('.ajax-load-btn').addClass('active');
                        $this.find('.ajax-load-btn').text(ajax_btn_str['loadmore']);
                        if(respond_length < entries){
                            $this.text(ajax_btn_str['nomore']);
                            $this.addClass('no-more');
                            $this.removeClass('active');
                        } 
                        $this.css("display", "inline-block");
                        $this.siblings('.loading-animation').css("display", "none");
                    }, 500);
                });
                run_review_canvas_script($container);
            });
        });
        
        
        // Blog Row
        jQuery('.row-ajax').on('click','.ajax-load-btn.active',function(){
            var $this = jQuery(this);
            if($this.hasClass('nomore')){
                $this.text(ajax_btn_str['nomore']);
                return;
            }
            var module_id = $this.parents('.module-row').attr('id');
            var entries = ajax_c[module_id]['entries'];
            var args =  ajax_c[module_id]['args'];
            var post_icon = ajax_c[module_id]['post_icon'];
            
            if (($this.parents('.module-row').find('.bk-tabs').length > 0) && ($this.parents('.module-row').find('.bk-tabs').hasClass('active'))) {
                var tabActive = $this.parents('.module-row').find('.bk-tabs.active').attr('class').split(" ")[0].split("-")[2];
                var currentCatID = ajax_c[module_id]['tab'+tabActive]['cat'];
            }else {
                var tabActive = 'first';
                var currentCatID = ajax_c[module_id]['tab'+tabActive]['cat'];
            }
            jQuery('.ajax-load-btn').removeClass('active');
            $this.css("display", "none");
            $this.siblings('.loading-animation').css("display", "inline-block")
             
            var container = $this.parents('.module-row').find('.bk-module-inner');  

            var bk_ajax_action = 'row_fw_load';
            
            var module_row_type = 'normal';
            var module_row_section = '';
            
            if($this.parents('.module-row').hasClass('tall')) {
                module_row_type = 'tall';
            }else {
                module_row_type = 'normal';
            }
            
            if($this.parents('.module-row').hasClass('layout-1')){
                module_row_type +=  '-layout-1';
            }else if($this.parents('.module-row').hasClass('layout-2')){
                module_row_type +=  '-layout-2';
            }else if($this.parents('.module-row').hasClass('layout-3')){
                module_row_type +=  '-layout-3';
            }else if($this.parents('.module-row').hasClass('layout-4')){
                module_row_type +=  '-layout-4';
            }
            
            if($this.parents('.module-row').hasClass('fw-module')){
                module_row_section =  'fullwidth';
            }else {
                module_row_section =  'has-sb';
            }
            
            if (tabActive == 'first') {
                var offset = parseInt(container.find('.content_out').length)+ parseInt(ajax_c[module_id]['offset']);
            }else {
                var offset = parseInt(container.find('.content_out').length);
            } 

            var data = {
                    action			: bk_ajax_action,
                    ajax_c          : ajax_c,
                    blockID         : module_id,
                    post_offset     : offset,
                    args            : args,
                    currentCatID    : currentCatID, 
                    module_row_type : module_row_type,
                    module_row_section : module_row_section,         
    			};
    		jQuery.post( ajaxurl, data, function( respond ){
                var el = $(respond).hide().fadeIn('1500');
                var respond_length = el.find('.post-c-wrap').length;
                container.append(el);
                container.imagesLoaded(function(){
                    setTimeout(function() {
                        jQuery(container).find('.thumb').removeClass('hide-thumb');
                        jQuery('.ajax-load-btn').addClass('active');
                        $this.find('.ajax-load-btn').text(ajax_btn_str['loadmore']);
                        if(respond_length < entries){
                            $this.text(ajax_btn_str['nomore']);
                            $this.addClass('no-more');
                            $this.removeClass('active');
                        } 
                        $this.css("display", "inline-block");
                        $this.siblings('.loading-animation').css("display", "none");
                    }, 500);
                });
                run_review_canvas_script(container);
            });
        });
        
        
        
        jQuery('.bk-tabs').on('click',function(e){
            e.preventDefault();
            
            if(($(this).hasClass('active')) || ($(this).hasClass('disable-click'))) {
                return;
            }

            // Check Load More Class Of Current Tab
            if($(this).parents('.bkmodule').find('.ajax-load-btn').hasClass('no-more')) {
                var ajaxLoadmore = 'stop';
            }else {
                var ajaxLoadmore = 'running';
            }
            
            $(this).parents('.bkmodule').find('.pagination-btn').removeClass('disable-click');
            
            var blockID = $(this).parents('.bkmodule').attr('id');
            var blockName = blockID.split("-")[0];
            var tabClicked = $(this).attr('class').split(" ")[0].split("-")[2];
            
            if ((ajax_c[blockID]['tab'+tabClicked]['offset'] == 0) || (ajax_c[blockID]['tab'+tabClicked]['prev'] == 1))  {
                $(this).parents('.bkmodule').find('.pagination-btn.prev').addClass('disable-click');
            }else if (ajax_c[blockID]['tab'+tabClicked]['next'] == 1) {
                $(this).parents('.bkmodule').find('.pagination-btn.next').addClass('disable-click');
            }
            
            $('.bk-tabs').each(function(){
                $(this).addClass('disable-click');
            });
            $('.bk-tab-original').each(function(){
                $(this).addClass('disable-click');
            });
            $(this).parents('.bk-tabs-wrapper').siblings('.bk-tab-original').removeClass('active');

            var previous_tab;
            if ($(this).siblings().hasClass('active')) {
                previous_tab = $(this).siblings('.bk-tabs.active').attr('class').split(" ")[0].split("-")[2];  //Save Previous tab number
            }else {
                previous_tab = 'first';
            }
            
            $(this).siblings('.bk-tabs').removeClass('active');
            $(this).addClass('active');
                        
            if((blockName === 'masonry') || (blockName === 'masonry_2') || (blockName === 'masonry_3')) {
                var container = $(this).parents('.module-title').siblings('.bk-masonry-wrap').children().children();
            }else if (blockName === 'square_grid') {
                var container = $(this).parents('.module-title').siblings('.bk-square-grid-wrap').children().children();
            }else if((blockName === 'classic_blog') || (blockName === 'large_blog') || (blockName === 'large_blog_2')){
                var container = $(this).parents('.module-title').siblings('.bk-module-inner').children();
            }else {
                var container = $(this).parents('.module-title').siblings('.bk-module-inner');
            }
            
            if ((blockName === 'large_blog') || (blockName === 'large_blog_2')){
                var columnClass = ajax_c[blockID]['columns_class'];
                var postIcon    = ajax_c[blockID]['post_icon'];
                var excerptLength = ajax_c[blockID]['excerpt_length'];
            }else if (blockName === 'classic_blog'){
                var columnClass = '';
                var postIcon    = ajax_c[blockID]['post_icon'];
                var excerptLength = '';
            }else if ((blockName === 'masonry') || (blockName === 'masonry_2') || (blockName === 'masonry_3')){
                var columnClass = '';
                var postIcon    = ajax_c[blockID]['post_icon'];
                var excerptLength = '';
            }else {
                var columnClass = '';
                var postIcon    = ajax_c[blockID]['post_icon'];
                var excerptLength = '';
            }
            
            container.append('<div class="bk-preload-wrapper"></div>');
            container.find('li').addClass('bk-preload-blur');
            container.find('.large-post').addClass('bk-preload-blur');            
            container.find('.bk-preload-wrapper').addClass('term-'+ajax_c[blockID]['tab'+tabClicked]['cat']);
            
            if ((previous_tab == 'first') || (ajax_c[blockID]['tab'+previous_tab]['content'] == '') || (ajax_c[blockID]['tab'+previous_tab]['content'] !== container[0].innerHTML)) {
                ajax_c[blockID]['tab'+previous_tab]['content'] = container[0].innerHTML;
                ajax_c[blockID]['tab'+previous_tab]['loadmore'] = ajaxLoadmore;                               
            }

            if (ajax_c[blockID]['tab'+tabClicked]['content'] == '') {
                var data = {
        				action			: blockName,
                        ajax_c          : ajax_c,
                        post_offset     : 0,                        
                        blockID         : blockID,
                        tabClicked      : tabClicked,
                        columnClass     : columnClass,
                        postIcon        : postIcon,
                        excerptLength   : excerptLength,
        			};
                jQuery.post( ajaxurl, data, function( respond ){
                    var el = $(respond).hide().fadeIn('1500');
                    container.empty();
                    container.append(el);
                    
                    container.parents('.bkmodule ').find('.module-title').attr('class','module-title');
                    container.parents('.bkmodule ').find('.module-title').addClass('module-term-'+ajax_c[blockID]['tab'+tabClicked]['cat']);

                    //Put the correct status of Load More button to the module
                    if((ajax_c[blockID]['tab'+tabClicked]['loadmore']) == 'stop') {
                        container.parents('.bkmodule').find('.ajax-load-btn').removeClass('active');
                        container.parents('.bkmodule').find('.ajax-load-btn').addClass('no-more');
                        container.parents('.bkmodule').find('.ajax-load-btn').text(ajax_btn_str['nomore']);
                    }else {
                        container.parents('.bkmodule').find('.ajax-load-btn').addClass('active');
                        container.parents('.bkmodule').find('.ajax-load-btn').removeClass('no-more');
                        container.parents('.bkmodule').find('.ajax-load-btn').text(ajax_btn_str['loadmore']);
                    }
                    
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    container.find('.large-post').removeClass('bk-preload-blur');
                    
                    container.imagesLoaded(function(){
                        setTimeout(function() {
                            jQuery(container).find('.thumb').removeClass('hide-thumb');
                            if ((blockName === 'masonry') || (blockName === 'masonry_2') || (blockName === 'masonry_3')){
                                jQuery(container).find('.post-c-wrap').removeClass('sink');
                                jQuery(container).find('.post-category').removeClass('sink');
                                var postionscroll = jQuery(window).scrollTop();
                                container.masonry('destroy');
                                container.masonry({
                                    itemSelector: '.item',
                                    columnWidth: 1,
                                    isAnimated: true,
                                    isFitWidth: true,
                                });
                                window.scrollTo(0,postionscroll);
                            }
                            $('.bk-tabs').each(function(){
                                $(this).removeClass('disable-click');
                            });
                            $('.bk-tab-original').each(function(){
                                $(this).removeClass('disable-click');
                            });
                        }, 500);
                    });
                    run_review_canvas_script(container);
                });
            }else {
                var el = $(ajax_c[blockID]['tab'+tabClicked]['content']).hide().fadeIn('1500');
                container.empty();
                container.append(el);
                
                container.parents('.bkmodule ').find('.module-title').attr('class','module-title');
                container.parents('.bkmodule ').find('.module-title').addClass('module-term-'+ajax_c[blockID]['tab'+tabClicked]['cat']);
                    
                //Put the correct status of Load More button to the module
                if((ajax_c[blockID]['tab'+tabClicked]['loadmore']) == 'stop') {
                    container.parents('.bkmodule').find('.ajax-load-btn').removeClass('active');
                    container.parents('.bkmodule').find('.ajax-load-btn').addClass('no-more');
                    container.parents('.bkmodule').find('.ajax-load-btn').text(ajax_btn_str['nomore']);
                }else {
                    container.parents('.bkmodule').find('.ajax-load-btn').addClass('active');
                    container.parents('.bkmodule').find('.ajax-load-btn').removeClass('no-more');
                    container.parents('.bkmodule').find('.ajax-load-btn').text(ajax_btn_str['loadmore']);
                }
            
                container.find('.bk-preload-wrapper').remove();
                container.find('li').removeClass('bk-preload-blur');
                container.find('.large-post').removeClass('bk-preload-blur');
                
                container.imagesLoaded(function(){
                    setTimeout(function() {
                        jQuery(container).find('.thumb').removeClass('hide-thumb');
                        if ((blockName === 'masonry') || (blockName === 'masonry_2') || (blockName === 'masonry_3')){
                            jQuery(container).find('.post-c-wrap').removeClass('sink');
                            jQuery(container).find('.post-category').removeClass('sink');
                            var postionscroll = jQuery(window).scrollTop();
                            container.masonry('destroy');
                            container.masonry({
                                itemSelector: '.item',
                                columnWidth: 1,
                                isAnimated: true,
                                isFitWidth: true,
                            });
                            window.scrollTo(0,postionscroll);
                        }
                        $('.bk-tabs').each(function(){
                            $(this).removeClass('disable-click');
                        });
                        $('.bk-tab-original').each(function(){
                            $(this).removeClass('disable-click');
                        });
                    }, 500);
                });
                run_review_canvas_script(container);
            }

        });
        jQuery('.bk-tab-original').on('click',function(e){
            e.preventDefault();
            if(($(this).hasClass('active')) || ($(this).hasClass('disable-click'))) {
                return;
            }
            // Check Load More Class Of the Tab
            if($(this).parents('.bkmodule').find('.ajax-load-btn').hasClass('no-more')) {
                var ajaxLoadmore = 'stop';
            }else {
                var ajaxLoadmore = 'running';
            }
            
            var previous_tab;
            
            if ($(this).siblings('.bk-tabs-wrapper').find('.bk-tabs').hasClass('active')) {
                previous_tab = $(this).siblings('.bk-tabs-wrapper').find('.bk-tabs.active').attr('class').split(" ")[0].split("-")[2];  //Save Previous tab number
            }else {
                previous_tab = '';
            }

            $('.bk-tab-original').addClass('disable-click');
            $(this).siblings('.bk-tabs-wrapper').find('.bk-tabs').removeClass('active');
            $('.bk-tabs').each(function(){
                $(this).addClass('disable-click');
            });
            $(this).addClass('active');
            var blockID = $(this).parents('.bkmodule').attr('id');
            var blockName = blockID.split("-")[0];
            
            $(this).parents('.bkmodule').find('.pagination-btn').removeClass('disable-click');
            
            if ((ajax_c[blockID]['tabfirst']['offset'] == 0) || (ajax_c[blockID]['tabfirst']['prev'] == 1))  {
                $(this).parents('.bkmodule').find('.pagination-btn.prev').addClass('disable-click');
            }else if (ajax_c[blockID]['tabfirst']['next'] == 1) {
                $(this).parents('.bkmodule').find('.pagination-btn.next').addClass('disable-click');
            }
            
            if ((blockName === 'masonry') || (blockName === 'masonry_2') || (blockName === 'masonry_3')){
                var container = $(this).parents('.module-title').siblings('.bk-masonry-wrap').children().children();
            }else if (blockName === 'square_grid') {
                var container = $(this).parents('.module-title').siblings('.bk-square-grid-wrap').children().children();
            }else if((blockName === 'classic_blog') || (blockName === 'large_blog') || (blockName === 'large_blog_2')){
                var container = $(this).parents('.module-title').siblings('.bk-module-inner').children();
            }else {
                var container = $(this).parents('.module-title').siblings('.bk-module-inner');
            }

            container.append('<div class="bk-preload-wrapper"></div>');
            container.find('li').addClass('bk-preload-blur');
            container.find('.large-post').addClass('bk-preload-blur');
                        
            if ((ajax_c[blockID]['tab'+previous_tab]['content'] == '') || (ajax_c[blockID]['tab'+previous_tab]['content'] !== container[0].innerHTML)) {
                ajax_c[blockID]['tab'+previous_tab]['content'] = container[0].innerHTML;
                ajax_c[blockID]['tab'+previous_tab]['loadmore'] = ajaxLoadmore;                               
            }
            
            var el = $(ajax_c[blockID]['tabfirst']['content']).hide().fadeIn('1500');
            container.empty();
            container.append(el);
            
            container.parents('.bkmodule ').find('.module-title').attr('class','module-title');
            
            container.find('.bk-preload-wrapper').remove();
            container.find('li').removeClass('bk-preload-blur');
            container.find('.large-post').removeClass('bk-preload-blur');
            
            //Put the correct status of Load More button to the module
            if((ajax_c[blockID]['tabfirst']['loadmore']) == 'stop') {
                container.parents('.bkmodule').find('.ajax-load-btn').removeClass('active');
                container.parents('.bkmodule').find('.ajax-load-btn').addClass('no-more');
                container.parents('.bkmodule').find('.ajax-load-btn').text(ajax_btn_str['nomore']);
            }else {
                container.parents('.bkmodule').find('.ajax-load-btn').addClass('active');
                container.parents('.bkmodule').find('.ajax-load-btn').removeClass('no-more');
                container.parents('.bkmodule').find('.ajax-load-btn').text(ajax_btn_str['loadmore']);
            }
                
            container.imagesLoaded(function(){
                setTimeout(function() {
                    jQuery(container).find('.thumb').removeClass('hide-thumb');
                    if ((blockName === 'masonry') || (blockName === 'masonry_2') || (blockName === 'masonry_3')){
                        jQuery(container).find('.post-c-wrap').removeClass('sink');
                        jQuery(container).find('.post-category').removeClass('sink');
                        var postionscroll = jQuery(window).scrollTop();
                        container.masonry('destroy');
                        container.masonry({
                            itemSelector: '.item',
                            columnWidth: 1,
                            isAnimated: true,
                            isFitWidth: true,
                        });
                        window.scrollTo(0,postionscroll);
                    }
                    $('.bk-tabs').each(function(){
                        $(this).removeClass('disable-click');
                    });
                    $('.bk-tab-original').each(function(){
                        $(this).removeClass('disable-click');
                    });
                }, 500);
            });
            run_review_canvas_script(container);
        });
        
        //Megamenu
        jQuery('.bk-mega-menu').find('li.menu-item.menu-item-object-category > a').on('click',function(e){
            
            e.preventDefault();
                        
            if($(this).parent().hasClass('active')) {
                return;
            }
            var currentCatAjax = $(this);
            
            var container = $(this).parents('.bk-sub-menu-wrap').siblings('.bk-with-sub').children('ul');
            
            $('.megamenu-pagination-btn').removeClass('disable-click');
            $('.megamenu-pagination-btn.prev').addClass('disable-click');
            
            $(this).parents('.bk-mega-menu ').find('.menu-item').removeClass('active');
                        
            var AllThisCatClassArray = $(this).parent().attr('class').split(' ');

            var tempArray = AllThisCatClassArray[AllThisCatClassArray.length-1].split('-');
            var thisCatClassID = tempArray[tempArray.length - 1];
            
            ajax_c['mega-'+thisCatClassID]['offset'] = 0;
            
            $(this).parents('.bk-mega-menu ').find('.bk-megamenu-category-link').attr('href', ajax_c['megamenu'][thisCatClassID]['cat-link']);
            
            var entries = 4;
            
            container.append('<div class="bk-preload-wrapper"></div>');
            container.find('li').addClass('bk-preload-blur');
            container.find('.bk-preload-wrapper').addClass('term-'+thisCatClassID);

            $(this).parent().addClass('active');
            
            if((ajax_c['megamenu'][thisCatClassID]['content']) == '') {
                var data = {
        				action			: 'megamenu_ajax',
                        bk_cat_id       : thisCatClassID,
                        post_offset     : 0,
                        entries         : entries,
        			};
                jQuery.post( ajaxurl, data, function( respond ){
                    ajax_c['megamenu'][thisCatClassID]['content'] = respond;
                    var el = $(respond).hide().fadeIn('1500');
                    if(currentCatAjax.parent().hasClass('active')) {
                        container.empty();
                        container.append(el);
                        container.find('.post-title a').addClass('term-'+thisCatClassID);
                    }
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    container.attr('title', 'mega-'+thisCatClassID);
                    run_review_canvas_script(container);
                });
            }else {
                var respond = ajax_c['megamenu'][thisCatClassID]['content'];
                var el = $(respond).hide().fadeIn('1500');
                if(currentCatAjax.parent().hasClass('active')) {
                    container.empty();
                    container.append(el);
                    container.find('.post-title a').addClass('term-'+thisCatClassID);
                }
                container.find('.bk-preload-wrapper').remove();
                container.find('li').removeClass('bk-preload-blur');
                container.attr('title', 'mega-'+thisCatClassID);
                run_review_canvas_script(container);
            }
            
        });
        $('.bk-mega-menu').mouseleave(function(){
            $(this).find('.menu-item').removeClass('active');
            var AllThisCatClassArray = $(this).parent().attr('class').split(' ');
            var tempArray = AllThisCatClassArray[AllThisCatClassArray.length-1].split('-');
            var thisCatClassID = tempArray[tempArray.length - 1];
            
            ajax_c['mega-'+thisCatClassID]['offset'] = 0;
            
            $(this).find('.bk-megamenu-category-link').attr('href', ajax_c['megamenu'][thisCatClassID]['cat-link']);
                        
            var container = $(this).children('.bk-sub-menu-wrap').siblings('.bk-with-sub').children('ul');
            
            var entries = 0;
            
            if ($(this).find('.bk-sub-posts').hasClass('bk-with-sub')) {
                entries = 4;
            }else {
                entries = 4;
            }
            $('.megamenu-pagination-btn').removeClass('disable-click');
            $('.megamenu-pagination-btn.prev').addClass('disable-click');
            
            container.append('<div class="bk-preload-wrapper"></div>');
            container.find('li').addClass('bk-preload-blur');
            container.find('.bk-preload-wrapper').addClass('term-'+thisCatClassID);
            
            if((ajax_c['megamenu'][thisCatClassID]['content']) == '') {
                var data = {
        				action			: 'megamenu_ajax',
                        bk_cat_id       : thisCatClassID,
                        post_offset     : 0,
                        entries         : entries,
        			};
                jQuery.post( ajaxurl, data, function( respond ){
                    ajax_c['megamenu'][thisCatClassID]['content'] = respond;
                    var el = $(respond).hide().fadeIn('1500');

                    container.empty();
                    container.append(el);

                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    
                    container.attr('title', 'mega-'+thisCatClassID);
                    run_review_canvas_script(container);
                });
            }else {
                var respond = ajax_c['megamenu'][thisCatClassID]['content'];
                var el = $(respond).hide().fadeIn('1500');

                container.empty();
                container.append(el);

                container.find('.bk-preload-wrapper').remove();
                container.find('li').removeClass('bk-preload-blur');
                
                container.attr('title', 'mega-'+thisCatClassID);
                run_review_canvas_script(container);
            }
        });
        
        jQuery('.megamenu-pagination-btn').on('click',function(e){
            
            e.preventDefault();
            
            var $this = $(this);
            if($(this).hasClass('disable-click')) {
                return;
            }
            
            $(this).addClass('disable-click');
            
            $(this).siblings().addClass('disable-click');
            
            var menucatID = $(this).parent('.bk-megamenu-pagination').siblings().attr('title');
            
            var currentCatID = menucatID.split('-')[1];
            
            var container = $(this).parents('.bk-megamenu-pagination').siblings('.mega-row');
            
            container.css('height', container.height()+'px');
            
            var offset = 0;
            
            var entries = 0;
            
            if ($(this).parents('.bk-mega-menu').find('.bk-sub-posts').hasClass('bk-with-sub')) {
                entries = 4;
            }else {
                entries = 4;
            }
            
            if($(this).hasClass('next')) {
                offset = ajax_c[menucatID]['offset'] = ajax_c[menucatID]['offset'] + entries;
            }else if($(this).hasClass('prev')) {
                offset = ajax_c[menucatID]['offset'] = ajax_c[menucatID]['offset'] - entries;
            }else {
                return;
            }
            container.append('<div class="bk-preload-wrapper"></div>');
            container.find('li').addClass('bk-preload-blur');        
            var data = {
        				action			: 'megamenu_ajax',
                        post_offset     : offset,
                        bk_cat_id       : currentCatID,
                        entries         : entries,
        			};
            
            jQuery.post( ajaxurl, data, function( respond ){
                var el = $(respond).hide().fadeIn('1500');
                var respond_length = el.find('.meta').length;

                if(respond_length != 0) {
                    
                    container.empty();
                    container.append(el);
    
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');

                    if ($this.hasClass('next')) {
                        if(respond_length < entries) {
                            $this.addClass('disable-click');
                        }else {
                            $this.removeClass('disable-click');
                        }
                        $this.siblings().removeClass('disable-click');
                    }else {
                        if(offset <= 0) {
                            $this.addClass('disable-click');
                        }else {
                            $this.removeClass('disable-click');
                        }
                        $this.siblings().removeClass('disable-click');
                    }
                    
                    container.imagesLoaded(function(){
                        container.css('height', 'auto');
                    });
                }else {
                    if ($this.hasClass('next')) {
                        ajax_c[menucatID]['offset'] = ajax_c[menucatID]['offset'] - entries;
                    }
                    $this.addClass('disable-click');
                    $this.siblings().removeClass('disable-click');
                    
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    container.css('height', 'auto');
                }
                run_review_canvas_script(container);
            });
        });
        jQuery('.pagination-btn').on('click',function(e){
            
            e.preventDefault();
            
            var $this = $(this);
            
            if($this.hasClass('nomore')){
                return;
            }
            
            if($this.hasClass('disable-click')) {
                return;
            }
            
            $this.addClass('disable-click');
            
            $this.siblings('.pagination-btn').addClass('disable-click');
            
            var blockID = $this.parents('.bkmodule').attr('id');
            
            var blockName = blockID.split("-")[0];
            
            var tabClicked, tabCurrent;
            
            if ($this.parents('.bkmodule').find('.bk-tabs').length){
                if ($this.parents('.bkmodule').find('.bk-tab-original').hasClass('active')) {
                    
                    tabClicked = 'tabfirst';
                    
                    tabCurrent = 'tabfirst';
                    
                }else {
                    
                    tabClicked = $this.parents('.bkmodule').find('.bk-tabs.active').attr('class').split(" ")[0].split("-")[2];
                    
                    tabCurrent = 'tab'+tabClicked;
                    
                }
            }else {
                tabClicked = 'tabfirst';
                    
                tabCurrent = 'tabfirst';
            }
            if($this.hasClass('next')) {
                
                ajax_c[blockID][tabCurrent]['prev'] = 0;
                
                if(ajax_c[blockID][tabCurrent]['next'] == 1) {
                    
                    $this.siblings('.pagination-btn').removeClass('disable-click');
                    
                    return;
                }
            }else {
                ajax_c[blockID][tabCurrent]['next'] = 0;
                
                if(ajax_c[blockID][tabCurrent]['prev'] == 1) {
                    
                    $this.siblings('.pagination-btn').removeClass('disable-click');
                    
                    return;
                }
            }
            
            var offset = 0;
            
            var thisTabArr = new Array();
            
            thisTabArr = ajax_c[blockID][tabCurrent];
            
            if ($this.hasClass('next')) {
                offset = ajax_c[blockID][tabCurrent]['offset'] = parseInt(thisTabArr['offset']) + parseInt(thisTabArr['entries']);
            }else {
                offset = ajax_c[blockID][tabCurrent]['offset'] = parseInt(thisTabArr['offset']) - parseInt(thisTabArr['entries']);
            }
            
            if((tabCurrent == 'tabfirst') && ((offset < 0) || (offset < ajax_c[blockID]['args']['offset']))) {
                $this.siblings('.pagination-btn').removeClass('disable-click');
                ajax_c[blockID][tabCurrent]['prev'] = 1;
                ajax_c[blockID][tabCurrent]['offset'] = parseInt(thisTabArr['offset']) + parseInt(thisTabArr['entries']);
                return;
            }
            
            var postIcon = ajax_c[blockID]['post_icon'];
            
            var columnClass = '';
            
            var excerptLength = '';
                
            var container = $this.parents('.bk-module-pagination').siblings('.bk-module-inner');
            
            container.append('<div class="bk-preload-wrapper"></div>');
            container.find('li').addClass('bk-preload-blur');
            container.find('.large-post').addClass('bk-preload-blur');
            
            if (tabClicked != 'tabfirst') {
                container.find('.bk-preload-wrapper').addClass('term-'+ajax_c[blockID]['tab'+tabClicked]['cat']);
            }
            var data = {
    				action			: blockName,
                    ajax_c          : ajax_c,
                    post_offset     : offset,
                    blockID         : blockID,
                    tabClicked      : tabClicked,
                    columnClass     : columnClass,
                    postIcon        : postIcon,
                    excerptLength   : excerptLength,
    			};
                    
    		jQuery.post( ajaxurl, data, function( respond ){
                var el = $(respond).hide().fadeIn('1500');
                
                var respond_length = el.find('.post-date').length;
                
                if(respond_length != 0) {
                    
                    container.empty();
                    
                    container.append(el);
    
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    container.find('.large-post').removeClass('bk-preload-blur');
                    
                    container.imagesLoaded(function(){
                        
                        setTimeout(function() {
                            
                            jQuery(container).find('.thumb').removeClass('hide-thumb');
                            
                            $this.siblings('.pagination-btn').removeClass('disable-click');
                            
                            if ($this.hasClass('next')) {
                            
                                if(respond_length < ajax_c[blockID][tabCurrent]['entries']) {
                                    
                                    ajax_c[blockID][tabCurrent]['next'] = 1;
                                    
                                }else {
                                    
                                    $this.removeClass('disable-click');
                                    
                                }
                            }else {
                                
                                if((offset == 0) || (offset < ajax_c[blockID][tabCurrent]['entries'])) {
                                    
                                    ajax_c[blockID][tabCurrent]['prev'] = 1;
                                    
                                    $this.addClass('disable-click');
                                    
                                }else {
                                    
                                    $this.removeClass('disable-click');
                                    
                                }
                                
                                if((tabCurrent == 'tabfirst') && (offset <= ajax_c[blockID]['args']['offset'])) {
                                    $this.addClass('disable-click');
                                    return;
                                }
                                
                            }
                            
                        }, 500);
                    });
                    run_review_canvas_script(container);
                }else if ($this.hasClass('next')){
                    
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    container.find('.large-post').removeClass('bk-preload-blur');
                    
                    $this.siblings('.pagination-btn').removeClass('disable-click');
                    
                    ajax_c[blockID][tabCurrent]['next'] = 1;
                    
                    ajax_c[blockID][tabCurrent]['offset'] = parseInt(thisTabArr['offset']) - parseInt(thisTabArr['entries']);

                    
                }else if ($this.hasClass('prev')){
                    
                    container.find('.bk-preload-wrapper').remove();
                    
                    container.find('li').removeClass('bk-preload-blur');
                    container.find('.large-post').removeClass('bk-preload-blur');
                    
                    $this.siblings('.pagination-btn').removeClass('disable-click');
                    
                    ajax_c[blockID][tabCurrent]['prev'] = 1;
                    
                }
            });

        });
        
        
        jQuery('.widget-pagination-btn').on('click',function(e){
            
            e.preventDefault();
            
            var $this = $(this);
            
            if($this.hasClass('nomore')){
                return;
            }
            
            if($this.hasClass('disable-click')) {
                return;
            }
            
            $this.addClass('disable-click');
            
            $this.siblings('.widget-pagination-btn').addClass('disable-click');
            
            var container = $this.parents('.bk-widget-wrap').find('.bk-widget-content');
            
            var blockID = $this.parents('.bk-widget-wrap').attr('id');
            
            var blockName = blockID.split("-")[0];
            
            var offset = 0;
            
            if ($this.hasClass('next')) {
                offset = ajax_c[blockID]['offset'] = parseInt(ajax_c[blockID]['offset']) + parseInt(ajax_c[blockID]['entries']);
            }else {
                offset = ajax_c[blockID]['offset'] = parseInt(ajax_c[blockID]['offset']) - parseInt(ajax_c[blockID]['entries']);
            }
            
            if($this.hasClass('next')) {
                
                ajax_c[blockID]['prev'] = 0;
                
                if(ajax_c[blockID]['next'] == 1) {
                    
                    $this.siblings('.widget-pagination-btn').removeClass('disable-click');
                    
                    return;
                }
            }else {
                ajax_c[blockID]['next'] = 0;
                
                if(ajax_c[blockID]['prev'] == 1) {
                    
                    $this.siblings('.widget-pagination-btn').removeClass('disable-click');
                    
                    return;
                }
            }
            
            if((offset < 0) || (offset < ajax_c[blockID]['args']['offset'])) {
                $this.siblings('.widget-pagination-btn').removeClass('disable-click');
                ajax_c[blockID]['prev'] = 1;
                ajax_c[blockID]['offset'] = parseInt(ajax_c[blockID]['offset']) + parseInt(ajax_c[blockID]['entries']);
                return;
            }
            
            container.append('<div class="bk-preload-wrapper"></div>');
            
            container.find('li').addClass('bk-preload-blur');
            container.find('.large-post').addClass('bk-preload-blur');
            
            var data = {
    				action			: blockName,
                    ajax_c          : ajax_c,
                    post_offset     : offset,
                    blockID         : blockID,
    			};  
            jQuery.post( ajaxurl, data, function( respond ){
                var el = $(respond).hide().fadeIn('1500');
                
                if(blockName == 'latest_8') {
                    var respond_length = el.find('.post-c-wrap').length;
                }else {
                    var respond_length = el.find('.post-date').length;
                }
                
                if(respond_length != 0) {
                    
                    container.empty();
                    
                    container.append(el);
    
                    container.find('.bk-preload-wrapper').remove();
                    
                    container.find('li').removeClass('bk-preload-blur');
                    container.find('.large-post').removeClass('bk-preload-blur');
                    
                    container.imagesLoaded(function(){
                        
                        setTimeout(function() {
                            
                            jQuery(container).find('.thumb').removeClass('hide-thumb');
                            
                            $this.siblings('.widget-pagination-btn').removeClass('disable-click');
                            
                            if ($this.hasClass('next')) {
                            
                                if(respond_length < ajax_c[blockID]['entries']) {
                                    
                                    ajax_c[blockID]['next'] = 1;
                                    
                                }else {
                                    
                                    $this.removeClass('disable-click');
                                    
                                }
                            }else {
                                
                                if((offset == 0) || (offset < ajax_c[blockID]['entries'])) {
                                    
                                    ajax_c[blockID]['prev'] = 1;
                                    
                                    $this.addClass('disable-click');
                                    
                                }else {
                                    
                                    $this.removeClass('disable-click');
                                    
                                }
                                
                                if(offset <= ajax_c[blockID]['args']['offset']) {
                                    $this.addClass('disable-click');
                                    return;
                                }
                            }
                            
                        }, 500);
                        run_review_canvas_script(container);
                    });
                }else if ($this.hasClass('next')){
                    
                    container.find('.bk-preload-wrapper').remove();
                    
                    container.find('li').removeClass('bk-preload-blur');
                    
                    container.find('.large-post').removeClass('bk-preload-blur');
                    
                    $this.siblings('.widget-pagination-btn').removeClass('disable-click');
                    
                    ajax_c[blockID]['next'] = 1;
                    
                    ajax_c[blockID]['offset'] = parseInt(ajax_c[blockID]['offset']) - parseInt(ajax_c[blockID]['entries']);
                    
                    run_review_canvas_script(container);
                    
                }else if ($this.hasClass('prev')){
                    
                    container.find('.bk-preload-wrapper').remove();
                    
                    container.find('li').removeClass('bk-preload-blur');
                    
                    container.find('.large-post').removeClass('bk-preload-blur');
                    
                    $this.siblings('.widget-pagination-btn').removeClass('disable-click');
                    
                    ajax_c[blockID]['prev'] = 1;
                    
                    run_review_canvas_script(container);
                    
                }
            });
        });
        
        jQuery('.related-pagination-btn').on('click',function(e){
            e.preventDefault();
            var $this = $(this);
            if($this.hasClass('nomore')){
                return;
            }
            if($this.hasClass('disable-click')) {
                return;
            }
            $this.addClass('disable-click');
            $this.siblings('.related-pagination-btn').addClass('disable-click');
            
            var container = $this.parents('.related-box').find('.bk-related-posts');
            var blockID = $this.parents('.related-box').attr('id');
            var offset = 0;
            
            if ($this.hasClass('next')) {
                offset = ajax_c[blockID]['offset'] = parseInt(ajax_c[blockID]['offset']) + parseInt(ajax_c[blockID]['entries']);
            }else {
                offset = ajax_c[blockID]['offset'] = parseInt(ajax_c[blockID]['offset']) - parseInt(ajax_c[blockID]['entries']);
            }
            
            if($this.hasClass('next')) {
                ajax_c[blockID]['prev'] = 0;
                if(ajax_c[blockID]['next'] == 1) {
                    $this.siblings('.related-pagination-btn').removeClass('disable-click');
                    return;
                }
            }else {
                ajax_c[blockID]['next'] = 0;
                if(ajax_c[blockID]['prev'] == 1) {
                    $this.siblings('.related-pagination-btn').removeClass('disable-click');
                    return;
                }
            }
            
            container.append('<div class="bk-preload-wrapper"></div>');
            container.find('li').addClass('bk-preload-blur');
            
            var data = {
    				action			: 'related_posts_load',
                    post_offset     : offset,
                    blockID         : blockID,
                    ajax_c          : ajax_c,
    			};
                
            jQuery.post( ajaxurl, data, function( respond ){
                var el = $(respond).hide().fadeIn('1500');
                var respond_length = el.find('.post-c-wrap').length;
                if(respond_length != 0) {
                    container.empty();
                    container.append(el);
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    container.imagesLoaded(function(){
                        setTimeout(function() {
                            jQuery(container).find('.thumb').removeClass('hide-thumb');
                            $this.siblings('.related-pagination-btn').removeClass('disable-click');
                            if ($this.hasClass('next')) {
                                if(respond_length < ajax_c[blockID]['entries']) {
                                    ajax_c[blockID]['next'] = 1;
                                }else {
                                    $this.removeClass('disable-click');
                                }
                            }else {
                                if((offset == 0) || (offset < ajax_c[blockID]['entries'])) {
                                    ajax_c[blockID]['prev'] = 1;
                                    $this.addClass('disable-click');
                                }else {
                                    $this.removeClass('disable-click');
                                }
                            }
                        }, 500);
                        run_review_canvas_script(container);
                    });
                }else if ($this.hasClass('next')){
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    $this.siblings('.related-pagination-btn').removeClass('disable-click');
                    ajax_c[blockID]['next'] = 1;
                    ajax_c[blockID]['offset'] = parseInt(ajax_c[blockID]['offset']) - parseInt(ajax_c[blockID]['entries']);
                    run_review_canvas_script(container);
                }else if ($this.hasClass('prev')){
                    container.find('.bk-preload-wrapper').remove();
                    container.find('li').removeClass('bk-preload-blur');
                    $this.siblings('.related-pagination-btn').removeClass('disable-click');
                    ajax_c[blockID]['prev'] = 1;
                    run_review_canvas_script(container);
                }
            });
        });
                
        jQuery('.related-tab').on('click',function(e){
            e.preventDefault();
            if($(this).hasClass('active')) {
                return;
            }
            var $previous_tab = $(this).siblings('.active').attr('id');
            var $container = $(this).parent().siblings('.bk-related-posts');
            var $current_tab = $(this).attr('id');
            
            $(this).siblings('.related-tab').removeClass('active');
            $(this).addClass('active');
            
            
            var $related_ajax_action = '';
            if($(this).attr('id') == 's-more-from-author-tab') {
                $(this).parents('.related-box').attr('id', 's-author-articles');
                $related_ajax_action = 'related_author_posts_load';
            }else if($(this).attr('id') == 's-more-from-category-tab') {
                $(this).parents('.related-box').attr('id', 's-category-articles');
                $related_ajax_action = 'related_category_posts_load';
            }else {
                $(this).parents('.related-box').attr('id', 's-related-articles');
                $related_ajax_action = 'related_author_posts_load';
            }
            $container.append('<div class="bk-preload-wrapper"></div>');
            
            $container.find('li').addClass('bk-preload-blur');
            
            if ((ajax_c[$previous_tab]['content'] == '') || (ajax_c[$previous_tab]['content'] !== $container[0].innerHTML)) {
                ajax_c[$previous_tab]['content'] = $container[0].innerHTML;
            }
            if(ajax_c[$current_tab]['content'] == ''){
                var data = {
        				action			: $related_ajax_action,
                        ajax_c          : ajax_c,
        			};
                jQuery.post( ajaxurl, data, function( respond ){
                    var el = $(respond).hide().fadeIn('1500');
                    $container.empty();
                    $container.append(el);
                    $container.find('.bk-preload-wrapper').remove();
                    $container.find('li').removeClass('bk-preload-blur');
                    $container.imagesLoaded(function(){
                        setTimeout(function() {
                            jQuery($container).find('.thumb').removeClass('hide-thumb');
                        }, 500);
                    });
                    $('.related-pagination-btn').removeClass('disable-click');
                    
                    
                    if($(this).attr('id') == 's-more-from-author-tab') {
                        if(ajax_c['s-author-articles']['next'] == 1) {
                            $('.related-pagination-btn.next').addClass('disable-click');
                        }
                        if(ajax_c['s-author-articles']['prev'] == 1) {
                            $('.related-pagination-btn.prev').addClass('disable-click');
                        }
                    }else if($(this).attr('id') == 's-more-from-category-tab') {
                        if(ajax_c['s-category-articles']['next'] == 1) {
                            $('.related-pagination-btn.next').addClass('disable-click');
                        }
                        if(ajax_c['s-category-articles']['prev'] == 1) {
                            $('.related-pagination-btn.prev').addClass('disable-click');
                        }
                    }else {
                        if(ajax_c['s-related-articles']['next'] == 1) {
                            $('.related-pagination-btn.next').addClass('disable-click');
                        }
                        if(ajax_c['s-related-articles']['prev'] == 1) {
                            $('.related-pagination-btn.prev').addClass('disable-click');
                        }
                    }
                    run_review_canvas_script($container);
                });
            }else {
                var el = $(ajax_c[$current_tab]['content']).hide().fadeIn('1500');
                $container.empty();
                $container.append(el);
                $container.find('.bk-preload-wrapper').remove();
                $container.find('li').removeClass('bk-preload-blur');
                $container.imagesLoaded(function(){
                    setTimeout(function() {
                        jQuery($container).find('.thumb').removeClass('hide-thumb');
                    }, 500);
                });
                
                $('.related-pagination-btn').removeClass('disable-click');
                
                if($(this).attr('id') == 's-more-from-author-tab') {
                    if(ajax_c['s-author-articles']['next'] == 1) {
                        $('.related-pagination-btn.next').addClass('disable-click');
                    }
                    if(ajax_c['s-author-articles']['prev'] == 1) {
                        $('.related-pagination-btn.prev').addClass('disable-click');
                    }
                }else if($(this).attr('id') == 's-more-from-category-tab') {
                    if(ajax_c['s-category-articles']['next'] == 1) {
                        $('.related-pagination-btn.next').addClass('disable-click');
                    }
                    if(ajax_c['s-category-articles']['prev'] == 1) {
                        $('.related-pagination-btn.prev').addClass('disable-click');
                    }
                }else {
                    if(ajax_c['s-related-articles']['next'] == 1) {
                        $('.related-pagination-btn.next').addClass('disable-click');
                    }
                    if(ajax_c['s-related-articles']['prev'] == 1) {
                        $('.related-pagination-btn.prev').addClass('disable-click');
                    }
                }
                run_review_canvas_script($container);
            }
        });
        
        // Ajax search 
        var $ajaxSearch = $('.ajax-form-search');
        $ajaxSearch.each(function() {
            var thisContainer = '';
            if($(this).hasClass('top-bar-ajax-search')) {
                thisContainer = $(this).parents('.top-bar');
            }else {
                thisContainer = $(this).parents('.header-wrap');
            }
            
            $(this).on('click', function() {
                if ($(this).siblings('.ajax-form').width() == 0){
                    thisContainer.find('.rubik-ajax-search-bg').css('z-index', '10');
                    thisContainer.find('.ajax-form input').css('width','100%');
                    thisContainer.find('.ajax-form').css('padding','0 30px 0 0');
                    thisContainer.find('.ajax-form input').css('padding','0 20px 0 0');
                    thisContainer.find('.header-6 .ajax-form input').css('padding','0 20px');
                    thisContainer.find('.ajax-form input').css('font-size','16px'); 
                    thisContainer.find('.ajax-form input').val('');
                    thisContainer.find('.ajax-form-search i.fa-search').css('display', 'none');
                    thisContainer.find('.ajax-form-search i.fa-times').css('display', 'inline-block');
                    thisContainer.find('.ajax-search-wrap').css({'width' : '100%'});
                    thisContainer.find('.boxed-layout .ajax-search-wrap').css({'width' : '100%'});
                } else {
                    thisContainer.find('.rubik-ajax-search-bg').css('z-index', '-1');
                    thisContainer.find('.ajax-form input').css('width','0');
                    thisContainer.find('.ajax-form').css('padding','0');
                    thisContainer.find('.ajax-form input').css('padding','0');
                    thisContainer.find('.header-6 .ajax-form input').css('padding','0');
                    thisContainer.find('.ajax-form input').css('font-size','0'); 
                    thisContainer.find('.ajax-search-result').empty().css('height', 'auto');
                    thisContainer.find('.ajax-search-result').css({'box-shadow' : 'none'});
                    thisContainer.find('.ajax-search-wrap').css({'width' : '0px'});
                    thisContainer.find('.boxed-layout .ajax-search-wrap').css({'width' : '0'});
                    thisContainer.find('.ajax-form-search i.fa-search').css('display', 'inline-block');
                    thisContainer.find('.ajax-form-search i.fa-times').css('display', 'none');
                }
                
            });
        
            thisContainer.find('.search-form-text').keyup(function () {
                var value = $(this).val();
                var $container = $('.ajax-search-result');
                if($(window).width() > 511) {
                    thisContainer.find('.ajax-search-wrap').css({'width' : '100%'});
                }else {
                    thisContainer.find('.ajax-search-wrap').css({'width' : '100%'});
                }
                delay(function () {
                    if (value) {
                        var search_class = thisContainer.find('.ajax-search-result');
                        search_class.fadeIn(300).html('<div class="loading-img-wrap"><div class="search-loadding"></div></div>');
                        var data = {
                            action: 'bk_ajax_search',
                            s: value
                        };
                        $.post(ajaxurl, data, function (response) {
                            response = $.parseJSON(response);
                            thisContainer.find('.ajax-search-result').empty().hide().css('height', 'auto').html(response.content).fadeIn(300).css('height', 'auto');
                            delay(function () {
                                $($container).find('.thumb').removeClass('hide-thumb');
                                $($container).find('h4.title').css('opacity','1');                            
                                $($container).css({'box-shadow' : '0px 1px 2px 1px rgba(0, 0, 0, 0.2)'});
                            }, 150);
                            run_review_canvas_script($container);
                        });
                    } else  thisContainer.find('.ajax-search-result').fadeOut(300, function () {
                        $(this).empty().css('height', 'auto');
                        $($container).css({'box-shadow' : 'none'});
                        $($container).find('h4.title').css('opacity','1');                    
                    });
        
                }, 450);
            });
        });
    });
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();
    function run_review_canvas_script(container){
        var canvasArray  = container.find('.rating-canvas');
        $.each(canvasArray, function(i,canvas){
            var percent = $(canvas).data('rating');
            var ctx     = canvas.getContext('2d');
    
            canvas.width  = $(canvas).parent().width();
            canvas.height = $(canvas).parent().height();
    
            var x = (canvas.width) / 2;
            var y = (canvas.height) / 2;
            if ($(canvas).parents().hasClass('rating-wrap')) {
                var radius = (canvas.width - 6) / 2;
                var lineWidth = 2;
            } else {
                var radius = (canvas.width - 10) / 2;
                var lineWidth = 4;
            }
                    
            var endAngle = (Math.PI * percent * 2 / 100);
            ctx.beginPath();
            ctx.arc(x, y, radius, -(Math.PI/180*90), endAngle - (Math.PI/180*90), false);   
            ctx.lineWidth = lineWidth;
            ctx.strokeStyle = "#fff";
            ctx.stroke();  
        });
    }
})(jQuery);