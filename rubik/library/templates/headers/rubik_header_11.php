<?php
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    $logo = array();
    $mobilelogo = array();
    if (isset($rubik_option)){  
        if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
            $logo = $rubik_option['bk-logo'];
        }
        if ((isset($rubik_option['bk-sticky-nav-logo'])) && (($rubik_option['bk-sticky-nav-logo']) != NULL)){ 
            $mobilelogo = $rubik_option['bk-sticky-nav-logo'];
            if (($mobilelogo != null) && (array_key_exists('url',$mobilelogo))) {
                if ($mobilelogo['url'] == '') {
                    if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
                        $mobilelogo = $rubik_option['bk-logo'];
                    }
                }
            }
        }else {
            if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
                $mobilelogo = $rubik_option['bk-logo'];
            }
        }
        $header_banner = $rubik_option['bk-header-banner-switch'];
        $ga_script = $rubik_option['bk-banner-script'];
        $backtop = $rubik_option['bk-backtop-switch'];
        if ($header_banner){ 
            $imgurl = $rubik_option['bk-header-banner']['imgurl'];
            $linkurl = $rubik_option['bk-header-banner']['linkurl'];
        }
        $canvasSwitch = isset($rubik_option['bk-canvas-desktop-switch']) ? $rubik_option['bk-canvas-desktop-switch'] : 0;
    }   $canvasPosition = isset($rubik_option['bk-canvas-button-position']) ? $rubik_option['bk-canvas-button-position'] : 'left';
    ?>    
    <?php get_template_part( 'library/templates/headers/rubik_canvas_menu' );?>
    <div id="page-inner-wrap">
        <div class="page-cover canvas-menu-close"></div>
        <div class="bk-page-header header-1 header-2 header-11">            
            <div class="header-wrap">
                <!-- ticker open -->
                <?php        
                    if (isset($rubik_option)){
                        if (isset($rubik_option['bk-header-ticker']) && ($rubik_option['bk-header-ticker'] == 1)) {?>
                            <div class="bk-ticker-module bk-white-bg">
                            <?php rubik_core::bk_get_ticker('header');?>
                            </div><!--end ticker-module-->
                        <?php }
                    }
                ?>
                <!-- ticker close -->
                <?php echo rubik_core::rubik_top_bar();?>
                <?php if ( has_nav_menu( 'main-menu' ) || has_nav_menu( 'canvas-menu' )) {?>
                <!-- nav open -->
        		<nav class="main-nav">
                    <div class="main-nav-inner bkwrapper container">
                        <div class="main-nav-container clearfix">
                            <div class="main-nav-wrap">
                                <div class="canvas-menu-wrap <?php if($canvasSwitch == 1) echo 'show-on-desktop';?> <?php if($canvasPosition != 'right') echo 'left'; else echo 'right';?>">
                                    <?php echo rubik_core::bk_canvas_button();?>
                                    <?php if(isset ($rubik_option['bk-logo-hide']) && ($rubik_option['bk-logo-hide'] != 0)) {?>
                                    <!-- logo open -->
                                        <?php if (($mobilelogo != null) && (array_key_exists('url',$mobilelogo))) {
                                                if ($mobilelogo['url'] != '') {
                                            ?>
                            			<div class="logo">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <img src="<?php echo esc_url($mobilelogo['url']);?>" alt="<?php esc_attr_e( 'Logo', 'rubik' ); ?>"/>
                                            </a>
                            			</div>
                            			<!-- logo close -->
                                        <?php } else {?> 
                                        <div class="logo logo-text">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </a>
                            			</div>
                                        <?php }
                                        } else {?> 
                                        <div class="logo logo-text">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </a>
                            			</div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                
                                <?php if ( has_nav_menu( 'main-menu' ) ) { 
                                    wp_nav_menu( array( 
                                        'theme_location' => 'main-menu',
                                        'container_class' => 'main-menu',
                                        'walker' => new BK_Walker,
                                        ) );}?>
                            </div>
                            <?php 
                                echo rubik_ajax_form_search();
                            ?> 
                        </div>    
                    </div><!-- main-nav-inner -->       
                    <div class="rubik-ajax-search-bg"></div>   
        		</nav>
                <?php }?>
                <!-- nav close -->
                <div class="header-logo-wrapper">
                    <div class="header container">
                        <div class="row">
                            <div class="col-md-12">
                    			<div class="header-inner <?php if($rubik_option['bk-header-logo-position'] == 'center') {echo 'header-center';}?>">
                        			<?php if(isset ($rubik_option['bk-logo-hide']) && ($rubik_option['bk-logo-hide'] != 0)) {?>
                                    <!-- logo open -->
                                        <?php if (($logo != null) && (array_key_exists('url',$logo))) {
                                                if ($logo['url'] != '') {
                                            ?>
                            			<div class="logo">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <img src="<?php echo esc_url($logo['url']);?>" alt="<?php esc_attr_e( 'Logo', 'rubik' ); ?>"/>
                                            </a>
                            			</div>
                            			<!-- logo close -->
                                        <?php } else {?> 
                                        <div class="logo logo-text">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </a>
                            			</div>
                                        <?php }
                                        } else {?> 
                                        <div class="logo logo-text">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </a>
                            			</div>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ( $header_banner ) : ?>
                                        <!-- header-banner open -->                             
                            			<div class="header-banner">
                                        <?php
                                            if (($ga_script != '') && ($ga_script != 'Put your google adsense code here')){
                                                echo rubik_core::bk_render_google_adsense_code();
                                            } else { ?>
                                                <a class="ads-banner-link" target="_blank" href="<?php echo esc_url( $linkurl ); ?>">
                                				    <img class="ads-banner" src="<?php echo esc_url( $imgurl ); ?>" alt="<?php esc_attr_e( 'Header Banner', 'rubik' ); ?>"/>
                                                </a>
                                            <?php }
                                        ?> 
                            			</div>                            
                            			<!-- header-banner close -->
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
    		</div>
            <?php 
                if(isset($rubik_option['bk-sticky-nav-switch']) && ($rubik_option['bk-sticky-nav-switch'] == 2)) :
                    get_template_part( 'library/templates/headers/rubik_sticky_header' );
                endif;    
            ?>
        </div>                
        <?php if(function_exists('rubik_breadcrumbs')) :?>         
            <div class="bk-breadcrumbs-container">
            <?php rubik_breadcrumbs();?>
            </div>
        <?php endif;?>
        
        <!-- backtop open -->
		<?php if ($backtop) { ?>
            <div id="back-top"><i class="fa fa-long-arrow-up"></i></div>
        <?php } ?>
		<!-- backtop close -->