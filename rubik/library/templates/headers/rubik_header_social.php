<?php
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
?>
<?php if ( isset($rubik_option ['bk-social-header-switch']) && ($rubik_option ['bk-social-header-switch'] == 1) ){ ?>
	<div class="header-social">
		<ul class="clearfix">
			<?php if ($rubik_option['bk-social-header']['fb']){ ?>
				<li class="social-icon fb"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Facebook', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['fb']); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
			<?php } ?>
			
			<?php if ($rubik_option['bk-social-header']['twitter']){ ?>
				<li class="social-icon twitter"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Twitter', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['twitter']); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
			<?php } ?>
			
			<?php if ($rubik_option['bk-social-header']['gplus']){ ?>
				<li class="social-icon gplus"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Google Plus', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['gplus']); ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
			<?php } ?>
			
			<?php if ($rubik_option['bk-social-header']['linkedin']){ ?>
				<li class="social-icon linkedin"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Linkedin', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['linkedin']); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
			<?php } ?>
			
			<?php if ($rubik_option['bk-social-header']['pinterest']){ ?>
				<li class="social-icon pinterest"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Pinterest', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['pinterest']); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
			<?php } ?>
			
			<?php if ($rubik_option['bk-social-header']['instagram']){ ?>
				<li class="social-icon instagram"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Instagram', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['instagram']); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
			<?php } ?>
			
			<?php if ($rubik_option['bk-social-header']['dribbble']){ ?>
				<li class="social-icon dribbble"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Dribbble', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['dribbble']); ?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
			<?php } ?>
			
			<?php if ($rubik_option['bk-social-header']['youtube']){ ?>
				<li class="social-icon youtube"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Youtube', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['youtube']); ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
			<?php } ?>      							
			                                    
            <?php if ($rubik_option['bk-social-header']['vimeo']){ ?>
				<li class="social-icon vimeo"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Vimeo', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['vimeo']); ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
			<?php } ?>
            
            <?php if ($rubik_option['bk-social-header']['vk']){ ?>
				<li class="social-icon vk"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('VKontakte', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['vk']); ?>" target="_blank"><i class="fa fa-vk"></i></a></li>
			<?php } ?>
            <?php if ($rubik_option['bk-social-header']['vine']){ ?>
				<li class="social-icon vine"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Vine', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['vine']); ?>" target="_blank"><i class="fa fa-vine"></i></a></li>
			<?php } ?>
            <?php if ($rubik_option['bk-social-header']['snapchat']){ ?>
				<li class="social-icon snapchat"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('SnapChat', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['snapchat']); ?>" target="_blank"><i class="fa fa-snapchat-ghost"></i></a></li>
			<?php } ?>
            <?php if (isset($rubik_option['bk-social-header']['telegram']) && ($rubik_option['bk-social-header']['telegram'])){ ?>
				<li class="social-icon telegram"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Telegram', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['telegram']); ?>" target="_blank"><i class="fa fa-telegram"></i></a></li>
			<?php } ?>                                        
            <?php if ($rubik_option['bk-social-header']['rss']){ ?>
				<li class="social-icon rss"><a class="bk-tipper-bottom" data-title="<?php esc_attr_e('Rss', 'rubik');?>" href="<?php echo esc_url($rubik_option['bk-social-header']['rss']); ?>" target="_blank"><i class="fa fa-rss"></i></a></li>
			<?php } ?>                    						
		</ul>
	</div>

<?php }?>  