<?php

/**
	ReduxFramework Config File
	For full documentation, please visit: https://github.com/ReduxFramework/ReduxFramework/wiki
**/

if ( !class_exists( "ReduxFramework" ) ) {
	return;
} 

if ( !class_exists( "Redux_Framework_config" ) ) {
	class Redux_Framework_config {

		public $args = array();
		public $sections = array();
		public $theme;
		public $ReduxFramework;

		public function __construct( ) {

			// Just for demo purposes. Not needed per say.
			$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();
			
			// Set a few help tabs so you can see how it's done
			$this->setHelpTabs();

			// Create the sections and fields
			$this->setSections();
			
			if ( !isset( $this->args['opt_name'] ) ) { // No errors please
				return;
			}
			
			$this->ReduxFramework = new ReduxFramework($this->sections, $this->args);

			// Dynamically add a section. Can be also used to modify sections/fields
			add_filter('redux/options/'.$this->args['opt_name'].'/sections', array( $this, 'dynamic_section' ) );

		}


		/**

			This is a test function that will let you see when the compiler hook occurs. 
			It only runs if a field	set with compiler=>true is changed.

		**/

		function compiler_action($options, $css) {

		}



		/**
		 
		 	Custom function for filtering the sections array. Good for child themes to override or add to the sections.
		 	Simply include this function in the child themes functions.php file.
		 
		 	NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
		 	so you must use get_template_directory_uri() if you want to use any of the built in icons
		 
		 **/

		function dynamic_section($sections){
		    return $sections;
		}
		
		
		/**

			Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

		**/
		
		function change_arguments($args){
		    //$args['dev_mode'] = true;
		    
		    return $args;
		}
			
		
		/**

			Filter hook for filtering the default value of any given field. Very useful in development mode.

		**/

		function change_defaults($defaults){
		    $defaults['str_replace'] = "Testing filter hook!";
		    
		    return $defaults;
		}


		public function setSections() {

			/**
			 	Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
			 **/


			// Background Patterns Reader
			$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
			$sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
			$sample_patterns      = array();

			ob_start();

			$ct = wp_get_theme();
			$this->theme = $ct;
			$item_name = $this->theme->get('Name'); 
			$tags = $this->theme->Tags;
			$screenshot = $this->theme->get_screenshot();
			$class = $screenshot ? 'has-screenshot' : '';

			$customize_title = sprintf( esc_html__( 'Customize &#8220;%s&#8221;','rubik' ), $this->theme->display('Name') );

			?>
			<div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
				<?php if ( $screenshot ) : ?>
					<?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
					<a href="<?php echo esc_url(wp_customize_url()); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr( $customize_title ); ?>">
						<img src="<?php echo esc_url( $screenshot ); ?>" alt="<?php esc_attr_e( 'Current theme preview', 'rubik' ); ?>" />
					</a>
					<?php endif; ?>
					<img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>" alt="<?php esc_attr_e( 'Current theme preview', 'rubik' ); ?>" />
				<?php endif; ?>

				<h4>
					<?php echo esc_attr($this->theme->display('Name')); ?>
				</h4>

				<div>
					<ul class="theme-info">
						<li><?php printf( esc_html__('By %s','rubik'), $this->theme->display('Author') ); ?></li>
						<li><?php printf( esc_html__('Version %s','rubik'), $this->theme->display('Version') ); ?></li>
						<li><?php echo '<strong>'.esc_html__('Tags', 'rubik').':</strong> '; ?><?php printf( $this->theme->display('Tags') ); ?></li>
					</ul>
					<p class="theme-description"><?php echo esc_attr($this->theme->display('Description')); ?></p>
					<?php if ( $this->theme->parent() ) {
						printf( ' <p class="howto">' . esc_html__( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'rubik' ) . '</p>',
							__( 'http://codex.wordpress.org/Child_Themes','rubik' ),
							$this->theme->parent()->display( 'Name' ) );
					} ?>
					
				</div>

			</div>

			<?php
			$item_info = ob_get_contents();
			    
			ob_end_clean();

			$sampleHTML = '';
            
			// ACTUAL DECLARATION OF SECTIONS
            
                $this->sections[] = array(
    				'icon' => 'el-icon-wrench',
    				'title' => esc_html__('General Settings', 'rubik'),
    				'fields' => array(
    					array(
    						'id'=>'bk-primary-color',
    						'type' => 'color',
    						'title' => esc_html__('Primary color', 'rubik'), 
    						'subtitle' => esc_html__('Pick a primary color for the theme.', 'rubik'),
    						'default' => '#EA2323',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-site-container-width',
    						'type' => 'select',
    						'title' => esc_html__('Site Container Width', 'rubik'),
    						'subtitle'=> esc_html__('Select a container width for your site', 'rubik'),
    						'options' => array(
                                '1110' => esc_html__('1100px', 'rubik'),
                                '1140' => esc_html__('1140px', 'rubik'),
                                '1170' => esc_html__('1170px', 'rubik'),
                                '1200' => esc_html__('1200px', 'rubik')
                            ),
    						'default' => '1110'
						),
                        array(
    						'id'=>'bk-site-layout',
    						'type' => 'button_set',
    						'title' => esc_html__('Site layout', 'rubik'),
    						'subtitle'=> esc_html__('Choose between wide and boxed layout', 'rubik'),
    						'options' => array('wide' => esc_html__('Wide', 'rubik'),'boxed' => esc_html__('Boxed', 'rubik')),
    						'default' => 'wide'
						),
                        array(
    						'id'=>'bk-body-bg',
    						'type' => 'background',
                            'required' => array('bk-site-layout','=','boxed'),
    						'output' => array('body'),
    						'title' => esc_html__('Site background', 'rubik'), 
    						'subtitle' => esc_html__('Choose background image or background color for boxed layout', 'rubik'),
						),
                        array(
    						'id'=>'bk-sb-responsive-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable sidebar in responsive layout (For small device width < 991px)', 'rubik'),
    						'subtitle' => esc_html__('Choose to display or hide sidebar in responsive layout', 'rubik'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
    						'id'=>'bk-backtop-switch',
    						'type' => 'switch', 
    						'title' => esc_html__('Scroll top button', 'rubik'),
    						'subtitle'=> esc_html__('Show scroll to top button in right lower corner of window', 'rubik'),
    						'default' 		=> 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
    				)
    			);
                $this->sections[] = array(
    				'icon' => 'el-icon-credit-card',
    				'title' => esc_html__('Header Settings', 'rubik'),
    				'fields' => array(
                        array(
    						'id'=>'bk-header-type',
    						'title' => esc_html__('Header Type', 'rubik'),
    						'subtitle' => esc_html__('Choose a Header Type', 'rubik'),
                            'type' => 'image_select', 
                            'options'  => array(
                                'header1' => get_template_directory_uri().'/images/admin_panel/header/1.jpg',
                                'header2' => get_template_directory_uri().'/images/admin_panel/header/2.jpg',
                                'header3' => get_template_directory_uri().'/images/admin_panel/header/3.jpg',
                                'header7' => get_template_directory_uri().'/images/admin_panel/header/7.jpg',
                                'header4' => get_template_directory_uri().'/images/admin_panel/header/4.jpg',
                                'header11' => get_template_directory_uri().'/images/admin_panel/header/11.jpg',
                                'header10' => get_template_directory_uri().'/images/admin_panel/header/10.jpg',
                                'header12' => get_template_directory_uri().'/images/admin_panel/header/12.jpg',
                            ),
                            'default' => 'header1',
						),
                        array(
    						'id'=>'bk-header-2-options',
    						'type' => 'select', 
                            'required' => array('bk-header-type','=','header2'),
    						'title' => esc_html__('Header Options', 'rubik'),
    						'subtitle' => '',
						    'default' => 'style-1',
    						'options' => array(
                                'style-1' => esc_html__('Main Menu with Shadow Bottom - Wide','rubik'),
                                'style-2' => esc_html__('Main Menu without Shadow - Wide','rubik'),
                                'style-3' => esc_html__('Main Menu without Shadow - Boxed','rubik'),
                                'style-4' => esc_html__('Main Menu with Shadow Top & Bottom - Wide','rubik'),
                                'style-5' => esc_html__('Main Menu with Shadow Top & Bottom - Boxed','rubik'),
                                'style-6' => esc_html__('Main Menu with Shadow Bottom & double TOP border - Wide','rubik'),
                            ),
						),
                        array(
    						'id'=>'bk-header-12-options',
    						'type' => 'select', 
                            'required' => array('bk-header-type','=','header12'),
    						'title' => esc_html__('Header Options', 'rubik'),
    						'subtitle' => '',
						    'default' => 'style-1',
    						'options' => array(
                                'style-1' => esc_html__('Main Menu with Border & Shadow Bottom','rubik'),
                                'style-2' => esc_html__('Main Menu without Border & Shadow','rubik'),
                            ),
						),
                        array(
    						'id'=>'bk-logo',
    						'type' => 'media', 
    						'url'=> true,
    						'title' => esc_html__('Site logo', 'rubik'),
    						'subtitle' => esc_html__('Upload logo of your site that is displayed in header', 'rubik'),
                            'placeholder' => esc_html__('No media selected','rubik')
						),
                        array(
    						'id'=>'bk-logo-hide',
    						'type' => 'switch', 
    						'title' => esc_html__('Show/Hide Logo', 'rubik'),
    						'subtitle' => '',
						    'default' => 1,
    						'on' => esc_html__('Show', 'rubik'),
    						'off' => esc_html__('Hide', 'rubik'),
						),
                        array(
    						'id'=>'bk-logo-margin-top',
    						'type' => 'text', 
                            'validate' => 'numeric',
    						'title' => esc_html__('Logo Margin Top - in pixel', 'rubik'),
    						'subtitle' => esc_html__('input: number', 'rubik'),
                            'default' => '50',
						),
                        array(
    						'id'=>'bk-logo-margin-bottom',
    						'type' => 'text', 
                            'validate' => 'numeric',
    						'title' => esc_html__('Logo Margin Bottom - in pixel', 'rubik'),
    						'subtitle' => esc_html__('input: number', 'rubik'),
                            'default' => '50',
						),
                        array(
    						'id'=>'bk-header-logo-position',
    						'type' => 'select', 
                            'required' => array(
                                'bk-header-type','equals',array( 'header1', 'header2', 'header4', 'header5', 'header6', 'header8', 'header9', 'header11' )
                            ),
    						'url'=> true,
    						'title' => esc_html__('Logo position', 'rubik'),
    						'subtitle' => esc_html__('Choose logo position between left or center', 'rubik'),
                            'options' => array('left'=>'Left', 'center'=>'Center'),
                            'default' => 'left',
						),
                        array(
    						'id'=>'bk-header-bg',
    						'type' => 'background',
                            'required' => array(
                                'bk-header-type','equals',array( 'header1', 'header2', 'header4', 'header11', 'header12' )
                            ),
    						'output' => array('.header-wrap'),
    						'title' => esc_html__('Header background', 'rubik'), 
    						'subtitle' => esc_html__('Choose background image or background color for site header', 'rubik'),
                            'default'  => array(
                                'background-color' => '#fff',
                            ),
						),
                        array(
    						'id'=>'bk-social-header-switch',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable social header ', 'rubik'),
    						'subtitle' => esc_html__('Enable social header by display icons', 'rubik'),
    						'default' => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),	
                        array(
    						'id'=>'bk-social-header',
    						'type' => 'text',
                            'required' => array('bk-social-header-switch','=','1'),
    						'title' => esc_html__('Social media', 'rubik'),
    						'subtitle' => esc_html__('Set up social links for site', 'rubik'),
    						'options' => array('fb'=>'Facebook Url', 'twitter'=>'Twitter Url', 'gplus'=>'GPlus Url', 'linkedin'=>'Linkedin Url',
                                               'pinterest'=>'Pinterest Url', 'instagram'=>'Instagram Url', 'dribbble'=>'Dribbble Url', 
                                               'youtube'=>'Youtube Url', 'vimeo'=>'Vimeo Url', 'vk'=>'VK Url', 'vine'=>'Vine URL',
                                               'snapchat'=>'SnapChat Url', 'telegram' => 'Telegram', 'rss'=>'RSS Url'),
    						'default' => array('fb'=>'', 'twitter'=>'', 'gplus'=>'', 'linkedin'=>'', 'pinterest'=>'', 'instagram'=>'', 'dribbble'=>'', 
                                                'youtube'=>'', 'vimeo'=>'', 'vk'=>'', 'vine'=>'', 'snapchat'=>'', 'telegram' => '', 'rss'=>'')
						),
                        array(
    						'id'=>'bk-header-banner-switch',
    						'type' => 'switch', 
                            'required' => array(
                                'bk-header-type','equals',array( 'header1', 'header2', 'header4', 'header5', 'header6', 'header8', 'header9', 'header11' )
                            ),
    						'title' => esc_html__('Enable header banner', 'rubik'),
    						'subtitle' => esc_html__('Enable banner in header', 'rubik'),
    						'default' => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
    						'id'=>'bk-header-banner',
    						'type' => 'text',
                            'required' => array('bk-header-banner-switch','=','1'),
    						'title' => esc_html__('Header banner', 'rubik'),
    						'subtitle' => esc_html__('Set up banner displays in header', 'rubik'),
    						'options' => array('imgurl'=> esc_html__('Image URL', 'rubik'), 'linkurl'=> esc_html__('Link URL', 'rubik')),
    						'default' => array('imgurl'=>'http://', 'linkurl'=>'http://')
						),
                        array(
                            'id'=>'bk-banner-script',
                            'type' => 'textarea',
                            'title' => esc_html__('Google Adsense Code', 'rubik'),
                            'required' => array('bk-header-banner-switch','=','1'),
                            'default' => '',
                        ),
                        array(
                            'id' => 'section-ticker-header-start',
                            'title' => esc_html__('Ticker Header Setting', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),                       
                        array(
    						'id'=>'bk-header-ticker',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable ticker', 'rubik'),
    						'subtitle' => esc_html__('Enable ticker in header', 'rubik'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
    						'id'=>'bk-ticker-title',
    						'type' => 'text',
                            'required' => array('bk-header-ticker','=','1'),
    						'title' => esc_html__('Ticker title', 'rubik'),
    						'subtitle' => '',
    						'default' => 'Breaking News',
          
						),
                        array(
    						'id'=>'bk-ticker-number',
    						'type' => 'text',
                            'required' => array('bk-header-ticker','=','1'),
    						'title' => esc_html__('Ticker post number', 'rubik'),
    						'subtitle' => esc_html__('Set up number of posts to be shown up', 'rubik'),
    						'default' => '5',
          
						),
                        array(
    						'id'=>'bk-ticker-featured',
    						'type' => 'switch', 
                            'required' => array('bk-header-ticker','=','1'),
    						'title' => esc_html__('Show Featured Posts (Sticky Posts)', 'rubik'),
    						'subtitle' => esc_html__('Display featured post', 'rubik'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
                            'id' => 'ticker-category',  
                            'type' => 'select',
                            'required' => array('bk-ticker-featured','=','0'),
                            'data' => 'categories',
                            'multi' => true,
                            'title' => esc_html__('Categories', 'rubik'),
                            'subtitle' => esc_html__('Set up category', 'rubik'),
                            'desc' => '',
                        ),
                         array(
                            'id' => 'section-ticker-header-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id' => 'section-header-index-start',
                            'title' => esc_html__('Header Index Posts', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),                       
                        array(
    						'id'=>'bk-header-index',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable Header Index Posts', 'rubik'),
    						'subtitle' => esc_html__('Enable ticker in header', 'rubik'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
    						'id'=>'bk-header-index-display',
    						'type' => 'select', 
    						'title' => esc_html__('Display', 'rubik'),
                            'required' => array('bk-header-index','=','1'),
    						'default' => 'front-page',
    						'options' => array(
                                'front-page' => esc_html__('Only on front page','rubik'),
                                'all' => esc_html__('On all pages','rubik'),
                            ),
						),
                        array(
    						'id'=>'bk-header-index-position',
    						'type' => 'select', 
    						'title' => esc_html__('Position', 'rubik'),
                            'required' => array('bk-header-index','=','1'),
    						'default' => 'top',
    						'options' => array(
                                'top' => esc_html__('Very top of the header','rubik'),
                                'bottom' => esc_html__('Under the header','rubik'),
                            ),
						),
                        array(
    						'id'=>'bk-header-index-featured',
    						'type' => 'switch', 
                            'required' => array('bk-header-index','=','1'),
    						'title' => esc_html__('Show Featured Posts (Sticky Posts)', 'rubik'),
    						'subtitle' => esc_html__('Display featured post', 'rubik'),
    						"default" => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
                            'id' => 'header-index-category',  
                            'type' => 'select',
                            'required' => array('bk-header-index-featured','=','0'),
                            'data' => 'categories',
                            'multi' => true,
                            'title' => esc_html__('Categories', 'rubik'),
                            'subtitle' => esc_html__('Set up category', 'rubik'),
                            'desc' => '',
                        ),
                         array(
                            'id' => 'section-header-index-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        )
    				)
    			);
                $this->sections[] = array(
    				'icon' => 'el-icon-credit-card',
    				'title' => esc_html__('Top Bar & The Menus', 'rubik'),
    				'fields' => array(
                        array(
                            'id' => 'section-top-bar-start',
                            'title' => esc_html__('Top Bar', 'rubik'),
                            'subtitle' => esc_html__('Top Bar Config Panel','rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-header-top-switch',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable Header Top Bar', 'rubik'),
    						'subtitle' => '',
						    'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),                                                                       
                        array(
    						'id'=>'bk-header-top-date',
    						'type' => 'switch', 
                            'required' => array('bk-header-top-switch','=','1'),
    						'title' => esc_html__('Enable Date on Header Top Bar', 'rubik'),
    						'subtitle' => '',
						    'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
            				'id'=>'bk-top-date-position',
            				'type' => 'select',
                            'required' => array('bk-header-top-date','=','1'),
                            'title' => esc_html__('Top Bar Date position', 'rubik'), 
    						'options' => array('left' => esc_html__('Left', 'rubik'),'right' => esc_html__('Right', 'rubik')),
    						'default' => 'left',
        				), 
                        array(
    						'id'=>'bk-topbar-bg-style',    
                            'required' => array('bk-header-top-switch','=','1'),                        
    						'type' => 'select',
    						'title' => esc_html__('Top Bar Background Style', 'rubik'),
    						'default'   => 'color',
                            'options'   => array(
                                'color'      => esc_html__( 'Background Color', 'rubik' ),
                                'gradient'   => esc_html__( 'Background Gradient', 'rubik' ),
                            ),
						),
                        array(
    						'id'=>'bk-topbar-bg-gradient',
                            'required' => array(
                                array ('bk-topbar-bg-style', 'equals' , array( 'gradient' )),
                            ),
    						'type' => 'color_gradient',
    						'title'    => esc_html__('Top Bar Background Gradient', 'rubik'),
                            'validate' => 'color',
                            'transparent' => false,
                            'default'  => array(
                                'from' => '#1e73be',
                                'to'   => '#00897e', 
                            ),
						),
                        array(
    						'id'=>'bk-topbar-bg-gradient-direction',
                            'required' => array(
                                array ('bk-topbar-bg-style', 'equals' , array( 'gradient' )),
                            ),
    						'type' => 'text',
    						'title'    => esc_html__('Gradient Direction(Degree Number)', 'rubik'),
                            'validate' => 'numeric',
						),
                        array(
    						'id'=>'bk-topbar-bg-color',
                            'required' => array(
                                array ('bk-topbar-bg-style', 'equals' , array( 'color' )),
                            ),
    						'type' => 'background',
                            'output' => array('.top-bar, .top-bar .rubik-ajax-search-bg'),
    						'title' => esc_html__('Top Bar Background Color', 'rubik'), 
    						'subtitle' => esc_html__('Choose background color for the Top Bar', 'rubik'),
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview_media' => false,
                            'background-image' => false,
                            'transparent' => false,
                            'default'  => array(
                                'background-color' => '#000',
                            ),
						), 
                        array(
    						'id'=>'bk-top-bar-text-color',
    						'type' => 'color',
                            'required' => array('bk-header-top-switch','=','1'),
    						'title' => esc_html__('Top Bar Text Color', 'rubik'), 
    						'subtitle' => esc_html__('Pick a color for the Top Bar Items.', 'rubik'),
                            'output' => array('#top-menu>ul>li>a, .rubik_data_time, .bk-page-header .top-bar .ajax-form-search i'),
    						'default' => '#fff',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-input-search-placeholder-top-bar',    
                            'required' => array(
                                array('bk-header-top-switch','=','1'),   
                                array('bk-header-type','=','header12'),   
                            ),                     
    						'type' => 'select',
    						'title' => esc_html__('Input Ajax-Search Placeholder', 'rubik'),
    						'default'   => 'placeholder-white',
                            'options'   => array(
                                'placeholder-white'   => esc_html__( 'White', 'rubik' ),
                                'placeholder-black'   => esc_html__( 'Black', 'rubik' ),
                            ),
						),                       
                        array(
                            'id' => 'section-top-bar-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        
                        array(
                            'id' => 'section-main-menu-start',
                            'title' => esc_html__('Main Menu', 'rubik'),
                            'subtitle' => esc_html__('Main Menu Config Panel','rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-mainnav-bg-style',                            
    						'type' => 'select',
    						'title' => esc_html__('Main nav Background Style', 'rubik'),
    						'default'   => 'color',
                            'options'   => array(
                                'color'      => esc_html__( 'Background Color', 'rubik' ),
                                'gradient'   => esc_html__( 'Background Gradient', 'rubik' ),
                            ),
						),
                        array(
    						'id'=>'bk-mainnav-bg-gradient',
                            'required' => array(
                                array ('bk-mainnav-bg-style', 'equals' , array( 'gradient' )),
                            ),
    						'type' => 'color_gradient',
    						'title'    => esc_html__('Main Nav Background Gradient', 'rubik'),
                            'validate' => 'color',
                            'transparent' => false,
                            'default'  => array(
                                'from' => '#1e73be',
                                'to'   => '#00897e', 
                            ),
						),
                        array(
    						'id'=>'bk-mainnav-bg-gradient-direction',
                            'required' => array(
                                array ('bk-mainnav-bg-style', 'equals' , array( 'gradient' )),
                            ),
    						'type' => 'text',
    						'title'    => esc_html__('Gradient Direction(Degree Number)', 'rubik'),
                            'validate' => 'numeric',
						),
                        array(
    						'id'=>'bk-mainnav-bg-color',
                            'required' => array(
                                array ('bk-mainnav-bg-style', 'equals' , array( 'color' )),
                            ),
    						'type' => 'background',
                            'output' => array('.main-nav .rubik-ajax-search-bg, .header-12 .main-nav'),
    						'title' => esc_html__('Main Nav Background Color', 'rubik'), 
    						'subtitle' => esc_html__('Choose background color for the Main Nav', 'rubik'),
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview_media' => false,
                            'background-image' => false,
                            'transparent' => false,
                            'default'  => array(
                                'background-color' => '#fff',
                            ),
						),
                        array(
    						'id'=>'bk-main-menu-text-color',
    						'type' => 'color',
    						'title' => esc_html__('Main Menu Text Color', 'rubik'), 
    						'subtitle' => esc_html__('Pick a color for the Main Menu Items.', 'rubik'),
                            'output' => array('.main-nav .main-menu > ul > li > a, .ajax-form-search i, .ajax-form input, .header-3 .header-wrap .logo'),
    						'default' => '#000',
    						'validate' => 'color',
						),
                                                
                        array(
    						'id'=>'bk-main-menu-hover-text-color',
    						'type' => 'color',
    						'title' => esc_html__('Main Menu Hover Text Color', 'rubik'), 
    						'subtitle' => esc_html__('Pick a color for the Main Menu Text hover.', 'rubik'),
                            'output' => array('.main-nav .main-menu > ul > li:hover > a, .main-nav .main-menu>ul>li.current-menu-item>a'),
    						'default' => '#f8f8f8',
    						'validate' => 'color',
						),
                        array(
            				'id'=>'bk-main-nav-layout',
            				'type' => 'select',
                            'required' => array(
                                'bk-header-type','equals',array( 'header1', 'header2', 'header4', 'header11', 'header12' )
                            ),
                            'title' => esc_html__('Main nav alignment', 'rubik'), 
                            'subtitle' => esc_html__('Choose between left and center aligned for the main navigation', 'rubik'),
    						'options' => array('left' => esc_html__('Left', 'rubik'),'center' => esc_html__('Center', 'rubik')),
    						'default' => 'left',
        				),
                        array(
    						'id'=>'bk-sticky-nav-switch',
    						'type' => 'button_set', 
    						'title' => esc_html__('Enable Sticky Header menu', 'rubik'),
    						'subtitle'=> esc_html__('Enable/Disable Sticky Header', 'rubik'),
                            'options' => array('2' => esc_html__('Enable', 'rubik'), '1' => esc_html__('Disable', 'rubik')),
    						'default' => '2',
						),
                        array(
    						'id'=>'bk-sticky-nav-logo',
    						'type' => 'media', 
                            'required' => array('bk-sticky-nav-switch','=','2'),
    						'url'=> true,
    						'title' => esc_html__('Sticky Nav logo', 'rubik'),
    						'subtitle' => esc_html__('Upload logo of your site that is displayed in sticky nav', 'rubik'),
                            'placeholder' => esc_html__('No media selected','rubik')
						),
                        array(
    						'id'=>'bk-input-search-placeholder-main-nav',                         
    						'type' => 'select',
    						'title' => esc_html__('Input Ajax-Search Placeholder', 'rubik'),
    						'default'   => 'placeholder-black',
                            'options'   => array(
                                'placeholder-white'   => esc_html__( 'White', 'rubik' ),
                                'placeholder-black'   => esc_html__( 'Black', 'rubik' ),
                            ),
						),
                        array(
                            'id' => 'section-main-menu-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id' => 'section-canvas-menu-start',
                            'title' => esc_html__('Off-Canvas Panel', 'rubik'),
                            'subtitle' => esc_html__('Off-Canvas Panel Configuration','rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-canvas-desktop-switch',
    						'type' => 'switch', 
    						'title' => esc_html__('Canvas Menu On Desktop', 'rubik'),
    						'subtitle' => esc_html__('Enable Canvas Menu On Desktop', 'rubik'),
    						'default' => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
            				'id'=>'bk-canvas-button-position',
            				'type' => 'select',
                            'required' => array(
                                'bk-header-type','equals',array( 'header1', 'header2', 'header3', 'header7', 'header4', 'header11', 'header10' )
                            ),
                            'title' => esc_html__('Canvas Button alignment', 'rubik'), 
                            'subtitle' => esc_html__('Choose between left and right align', 'rubik'),
    						'options' => array('left' => esc_html__('Left', 'rubik'),'right' => esc_html__('Right', 'rubik')),
    						'default' => 'left',
        				),
                        array(
    						'id'=>'bk-canvas-panel-style',
    						'type' => 'select', 
    						'title' => esc_html__('Canvas Panel Style', 'rubik'),
    						'default' => 'light',
    						'options' => array(
                                    'light' => esc_html__('Light', 'rubik'),
                                    'dark' => esc_html__('Dark', 'rubik'),
                            )
						),
                        array(
    						'id'=>'bk-off-canvas-panel-bg',
    						'type' => 'background',
    						'output' => array('#main-canvas-menu .rubik-canvas-background-img'),
                            'background-color'  => false,
                            'background-attachment'  => false,
                            'preview' => true,
    						'title' => esc_html__('Off-Canvas Background', 'rubik'), 
						),
                        array(
    						'id'=>'bk-offcanvas-logo',
    						'type' => 'media', 
    						'url'=> true,
    						'title' => esc_html__('Off-Canvas logo', 'rubik'),
    						'subtitle' => esc_html__('Upload logo of your site that is displayed in Off-Canvas menu', 'rubik'),
                            'placeholder' => esc_html__('No media selected','rubik')
						),
                        array(
            				'id'=>'bk-offcanvas-menu-setup',
            				'type' => 'typography', 
                            'output' => array('#main-canvas-menu .rubik-canvas-menu li a, #main-canvas-menu .expand'),
            				'title' => esc_html__('Off-Canvas Menu Font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google' => true, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => true,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => true,
            				'text-transform' => true,
                            'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>true,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>false, // Defaults to px
                            'text-align' => false,
                            'default'     => array(
                                'font-size' => '16px',
                                'color'     => '#ccc',
                            ),
                        ), 
                        array(
    						'id'=>'bk-offcanvas-social-header-switch',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable social header in Off-Canvas Menu ', 'rubik'),
    						'default' => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
                            'id'=>'bk-offcanvas-copyright',
                            'type' => 'textarea',
                            'title' => esc_html__('Off-Canvas Copyright', 'rubik'),
                            'default' => '',
                        ),
                        array(
    						'id'=>'bk-canvas-button-style',
    						'type' => 'select', 
    						'title' => esc_html__('Canvas Button Style', 'rubik'),
    						'default' => 'normal',
    						'options' => array(
                                    'normal' => esc_html__('Normal', 'rubik'),
                                    'bold'   => esc_html__('Bold', 'rubik'),
                            )
						),
                        array(
    						'id'=>'bk-canvas-menu-button-color',
    						'type' => 'color',
    						'title' => esc_html__('Off-Canvas Menu Button Color', 'rubik'), 
    						'subtitle' => esc_html__('Pick a color', 'rubik'),
                            'output' => array('.canvas-nav-btn, .header-12 .header-social .social-icon a'),
    						'default' => '#222',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-canvas-menu-button-color-hover',
    						'type' => 'color',
    						'title' => esc_html__('Off-Canvas Menu Button Color in Hover', 'rubik'), 
    						'subtitle' => esc_html__('Pick a color', 'rubik'),
                            'output' => array('.canvas-nav-btn:hover'),
    						'default' => '#000',
    						'validate' => 'color',
						),
                        
                        array(
    						'id'=>'bk-canvas-menu-12-button-color',
    						'type' => 'color',
                            'required' => array(
                                array('bk-header-type','=','header12'),   
                            ), 
    						'title' => esc_html__('Off-Canvas on Main Menu Button Color', 'rubik'), 
    						'subtitle' => esc_html__('Pick a color', 'rubik'),
                            'output' => array('.header-12 .main-nav-wrap .canvas-menu-wrap .canvas-nav-btn'),
    						'default' => '#222',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-canvas-menu-12-button-color-hover',
    						'type' => 'color',
                            'required' => array(
                                array('bk-header-type','=','header12'),   
                            ), 
    						'title' => esc_html__('Off-Canvas on Main Menu Button Color in Hover', 'rubik'), 
    						'subtitle' => esc_html__('Pick a color', 'rubik'),
                            'output' => array('.header-12 .main-nav-wrap .canvas-menu-wrap .canvas-nav-btn:hover'),
    						'default' => '#000',
    						'validate' => 'color',
						),
                        array(
                            'id' => 'section-canvas-menu-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                    )
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-credit-card',
    				'title' => esc_html__('Footer Settings', 'rubik'),
    				'fields' => array(
                        array(
                            'id'       => 'footer-sortable',
                            'type'     => 'sortable',
                            'title'    => esc_html__('Footer Sortable Option', 'rubik'),
                            'subtitle' => esc_html__('Reorder the footer type you want.', 'rubik'),
                            'mode'     => 'checkbox',
                            'options'  => array(
                                '1'     => 'Footer 1/3 1/3 1/3',
                                '2'     => 'Footer 40 30 30',
                                '3'     => 'Footer 60 40',
                                '4'     => 'Footer Full Width 1 Column',
                            ),
                            // For checkbox mode
                            'default' => array(
                                '1' => false,
                                '2' => true,
                                '3' => false
                            ),
                        ), 
                        array(
    						'id'=>'bk-footer-padding-top',
    						'type' => 'text', 
                            'validate' => 'numeric',
    						'title' => esc_html__('Footer Padding Top - in pixel', 'rubik'),
    						'subtitle' => esc_html__('input: number', 'rubik'),
                            'default' => '50',
						),
                        array(
    						'id'=>'bk-footer-padding-bottom',
    						'type' => 'text', 
                            'validate' => 'numeric',
    						'title' => esc_html__('Footer Padding Bottom - in pixel', 'rubik'),
    						'subtitle' => esc_html__('input: number', 'rubik'),
                            'default' => '50',
						),
                        array(
                            'id' => 'section-footer-bg-start',
                            'title' => esc_html__('Background', 'rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-footer-bg-style',                  
    						'type' => 'select',
    						'title' => esc_html__('Footer Background Style', 'rubik'),
    						'default'   => 'color',
                            'options'   => array(
                                'color'      => esc_html__( 'Background Color', 'rubik' ),
                                'image'      => esc_html__( 'Background Image', 'rubik' ),
                                'gradient'   => esc_html__( 'Background Gradient', 'rubik' ),
                            ),
						),
                        array(
    						'id'=>'bk-footer-bg-gradient',
                            'required' => array(
                                array ('bk-footer-bg-style', 'equals' , array( 'gradient' )),
                            ),
    						'type' => 'color_gradient',
    						'title'    => esc_html__('Background Gradient', 'rubik'),
                            'validate' => 'color',
                            'transparent' => false,
                            'default'  => array(
                                'from' => '#1e73be',
                                'to'   => '#00897e', 
                            ),
						),
                        array(
    						'id'=>'bk-footer-bg-gradient-direction',
                            'required' => array(
                                array ('bk-footer-bg-style', 'equals' , array( 'gradient' )),
                            ),
    						'type' => 'text',
    						'title'    => esc_html__('Gradient Direction(Degree Number)', 'rubik'),
                            'validate' => 'numeric',
						),
                        array(
    						'id'=>'bk-footer-bg-img',
                            'title' => esc_html__('Background Image', 'rubik'), 
                            'required' => array(
                                array ('bk-footer-bg-style', 'equals' , array( 'image' )),
                            ),
    						'type' => 'background',
                            'background-color' => false,
    						'output' => array('.footer'),
						),
                        array(
    						'id'=>'bk-footer-bg-color',
                            'required' => array(
                                array ('bk-footer-bg-style', 'equals' , array( 'color' )),
                            ),
    						'type' => 'background',
                            'output' => array('.footer, .footer:before'),
    						'title' => esc_html__('Background Color', 'rubik'), 
    						'subtitle' => esc_html__('Choose background color for the Footer', 'rubik'),
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview_media' => false,
                            'background-image' => false,
                            'transparent' => false,
                            'default'  => array(
                                'background-color' => '#000',
                            ),
						),
                        array(
    						'id'=>'bk-footer-bg-overlay',
                            'required' => array(
                                array ('bk-footer-bg-style', 'equals' , array( 'image' )),
                            ),
    						'type' => 'background',
                            'output' => array('.footer:before'),
    						'title' => esc_html__('Footer Overlay', 'rubik'), 
    						'subtitle' => esc_html__('Choose an overlay color for the Footer', 'rubik'),
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview_media' => false,
                            'background-image' => false,
                            'transparent' => true,
                            'default'  => array(
                                'background-color' => '#000',
                            ),
						),
                        array(
                            'id' => 'section-footer-bg-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-footer-content-style',
    						'type' => 'select', 
    						'title' => esc_html__('Footer Content Color', 'rubik'),
    						'default' => 'bk-footer-dark',
                            'options' => array('bk-footer-dark'=>'Dark', 'bk-footer-light'=>'Light'),
						),
                        array(
    						'id'=>'bk-footer-a-hover-color',
    						'type' => 'color',
    						'title' => esc_html__('Force Footer Link Hover Color', 'rubik'), 
    						'subtitle' => esc_html__('Leave empty to use the default settings', 'rubik'),
    						'default' => '',
    						'validate' => 'color',
						),
                        
                        array(
    						'id'=>'bk-footer-menu-social-position',
    						'type' => 'select', 
    						'title' => esc_html__('Footer Menu + Social Media Position', 'rubik'),
                            'options' => array('top'=>'Footer Top', 'bottom'=>'Footer Bottom', 'disable' => 'Disable'),
                            'default' => 'disable',
						),
                        
                        array(
                            'id' => 'section-footer-menu-start',
                            'required' => array('bk-footer-menu-social-position','!=','disable'),
                            'title' => esc_html__('Footer Menu + Social Media Section', 'rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                                                                                                
                        array(
            				'id'=>'bk-footer-title-font-setup',
            				'type' => 'typography', 
                            'output' => array('#footer-menu'),
            				'title' => esc_html__('Footer Menu Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => true,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => true,
            				'text-transform' => true,
                            'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>true,
            				'preview'=>true, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => true,
            				'subtitle'=> esc_html__('Font options for the Footer Menu Font', 'rubik'),
                            'default'     => array(
                                'font-family'=>'Open Sans', 
            					'google' => true,
                                'font-style' => 'normal',
                                'font-size'   => '16px',
                                'text-transform' => 'uppercase',
                                'text-align' => 'center',
                            ),
                        ),   
                        array(
    						'id'=>'bk-social-footer-switch',
    						'type' => 'switch',
    						'title' => esc_html__('Enable social footer ', 'rubik'),
    						'subtitle' => esc_html__('Enable social header by display icons', 'rubik'),
    						'default' => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
    						'id'=>'bk-social-footer',
    						'type' => 'text',
                            'required' => array('bk-social-footer-switch','=','1'),
    						'title' => esc_html__('Social media', 'rubik'),
    						'subtitle' => esc_html__('Set up social links for site', 'rubik'),
    						'options' => array('fb'=>'Facebook Url', 'twitter'=>'Twitter Url', 'gplus'=>'GPlus Url', 'linkedin'=>'Linkedin Url',
                                               'pinterest'=>'Pinterest Url', 'instagram'=>'Instagram Url', 'dribbble'=>'Dribbble Url', 
                                               'youtube'=>'Youtube Url', 'vimeo'=>'Vimeo Url', 'vk'=>'VK Url', 'vine'=>'Vine URL',
                                               'snapchat'=>'SnapChat Url', 'telegram' => 'Telegram', 'rss'=>'RSS Url'),
    						'default' => array('fb'=>'', 'twitter'=>'', 'gplus'=>'', 'linkedin'=>'', 'pinterest'=>'', 'instagram'=>'', 'dribbble'=>'', 
                                                'youtube'=>'', 'vimeo'=>'', 'vk'=>'', 'vine'=>'', 'snapchat'=>'', 'telegram' => '', 'rss'=>'')
						),
                        array(
            				'id'=>'bk-social-item-footer-setup',
                            'required' => array('bk-social-footer-switch','=','1'),
            				'type' => 'typography', 
                            'output' => array('.footer-social ul li'),
            				'title' => esc_html__('Social Items Footer Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'text-transform' => false,
                            'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>false, // Defaults to false
            				'color'=>true,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>false, // Defaults to px
                            'text-align' => false,
                            'default'     => array(
                                'font-size'   => '16px',
                                'color'     => '#ccc',
                            ),
                        ), 
                        array(
    						'id'=>'bk-social-item-footer-bg-hover',
                            'required' => array('bk-social-footer-switch','=','1'),
    						'type' => 'background',
    						'output' => array('.footer-social ul li:hover'),
                            'background-image' => false,
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview' => false,
    						'title' => esc_html__('Social Items Footer Background on Hover', 'rubik'), 
                            'default'  => array(
                                'background-color' => '#222',
                            ),
						),
                        array(
                            'id' => 'section-footer-menu-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),   
                        array(
                            'id' => 'section-footer-instagram-start',
                            'title' => esc_html__('Footer Instagram Section', 'rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-footer-instagram-title',
    						'type' => 'text', 
    						'title' => esc_html__('Instagram Title', 'rubik'),
                            'default' => '',
						),
                        array(
    						'id'=>'bk-footer-instagram-access-token',
    						'type' => 'text', 
    						'title' => esc_html__('Instagram Access Token', 'rubik'),
                            'desc'  => esc_html__('Please get the Instagram Token here: https://instagram.pixelunion.net', 'rubik'),
                            'default' => '',
						),
                        array(
    						'id'=>'bk-footer-instagram-image-count',
    						'type' => 'text', 
    						'title' => esc_html__('Image Count', 'rubik'),
                            'validate' => 'numeric',
                            'default' => 12,
						),
                        array(
    						'id'=>'bk-footer-instagram-columns',
    						'type' => 'text', 
    						'title' => esc_html__('Instagram Columns on Desktop', 'rubik'),
                            'validate' => 'numeric',
                            'default' => 7,
						),
                        array(
                            'id' => 'section-footer-instagram-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
                            'id' => 'section-footer-lower-start',
                            'title' => esc_html__('Footer Lower Section', 'rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-footer-lower-bg-style',                           
    						'type' => 'select',
    						'title' => esc_html__('Footer Lower Background Style', 'rubik'),
    						'default'   => 'color',
                            'options'   => array(
                                'color'      => esc_html__( 'Background Color', 'rubik' ),
                                'gradient'   => esc_html__( 'Background Gradient', 'rubik' ),
                            ),
						),
                        array(
    						'id'=>'bk-footer-lower-bg-gradient',
                            'required' => array(
                                array ('bk-footer-lower-bg-style', 'equals' , array( 'gradient' )),
                            ),
    						'type' => 'color_gradient',
    						'title'    => esc_html__('Footer Lower Background Gradient', 'rubik'),
                            'validate' => 'color',
                            'transparent' => false,
                            'default'  => array(
                                'from' => '#1e73be',
                                'to'   => '#00897e', 
                            ),
						),
                        array(
    						'id'=>'bk-footer-lower-bg-gradient-direction',
                            'required' => array(
                                array ('bk-footer-lower-bg-style', 'equals' , array( 'gradient' )),
                            ),
    						'type' => 'text',
    						'title'    => esc_html__('Gradient Direction(Degree Number)', 'rubik'),
                            'validate' => 'numeric',
						),
                        array(
    						'id'=>'bk-footer-lower-bg-color',
                            'required' => array(
                                array ('bk-footer-lower-bg-style', 'equals' , array( 'color' )),
                            ),
    						'type' => 'background',
                            'output' => array('.footer .footer-lower'),
    						'title' => esc_html__('Footer Lower Background Color', 'rubik'), 
    						'subtitle' => esc_html__('Choose background color for the Footer Lower', 'rubik'),
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview_media' => false,
                            'background-image' => false,
                            'transparent' => false,
                            'default'  => array(
                                'background-color' => '#000',
                            ),
						), 
                        array(
    						'id'=>'cr-text',
    						'type' => 'textarea',
    						'title' => esc_html__('[Left Side] Copyright text - HTML Validated', 'rubik'), 
    						'subtitle' => esc_html__('HTML Allowed (wp_kses)', 'rubik'),
    						'validate' => 'html',
    						'default' => ''
						),
                        array(
    						'id'=>'bk-right-side-footer-lower',
    						'type' => 'select', 
    						'title' => esc_html__('[Right Side] Footer Lower Menu or Social Media', 'rubik'),
                            'options' => array('footer-lower-menu'=>'Footer Lower Menu', 'social-media'=>'Social Media Items', 'disable' => 'Disable'),
                            'default' => 'social-media',
						),
                        array(
    						'id'=>'bk-footer-lower-social',
    						'type' => 'text',
                            'required' => array('bk-right-side-footer-lower','=','social-media'),
    						'title' => esc_html__('Social media', 'rubik'),
    						'subtitle' => esc_html__('Set up social links', 'rubik'),
    						'options' => array('fb'=>'Facebook Url', 'twitter'=>'Twitter Url', 'gplus'=>'GPlus Url', 'linkedin'=>'Linkedin Url',
                                               'pinterest'=>'Pinterest Url', 'instagram'=>'Instagram Url', 'dribbble'=>'Dribbble Url', 
                                               'youtube'=>'Youtube Url', 'vimeo'=>'Vimeo Url', 'vk'=>'VK Url', 'vine'=>'Vine URL',
                                               'snapchat'=>'SnapChat Url', 'telegram' => 'Telegram', 'rss'=>'RSS Url'),
    						'default' => array('fb'=>'', 'twitter'=>'', 'gplus'=>'', 'linkedin'=>'', 'pinterest'=>'', 'instagram'=>'', 'dribbble'=>'', 
                                                'youtube'=>'', 'vimeo'=>'', 'vk'=>'', 'vine'=>'', 'snapchat'=>'', 'telegram' => '', 'rss'=>'')
						),
                        array(
            				'id'=>'bk-footer-lower-font-setup',
            				'type' => 'typography', 
                            'output' => array('.footer-lower-left .bk-copyright, .footer-lower-right #footer-menu-lower, .footer-lower-right ul li.social-icon'),
            				'title' => esc_html__('Footer Lower Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => true,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => true,
            				'text-transform' => true,
                            'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>true,
            				'preview'=>true, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Footer Menu Font', 'rubik'),
                            'default'     => array(
                                'font-family'=>'Open Sans', 
            					'google' => true,
                                'font-style' => 'normal',
                                'font-size'   => '16px',
                                'text-transform' => 'normal',
                            ),
                        ),   
                        array(
                            'id' => 'section-footer-lower-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
    				)
    			);
                $this->sections[] = array(
            		'icon'    => ' el-icon-font',
            		'title'   => esc_html__('General Typography', 'rubik'),
            		'fields'  => array(
                        array(
            				'id'=>'bk-body-font-setup',
            				'type' => 'typography', 
                            'output' => array('body'),
            				'title' => esc_html__('Body Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Small Post Layout', 'rubik'),
                            'default'     => array(
                                'font-size'   => '16px',
                            ),
                        ),
                        array(
                            'id' => 'section-top-menu-typography-start',
                            'title' => esc_html__('Top Bar', 'rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
            				'id'=>'bk-top-menu-font',
            				'type' => 'typography', 
                            'output' => array('#top-menu>ul>li, #top-menu>ul>li .sub-menu li, .rubik_data_time'),
            				'title' => esc_html__('Top-menu font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for top menu', 'rubik'),
            				'default'=> array( 
            					'font-weight'=>'500', 
            					'font-family'=>'Roboto', 
            					'google' => true,
            				    ),
                        ),
                        array(
                            'id' => 'section-top-menu-typography-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id' => 'section-main-menu-typography-start',
                            'title' => esc_html__('Main Menu', 'rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
            				'id'=>'bk-main-menu-font',
            				'type' => 'typography', 
                            'output' => array('.main-nav .main-menu .menu > li, .main-nav .main-menu .menu > li > a, .mega-title h3, .header .logo.logo-text h1,
                            .comment-box .comment-author-name, .today-date'),
            				'title' => esc_html__('Main-menu font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
                            'text-transform' => true,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for main menu', 'rubik'),
            				'default'=> array( 
            					'font-weight'=>'700', 
            					'font-family'=>'Roboto', 
            					'google' => true,
                                'text-transform' => 'Uppercase'
            				    ),
                        ),
                        array(
            				'id'=>'bk-main-menu-font-setup',
            				'type' => 'typography', 
                            'output' => array('.main-nav .main-menu .menu > li > a, .bk-sub-menu>li>a, .bk-sub-sub-menu>li>a'),
            				'title' => esc_html__('Main-menu Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for main menu', 'rubik'),
                        ),
                        array(
            				'id'=>'bk-main-menu-post-title-setup',
            				'type' => 'typography', 
                            'output' => array('.main-nav .bk-sub-posts .post-title a'),
            				'title' => esc_html__('Mega-menu Post Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for post title', 'rubik'),
                        ),
                        array(
                            'id' => 'section-main-menu-typography-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        array(
            				'id'=>'bk-meta-font',
            				'type' => 'typography', 
                            'output' => array('.meta, .post-category, .widget_comment .cm-header div, .module-feature2 .post-author, .comment-box .comment-time, .share-box ul li .share-item__value',
                            '.share-box .bk-share .share-item__valuem, .share-total, .loadmore span.ajaxtext,
                            .post-c-wrap .readmore a, .breadcrumbs, .s-post-nav .nav-title span, .product_meta .post-tags-title,
                            .button, .bk-search-content .nothing-respond'
                            ),
            				'title' => esc_html__('Meta font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for meta', 'rubik'),
            				'default'=> array( 
            					'font-weight'=>'600', 
            					'font-family'=>'Open Sans', 
            					'google' => true,
            				    ),
                        ),
                        array(
            				'id'=>'bk-title-font',
            				'type' => 'typography', 
                            'output' => array('h1, h2, h3, h4, h5, .ticker-title, .bk-copyright, .rubik-widget-categories li,
                            .widget-social-counter .counter, .widget-social-counter ul li .data .subscribe, .related-box .related-box-tabs a,
                            .bk_tabs  .ui-tabs-nav li, .bk-tabs-more, .bkteamsc .team-member .member-name, .buttonsc, .bk-forum-title, .bk-sub-posts .post-title, 
                            .module-title .bk-tabs, .widget_recent_entries > ul li, .widget_archive > ul li, .widget_categories > ul li, .widget_meta > ul li, .widget_pages > ul li, .widget_recent_comments > ul li, .widget_nav_menu > div > ul li,
                            .bk-module-pagination .pagination-btn, .bk-widget-pagination .widget-pagination-btn, .bk-related-pagination .related-pagination-btn, .bk-megamenu-pagination .megamenu-pagination-btn '),
            				'title' => esc_html__('Title font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for title', 'rubik'),
            				'default'=> array( 
            					'font-weight'=>'700', 
            					'font-family'=>'Open Sans', 
            					'google' => true,
            				    ),
                        ),
                        array(
            				'id'=>'bk-body-font',
            				'type' => 'typography',
                            'output' => array('body, textarea, input, p, 
                            .entry-excerpt, .comment-text, .comment-author, .article-content,
                            .comments-area, .tag-list, .bk-mega-menu .bk-sub-posts .feature-post .menu-post-item .post-date, .comments-area small'), 
            				'title' => esc_html__('Text font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>false,
            				'line-height'=>false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for text body', 'rubik'),
            				'default'=> array(
            					'font-weight'=>'400', 
            					'font-family'=>'Lato', 
            					'google' => true,
                            ),
            			),
                        array(
                            'id' => 'section-blockquote-typography-start',
                            'title' => esc_html__('Blockquote', 'rubik'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
            				'id'=>'bk-blockquote-font',
            				'type' => 'typography', 
                            'output' => array('blockquote'),
            				'title' => esc_html__('Blockquote Font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				'font-style'=>true, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>true,
            				'line-height'=>true,
                            'text-transform' => true,
                            'text-align' => true,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>true,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Post Body', 'rubik'),
            				'default'=> array( 
            					'font-weight'=>'400', 
            					'font-family'=>'Open Sans', 
            					'google' => true,
                                'font-size'=> '22px',
                                'line-height'=> '35px',
                                'color'=>'#fff',
                                'text-transform' => 'inherit',
                                'text-align' => 'initial',
            				    ),
                        ),
                        array(
    						'id'=>'bk-blockquote-bg',
    						'type' => 'background',
    						'output' => array('blockquote'),
                            'background-image' => false,
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview' => false,
    						'title' => esc_html__('Blockquote Background', 'rubik'), 
                            'default'  => array(
                                'background-color' => '#EA2323',
                            ),
						),
                        array(
                            'id' => 'section-blockquote-typography-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                    ),
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-file-edit',
    				'title' => esc_html__('Small Post Settings', 'rubik'),
    				'fields' => array(
                        array(
            				'id'=>'bk-small-post-font-setup',
            				'type' => 'typography', 
                            'output' => array('.content_out.small-post h4'),
            				'title' => esc_html__('Small Post Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => true,
            				'font-style'=> false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Small Post Layout', 'rubik'),
                        ),                        
    				)
    			);
                $this->sections[] = array(
            		'icon'    => 'el-icon-book',
            		'title'   => esc_html__('Pages in General', 'rubik'),
            		'heading' => esc_html__('Pages in General','rubik'),
            		'fields'  => array(
                    /*** breadcrumbs ***/
                    array(
                        'id' => 'section-breadcrumb-start',
                        'title' => esc_html__('Breadcrumb Setting', 'rubik'),
                        'subtitle' => '',
                        'type' => 'section',                             
                        'indent' => true // Indent all options below until the next 'section' option is set.
                    ),
                    array(
                        'id' => 'bk-breadcrumbs',  
                        'type' => 'button_set',
                        'multi' => false,
                        'title' => esc_html__('BreadCrumb Setting', 'rubik'),
                        'subtitle' => esc_html__('Show/Hide Breadcrumbs on pages', 'rubik'),
                        'desc' => '',
                        'options' => array('enable' => esc_html__('Show', 'rubik'), 
                                            'disable' => esc_html__('Hide', 'rubik')
                                            ),
                        'default' => 'disable',
                    ),
                    array(
        				'id'=>'bk-breadcrumb-font-setup',
        				'type' => 'typography', 
                        'required' => array('bk-breadcrumbs','=','enable'),
                        'output' => array('.bk-breadcrumbs-wrap .breadcrumbs'),
        				'title' => esc_html__('Breadcrumb Font Setup', 'rubik'),
        				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
        				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-weight' => false,
        				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
        				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                        'font-family' => false,
        				'font-size'=>true,
        				'line-height'=>false,
        				'word-spacing'=>false, // Defaults to false
        				'letter-spacing'=>true, // Defaults to false
        				'color'=>false,
        				'preview'=>false, // Disable the previewer
        				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
        				'units'=>'px', // Defaults to px
                        'text-align' => false,
        				'subtitle'=> esc_html__('Font options for the Breadcrumb', 'rubik'),
                    ),
                    array(
                        'id'             => 'opt-spacing',
                        'required' => array('bk-breadcrumbs','=','enable'),
                        'type'           => 'spacing',
                        'output'         => array('.bk-breadcrumbs-wrap .breadcrumbs'),
                        'mode'           => 'padding',
                        'units'          => array('em', 'px'),
                        'units_extended' => 'false',
                        'title'          => esc_html__('Padding/Margin Option', 'rubik'),
                        'subtitle'       => esc_html__('Allow your users to choose the spacing or margin they want.', 'rubik'),
                        'default'            => array(
                            'margin-top'     => '16px', 
                            'margin-right'   => '24px', 
                            'margin-bottom'  => '16px', 
                            'margin-left'    => '24px',
                            'units'          => 'px', 
                        )
                    ),
                    array(
                        'id' => 'section-breadcrumb-end',
                        'type' => 'section',                             
                        'indent' => false // Indent all options below until the next 'section' option is set.
                    ), 
                    /*** Pagebuilder ***/
                    array(
                            'id' => 'section-front-layout-start',
                            'title' => esc_html__('Pagebuilder Setting', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),   
                        array(
                            'id' => 'pagebuilder-sidebar',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Enable / Disable Stick Sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'enable',
                        ),
                        array(
    						'id'=>'bk-youtube-api-key',
    						'type' => 'text',
    						'title' => esc_html__('Youtube API Key', 'rubik'),
    						'subtitle' => '',
    						'default' => '',
						),
                        array(
                            'id' => 'section-blog-layout-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                    ),
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-inbox-box',
    				'title' => esc_html__('Category', 'rubik'),
                    'heading'   => esc_html__('Category Pages', 'rubik'),
                    'desc'   => esc_html__('Only use for category pages', 'rubik'),
    				'fields' => array(
                        array(
            				'id'=>'bk-category-layout',
            				'type' => 'select',
                            'title' => esc_html__('Category page layout', 'rubik'),
                            'subtitle' => esc_html__('Global setting for layout of category archive page, will be overridden by layout option in category edit page.', 'rubik'), 
    						'options' => array('classic-blog' => esc_html__('Classic Blog', 'rubik'), 
                                                'large-blog' => esc_html__('Large Blog with Sidebar', 'rubik'), 
                                                'large-blog-nosb' => esc_html__('Large Blog No Sidebar', 'rubik'), 
                                                'large-blog-2' => esc_html__('Large Blog 2 with Sidebar', 'rubik'),
                                                'large-blog-2-nosb' => esc_html__('Large Blog 2 No Sidebar', 'rubik'),
                                                'row'=>esc_html__('Row with Sidebar', 'rubik'),
                                                'row-nosb' => esc_html__('Row no Sidebar', 'rubik'),
                                                'masonry' => esc_html__('Masonry with Sidebar', 'rubik'),
                                                'masonry-nosb' => esc_html__('Masonry No Sidebar', 'rubik'),
                                                'square-grid-3-s1' => esc_html__('Square Grid No Sidebar', 'rubik'),                                                             
                                                'square-grid-2-s1' => esc_html__('Square Grid with Sidebar', 'rubik'),
                                                ),
    						'default' => 'masonry',
            			),
                        array(
            				'id'=>'bk-category-font-setup',
            				'type' => 'typography', 
                            'output' => array('.bk-category-content .post-c-wrap h4.title, .bk-category-content .content_in .post-c-wrap h4.title'),
            				'title' => esc_html__('Category page Post Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Leave blank to have the default font setup', 'rubik'),
                        ),
                        array(
                            'id' => 'category_post_icon',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Enable Post Icon on category page', 'rubik'),
                            'subtitle' => esc_html__('Support for "Classic Blog layout", "Large Blog layout" and "Masonry Layout"', 'rubik'),
                            'desc' => '',
                            'options' => array('show' => esc_html__('Show', 'rubik'), 
                                                'hide' => esc_html__('Hide', 'rubik')
                                                ),
                            'default' => 'hide',
                        ),
                        array(
                            'id' => 'category-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Choose sidebar for Category page', 'rubik'),
                            'desc' => '',
                            'default' => 'home_sidebar',
                        ),
                        array(
                            'id' => 'category-page-sidebar-position',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Sidebar Position', 'rubik'),
                            'subtitle' => esc_html__('Support right and left sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('right' => esc_html__('Right', 'rubik'), 
                                                'left' => esc_html__('Left', 'rubik')
                                                ),
                            'default' => 'right',
                        ),
                        array(
                            'id' => 'category-stick-sidebar',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'enable',
                        ),
                    )
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-tags',
    				'title' => esc_html__('Archive', 'rubik'),
                    'heading'   => esc_html__('Archive Pages', 'rubik'),
                    'desc'   => esc_html__('Use for Tag / Archive Pages', 'rubik'),
    				'fields' => array(
                        array(
            				'id'=>'bk-archive-layout',
            				'type' => 'select',
                            'title' => esc_html__('Archive page layout', 'rubik'), 
                            'subtitle' => esc_html__('Layout for Archive page and Tag archive.', 'rubik'),
    						'options' => array('classic-blog' => esc_html__('Classic Blog', 'rubik'), 
                                                'large-blog' => esc_html__('Large Blog with Sidebar', 'rubik'), 
                                                'large-blog-nosb' => esc_html__('Large Blog No Sidebar', 'rubik'), 
                                                'large-blog-2' => esc_html__('Large Blog 2 with Sidebar', 'rubik'),
                                                'large-blog-2-nosb' => esc_html__('Large Blog 2 No Sidebar', 'rubik'),
                                                'row'=>esc_html__('Row with Sidebar', 'rubik'),
                                                'row-nosb' => esc_html__('Row no Sidebar', 'rubik'),
                                                'masonry' => esc_html__('Masonry with Sidebar', 'rubik'),
                                                'masonry-nosb' => esc_html__('Masonry no Sidebar', 'rubik'),
                                                'square-grid-3-s1' => esc_html__('Square Grid no Sidebar', 'rubik'),                                                             
                                                'square-grid-2-s1' => esc_html__('Square Grid with Sidebar', 'rubik'),
                                                ),
    						'default' => 'classic-blog',
            			),
                        array(
            				'id'=>'bk-archive-font-setup',
            				'type' => 'typography', 
                            'output' => array('.bk-archive-content .post-c-wrap h4.title, .bk-archive-content .content_in .post-c-wrap h4.title'),
            				'title' => esc_html__('Archive page Post Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Leave blank to have the default font setup', 'rubik'),
                        ),
                        array(
                            'id' => 'archive_post_icon',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Enable Post Icon on Archive page', 'rubik'),
                            'subtitle' => esc_html__('Support for "Classic Blog layout", "Large Blog layout" and "Masonry Layout"', 'rubik'),
                            'desc' => '',
                            'options' => array('show' => esc_html__('Show', 'rubik'), 
                                                'hide' => esc_html__('Hide', 'rubik')
                                                ),
                            'default' => 'hide',
                        ),
                        array(
                            'id' => 'archive-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Choose sidebar for Archive page', 'rubik'),
                            'desc' => '',
                            'default' => 'home_sidebar',
                        ),
                        array(
                            'id' => 'archive-page-sidebar-position',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Sidebar Position', 'rubik'),
                            'subtitle' => esc_html__('Support right and left sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('right' => esc_html__('Right', 'rubik'), 
                                                'left' => esc_html__('Left', 'rubik')
                                                ),
                            'default' => 'right',
                        ),
                        array(
                            'id' => 'archive-stick-sidebar',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'enable',
                        ),
                    )
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-group',
    				'title' => esc_html__('Author Page', 'rubik'),
    				'fields' => array(
                        array(
            				'id'=>'bk-author-layout',
            				'type' => 'select',
                            'title' => esc_html__('Author page layout', 'rubik'), 
    						'options' => array('classic-blog' => esc_html__('Classic Blog', 'rubik'), 
                                                'large-blog' => esc_html__('Large Blog with Sidebar', 'rubik'), 
                                                'large-blog-nosb' => esc_html__('Large Blog No Sidebar', 'rubik'), 
                                                'large-blog-2' => esc_html__('Large Blog 2 with Sidebar', 'rubik'),
                                                'large-blog-2-nosb' => esc_html__('Large Blog 2 No Sidebar', 'rubik'),
                                                'row'=>esc_html__('Row with Sidebar', 'rubik'),
                                                'row-nosb' => esc_html__('Row no Sidebar', 'rubik'),
                                                'masonry' => esc_html__('Masonry with Sidebar', 'rubik'),
                                                'masonry-nosb' => esc_html__('Masonry no Sidebar', 'rubik'),
                                                'square-grid-3-s1' => esc_html__('Square Grid no Sidebar', 'rubik'),                                                             
                                                'square-grid-2-s1' => esc_html__('Square Grid with Sidebar', 'rubik'),
                                                ),
    						'default' => 'classic-blog',
            			),
                        array(
            				'id'=>'bk-author-font-setup',
            				'type' => 'typography', 
                            'output' => array('.bk-author-page .bkpage-content h4.title, .bk-author-page .bkpage-content .content_in h4.title'),
            				'title' => esc_html__('Author page Post Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Leave blank to have the default font setup', 'rubik'),
                        ),
                        array(
                            'id' => 'author_post_icon',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Enable Post Icon on Author page', 'rubik'),
                            'subtitle' => esc_html__('Support for "Classic Blog layout", "Large Blog layout" and "Masonry Layout"', 'rubik'),
                            'desc' => '',
                            'options' => array('show' => esc_html__('Show', 'rubik'), 
                                                'hide' => esc_html__('Hide', 'rubik')
                                                ),
                            'default' => 'hide',
                        ),
                        array(
                            'id' => 'author-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Choose sidebar for Author page', 'rubik'),
                            'desc' => '',
                            'default' => 'home_sidebar',
                        ),
                        array(
                            'id' => 'author-page-sidebar-position',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Sidebar Position', 'rubik'),
                            'subtitle' => esc_html__('Support right and left sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('right' => esc_html__('Right', 'rubik'), 
                                                'left' => esc_html__('Left', 'rubik')
                                                ),
                            'default' => 'right',
                        ),
                        array(
                            'id' => 'author-stick-sidebar',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'enable',
                        ),
                    )
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-search',
    				'title' => esc_html__('Search Page', 'rubik'),
    				'fields' => array(
                        array(
            				'id'=>'bk-search-layout',
            				'type' => 'select',
                            'title' => esc_html__('Search page layout', 'rubik'), 
    						'options' => array('classic-blog' => esc_html__('Classic Blog', 'rubik'), 
                                                'large-blog' => esc_html__('Large Blog with Sidebar', 'rubik'), 
                                                'large-blog-nosb' => esc_html__('Large Blog No Sidebar', 'rubik'), 
                                                'large-blog-2' => esc_html__('Large Blog 2 with Sidebar', 'rubik'),
                                                'large-blog-2-nosb' => esc_html__('Large Blog 2 No Sidebar', 'rubik'),
                                                'row'=>esc_html__('Row with Sidebar', 'rubik'),
                                                'row-nosb' => esc_html__('Row no Sidebar', 'rubik'),
                                                'masonry' => esc_html__('Masonry with Sidebar', 'rubik'),
                                                'masonry-nosb' => esc_html__('Masonry no Sidebar', 'rubik'),
                                                'square-grid-3-s1' => esc_html__('Square Grid no Sidebar', 'rubik'),                                                             
                                                'square-grid-2-s1' => esc_html__('Square Grid with Sidebar', 'rubik'),
                                                ),
    						'default' => 'classic-blog',
            			),
                        array(
                            'id' => 'search-result-setting',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Search Result Setting', 'rubik'),
                            'desc' => '',
                            'options' => array(
                                            'post' => esc_html__('Show only posts in the result', 'rubik'), 
                                            'all' => esc_html__('Show all results', 'rubik')
                                        ),
                            'default' => 'all',
                        ),
                        array(
            				'id'=>'bk-search-font-setup',
            				'type' => 'typography', 
                            'output' => array('.bk-search-content .post-c-wrap h4.title, .bk-search-content .content_in .post-c-wrap h4.title'),
            				'title' => esc_html__('Search page Post Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Leave blank to have the default font setup', 'rubik'),
                        ),
                        array(
                            'id' => 'search_post_icon',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Enable Post Icon on Search page', 'rubik'),
                            'subtitle' => esc_html__('Support for "Classic Blog layout", "Large Blog layout" and "Masonry Layout"', 'rubik'),
                            'desc' => '',
                            'options' => array('show' => esc_html__('Show', 'rubik'), 
                                                'hide' => esc_html__('Hide', 'rubik')
                                                ),
                            'default' => 'hide',
                        ),
                        array(
                            'id' => 'search-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Choose sidebar for Search page', 'rubik'),
                            'desc' => '',
                            'default' => 'home_sidebar',
                        ),
                        array(
                            'id' => 'search-page-sidebar-position',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Sidebar Position', 'rubik'),
                            'subtitle' => esc_html__('Support right and left sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('right' => esc_html__('Right', 'rubik'), 
                                                'left' => esc_html__('Left', 'rubik')
                                                ),
                            'default' => 'right',
                        ),
                        array(
                            'id' => 'search-stick-sidebar',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'enable',
                        ),
                    )
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-pencil',
    				'title' => esc_html__('Blog Page', 'rubik'),
    				'fields' => array( 
                        array(
            				'id'=>'bk-blog-layout',
            				'type' => 'select',
                            'title' => esc_html__('Blog page layout', 'rubik'), 
    						'options' => array('classic-blog' => esc_html__('Classic Blog', 'rubik'), 
                                                'large-blog' => esc_html__('Large Blog with Sidebar', 'rubik'), 
                                                'large-blog-nosb' => esc_html__('Large Blog No Sidebar', 'rubik'), 
                                                'large-blog-2' => esc_html__('Large Blog 2 with Sidebar', 'rubik'),
                                                'large-blog-2-nosb' => esc_html__('Large Blog 2 No Sidebar', 'rubik'),
                                                'row'=>esc_html__('Row with Sidebar', 'rubik'),
                                                'row-nosb' => esc_html__('Row no Sidebar', 'rubik'),
                                                'masonry' => esc_html__('Masonry with Sidebar', 'rubik'),
                                                'masonry-nosb' => esc_html__('Masonry no Sidebar', 'rubik'),
                                                'square-grid-3-s1' => esc_html__('Square Grid no Sidebar', 'rubik'),                                                             
                                                'square-grid-2-s1' => esc_html__('Square Grid with Sidebar', 'rubik'),
                                                ),
    						'default' => 'classic-blog',
            			),
                        array(
            				'id'=>'bk-blog-font-setup',
            				'type' => 'typography', 
                            'output' => array('.bk-blog-template-page .bkpage-content h4.title, .bk-blog-template-page .bkpage-content .content_in h4.title'),
            				'title' => esc_html__('Blog page Post Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Leave blank to have the default font setup', 'rubik'),
                        ),
                        array(
                            'id' => 'blog_post_icon',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Enable Post Icon on Blog page', 'rubik'),
                            'subtitle' => esc_html__('Support for "Classic Blog layout", "Large Blog layout" and "Masonry Layout"', 'rubik'),
                            'desc' => '',
                            'options' => array('show' => esc_html__('Show', 'rubik'), 
                                                'hide' => esc_html__('Hide', 'rubik')
                                                ),
                            'default' => 'hide',
                        ),
                        array(
                            'id' => 'blog-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars',
                            'multi' => false,
                            'title' => esc_html__('Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Choose sidebar for blog page', 'rubik'),
                            'desc' => '',
                            'default' => 'home_sidebar',
                        ),
                        array(
                            'id' => 'blog-page-sidebar-position',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Sidebar Position', 'rubik'),
                            'subtitle' => esc_html__('Support right and left sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('right' => esc_html__('Right', 'rubik'), 
                                                'left' => esc_html__('Left', 'rubik')
                                                ),
                            'default' => 'right',
                        ),
                        array(
                            'id' => 'blog-stick-sidebar',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'enable',
                        ),
                    ),
                );
                $this->sections[] = array(
    				'icon' => 'el-icon-minus',
    				'title' => esc_html__('Default Page Template', 'rubik'),
                    'heading'   => esc_html__('Default Page Template', 'rubik'),
                    'desc'   => esc_html__('Default Page Template Configuration', 'rubik'),
    				'fields' => array(
                        array(
                            'id'        => 'bk_page_feat_img',
                            'title'     => esc_html__( 'Feature Image Show/Hide', 'rubik' ),
                            'type'      => 'switch', 
                			'options'   => array(          
                                1 => esc_html__( 'Show', 'rubik' ),
                                0 => esc_html__( 'Hide', 'rubik' ),
        				    ),
                			'default'    => 1,
                        ),
                        array(
    						'id'=>'bk_page_layout',
    						'type' => 'select', 
    						'title' => esc_html__('Layout', 'rubik'),
                            'options'  => array(
                                'has_sidebar' => esc_html__( 'Has Sidebar', 'rubik' ),
                                'no_sidebar'  => esc_html__( 'Full Width -- No sidebar', 'rubik' ),
        				    ),
                            'default' => 'has_sidebar',
						),
                        array(
                            'id'        => 'bk_page_sidebar_select',  
                            'type'      => 'select',
                            'data'      => 'sidebars', 
                            'multi'     => false,
                            'title'     => esc_html__('Page Sidebar', 'rubik'),
                            'subtitle'  => esc_html__('Choose a sidebar for the page', 'rubik'),
                            'default'   => 'home_sidebar',
                        ),
                        array(
                            'id'        => 'bk_page_sidebar_position',  
                            'type'      => 'select',
                            'multi'     => false,
                            'title'     => esc_html__('Sidebar Position', 'rubik'),
                            'subtitle'  => esc_html__('Support right and left sidebar', 'rubik'),
                            'desc'      => '',
                            'options'   => array(
                                'right' => esc_html__('Right', 'rubik'), 
                                'left' => esc_html__('Left', 'rubik')
                            ),
                            'default' => 'right',
                        ),
                        array(
                            'id'        => 'bk_page_sidebar_sticky',  
                            'type'      => 'button_set',
                            'multi'     => false,
                            'title'     => esc_html__('Stick Sidebar', 'rubik'),
                            'subtitle'  => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'rubik'),
                            'desc'      => '',
                            'options'   => array(
                                'enable'    => esc_html__( 'Enable', 'rubik' ),
				                'disable'   => esc_html__( 'Disable', 'rubik' ),
                            ),
                            'default' => 'enable',
                        ),
    				)
    			);
                $this->sections[] = array(
    				'icon' => 'el-icon-error-alt',
    				'title' => esc_html__('404 Page', 'rubik'),
                    'heading'   => esc_html__('404 Page', 'rubik'),
                    'desc'   => esc_html__('404 Page Configuration', 'rubik'),
    				'fields' => array(
                        array(
                            'id'=>'bk-404-title',
                            'type' => 'textarea',
                            'title' => esc_html__('404 Title', 'rubik'),
                            'default' =>  esc_html__( 'Page not found', 'rubik' ),
                        ),
                        array(
                            'id'=>'bk-404-description',
                            'type' => 'textarea',
                            'title' => esc_html__('404 Description', 'rubik'),
                            'default' =>  esc_html__( 'Oops! The page you were looking for was not found. Perhaps searching can help.', 'rubik' ),
                        ),
                        array(
                            'id'        => 'bk-404-search',  
                            'type'      => 'button_set',
                            'multi'     => false,
                            'title'     => esc_html__('404 Search', 'rubik'),
                            'subtitle'  => esc_html__('Enable Search / Disable Search on 404 Page', 'rubik'),
                            'desc'      => '',
                            'options'   => array(
                                'enable'    => esc_html__( 'Enable', 'rubik' ),
				                'disable'   => esc_html__( 'Disable', 'rubik' ),
                            ),
                            'default' => 'enable',
                        ),
                        array(
                            'id'        => 'bk-404-latest',  
                            'type'      => 'button_set',
                            'multi'     => false,
                            'title'     => esc_html__('404 Latest Article Section', 'rubik'),
                            'subtitle'  => esc_html__('Enable Search / Disable Search latest articles section', 'rubik'),
                            'desc'      => '',
                            'options'   => array(
                                'enable'    => esc_html__( 'Enable', 'rubik' ),
				                'disable'   => esc_html__( 'Disable', 'rubik' ),
                            ),
                            'default' => 'enable',
                        ),
                        array(
            				'id'=>'bk-404-post-font',
                            'required' => array('bk-404-latest','=','enable'),
            				'type' => 'typography', 
                            'output' => array('.error-404-page-posts-wrap .module-square-grid .content_in .post-c-wrap .title'),
            				'title' => esc_html__('Post Title Font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>true,
            				'line-height'=>false,
                            'text-transform' => true,
                            'font-family'=> false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for title', 'rubik'),
            				'default'=> array( 
            					'font-weight'=>'700', 
            					'google' => true,
                                'font-size' => '16px',
                                'text-transform' => 'inherit'
            				    ),
                        ),
                        array(
                            'id'=>'bk-404-latest-title',
                            'type' => 'textarea',
                            'title' => esc_html__('Title Text', 'rubik'),
                            'required' => array('bk-404-latest','=','enable'),
                            'default' => esc_html__( 'Latest Articles', 'rubik' ),
                        ),
    				)
    			);
                $this->sections[] = array(
    				'icon' => 'el-icon-list-alt',
    				'title' => esc_html__('Single Page', 'rubik'),
    				'fields' => array(
                        array(
    						'id'=>'bk-single-template',
    						'type' => 'image_select', 
    						'title' => esc_html__('Single Page Layout', 'rubik'),
                            'options' => array(
                                                'single_template_1' => array(
                                                    'alt' => 'Single Template 1',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_1.png',
                                                    'title' => esc_html__('Template 1', 'rubik')
                                                ),
                                                'single_template_2' => array(
                                                    'alt' => 'Single Template 2',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_2.png',
                                                    'title' => esc_html__('Template 2', 'rubik')
                                                ),
                                                'single_template_3' => array(
                                                    'alt' => 'Single Template 3',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_3.png',
                                                    'title' => esc_html__('Template 3', 'rubik')
                                                ),
                                                'single_template_4' => array(
                                                    'alt' => 'Single Template 4',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_4.png',
                                                    'title' => esc_html__('Template 4', 'rubik')
                                                ),
                                                'single_template_5' => array(
                                                    'alt' => 'Single Template 5',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_5.png',
                                                    'title' => esc_html__('Template 5', 'rubik')
                                                ),
                                                'single_template_6' => array(
                                                    'alt' => 'Single Template 6',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_6.png',
                                                    'title' => esc_html__('Template 6', 'rubik')
                                                ),
                                                'single_template_7' => array(
                                                    'alt' => 'Single Template 7',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_7.png',
                                                    'title' => esc_html__('Template 7', 'rubik')
                                                ),
                                                'single_template_8' => array(
                                                    'alt' => 'Single Template 8',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_8.png',
                                                    'title' => esc_html__('Template 8', 'rubik')
                                                ),
                                                'single_template_9' => array(
                                                    'alt' => 'Single Template 9',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_9.png',
                                                    'title' => esc_html__('Template 9', 'rubik')
                                                ),
                                                'single_template_10' => array(
                                                    'alt' => 'Single Template 10',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_10.png',
                                                    'title' => esc_html__('Template 10', 'rubik')
                                                ),
                                                'single_template_11' => array(
                                                    'alt' => 'Single Template 11',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_11.png',
                                                    'title' => esc_html__('Template 11', 'rubik')
                                                ),
                                                'single_template_12' => array(
                                                    'alt' => 'Single Template 12',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_12.png',
                                                    'title' => esc_html__('Template 12', 'rubik')
                                                ),
                                                'single_template_13' => array(
                                                    'alt' => 'Single Template 13',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_13.png',
                                                    'title' => esc_html__('Template 13', 'rubik')
                                                ),
                                                'single_template_14' => array(
                                                    'alt' => 'Single Template 14',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_14.png',
                                                    'title' => esc_html__('Template 14', 'rubik')
                                                ),
                                                'single_template_15' => array(
                                                    'alt' => 'Single Template 15',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_15.png',
                                                    'title' => esc_html__('Template 15', 'rubik')
                                                ),
                                                'single_template_16' => array(
                                                    'alt' => 'Single Template 16',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_16.png',
                                                    'title' => esc_html__('Template 16', 'rubik')
                                                ),
                                                'single_template_17' => array(
                                                    'alt' => 'Single Template 17',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/single_page/single_template_17.png',
                                                    'title' => esc_html__('Template 17', 'rubik')
                                                ),
                                        ),
                            'default' => 'single_template_1',
						),
                        array(
                            'id' => 'section-single-heading-related-start',
                            'title' => esc_html__('Single Heading Related', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
                            'id' => 'single-heading-related-posts',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Single Heading Related Posts', 'rubik'),
                            'subtitle' => esc_html__('Enable / Disable', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'enable',
                        ),
                        array(
    						'id'=>'single-heading-related-posts-entries',
    						'type' => 'text', 
                            'validate' => 'numeric',
    						'title' => esc_html__('Single Heading Related Posts Entries', 'rubik'),
    						'subtitle' => esc_html__('input: number', 'rubik'),
                            'default' => '3',
						),
                        array(
                            'id' => 'section-single-heading-related-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
                            'id' => 'section-single-typography-start',
                            'title' => esc_html__('Single Post Typography Setting', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
            				'id'=>'bk-single-title-font',
            				'type' => 'typography', 
                            'output' => array('.s-post-header h1'),
            				'title' => esc_html__('Post Title Font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>true,
            				'line-height'=>false,
                            'text-transform' => true,
                            'font-family'=> false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for title', 'rubik'),
            				'default'=> array( 
            					'font-weight'=>'700', 
            					'google' => true,
                                'font-size' => '32px',
                                'text-transform' => 'inherit'
            				    ),
                        ),
                        array(
            				'id'=>'bk-single-body-font',
            				'type' => 'typography', 
                            'output' => array('.single-page .main .article-content'),
            				'title' => esc_html__('Post Body font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>true,
            				'line-height'=>true,
                            'text-transform' => true,
                            'text-align' => true,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>true,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Post Body', 'rubik'),
            				'default'=> array( 
            					'font-weight'=>'400', 
            					'font-family'=>'Open Sans', 
            					'google' => true,
                                'font-size' => '16px',
                                'line-height'=> '27px',
                                'color'=>'#111',
                                'text-transform' => 'inherit',
                                'text-align' => 'initial',
            				),
                        ),
                        array(
                            'id'             => 'paragraph-spacing',
                            'type'           => 'spacing',
                            'output'         => array('.single-page .article-content p, .single-page blockquote'),
                            'mode'           => 'margin',
                            'top'            => false,
                            'left'           => false,
                            'right'          => false,
                            'bottom'         => true,
                            'units'          => array('em', 'px'),
                            'units_extended' => 'false',
                            'title'          => esc_html__('Post Content Paragraph Space', 'rubik'),
                            'subtitle'       => esc_html__('Allow your users to choose the spacing or margin they want.', 'rubik'),
                            'default'            => array(
                                'margin-bottom'  => '25px', 
                                'units'          => 'px', 
                            )
                        ),
                        array(
                            'id' => 'section-single-typography-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
                            'id' => 'single-sidebar-switch',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Single Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Enable / Disable Sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'enable',
                        ),
                        array(
                            'id'=>'section-single-sidebar-start',
                            'required' => array('single-sidebar-switch','=','enable'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id' => 'single-page-sidebar',  
                            'type' => 'select',
                            'data' => 'sidebars', 
                            'multi' => false,
                            'title' => esc_html__('Single Page Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Choose sidebar for single page', 'rubik'),
                            'desc' => '',
                            'default' => 'single_sidebar',
                        ),
                        array(
                            'id' => 'single-sidebar-position',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Sidebar Position', 'rubik'),
                            'subtitle' => esc_html__('Setup Sidebar Position to be left or right', 'rubik'),
                            'desc' => '',
                            'options' => array('left' => esc_html__('Left', 'rubik'), 
                                                'right' => esc_html__('Right', 'rubik')
                                                ),
                            'default' => 'right',
                        ),
                        array(
                            'id' => 'single-stick-sidebar',  
                            'type' => 'button_set',
                            'multi' => false,
                            'title' => esc_html__('Stick Sidebar', 'rubik'),
                            'subtitle' => esc_html__('Enable Stick Sidebar / Disable Stick Sidebar', 'rubik'),
                            'desc' => '',
                            'options' => array('enable' => esc_html__('Enable', 'rubik'), 
                                                'disable' => esc_html__('Disable', 'rubik')
                                                ),
                            'default' => 'disable',
                        ),
                        array(
                            'id'=>'section-single-sidebar-end',
                            'type' => 'section', 
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-og-tag',
    						'type' => 'switch',
    						'title' => esc_html__('Insert Open Graph Meta Tags to Header', 'rubik'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                            'indent' => true
						),
                        array(
    						'id'=>'bk-sharebox-top-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable share box on top of the post content', 'rubik'),
    						'subtitle' => esc_html__('Enable share links below single post', 'rubik'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                            'indent' => true
						),
                        array(
                            'id'=>'section-sharebox-top-start',
                            'required' => array('bk-sharebox-top-sw','=','1'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id' => 'top-share-items-shown',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Show the top share items', 'rubik'),
                            'subtitle' => esc_html__('The rest will be hide and will be shown when you click + icon', 'rubik'),
                            'desc' => '',
                            'options' => array( '1' => esc_html__('1', 'rubik'), 
                                                '2' => esc_html__('2', 'rubik'),
                                                '3' => esc_html__('3', 'rubik'), 
                                                '4' => esc_html__('4', 'rubik'),
                                                '5' => esc_html__('5', 'rubik'), 
                                                '6' => esc_html__('6', 'rubik')
                                                ),
                            'default' => '2',
                        ),                        
                        array(
                            'id'=>'bk-fb-top-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Facebook share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-fb-top-text',
                            'type' => 'text',
                            'required' => array('bk-fb-top-sw','=','1'),
                            'title' => esc_html__('Facebook Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-tw-top-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Twitter share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-tw-top-text',
                            'type' => 'text',
                            'required' => array('bk-tw-top-sw','=','1'),
                            'title' => esc_html__('Twitter Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-gp-top-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Google+ share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-gp-top-text',
                            'type' => 'text',
                            'required' => array('bk-gp-top-sw','=','1'),
                            'title' => esc_html__('Google Plus Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-pi-top-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Pinterest share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-pi-top-text',
                            'type' => 'text',
                            'required' => array('bk-pi-top-sw','=','1'),
                            'title' => esc_html__('Pinterest Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-stu-top-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Stumbleupon share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-stu-top-text',
                            'type' => 'text',
                            'required' => array('bk-stu-top-sw','=','1'),
                            'title' => esc_html__('Stumbleupon Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-li-top-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Linkedin share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-li-top-text',
                            'type' => 'text',
                            'required' => array('bk-li-top-sw','=','1'),
                            'title' => esc_html__('Linkedin Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'section-sharebox-top-end',
                            'type' => 'section', 
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-sharebox-bottom-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable Share Box Under The Post Content', 'rubik'),
    						'subtitle' => esc_html__('Enable share links below single post', 'rubik'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                            'indent' => true
						),
                        array(
                            'id'=>'section-sharebox-bottom-start',
                            'required' => array('bk-sharebox-bottom-sw','=','1'),
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id' => 'bottom-share-items-shown',  
                            'type' => 'select',
                            'multi' => false,
                            'title' => esc_html__('Show the bottom share items', 'rubik'),
                            'subtitle' => esc_html__('The rest will be hide and will be shown when you click + icon', 'rubik'),
                            'desc' => '',
                            'options' => array( '1' => esc_html__('1', 'rubik'), 
                                                '2' => esc_html__('2', 'rubik'),
                                                '3' => esc_html__('3', 'rubik'), 
                                                '4' => esc_html__('4', 'rubik'),
                                                '5' => esc_html__('5', 'rubik'), 
                                                '6' => esc_html__('6', 'rubik')
                                                ),
                            'default' => '2',
                        ),
                        array(
                            'id'=>'bk-fb-bottom-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Facebook share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-fb-bottom-text',
                            'type' => 'text',
                            'required' => array('bk-fb-bottom-sw','=','1'),
                            'title' => esc_html__('Facebook Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-tw-bottom-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Twitter share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-tw-bottom-text',
                            'type' => 'text',
                            'required' => array('bk-tw-bottom-sw','=','1'),
                            'title' => esc_html__('Twitter Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-gp-bottom-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Google+ share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-gp-bottom-text',
                            'type' => 'text',
                            'required' => array('bk-gp-bottom-sw','=','1'),
                            'title' => esc_html__('Google Plus Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-pi-bottom-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Pinterest share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-pi-bottom-text',
                            'type' => 'text',
                            'required' => array('bk-pi-bottom-sw','=','1'),
                            'title' => esc_html__('Pinterest Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-stu-bottom-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Stumbleupon share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-stu-bottom-text',
                            'type' => 'text',
                            'required' => array('bk-stu-bottom-sw','=','1'),
                            'title' => esc_html__('Stumbleupon Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'bk-li-bottom-sw',
                            'type' => 'switch',
                            'title' => esc_html__('Enable Linkedin share link', 'rubik'),
                            'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
                        ),
                        array(
                            'id'=>'bk-li-bottom-text',
                            'type' => 'text',
                            'required' => array('bk-li-bottom-sw','=','1'),
                            'title' => esc_html__('Linkedin Share Text', 'rubik'),
                            'default' => '',
                        ),
                        array(
                            'id'=>'section-sharebox-end',
                            'type' => 'section', 
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-authorbox-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable author box', 'rubik'),
    						'subtitle' => esc_html__('Enable author information below single post', 'rubik'),
    						'default' => 0,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
    						'id'=>'bk-postnav-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable post navigation', 'rubik'),
    						'subtitle' => esc_html__('Enable post navigation below single post', 'rubik'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
                            'id'=>'section-postnav-start',
                            'required' => array('bk-postnav-sw','=',1),                       
                            'type' => 'section', 
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-post-nav-style',
    						'type' => 'select', 
    						'title' => esc_html__('Single Post Nav Style', 'rubik'),
                            'options' => array(
                                                'under-content' => esc_html__('Under Post Content', 'rubik'),
                                                'fixed-position' => esc_html__('Fixed Position on Left and Right Side', 'rubik'),
                                        ),
                            'default' => 'under-content',
						),
                        array(
            				'id'=>'bk-single-post-nav-font',
            				'type' => 'typography', 
                            'output' => array('.s-static-post-nav .nav-title h3, .s-post-nav .nav-title h3'),
            				'title' => esc_html__('Post Nav Font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
                            'font-weight' => false,
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>true,
            				'line-height'=>false,
                            'text-transform' => true,
                            'font-family'=> false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for title', 'rubik'),
            				'default'=> array( 
            					'google' => true,
                                'font-size' => '16px',
                                'text-transform' => 'inherit'
            				    ),
                        ),
                        array(
                            'id'=>'section-postnav-end',
                            'type' => 'section', 
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-related-sw',
    						'type' => 'switch',
    						'title' => esc_html__('Enable related posts', 'rubik'),
    						'subtitle' => esc_html__('Enable related posts below single post', 'rubik'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
                            'id' => 'section-related-start',
                            'required' => array('bk-related-sw','=','1'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),  
                        
                         array(
    						'id'=>'bk-related-output',
    						'type' => 'select',
    						'title' => esc_html__('Related Posts Output', 'rubik'),
    						'subtitle' => esc_html__('Select number of Posts', 'rubik'),
                            'options' => array( '3' => esc_html__('3', 'rubik'), 
                                                '6' => esc_html__('6', 'rubik'),
                                                '9' => esc_html__('9', 'rubik'), 
                                                '12' => esc_html__('12', 'rubik'),
                                                '15' => esc_html__('15', 'rubik'), 
                                                '18' => esc_html__('18', 'rubik')
                                        ),
                            'default' => '3',
						),
                        array(
            				'id'=>'bk-single-related-post-font',
            				'type' => 'typography', 
                            'output' => array('.related-box .post-c-wrap h4'),
            				'title' => esc_html__('Related Section Post Title Font', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
                            'font-weight' => false,
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
            				'font-size'=>true,
            				'line-height'=>false,
                            'text-transform' => true,
                            'font-family'=> false,
            				//'word-spacing'=>true, // Defaults to false
            				//'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				//'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for title', 'rubik'),
            				'default'=> array( 
            					'google' => true,
                                'font-size' => '16px',
                                'text-transform' => 'inherit'
            				    ),
                        ),
                        array(
                            'id' => 'section-related-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        array(
    						'id'=>'bk-comment-sw',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable comment section', 'rubik'),
    						'subtitle' => esc_html__('Enable comment section below single post', 'rubik'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),
                        array(
                            'id' => 'section-recommend-start',
                            'title' => esc_html__('Recommend Box Setting', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),     
                        array(
    						'id'=>'bk-recommend-box',
    						'type' => 'switch', 
    						'title' => esc_html__('Enable Recommend Box', 'rubik'),
    						'subtitle' => esc_html__('A random post appear on single page', 'rubik'),
    						'default' => 1,
    						'on' => esc_html__('Enabled', 'rubik'),
    						'off' => esc_html__('Disabled', 'rubik'),
						),     
                        array(
                            'id'       => 'recommend-box-title',
                            'type'     => 'text',
                            'title'    => esc_html__('Recommend Box title', 'rubik'),
                            'default'  => ''
                        ),
                        array(
            				'id'=>'bk-recommend-title-font-setup',
            				'type' => 'typography', 
                            'output' => array('.recommend-box h3'),
            				'title' => esc_html__('Recommend Box title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => false,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Recommend Box title', 'rubik'),
                        ),
                        array(
                            'id' => 'recommend-categories',
                            'type' => 'select',
                            'data' => 'categories',
                            'multi' => true,
                            'title' => esc_html__('Categories', 'rubik')
                        ),
                        array(
                            'id'       => 'recommend-number',
                            'type'     => 'text',
                            'title'    => esc_html__('Number of posts', 'rubik'),
                            'subtitle' => esc_html__('Type number of posts will be displayed', 'rubik'),
                            'default'  => ''
                        ),
                        array(
                            'id' => 'section-recommend-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ) 
    				)
    			);
                $this->sections[] = array(
            		'icon'    => ' el-icon-font',
            		'title'   => esc_html__('Module Heading, Sidebar Heading + Footer Widget Heading', 'rubik'),
            		'fields'  => array(
                        
                        // Module Header
                        
                        array(
                            'id' => 'section-module-header-start',
                            'title' => esc_html__('Module Heading', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-module-header',
    						'type' => 'image_select', 
    						'title' => esc_html__('Module Heading Style ', 'rubik'),
                            'options' => array(
                                                'module_header_1' => array(
                                                    'alt' => 'Header Style 1',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header5.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_2' => array(
                                                    'alt' => 'Header Style 2',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header6.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_3' => array(
                                                    'alt' => 'Header Style 3',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header11.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_4' => array(
                                                    'alt' => 'Header Style 4',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header12.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_5' => array(
                                                    'alt' => 'Header Style 5',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header15.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_6' => array(
                                                    'alt' => 'Header Style 6',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header16.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_7' => array(
                                                    'alt' => 'Header Style 7',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header17.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_8' => array(
                                                    'alt' => 'Header Style 8',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header18.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_9' => array(
                                                    'alt' => 'Header Style 9',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header19.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_10' => array(
                                                    'alt' => 'Header Style 10',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header22.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_11' => array(
                                                    'alt' => 'Header Style 11',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header25.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_12' => array(
                                                    'alt' => 'Header Style 12',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header26.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_13' => array(
                                                    'alt' => 'Header Style 13',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header27.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'module_header_14' => array(
                                                    'alt' => 'Header Style 14',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header29.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                        ),
                            'default' => 'module_header_7',
						),
                        array(
    						'id'=>'bk-module-title-height',
    						'type' => 'text', 
                            'validate' => 'numeric',
    						'title' => esc_html__('Module Title Line Height - in pixel', 'rubik'),
    						'subtitle' => esc_html__('input: number', 'rubik'),
                            'default' => '40',
						),
                        array(
                            'id'             => 'module-heading-margin',
                            'type'           => 'spacing',
                            'output'         => array('.module-title, .page-title-wrapper'),
                            'mode'           => 'margin',
                            'units'          => array('px'),
                            'units_extended' => false,
                            'top'            => false,
                            'left'           => false,
                            'right'          => false,
                            'title'          => esc_html__('Module Heading Margin Option', 'rubik'),
                            'subtitle'       => esc_html__('Allow to choose the spacing or margin.', 'rubik'),
                            'default'            => array(
                                'margin-bottom'  => '30', 
                                'units'          => 'px', 
                            )
                        ),
                        array(
            				'id'=>'bk-module-title-font-setup',
            				'type' => 'typography', 
                            'output' => array('.module-title h2, .page-title h2'),
            				'title' => esc_html__('Module Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => true,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Module Title Font', 'rubik'),
                            'default'     => array(
                                'font-size'   => '16px',
                            ),
                        ),
                        array(
    						'id'=>'bk-module-title-color-default',
    						'type' => 'color',
    						'title' => esc_html__('Module Title Color', 'rubik'), 
                            'subtitle' => esc_html__('Default Setting', 'rubik'),
    						'default' => '#000',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-module-title-text-color-default',
    						'type' => 'color',
    						'title' => esc_html__('Module Title Text Color', 'rubik'), 
    						'subtitle' => esc_html__('Default Setting', 'rubik'),
                            'default' => '#fff',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-module-tab-color-default',
    						'type' => 'color',
    						'title' => esc_html__('Module Tab Color', 'rubik'), 
                            'subtitle' => esc_html__('Default Setting', 'rubik'),
    						'default' => '#777',
    						'validate' => 'color',
						),
                        array(
                            'id' => 'section-module-header-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        
                        //Header Sidebar   
                        array(
                            'id' => 'section-sidebar-header-start',
                            'title' => esc_html__('Sidebar Heading', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ),  
                        
                        array(
    						'id'=>'bk-sidebar-header',
    						'type' => 'image_select', 
    						'title' => esc_html__('Sidebar Header Style ', 'rubik'),
                            'options' => array(
                                                'sidebar_header_1' => array(
                                                    'alt' => 'Header Style 1',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header1.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_2' => array(
                                                    'alt' => 'Header Style 2',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header2.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_3' => array(
                                                    'alt' => 'Header Style 3',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header3.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_4' => array(
                                                    'alt' => 'Header Style 4',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header4.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_5' => array(
                                                    'alt' => 'Header Style 5',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header5.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_6' => array(
                                                    'alt' => 'Header Style 6',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header6.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_7' => array(
                                                    'alt' => 'Header Style 7',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header7.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_8' => array(
                                                    'alt' => 'Header Style 8',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header8.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_9' => array(
                                                    'alt' => 'Header Style 9',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header9.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_10' => array(
                                                    'alt' => 'Header Style 10',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header10.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_11' => array(
                                                    'alt' => 'Header Style 11',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header11.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_12' => array(
                                                    'alt' => 'Header Style 12',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header12.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_13' => array(
                                                    'alt' => 'Header Style 13',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header13.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_14' => array(
                                                    'alt' => 'Header Style 14',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header14.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_15' => array(
                                                    'alt' => 'Header Style 15',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header15.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_16' => array(
                                                    'alt' => 'Header Style 16',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header16.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_17' => array(
                                                    'alt' => 'Header Style 17',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header17.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_18' => array(
                                                    'alt' => 'Header Style 17',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header18.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_19' => array(
                                                    'alt' => 'Header Style 19',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header19.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_20' => array(
                                                    'alt' => 'Header Style 20',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header20.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_21' => array(
                                                    'alt' => 'Header Style 21',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header21.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_22' => array(
                                                    'alt' => 'Header Style 22',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header22.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_23' => array(
                                                    'alt' => 'Header Style 23',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header23.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_24' => array(
                                                    'alt' => 'Header Style 24',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header24.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_25' => array(
                                                    'alt' => 'Header Style 25',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header25.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_26' => array(
                                                    'alt' => 'Header Style 26',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header26.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_27' => array(
                                                    'alt' => 'Header Style 27',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header27.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'sidebar_header_28' => array(
                                                    'alt' => 'Header Style 28',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header28.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                        ),
                            'default' => 'sidebar_header_1',
						),
                        array(
    						'id'=>'bk-widget-title-height',
    						'type' => 'text', 
                            'validate' => 'numeric',
    						'title' => esc_html__('Widget Title Line Height - in pixel', 'rubik'),
    						'subtitle' => esc_html__('input: number', 'rubik'),
                            'default' => '40',
						),
                        array(
                            'id'             => 'widget-heading-margin',
                            'type'           => 'spacing',
                            'output'         => array('.sidebar-wrap .widget-title'),
                            'mode'           => 'margin',
                            'units'          => array('px'),
                            'units_extended' => false,
                            'top'            => false,
                            'left'           => false,
                            'right'          => false,
                            'title'          => esc_html__('Widget Heading Margin Option', 'rubik'),
                            'subtitle'       => esc_html__('Allow to choose the spacing or margin.', 'rubik'),
                            'default'            => array(
                                'margin-bottom'  => '28', 
                                'units'          => 'px', 
                            )
                        ),
                        /** Header 1234 **/
                        array(
    						'id'=>'bk-sidebar-header-1234-bg',
    						'type' => 'background',
                            'required' => array(
                                'bk-sidebar-header','equals',array( 'sidebar_header_1', 'sidebar_header_2', 'sidebar_header_3', 'sidebar_header_4' )
                            ),
                            'background-image' => false,
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview_media' => false,
                            'preview'  => false,
                            'default'  => array(
                                'background-color' => '#333',
                                ),
                            'output' => array('.sidebar_header_1 .sidebar-wrap .widget-title', 
                                                '.sidebar_header_2 .sidebar-wrap .widget-title',
                                                '.sidebar_header_3 .sidebar-wrap .widget-title',
                                                '.sidebar_header_4 .sidebar-wrap .widget-title',
                                        ),
    						'title' => esc_html__('Sidebar Widget Header Color', 'rubik'),
						),
                        array( 
                            'id'       => 'bk-sidebar-header-arrow',
                            'type' => 'color',
                            'required' => array(
                                'bk-sidebar-header','equals',array( 'sidebar_header_3', 'sidebar_header_4', 'sidebar_header_24', 'sidebar_header_27', 'sidebar_header_28' )
                            ),
                            'title'    => esc_html__('Sidebar Header Arrow', 'rubik'),
                            'default' => '#333',
    						'validate' => 'color',
                        ),
                        
                        /** Header 5 - 16**/
                        array(
    						'id'=>'bk-sidebar-header-5-16-bg',
    						'type' => 'background',
                            'required' => array(
                                'bk-sidebar-header','equals',array( 'sidebar_header_5', 'sidebar_header_6', 'sidebar_header_7', 'sidebar_header_8',
                                                                    'sidebar_header_9', 'sidebar_header_10','sidebar_header_11', 'sidebar_header_12',
                                                                    'sidebar_header_13', 'sidebar_header_14', 'sidebar_header_15', 'sidebar_header_16',
                                                                    'sidebar_header_17', 'sidebar_header_18', 'sidebar_header_21', 'sidebar_header_22',
                                                                    'sidebar_header_23',
                                                                    'sidebar_header_25', 'sidebar_header_26', 'sidebar_header_27', 'sidebar_header_28' )
                            ),
                            'background-image' => false,
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview_media' => false,
                            'preview' => false,
                            'default'  => array(
                                'background-color' => '#333',
                                ),
    						'output' => array('.sidebar_header_5 .sidebar-wrap .widget-title h3, .sidebar_header_5 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_6 .sidebar-wrap .widget-title h3, .sidebar_header_6 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_7 .sidebar-wrap .widget-title h3, .sidebar_header_7 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_8 .sidebar-wrap .widget-title h3, .sidebar_header_8 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_9 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_10 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_11 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_12 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_13 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_14 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_15 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_16 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_17 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_18 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_21 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_22 .sidebar-wrap .widget-title h3:before,
                                               .sidebar_header_23 .sidebar-wrap .widget-title h3:before, .sidebar_header_23 .sidebar-wrap .widget-title h3:after,
                                               .sidebar_header_25 .sidebar-wrap .widget-title h3, .sidebar_header_25 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_26 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_27 .sidebar-wrap .widget-title h3, .sidebar_header_27 .sidebar-wrap .widget-title:before,
                                               .sidebar_header_28 .sidebar-wrap .widget-title h3, .sidebar_header_28 .sidebar-wrap .widget-title:before'),
    						'title' => esc_html__('Sidebar Widget Header Background', 'rubik'),
						),
                        
                        array(
    						'id'=>'bk-sidebar-header-dark-text-color',
    						'type' => 'color',
                            'required' => array(
                                'bk-sidebar-header','equals',array( 'sidebar_header_1', 'sidebar_header_2', 'sidebar_header_3', 'sidebar_header_4',
                                                                    'sidebar_header_5', 'sidebar_header_6', 'sidebar_header_7', 'sidebar_header_8',
                                                                    'sidebar_header_25', 'sidebar_header_27', 'sidebar_header_28' )
                            ),
    						'title' => esc_html__('Sidebar Header Text Color', 'rubik'), 
                            'output' => array('.sidebar_header_1 .sidebar-wrap .widget-title',
                                                '.sidebar_header_2 .sidebar-wrap .widget-title',
                                                '.sidebar_header_3 .sidebar-wrap .widget-title',
                                                '.sidebar_header_4 .sidebar-wrap .widget-title',
                                                '.sidebar_header_5 .sidebar-wrap .widget-title',
                                                '.sidebar_header_6 .sidebar-wrap .widget-title',
                                                '.sidebar_header_7 .sidebar-wrap .widget-title',
                                                '.sidebar_header_8 .sidebar-wrap .widget-title',
                                                '.sidebar_header_25 .sidebar-wrap .widget-title',
                                                '.sidebar_header_27 .sidebar-wrap .widget-title',
                                                '.sidebar_header_28 .sidebar-wrap .widget-title',
                                        ),
    						'default' => '#fff',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-sidebar-header-light-text-color',
    						'type' => 'color',
                            'required' => array(
                                'bk-sidebar-header','equals',array( 'sidebar_header_9', 'sidebar_header_10', 'sidebar_header_11', 'sidebar_header_12',
                                                                    'sidebar_header_13', 'sidebar_header_14', 'sidebar_header_15', 'sidebar_header_16',
                                                                    'sidebar_header_17', 'sidebar_header_18', 'sidebar_header_19', 'sidebar_header_20',
                                                                    'sidebar_header_21', 'sidebar_header_24', 'sidebar_header_26')
                            ),
    						'title' => esc_html__('Sidebar Header Text Color', 'rubik'), 
                            'output' => array('.sidebar_header_9 .sidebar-wrap .widget-title',
                                                '.sidebar_header_10 .sidebar-wrap .widget-title',
                                                '.sidebar_header_11 .sidebar-wrap .widget-title',
                                                '.sidebar_header_12 .sidebar-wrap .widget-title',
                                                '.sidebar_header_13 .sidebar-wrap .widget-title',
                                                '.sidebar_header_14 .sidebar-wrap .widget-title',
                                                '.sidebar_header_15 .sidebar-wrap .widget-title',
                                                '.sidebar_header_16 .sidebar-wrap .widget-title',
                                                '.sidebar_header_17 .sidebar-wrap .widget-title',
                                                '.sidebar_header_18 .sidebar-wrap .widget-title',
                                                '.sidebar_header_19 .sidebar-wrap .widget-title',
                                                '.sidebar_header_20 .sidebar-wrap .widget-title',
                                                '.sidebar_header_21 .sidebar-wrap .widget-title',
                                                '.sidebar_header_24 .sidebar-wrap .widget-title',
                                                '.sidebar_header_26 .sidebar-wrap .widget-title',
                                        ),
    						'default' => '#333',
    						'validate' => 'color',
						),
                        array(
    						'id'=>'bk-sidebar-header-border-color',
    						'type' => 'color',
                            'required' => array(
                                'bk-sidebar-header','equals',array( 'sidebar_header_24')
                            ),
    						'title' => esc_html__('Sidebar Header Border Color', 'rubik'),
    						'default' => '#333',
    						'validate' => 'color',
						),
                        
                        
                        array(
            				'id'=>'bk-widget-sidebar-font-setup',
            				'type' => 'typography', 
                            'output' => array('.sidebar-wrap .widget-title h3'),
            				'title' => esc_html__('Widget (Sidebar) Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => true,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Widget (Sidebar) Title Font', 'rubik'),
                            'default'     => array(
                                'font-size'   => '15px',
                            ),
                        ),
                        
                        array(
                            'id' => 'section-sidebar-header-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                        
                        // Footer Widget Header
                        
                        array(
                            'id' => 'section-footer-header-start',
                            'title' => esc_html__('Footer Widget Heading', 'rubik'),
                            'subtitle' => '',
                            'type' => 'section',                             
                            'indent' => true // Indent all options below until the next 'section' option is set.
                        ), 
                        array(
    						'id'=>'bk-footer-header',
    						'type' => 'image_select', 
    						'title' => esc_html__('Footer Widget Header Style ', 'rubik'),
                            'options' => array(
                                                'footer_header_1' => array(
                                                    'alt' => 'Header Style 1',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header5.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_2' => array(
                                                    'alt' => 'Header Style 2',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header6.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_3' => array(
                                                    'alt' => 'Header Style 3',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header7.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_4' => array(
                                                    'alt' => 'Header Style 4',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header8.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_5' => array(
                                                    'alt' => 'Header Style 5',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header13.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_6' => array(
                                                    'alt' => 'Header Style 6',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header14.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_7' => array(
                                                    'alt' => 'Header Style 7',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header15.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_8' => array(
                                                    'alt' => 'Header Style 8',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header16.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_9' => array(
                                                    'alt' => 'Header Style 9',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header17.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_10' => array(
                                                    'alt' => 'Header Style 10',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header18.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_11' => array(
                                                    'alt' => 'Header Style 11',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header19.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_12' => array(
                                                    'alt' => 'Header Style 12',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header20.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_13' => array(
                                                    'alt' => 'Header Style 13',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header22.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_14' => array(
                                                    'alt' => 'Header Style 14',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header23.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_15' => array(
                                                    'alt' => 'Header Style 15',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header25.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_16' => array(
                                                    'alt' => 'Header Style 16',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header26.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_17' => array(
                                                    'alt' => 'Header Style 17',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header27.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                                'footer_header_18' => array(
                                                    'alt' => 'Header Style 18',
                                                    'img' => get_template_directory_uri().'/images/admin_panel/widget-headers/widget-header28.png',
                                                    'title' => esc_html__(' ', 'rubik')
                                                ),
                                        ),
                            'default' => 'footer_header_1',
						),
                        array(
    						'id'=>'bk-footer-widget-title-height',
    						'type' => 'text', 
                            'validate' => 'numeric',
    						'title' => esc_html__('Footer Widget Title Line Height - in pixel', 'rubik'),
    						'subtitle' => esc_html__('input: number', 'rubik'),
                            'default' => '40',
						),
                        array(
                            'id'             => 'footer-heading-margin',
                            'type'           => 'spacing',
                            'output'         => array('.footer .widget-title'),
                            'mode'           => 'margin',
                            'units'          => array('px'),
                            'units_extended' => false,
                            'top'            => false,
                            'left'           => false,
                            'right'          => false,
                            'title'          => esc_html__('Footer Widget Heading Margin Option', 'rubik'),
                            'subtitle'       => esc_html__('Allow to choose the spacing or margin.', 'rubik'),
                            'default'            => array(
                                'margin-bottom'  => '28', 
                                'units'          => 'px', 
                            )
                        ),
                        /** Header BG **/
                        array(
    						'id'=>'bk-footer-widget-header-bg',
    						'type' => 'background',
                            'required' => array(
                                'bk-footer-header','equals',array( 'footer_header_1', 'footer_header_2', 'footer_header_3', 'footer_header_4',
                                                                    'footer_header_5', 'footer_header_6', 'footer_header_7', 'footer_header_8',
                                                                    'footer_header_9', 'footer_header_10', 'footer_header_13', 'footer_header_14',
                                                                    'footer_header_15', 'footer_header_16', 'footer_header_17', 'footer_header_18' )
                            ),
                            'background-image' => false,
                            'background-position' => false,
                            'background-repeat' => false,
                            'background-size' => false,
                            'background-attachment' => false,
                            'preview_media' => false,
                            'preview'  => false,
                            'default'  => array(
                                'background-color' => '#333',
                                ),
    						'output' => array( '.footer_header_1 .footer .widget-title h3, .footer_header_1 .footer .widget-title:before, 
                                                .footer_header_2 .footer .widget-title h3, .footer_header_2 .footer .widget-title:before,
                                                .footer_header_3 .footer .widget-title h3, .footer_header_3 .footer .widget-title:before,
                                                .footer_header_4 .footer .widget-title h3, .footer_header_4 .footer .widget-title:before,
                                                .footer_header_5 .footer .widget-title:before, .footer_header_6 .footer .widget-title:before,
                                                .footer_header_7 .footer .widget-title:before, .footer_header_8 .footer .widget-title:before,
                                                .footer_header_9 .footer .widget-title:before, .footer_header_10 .footer .widget-title:before,
                                                .footer_header_13 .footer .widget-title h3:before, .footer_header_14 .footer .widget-title h3:before,
                                                .footer_header_14 .footer .widget-title h3:after, 
                                                .footer_header_15 .footer .widget-title h3, .footer_header_15 .footer .widget-title:before,
                                                .footer_header_16 .footer .widget-title:before, .footer_header_17 .footer .widget-title h3,
                                                .footer_header_18 .footer .widget-title h3'),
    						'title' => esc_html__('Footer Widget Header Background', 'rubik'),
						),
                        array( 
                            'id'       => 'bk-footer-widget-header-arrow',
                            'type' => 'color',
                            'required' => array(
                                'bk-footer-header','equals',array( )
                            ),
                            'title'    => esc_html__('Footer Widget Header Arrow', 'rubik'),
                            'default' => '#333',
    						'validate' => 'color',
                        ),
                        array(
    						'id'=>'bk-footer-widget-header-text-color',
    						'type' => 'color',
                            'required' => array(
                                'bk-footer-header','equals',array( 'footer_header_1', 'footer_header_2', 'footer_header_3', 'footer_header_4',
                                                                   'footer_header_5', 'footer_header_6', 'footer_header_7', 'footer_header_8',
                                                                   'footer_header_9', 'footer_header_10', 'footer_header_11', 'footer_header_12',
                                                                   'footer_header_13', 'footer_header_14', 'footer_header_15', 'footer_header_16',
                                                                   'footer_header_17', 'footer_header_18' )
                            ),
    						'title' => esc_html__('Footer Widget Header Color', 'rubik'), 
                            'output' => array( '.footer_header_1 .footer .widget-title h3, .footer_header_2 .footer .widget-title h3,
                                                .footer_header_3 .footer .widget-title h3, .footer_header_4 .footer .widget-title h3,
                                                .footer_header_5 .footer .widget-title h3, .footer_header_6 .footer .widget-title h3,
                                                .footer_header_7 .footer .widget-title h3, .footer_header_8 .footer .widget-title h3,
                                                .footer_header_9 .footer .widget-title h3, .footer_header_10 .footer .widget-title h3,
                                                .footer_header_11 .footer .widget-title h3, .footer_header_12 .footer .widget-title h3,
                                                .footer_header_13 .footer .widget-title h3, .footer_header_14 .footer .widget-title h3,
                                                .footer_header_15 .footer .widget-title h3, .footer_header_16 .footer .widget-title h3,
                                                .footer_header_17 .footer .widget-title h3, .footer_header_18 .footer .widget-title h3'),
    						'default' => '#fff',
    						'validate' => 'color',
						),
                        array( 
                            'id'       => 'bk-footer-header-arrow',
                            'type' => 'color',
                            'required' => array(
                                'bk-footer-header','equals',array( 'footer_header_17', 'footer_header_18' )
                            ),
                            'title'    => esc_html__('Footer Header Arrow', 'rubik'),
                            'default' => '#333',
    						'validate' => 'color',
                        ),
                        array(
            				'id'=>'bk-footerwidget-title-font-setup',
            				'type' => 'typography', 
                            'output' => array('.footer .widget-title h3'),
            				'title' => esc_html__('Footer Widget Title Font Setup', 'rubik'),
            				//'compiler'=>true, // Use if you want to hook in your own CSS compiler
            				'google'=>false, // Disable google fonts. Won't work if you haven't defined your google api key
                            'font-weight' => true,
            				'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
            				//'subsets'=>false, // Only appears if google is true and subsets not set to false
                            'font-family' => false,
            				'font-size'=>true,
            				'line-height'=>false,
            				'word-spacing'=>false, // Defaults to false
            				'letter-spacing'=>true, // Defaults to false
            				'color'=>false,
            				'preview'=>false, // Disable the previewer
            				'all_styles' => true, // Enable all Google Font style/weight variations to be added to the page
            				'units'=>'px', // Defaults to px
                            'text-align' => false,
            				'subtitle'=> esc_html__('Font options for the Footer Widget Title Font', 'rubik'),
                            'default'     => array(
                                'font-size'   => '16px',
                            ),
                        ),
                        array(
                            'id' => 'section-footer-header-end',
                            'type' => 'section',                             
                            'indent' => false // Indent all options below until the next 'section' option is set.
                        ),
                    ),
                );
					

			$theme_info = '<div class="redux-framework-section-desc">';
			$theme_info .= '<p class="redux-framework-theme-data description theme-uri">'.esc_html__('<strong>Theme URL:</strong> ', 'rubik').'<a href="'.$this->theme->get('ThemeURI').'" target="_blank">'.$this->theme->get('ThemeURI').'</a></p>';
			$theme_info .= '<p class="redux-framework-theme-data description theme-author">'.esc_html__('<strong>Author:</strong> ', 'rubik').$this->theme->get('Author').'</p>';
			$theme_info .= '<p class="redux-framework-theme-data description theme-version">'.esc_html__('<strong>Version:</strong> ', 'rubik').$this->theme->get('Version').'</p>';
			$theme_info .= '<p class="redux-framework-theme-data description theme-description">'.$this->theme->get('Description').'</p>';
			$tabs = $this->theme->get('Tags');
			if ( !empty( $tabs ) ) {
				$theme_info .= '<p class="redux-framework-theme-data description theme-tags">'.esc_html__('<strong>Tags:</strong> ', 'rubik').implode(', ', $tabs ).'</p>';	
			}
			$theme_info .= '</div>';

			$this->sections[] = array(
				'type' => 'divide',
			);

		}	

		public function setHelpTabs() {

		}


		/**
			
			All the possible arguments for Redux.
			For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

		 **/
		public function setArguments() {
			
			$theme = wp_get_theme(); // For use with some settings. Not necessary.

			$this->args = array(
	            
	            // TYPICAL -> Change these values as you need/desire
				'opt_name'          	=> 'rubik_option', // This is where your data is stored in the database and also becomes your global variable name.
				'display_name'			=> $theme->get('Name'), // Name that appears at the top of your panel
				'display_version'		=> $theme->get('Version'), // Version that appears at the top of your panel
				'menu_type'          	=> 'menu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu'     	=> true, // Show the sections below the admin menu item or not
				'menu_title'			=> esc_html__( 'Theme Options', 'rubik' ),
	            'page'		 	 		=> esc_html__( 'Theme Options', 'rubik' ),
	            'google_api_key'   	 	=> 'AIzaSyBdxbxgVuwQcnN5xCZhFDSpouweO-yJtxw', // Must be defined to add google fonts to the typography module
	            'global_variable'    	=> '', // Set a different name for your global variable other than the opt_name
	            'dev_mode'           	=> false, // Show the time the page took to load, etc
	            'customizer'         	=> true, // Enable basic customizer support
                'google_update_weekly'  => true, //This will only function if you have a google_api_key provided. This argument tells the core to grab the Google fonts cache weekly, ensuring your font list is always up to date.

	            // OPTIONAL -> Give you extra features
	            'page_priority'      	=> null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
	            'page_parent'        	=> 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	            'page_permissions'   	=> 'manage_options', // Permissions needed to access the options panel.
	            'menu_icon'          	=> '', // Specify a custom URL to an icon
	            'last_tab'           	=> '', // Force your panel to always open to a specific tab (by id)
	            'page_icon'          	=> 'icon-themes', // Icon displayed in the admin panel next to your menu_title
	            'page_slug'          	=> '_options', // Page slug used to denote the panel
	            'save_defaults'      	=> true, // On load save the defaults to DB before user clicks save or not
	            'default_show'       	=> false, // If true, shows the default value next to each field that is not the default value.
	            'default_mark'       	=> '', // What to print by the field's title if the value shown is default. Suggested: *


	            // CAREFUL -> These options are for advanced use only
	            'transient_time' 	 	=> 60 * MINUTE_IN_SECONDS,
	            'output'            	=> true, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
	            'output_tag'            	=> true, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
	            //'domain'             	=> 'redux-framework', // Translation domain key. Don't change this unless you want to retranslate all of Redux.
	            //'footer_credit'      	=> '', // Disable the footer credit of Redux. Please leave if you can help it.
	            

	            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
	            'database'           	=> '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
	            
	        
	            'show_import_export' 	=> true, // REMOVE
	            'system_info'        	=> false, // REMOVE
	            
	            'help_tabs'          	=> array(),
	            'help_sidebar'       	=> '', // esc_html__( '', $this->args['domain'] );            
				);


			// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.		
			$this->args['share_icons'][] = array(
			    'url' => 'https://github.com/ReduxFramework/ReduxFramework',
			    'title' => 'Visit us on GitHub', 
			    'icon' => 'el-icon-github'
			    // 'img' => '', // You can use icon OR img. IMG needs to be a full URL.
			);		
			$this->args['share_icons'][] = array(
			    'url' => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
			    'title' => 'Like us on Facebook', 
			    'icon' => 'el-icon-facebook'
			);
			$this->args['share_icons'][] = array(
			    'url' => 'http://twitter.com/reduxframework',
			    'title' => 'Follow us on Twitter', 
			    'icon' => 'el-icon-twitter'
			);
			$this->args['share_icons'][] = array(
			    'url' => 'http://www.linkedin.com/company/redux-framework',
			    'title' => 'Find us on LinkedIn', 
			    'icon' => 'el-icon-linkedin'
			);

			
	 
			// Panel Intro text -> before the form
			if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false ) {
				if (!empty($this->args['global_variable'])) {
					$v = $this->args['global_variable'];
				} else {
					$v = str_replace("-", "_", $this->args['opt_name']);
				}
				$this->args['intro_text'] = '';
			} else {
				$this->args['intro_text'] = '';
			}

			// Add content after the form.
			$this->args['footer_text'] = '' ;

		}
	}
	new Redux_Framework_config();

}


/** 

	Custom function for the callback referenced above

 */
if ( !function_exists( 'redux_my_custom_field' ) ):
	function redux_my_custom_field($field, $value) {
	    print_r($field);
	    print_r($value);
	}
endif;

/**
 
	Custom function for the callback validation referenced above

**/
if ( !function_exists( 'redux_validate_callback_function' ) ):
	function redux_validate_callback_function($field, $value, $existing_value) {
	    $error = false;
	    $value =  'just testing';
	    
	    $return['value'] = $value;
	    if($error == true) {
	        $return['error'] = $field;
	    }
	    return $return;
	}
endif;
