<?php
$rubik_default_options = array
(
    'bk-primary-color' => '#EA2323',
    'bk-site-container-width' => '1110',
    'bk-site-layout' => 'wide',
    'bk-sb-responsive-sw' => '1',
    'bk-backtop-switch' => '1',
    'bk-header-type' => 'header1',
    'bk-header-2-options' => 'style-1',
    'bk-header-12-options' => 'style-1',
    'bk-logo-hide' => '1',
    'bk-logo-margin-top' => '50',
    'bk-logo-margin-bottom' => '50',
    'bk-header-logo-position' => 'left',
    'bk-header-bg' => array
        (
            'background-color' => '#fff',
        ),

    'bk-social-header-switch' => '0',
    'bk-social-header' => array
        (
            'fb' => '',
            'twitter' => '',
            'gplus' => '',
            'linkedin' => '',
            'pinterest' => '',
            'instagram' => '',
            'dribbble' => '',
            'youtube' => '',
            'vimeo' => '',
            'vk' => '',
            'vine' => '',
            'snapchat' => '',
            'rss' => '',
        ),

    'bk-header-banner-switch' => '0',
    'bk-header-banner' => array
        (
            'imgurl' => 'http://',
            'linkurl' => 'http://',
        ),

    'bk-banner-script' => '',
    'bk-header-ticker' => '0',
    'bk-ticker-title' => 'Breaking News',
    'bk-ticker-number' => '5',
    'bk-ticker-featured' => '0',
    'bk-header-top-switch' => '1',
    'bk-header-top-date' => '1',
    'bk-top-date-position' => 'left',
    'bk-topbar-bg-style' => 'color',
    'bk-topbar-bg-gradient' => array
        (
            'from' => '#1e73be',
            'to' => '#00897e',
        ),

    'bk-topbar-bg-color' => array
        (
            'background-color' => '#000',
        ),

    'bk-top-bar-text-color' => '#fff',
    'bk-input-search-placeholder-top-bar' => 'placeholder-white',
    'bk-mainnav-bg-style' => 'color',
    'bk-mainnav-bg-gradient' => array
        (
            'from' => '#1e73be',
            'to' => '#00897e',
        ),

    'bk-mainnav-bg-color' => array
        (
            'background-color' => '#fff',
        ),

    'bk-main-menu-text-color' => '#000',
    'bk-main-menu-hover-text-color' => '#ccc',
    'bk-main-nav-layout' => 'left',
    'bk-sticky-nav-switch' => '2',
    'bk-input-search-placeholder-main-nav' => 'placeholder-black',
    'bk-canvas-desktop-switch' => '0',
    'bk-canvas-panel-style' => 'light',
    'bk-offcanvas-menu-setup' => array
        (
            'font-size' => '16px',
            'color' => '#ccc',
        ),

    'bk-offcanvas-social-header-switch' => '0',
    'bk-offcanvas-copyright' => '',
    'bk-canvas-button-style' => 'normal',
    'bk-canvas-menu-button-color' => '#222',
    'bk-canvas-menu-button-color-hover' => '#000',
    'bk-canvas-menu-12-button-color' => '#222',
    'bk-canvas-menu-12-button-color-hover' => '#000',
    'footer-sortable' => array
        (
            '1' => '',
            '2' => '1',
            '3' => '',
        ),

    'bk-footer-padding-top' => '50',
    'bk-footer-padding-bottom' => '50',
    'bk-footer-bg-style' => 'color',
    'bk-footer-bg-gradient' => array
        (
            'from' => '#1e73be',
            'to' => '#00897e',
        ),

    'bk-footer-bg-color' => array
        (
            'background-color' => '#000',
        ),

    'bk-footer-bg-overlay' => array
        (
            'background-color' => '#000',
        ),

    'bk-footer-content-style' => 'bk-footer-dark',
    'bk-footer-a-hover-color' => '',
    'bk-footer-menu-social-position' => 'disable',
    'bk-footer-title-font-setup' => array
        (
            'font-family' => 'Open Sans',
            'google' => '1',
            'font-style' => 'normal',
            'font-size' => '16px',
            'text-transform' => 'uppercase',
            'text-align' => 'center',
        ),

    'bk-social-footer-switch' => '0',
    'bk-social-footer' => array
        (
            'fb' => '',
            'twitter' => '',
            'gplus' => '',
            'linkedin' => '',
            'pinterest' => '',
            'instagram' => '',
            'dribbble' => '',
            'youtube' => '',
            'vimeo' => '',
            'vk' => '',
            'vine' => '',
            'snapchat' => '',
            'rss' => '',
        ),

    'bk-social-item-footer-setup' => array
        (
            'font-size' => '16px',
            'color' => '#ccc',
        ),

    'bk-social-item-footer-bg-hover' => array
        (
            'background-color' => '#222',
        ),

    'bk-footer-instagram-title' => '',
    'bk-footer-instagram-access-token' => '',
    'bk-footer-instagram-image-count' => '12',
    'bk-footer-instagram-columns' => '7',
    'bk-footer-lower-bg-style' => 'color',
    'bk-footer-lower-bg-gradient' => array
        (
            'from' => '#1e73be',
            'to' => '#00897e',
        ),

    'bk-footer-lower-bg-color' => array
        (
            'background-color' => '#000',
        ),

    'cr-text' => '',
    'bk-right-side-footer-lower' => 'social-media',
    'bk-footer-lower-social' => array
        (
            'fb' => '',
            'twitter' => '',
            'gplus' => '',
            'linkedin' => '',
            'pinterest' => '',
            'instagram' => '',
            'dribbble' => '',
            'youtube' => '',
            'vimeo' => '',
            'vk' => '',
            'vine' => '',
            'snapchat' => '',
            'rss' => '',
        ),

    'bk-footer-lower-font-setup' => array
        (
            'font-family' => 'Open Sans',
            'google' => '1',
            'font-style' => 'normal',
            'font-size' => '16px',
            'text-transform' => 'normal',
        ),

    'bk-body-font-setup' => array
        (
            'font-size' => '16px',
        ),

    'bk-top-menu-font' => array
        (
            'font-weight' => '500',
            'font-family' => 'Roboto',
            'google' => '1',
        ),

    'bk-main-menu-font' => array
        (
            'font-weight' => '700',
            'font-family' => 'Roboto',
            'google' => '1',
            'text-transform' => 'Uppercase',
        ),

    'bk-meta-font' => array
        (
            'font-weight' => '600',
            'font-family' => 'Open Sans',
            'google' => '1',
        ),

    'bk-title-font' => array
        (
            'font-weight' => '700',
            'font-family' => 'Open Sans',
            'google' => '1',
        ),

    'bk-body-font' => array
        (
            'font-weight' => '400',
            'font-family' => 'Lato',
            'google' => '1',
        ),

    'bk-blockquote-font' => array
        (
            'font-weight' => '400',
            'font-family' => 'Open Sans',
            'google' => '1',
            'font-size' => '22px',
            'line-height' => '35px',
            'color' => '#fff',
            'text-transform' => 'inherit',
            'text-align' => 'initial',
        ),

    'bk-blockquote-bg' => array
        (
            'background-color' => '#EA2323',
        ),

    'bk-breadcrumbs' => 'disable',
    'opt-spacing' => array
        (
            'margin-top' => '16px',
            'margin-right' => '24px',
            'margin-bottom' => '16px',
            'margin-left' => '24px',
            'units' => 'px',
        ),

    'pagebuilder-sidebar' => 'enable',
    'bk-category-layout' => 'masonry',
    'category_post_icon' => 'hide',
    'category-page-sidebar' => 'home_sidebar',
    'category-page-sidebar-position' => 'right',
    'category-stick-sidebar' => 'enable',
    'bk-archive-layout' => 'classic-blog',
    'archive_post_icon' => 'hide',
    'archive-page-sidebar' => 'home_sidebar',
    'archive-page-sidebar-position' => 'right',
    'archive-stick-sidebar' => 'enable',
    'bk-author-layout' => 'classic-blog',
    'author_post_icon' => 'hide',
    'author-page-sidebar' => 'home_sidebar',
    'author-page-sidebar-position' => 'right',
    'author-stick-sidebar' => 'enable',
    'bk-search-layout' => 'classic-blog',
    'search-result-setting' => 'all',
    'search_post_icon' => 'hide',
    'search-page-sidebar' => 'home_sidebar',
    'search-page-sidebar-position' => 'right',
    'search-stick-sidebar' => 'enable',
    'bk-blog-layout' => 'classic-blog',
    'blog_post_icon' => 'hide',
    'blog-page-sidebar' => 'home_sidebar',
    'blog-page-sidebar-position' => 'right',
    'blog-stick-sidebar' => 'enable',
    'bk_page_feat_img' => '1',
    'bk_page_layout' => 'has_sidebar',
    'bk_page_sidebar_select' => 'home_sidebar',
    'bk_page_sidebar_position' => 'right',
    'bk_page_sidebar_sticky' => 'enable',
    'bk-404-title' => 'Page not found',
    'bk-404-description' => 'Oops! The page you were looking for was not found. Perhaps searching can help.',
    'bk-404-search' => 'enable',
    'bk-404-latest' => 'enable',
    'bk-404-post-font' => array
        (
            'font-weight' => '700',
            'google' => '1',
            'font-size' => '16px',
            'text-transform' => 'inherit',
        ),

    'bk-404-latest-title' => 'Latest Articles',
    'bk-single-template' => 'single_template_1',
    'single-heading-related-posts' => 'enable',
    'single-heading-related-posts-entries' => '3',
    'bk-single-title-font' => array
        (
            'font-weight' => '700',
            'google' => '1',
            'font-size' => '32px',
            'text-transform' => 'inherit',
        ),

    'bk-single-body-font' => array
        (
            'font-weight' => '400',
            'font-family' => 'Open Sans',
            'google' => '1',
            'font-size' => '16px',
            'line-height' => '27px',
            'color' => '#111',
            'text-transform' => 'inherit',
            'text-align' => 'initial',
        ),

    'paragraph-spacing' => array
        (
            'margin-bottom' => '25px',
            'units' => 'px',
        ),

    'single-sidebar-switch' => 'enable',
    'single-page-sidebar' => 'single_sidebar',
    'single-sidebar-position' => 'right',
    'single-stick-sidebar' => 'disable',
    'bk-og-tag' => '1',
    'bk-sharebox-top-sw' => '1',
    'top-share-items-shown' => '2',
    'bk-fb-top-sw' => '1',
    'bk-fb-top-text' => '',
    'bk-tw-top-sw' => '1',
    'bk-tw-top-text' => '',
    'bk-gp-top-sw' => '1',
    'bk-gp-top-text' => '',
    'bk-pi-top-sw' => '1',
    'bk-pi-top-text' => '',
    'bk-stu-top-sw' => '1',
    'bk-stu-top-text' => '',
    'bk-li-top-sw' => '1',
    'bk-li-top-text' => '',
    'bk-sharebox-bottom-sw' => '1',
    'bottom-share-items-shown' => '2',
    'bk-fb-bottom-sw' => '1',
    'bk-fb-bottom-text' => '',
    'bk-tw-bottom-sw' => '1',
    'bk-tw-bottom-text' => '',
    'bk-gp-bottom-sw' => '1',
    'bk-gp-bottom-text' => '',
    'bk-pi-bottom-sw' => '1',
    'bk-pi-bottom-text' => '',
    'bk-stu-bottom-sw' => '1',
    'bk-stu-bottom-text' => '',
    'bk-li-bottom-sw' => '1',
    'bk-li-bottom-text' => '',
    'bk-authorbox-sw' => '0',
    'bk-postnav-sw' => '1',
    'bk-post-nav-style' => 'under-content',
    'bk-single-post-nav-font' => array
        (
            'google' => '1',
            'font-size' => '16px',
            'text-transform' => 'inherit',
        ),

    'bk-related-sw' => '1',
    'bk-related-output' => '3',
    'bk-single-related-post-font' => array
        (
            'google' => '1',
            'font-size' => '16px',
            'text-transform' => 'inherit',
        ),

    'bk-comment-sw' => '1',
    'bk-recommend-box' => '1',
    'recommend-box-title' => '',
    'recommend-number' => '',
    'bk-module-header' => 'module_header_7',
    'bk-module-title-height' => '40',
    'module-heading-margin' => array
        (
            'margin-bottom' => '30',
            'units' => 'px',
        ),

    'bk-module-title-font-setup' => array
        (
            'font-size' => '16px',
        ),

    'bk-module-title-color-default' => '#000',
    'bk-module-title-text-color-default' => '#fff',
    'bk-module-tab-color-default' => '#777',
    'bk-sidebar-header' => 'sidebar_header_1',
    'bk-widget-title-height' => '40',
    'widget-heading-margin' => array
        (
            'margin-bottom' => '28',
            'units' => 'px',
        ),

    'bk-sidebar-header-1234-bg' => array
        (
            'background-color' => '#333',
        ),

    'bk-sidebar-header-arrow' => '#333',
    'bk-sidebar-header-5-16-bg' => array
        (
            'background-color' => '#333',
        ),

    'bk-sidebar-header-dark-text-color' => '#fff',
    'bk-sidebar-header-light-text-color' => '#333',
    'bk-sidebar-header-border-color' => '#333',
    'bk-widget-sidebar-font-setup' => array
        (
            'font-size' => '15px',
        ),

    'bk-footer-header' => 'footer_header_1',
    'bk-footer-widget-title-height' => '40',
    'footer-heading-margin' => array
        (
            'margin-bottom' => '28',
            'units' => 'px',
        ),

    'bk-footer-widget-header-bg' => array
        (
            'background-color' => '#333',
        ),

    'bk-footer-widget-header-arrow' => '#333',
    'bk-footer-widget-header-text-color' => '#fff',
    'bk-footer-header-arrow' => '#333',
    'bk-footerwidget-title-font-setup' => array
        (
            'font-size' => '16px',
        ),

    'REDUX_last_saved' => '1533918240',
    'REDUX_LAST_SAVE' => '1533918240',
);

if (!class_exists('rubik_core')) {
    class rubik_core {
        static function bk_get_global_var($bk_var){  
            if ($bk_var == 'rubik_option') {
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    global $rubik_option;
                    return $rubik_option;
                }else {
                    global $rubik_default_options;
                    return $rubik_default_options;
                }
            }else if ($bk_var == 'rubik_justified_ids') {
                global $rubik_justified_ids;
                return $rubik_justified_ids;
            }else if ($bk_var == 'rubik_ajax_btnstr') {
                global $rubik_ajax_btnstr;
                return $rubik_ajax_btnstr;
            }else if ($bk_var == 'rubik_dynamic_css') {
                global $rubik_dynamic_css;
                return $rubik_dynamic_css;
            }
        }
        static function bk_rwmb_meta($theoption, $arg, $termID ){
            $ret = '';
            if(function_exists('rwmb_meta')) {
                $ret = rwmb_meta( $theoption, $arg, $termID );  
            }else {
                $ret = '';
            }
            return $ret;
        }        
        static function bk_widget_query_arg($widget_opts){
            $widget_opts = shortcode_atts (
    		         array(
    			         'category_ids'   => '',
    			         'category_id'    => '',
    			         'author_id'      => '',
    			         'tags'           => '',
    			         'posts_per_page' => '',
    			         'offset'         => '',
    			         'orderby'        => '',
    			         'post_types'     => '',
    			         'meta_key'       => '',
                         'widget_type'    => '',
                         'editor_pick'   => '',
                         'editor_exclude' => '',
                         'entries'        => '',
    		         ), $widget_opts
    	         );
            //get Query Args
			$query_args  = array(
                'post_status'           => 'publish',
                'ignore_sticky_posts'   => 1,
                'post_type'             => 'post',
                'orderby'               => 'date',
                'posts_per_page'        => $widget_opts['entries'],
            );
            
            if(isset($widget_opts['editor_pick']) && ($widget_opts['editor_pick'] != null)) {
                $editorPickPosts = array_map('intval', explode(',',$widget_opts['editor_pick']));
                $query_args = array(
        			'post_type'         => 'post',
                    'ignore_sticky_posts' => 1,
        			'posts_per_page'    => $widget_opts['entries'],
                    'post__in'          =>  $editorPickPosts,
                    'orderby'           => 'post__in',
                    'offset'            => $widget_opts['offset'],
        		);
                return $query_args;
            }
            
            $widget_type = $widget_opts['widget_type'];
            if($widget_type == 'review') {
                $query_args['meta_query'] = array(
    				array(
    					'key' => 'bk_review_checkbox',
    					'value' => '1',
    				)
                 );
            }else if($widget_type == 'comment') {
                
            }
            
			if ($widget_opts['editor_exclude'] != '') {
				$query_args['post__not_in'] = array_map('intval', explode(',',$widget_opts['editor_exclude']));
			}

			//post per page
			if ( $widget_opts['entries'] != '' ) {
                $query_args['posts_per_page'] = $widget_opts['entries'];
			} else {
				$query_args['posts_per_page'] = 4;
			}

			//categories query
			if ( $widget_opts['category_ids'] != '') {
				$query_args['category__in'] = array_map('intval', explode(',',$widget_opts['category_ids']));
			} else {
				if ( ! empty( $widget_opts['category_id'] ) ) {
					$query_args['cat'] = $widget_opts['category_id'];
				}
			}

			//tag in query
			if ( $widget_opts['tags'] != '' ) {
				//$query_args['tag__in'] = $widget_opts['tags'];
                $query_args['tag__in'] = array_map('intval', explode(',',$widget_opts['tags']));
			}

			//offset query
			if ( $widget_opts['offset'] ) {
				$query_args['offset'] = intval( $widget_opts['offset'] );
			}else {
                $query_args['offset'] = 0;
			}

			switch ( $widget_opts['orderby'] ) {

				//Date post
				case 'date' :
					$query_args['orderby'] = 'date';
					break;

				//Popular comment
				case 'comment_count' :
					$query_args['orderby'] = 'comment_count';
					break;
                
                //Popular Views
				case 'view_count' :
                    $query_args['meta_key'] = 'post_views_count';
					$query_args['orderby']  = 'meta_value_num';
					$query_args['order']    = 'DESC';
					break;

				//Modified
				case 'modified' :
					$query_args['orderby'] = 'modified';
					break;
                    
                // Best Review
				case 'top_review' :
					$query_args['meta_key'] = 'bk_final_score';
					$query_args['orderby']  = 'meta_value_num';
					$query_args['order']    = 'DESC';
					break;
                    
                // Latest Review
				case 'latest_review' :
					$query_args['meta_key'] = 'bk_final_score';
                    $query_args['orderby']  = 'date';
					$query_args['order']    = 'DESC';
                    $query_args['meta_query'] = array(
                                    				array(
                                    					'key' => 'bk_review_checkbox',
                                    					'value' => '1',
                                    				)
                                                );
					break;
				//Random
				case 'rand':
					$query_args['orderby'] = 'rand';
					break;

				//Alphabet decs
				case 'alphabetical_decs':
					$query_args['orderby'] = 'title';
					$query_args['order']   = 'DECS';
					break;

				//Alphabet asc
				case 'alphabetical_asc':
					$query_args['orderby'] = 'title';
					$query_args['order']   = 'ASC';
					break;
                
                // Default
                default:
                    $query_args['orderby'] = 'date';
					break;
			}
            //$the_query = new WP_Query( $query_args );
            return $query_args;
        }
        static function bk_print_dynamic_css(){
            // Dynamic CSS
            $rubik_dynamic_css = rubik_core::bk_get_global_var('rubik_dynamic_css');
            
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            
            $moduleHeaderStyle = 'module_header_1';
            if (isset($rubik_option['bk-module-header']) && ($rubik_option['bk-module-header'] != '')) {
                $moduleHeaderStyle = $rubik_option['bk-module-header'];
            }else {
                $moduleHeaderStyle = 'module_header_1';
            }
            
            $bk_dynamic_css = '';
            $bk_dynamic_css = '<style type="text/css" media="all">';
            if ( $rubik_dynamic_css != '') {
                foreach ($rubik_dynamic_css as $module_id => $value) {
                    $thismodule = explode("-",$module_id);
                    if(($thismodule[0] == 'block_1') || ($thismodule[0] == 'hero')) {
                        $bk_dynamic_css .= '#'.$module_id.' .large-post .post-c-wrap .title {font-size: '.$rubik_dynamic_css[$module_id]['title_font_size'].'px; letter-spacing: '.$rubik_dynamic_css[$module_id]['title_letter_spacing'].'px;}';
                    }else if($thismodule[0] == 'classic') {
                        $bk_dynamic_css .= '#'.$module_id.' .bk-blog-content h4 {font-size: '.$rubik_dynamic_css[$module_id]['title_font_size'].'px; letter-spacing: '.$rubik_dynamic_css[$module_id]['title_letter_spacing'].'px;}'; 
                    }else if(($thismodule[0] == 'youtube_playlist') || ($thismodule[0] == 'youtube_playlist_2') || ($thismodule[0] == 'youtube_playlist_3') || ($thismodule[0] == 'youtube_playlist_widget')){
                        $bk_dynamic_css .= '#'.$module_id.'.bk-module-inline-bg {background-color: '.$rubik_dynamic_css[$module_id]['module_bg'].';}';
                        $bk_dynamic_css .= '#'.$module_id.' .bk-current-video-description {background-color: '.$rubik_dynamic_css[$module_id]['playing_title_bg'].';}';
                        $bk_dynamic_css .= '#'.$module_id.' .bk-current-video-title, #'.$module_id.' .bk-current-video-time {color: '.$rubik_dynamic_css[$module_id]['playing_title_color'].';}';
                        if($thismodule[0] != 'youtube_playlist_widget') {
                            $bk_dynamic_css .= '#'.$module_id.' .video_playlist_module_title .video_title_text  {font-size: '.$rubik_dynamic_css[$module_id]['title_font_size'].'px; letter-spacing: '.$rubik_dynamic_css[$module_id]['title_letter_spacing'].'px;}'; 
                        }
                    }else if(($thismodule[0] == 'row_2') || ($thismodule[0] == 'masonry_2') || ($thismodule[0] == 'row_tall_2')
                            || ($thismodule[0] == 'row_2_fw') || ($thismodule[0] == 'masonry_2_fw') || ($thismodule[0] == 'row_tall_2_fw')) {
                        $bk_dynamic_css .= '#'.$module_id.' .row-head .title {font-size: '.$rubik_dynamic_css[$module_id]['title_font_size'].'px; letter-spacing: '.$rubik_dynamic_css[$module_id]['title_letter_spacing'].'px;}'; 
                    }else {
                        $bk_dynamic_css .= '#'.$module_id.' .post-c-wrap .title {font-size: '.$rubik_dynamic_css[$module_id]['title_font_size'].'px; letter-spacing: '.$rubik_dynamic_css[$module_id]['title_letter_spacing'].'px;}'; 
                    }
                    if(isset($rubik_dynamic_css[$module_id]['title_font_weight']) && ($rubik_dynamic_css[$module_id]['title_font_weight'] != '')) {
                        if(($thismodule[0] == 'row_2') || ($thismodule[0] == 'masonry_2') || ($thismodule[0] == 'row_tall_2')
                            || ($thismodule[0] == 'row_2_fw') || ($thismodule[0] == 'masonry_2_fw') || ($thismodule[0] == 'row_tall_2_fw')) {
                                
                            $bk_dynamic_css .= '#'.$module_id.' .row-head .title {font-weight: '.$rubik_dynamic_css[$module_id]['title_font_weight'].'}';
                            
                        }else {
                            $bk_dynamic_css .= '#'.$module_id.' .post-c-wrap .title {font-weight: '.$rubik_dynamic_css[$module_id]['title_font_weight'].'}';
                        }
                    }
                    
                    if(isset($rubik_dynamic_css[$module_id]['font_size_small_post']) && ($rubik_dynamic_css[$module_id]['font_size_small_post'] != '')) {
                        $bk_dynamic_css .= '#'.$module_id.' .bk-post-title-small .post-c-wrap h4 {font-size: '.$rubik_dynamic_css[$module_id]['font_size_small_post'].'px; letter-spacing: '.$rubik_dynamic_css[$module_id]['title_letter_spacing'].'px;}';
                    }
                    
                    if(isset($rubik_dynamic_css[$module_id]['font_weight_small_post']) && ($rubik_dynamic_css[$module_id]['font_weight_small_post'] != '')) {
                        $bk_dynamic_css .= '#'.$module_id.' .bk-post-title-small .post-c-wrap h4 {font-weight: '.$rubik_dynamic_css[$module_id]['font_weight_small_post'].'}';
                    }
                    if((isset($rubik_dynamic_css[$module_id]['module_title_color'])) && ($rubik_dynamic_css[$module_id]['module_title_color'] != '')) {
                        $bk_dynamic_css .= '#'.$module_id.' .module-title .bk-tab-original, #'.$module_id.' .module-title:before {background-color: '.$rubik_dynamic_css[$module_id]['module_title_color'].';}';
                    }                    
                    
                    if((isset($rubik_dynamic_css[$module_id]['module_title_text_color'])) && ($rubik_dynamic_css[$module_id]['module_title_text_color'] != '')) {
                        $bk_dynamic_css .= '#'.$module_id.' .module-title h2 {color: '.$rubik_dynamic_css[$module_id]['module_title_text_color'].' !important;}';
                    }
                    if((isset($rubik_dynamic_css[$module_id]['module_tab_color'])) && ($rubik_dynamic_css[$module_id]['module_tab_color'] != '')) {
                        $bk_dynamic_css .= '#'.$module_id.' .module-title .bk-tabs {color: '.$rubik_dynamic_css[$module_id]['module_tab_color'].' !important;}';
                    }
                    if(isset($rubik_dynamic_css[$module_id]['module_background']) && ($rubik_dynamic_css[$module_id]['module_background'] != '')) {
                        $bk_dynamic_css .= '#'.$module_id.' {background-color: '.$rubik_dynamic_css[$module_id]['module_background'].'; padding: 60px 0;}';
                    }
                    if(isset($rubik_dynamic_css[$module_id]['module_margin']) && ($rubik_dynamic_css[$module_id]['module_margin'] != '')) {
                        $bk_dynamic_css .= '#'.$module_id.' {margin: '.$rubik_dynamic_css[$module_id]['module_margin'].'}';
                    }
                    
                    // Module Header
                    if(isset($rubik_dynamic_css[$module_id]['module_title_color']) && ($rubik_dynamic_css[$module_id]['module_title_color'] != '')) {
                        //style 1
                        if($moduleHeaderStyle == 'module_header_1') {
                            $bk_dynamic_css .= '.module_header_1 #'.$module_id.' .module-title:before, .module_header_1 #'.$module_id.' .module-title h2{background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 2
                        else if($moduleHeaderStyle == 'module_header_2') {
                            $bk_dynamic_css .= '.module_header_2 #'.$module_id.' .module-title:before, .module_header_2 #'.$module_id.' .module-title h2{background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 3
                        else if($moduleHeaderStyle == 'module_header_3') {
                            $bk_dynamic_css .= '.module_header_3 #'.$module_id.' .module-title:before {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'} .module_header_3 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 4
                        else if($moduleHeaderStyle == 'module_header_4') {
                            $bk_dynamic_css .= '.module_header_4 #'.$module_id.' .module-title:before {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'} .module_header_4 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 5
                        else if($moduleHeaderStyle == 'module_header_5') {
                            $bk_dynamic_css .= '.module_header_5 #'.$module_id.' .module-title:before {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'} .module_header_5 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 6
                        else if($moduleHeaderStyle == 'module_header_6') {
                            $bk_dynamic_css .= '.module_header_6 #'.$module_id.' .module-title:before {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'} .module_header_6 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 7
                        else if($moduleHeaderStyle == 'module_header_7') {
                            $bk_dynamic_css .= '.module_header_7 #'.$module_id.' .module-title:before {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'} .module_header_7 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 8
                        else if($moduleHeaderStyle == 'module_header_8') {
                            $bk_dynamic_css .= '.module_header_8 #'.$module_id.' .module-title:before {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'} .module_header_8 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 9
                        else if($moduleHeaderStyle == 'module_header_9') {
                            $bk_dynamic_css .= '.module_header_9 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 10
                        else if($moduleHeaderStyle == 'module_header_10') {
                            $bk_dynamic_css .= '.module_header_10 #'.$module_id.' .module-title h2:before {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'} .module_header_10 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 11
                        else if($moduleHeaderStyle == 'module_header_11') {
                            $bk_dynamic_css .= '.module_header_11 #'.$module_id.' .module-title:before, .module_header_11 #'.$module_id.' .module-title h2 {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                            $bk_dynamic_css .= '.module_header_11 #'.$module_id.' .module-title h2:after {border-left-color: '.$rubik_dynamic_css[$module_id]['module_title_color'].';}';
                        }
                        //style 12
                        else if($moduleHeaderStyle == 'module_header_12') {
                            $bk_dynamic_css .= '.module_header_12 #'.$module_id.' .module-title:before {background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'} .module_header_12 #'.$module_id.' .module-title h2{color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 13
                        else if($moduleHeaderStyle == 'module_header_13') {
                            $bk_dynamic_css .= '.module_header_13 #'.$module_id.' .module-title h2:before {border-color: '.$rubik_dynamic_css[$module_id]['module_title_color'].' transparent transparent transparent} .module_header_13 #'.$module_id.' .module-title h2{background: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                        //style 14
                        else if($moduleHeaderStyle == 'module_header_14') {
                            $bk_dynamic_css .= '.module_header_14 #'.$module_id.' .module-title h2:before {border-color: '.$rubik_dynamic_css[$module_id]['module_title_color'].'}';
                        }
                    }
                }
            }
            
            $moduleTitleHeight = 40;
            if (isset($rubik_option['bk-module-title-height']) && ($rubik_option['bk-module-title-height'] != '') && ($rubik_option['bk-module-title-height'] != 0)) {
                $moduleTitleHeight = (int)$rubik_option['bk-module-title-height'];
            }else {
                $moduleTitleHeight = 40;
            }
            
            $bk_dynamic_css .= '.module-title h2, .module-title .bk-tabs, .bk-tabs-more {line-height: '.$moduleTitleHeight.'px !important;}';
            
            $footerTitleHeight = 40;
            if (isset($rubik_option['bk-footer-widget-title-height']) && ($rubik_option['bk-footer-widget-title-height'] != '') && ($rubik_option['bk-footer-widget-title-height'] != 0)) {
                $footerTitleHeight = (int)$rubik_option['bk-footer-widget-title-height'];
            }else {
                $footerTitleHeight = 40;
            }
            $bk_dynamic_css .= '.footer .widget-title h3 {line-height: '.$footerTitleHeight.'px !important;}';
            
            $widgetTitleHeight = 40;
            if (isset($rubik_option['bk-widget-title-height']) && ($rubik_option['bk-widget-title-height'] != '') && ($rubik_option['bk-widget-title-height'] != 0)) {
                $widgetTitleHeight = (int)$rubik_option['bk-widget-title-height'];
            }else {
                $widgetTitleHeight = 40;
            }
            
            $bk_dynamic_css .= '.sidebar-wrap .widget-title h3 {line-height: '.$widgetTitleHeight.'px !important;}';
            
            $bk_dynamic_css .= '.bkmodule .post-c-wrap .title, h4.title {opacity: 1;}';
            if((is_category()) || is_archive()) {
                global $wp_query;
                $bk_cur_id = $wp_query->get_queried_object_id();
                $cat_opt = get_option('bk_cat_opt');
                $bk_dynamic_css .= self::bk_print_dynamic_css_category_archive($cat_opt, $bk_cur_id);
            }
            $bk_dynamic_css .= '</style>';
            ?>
            <script type="text/javascript">
                var MyJSStringVar = '<?php Print($bk_dynamic_css); ?>';
                jQuery(document).ready(function($){
                    jQuery('head').append(MyJSStringVar);
                });
            </script>
            <?php
        }
        static function bk_print_dynamic_css_category_archive($cat_opt, $bk_cur_id) {
            $bk_dynamic_css = '';
            if (isset($cat_opt[$bk_cur_id]) && is_array($cat_opt[$bk_cur_id])) {
                if(isset($cat_opt[$bk_cur_id]['font_size'])) {
                    $bk_dynamic_css .= '.bk-cat-arc-page-'.$bk_cur_id.' .bk-cat-arc-content .post-c-wrap h4.title {font-size: '.$cat_opt[$bk_cur_id]['font_size'].'px;}';
                }
                if(isset($cat_opt[$bk_cur_id]['letter_spacing'])) {
                    $bk_dynamic_css .= '.bk-cat-arc-page-'.$bk_cur_id.' .bk-cat-arc-content .post-c-wrap h4.title {letter-spacing: '.$cat_opt[$bk_cur_id]['letter_spacing'].'px;}';
                }
            }
            return $bk_dynamic_css;
        }
        static function bk_canvas_button() {
            $canvasRender = '';
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $buttonStyle = $rubik_option['bk-canvas-button-style'];
            
            $canvasRender .= '<a class="canvas-nav-btn nav-open-btn '.$buttonStyle.'">';
            $canvasRender .= '<span></span><span></span><span></span>';
            $canvasRender .= '</a>';
            
            return $canvasRender;
        }
        static function bk_check_has_post_thumbnail($postID){
            $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( $postID ) );
            if  ( ! empty( $featured_image_url ) ) {
                return true;
            }else {
                return false;
            }
        }
        static function bk_get_load_more_buttom() {
            $html = '';
            $html .= '<span class="ajaxtext ajax-load-btn">'.esc_html__("Load More",'rubik').'</span>';
            $html .= '<span class="loading-animation"></span>';
            return $html;
        }
    /*-----------------------------------------------------------------------------------*/
    # Get Ticker
    /*-----------------------------------------------------------------------------------*/		
        static function bk_get_ticker() {
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $ticker_cat = array();
            
            if (isset($rubik_option)){
                $title = $rubik_option['bk-ticker-title'];
                if ( $title == '' ) { $title = 'Breaking News';}
                
                $ticker_num = $rubik_option['bk-ticker-number'];
                if ( $ticker_num == '' ) { $ticker_num = 5;}                
                
                $featured_enable = $rubik_option['bk-ticker-featured'];
                if ($featured_enable == 1) {
                    $args = array(
            			'post__in'  => get_option( 'sticky_posts' ),
            			'post_status' => 'publish',
            			'ignore_sticky_posts' => 1,
            			'posts_per_page' => $ticker_num,
                    );
                }else {
                    if(isset($rubik_option['ticker-category'])) {
                        $ticker_cat = $rubik_option['ticker-category'];
                    }else {
                        $ticker_cat = '';
                    }
                        if (sizeof($ticker_cat)) {
                        $args = array(
                            'category__in'  => $ticker_cat,
                            'post_status' => 'publish',
                            'ignore_sticky_posts' => 1,
                            'posts_per_page' => $ticker_num,
                        );
                    }
                }
    
                $ticker_query = new WP_Query( $args );
                $uid = uniqid('ticker-');
                
                rubik_section_parent::$rubik_ajax_c['tickerid'] = $uid;
                rubik_section_parent::$rubik_ajax_c['tickertitle'] = $title;
                ?>
                <div class="bkwrapper container">
                    <div class="bk-ticker-inner">
                        <ul id="<?php echo esc_attr($uid);?>" class="bk-ticker-wrapper">
                            <?php while ( $ticker_query->have_posts() ): $ticker_query->the_post(); ?>
                                <li class="news-item">
                                    <div class="bk-article-wrapper">
                                        <?php echo self::bk_get_post_title(get_the_ID(), '');?>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>                        
            <?php
            wp_reset_postdata();
            }
        }
    /*-----------------------------------------------------------------------------------*/
    # Get Header Index
    /*-----------------------------------------------------------------------------------*/		
        static function bk_show_header_index() {
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            if($rubik_option['bk-header-index-display'] == 'front-page') {
                if(is_front_page()) {
                    return true;
                }else {
                    return false;
                }
            }else {
                return true;
            }
        }
        static function bk_get_header_index() {
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $hI_cat = array();
            
            if (isset($rubik_option)){              
                
                $featured_enable = $rubik_option['bk-header-index-featured'];
                if ($featured_enable == 1) {
                    $args = array(
            			'post__in'  => get_option( 'sticky_posts' ),
            			'post_status' => 'publish',
            			'ignore_sticky_posts' => 1,
            			'posts_per_page' => 5,
                    );
                }else {
                    if(isset($rubik_option['header-index-category'])) {
                        $hI_cat = $rubik_option['header-index-category'];
                    }else {
                        $hI_cat = '';
                    }
                        if (sizeof($hI_cat)) {
                        $args = array(
                            'category__in'  => $hI_cat,
                            'post_status' => 'publish',
                            'ignore_sticky_posts' => 1,
                            'posts_per_page' => 5,
                        );
                    }
                }
    
                $the_query = new WP_Query( $args );
                
                $bk_contentout8 = new bk_contentout8;
                $custom_var = array (
                    'thumbnail'     => 'rubik-225-110',
                    'post-icon'     => 'hide',
                    'meta'          => 'off',
                    'cat'           => 'off',
                );
                ?>
                <div class="bkwrapper container">
                    <ul class="bk-header-index-wrap liststyle-none clearfix">
                        <?php while ( $the_query->have_posts() ): $the_query->the_post(); ?>
                            <li class="content_out bk-post-title-small clearfix">
                                <?php echo rubik_core::bk_render_html_string($bk_contentout8->render($custom_var));?>
                            </li><!-- End post -->
                        <?php endwhile; ?>
                    </ul>
                </div>                        
            <?php
            wp_reset_postdata();
            }
        }
        static function rubik_top_bar(){
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            ?>
            <div class="top-bar">
                <div class="bkwrapper container">
                    <div class="top-nav clearfix">
                        <?php if($rubik_option['bk-top-date-position'] == 'left') {?>
                        <div class="rubik_data_time rubik-float-left">
                            <?php
                                echo date_i18n(stripslashes('l, F j')); 
                            ?>
                        </div>
                        <?php }?>
                        <?php if ( has_nav_menu('menu-top') ) {?> 
                            <?php wp_nav_menu(array('theme_location' => 'menu-top','container_id' => 'top-menu'));?> 
                        <?php }?>
                        
                        <?php get_template_part( 'library/templates/headers/rubik_header_social' );?>
                        
                        <?php if($rubik_option['bk-top-date-position'] == 'right') {?>
                        <div class="rubik_data_time rubik-float-right">
                            <?php
                                echo date_i18n(stripslashes('l, F j')); 
                            ?>
                        </div>
                        <?php }?>
                    </div><!--top-nav-->
                </div>
                </div><!--top-bar-->
            <?php
        }
        static function bk_get_megamenu_posts($bk_qry_latest, $bk_qry_amount){
            $bk_posts = '';         
            $icon_html = '';   
            $post_icon = 'video-only';
            $i = 1;  
                   
            foreach ( $bk_qry_latest->posts as $bk_post ) {
                
                setup_postdata( $bk_post ); 
                    
                $bk_post_id = $bk_post->ID;
                
                $icon_html = '';
                $review_checkbox = get_post_meta($bk_post_id, 'bk_review_checkbox', true );
                
                if(rubik_core::bk_check_has_post_thumbnail( $bk_post_id )) {
                    $bk_img = get_the_post_thumbnail($bk_post_id, 'rubik-360-240');
                    if(($post_icon != 'hide') || ($review_checkbox == 1)) {
                        $icon_html .= '<div class="rubik-post-icon-wrap '.$bk_post_id.'">';
                        if ($post_icon == 'show') {
                            $icon_html .= rubik_core::bk_get_post_icon($bk_post_id);
                        }else if ($post_icon == 'video-only') {
                            $icon_html .= rubik_core::bk_get_post_icon_video_only($bk_post_id);
                        }
                        if($review_checkbox == 1) {
                            $bk_final_score = get_post_meta($bk_post_id, 'bk_final_score', true );
                            if($bk_final_score != 0) {
                                $icon_html .= rubik_core::bk_get_review_score($bk_post_id, $bk_final_score);
                            }
                        }
                        $icon_html .= '</div>';
                    }
                }else {
                    $bk_img = '';
                    $icon_html = '';
                }

                $bk_permalink = get_permalink($bk_post_id);
                $bk_post_title = rubik_core::the_excerpt_limit_by_word($bk_post->post_title,12);
                $bk_review_score =  rubik_review_score($bk_post_id);
                $date = get_the_date( '', $bk_post_id );
                $thepost= get_post($bk_post_id);
                
                if ($bk_img == '<div class="icon-thumb"></div>') {$bk_review_score = '';};
                if ($bk_qry_amount == 3) { 
                    $bk_posts .= ' <li class="col-md-4 bk-sub-post">
                                    <div class="thumb">
                                        <a href="'.$bk_permalink.'" class="thumb-link">'. $bk_img.'</a>
                                        '.$icon_html.'
                                    </div>
                                            
                                    <h4 class="post-title"><a href="'.$bk_permalink.'">'.$bk_post_title.'</a></h4>  
                                    <div class="meta clearfix">
                                        <div class="post-date">'.$date.'</div>
                                    </div>          
                                   </li>'; 
                }else if($bk_qry_amount == 4) {
                    $bk_posts .= ' <li class="col-md-3 bk-sub-post">
                                    <div class="thumb">
                                        <a href="'.$bk_permalink.'" class="thumb-link">'. $bk_img.'</a>
                                        '.$icon_html.'
                                    </div>
                                            
                                    <h4 class="post-title"><a href="'.$bk_permalink.'">'.$bk_post_title.'</a></h4>  
                                    <div class="meta clearfix">
                                        <div class="post-date">'.$date.'</div>
                                    </div>          
                                   </li>'; 
                }
                    
                $i++;
            }
            return $bk_posts;
        }
        
        static function bk_getPostViews($postID){
            $count_key = 'post_views_count';
            $count = get_post_meta($postID, $count_key, true);
            if($count==''){
                delete_post_meta($postID, $count_key);
                add_post_meta($postID, $count_key, '0');
                return "0";
           }
           return $count;
        }
        static function bk_setPostViews($postID){
            $count_key = 'post_views_count';
            $count = get_post_meta($postID, $count_key, true);
            if($count==''){
                $count = 0;
                delete_post_meta($postID, $count_key);
                add_post_meta($postID, $count_key, '0');
            }else{
                $count++;
                update_post_meta($postID, $count_key, $count);
            }
            return false;   
        }
        static function bk_module_ajax_pagination(){
            
            $bk_ajax_pagination = '';
            $bk_ajax_pagination .= '<div class="bk-module-pagination">';
            $bk_ajax_pagination .= '<a href="#" class="pagination-btn prev disable-click" title="'.esc_attr__('Previous', 'rubik').'" rel="prev"><i class="fa fa-angle-left" aria-hidden="true"></i>'.esc_html__('Prev', 'rubik').'</a>';
            $bk_ajax_pagination .= '<a href="#" class="pagination-btn next" title="'.esc_attr__('Next', 'rubik').'" rel="next">'.esc_html__('Next', 'rubik').'<i class="fa fa-angle-right" aria-hidden="true"></i></a>';
            $bk_ajax_pagination .= '</div>';
           
            return $bk_ajax_pagination;
        } 
        static function bk_widget_ajax_pagination(){
            
            $bk_ajax_pagination = '';
            $bk_ajax_pagination .= '<div class="bk-widget-pagination">';
            $bk_ajax_pagination .= '<a href="#" class="widget-pagination-btn prev disable-click" title="'.esc_attr__('Previous', 'rubik').'" rel="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>';
            $bk_ajax_pagination .= '<a href="#" class="widget-pagination-btn next" title="'.esc_attr__('Next', 'rubik').'" rel="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>';
            $bk_ajax_pagination .= '</div>';
           
            return $bk_ajax_pagination;
        }   
        static function bk_module_related_post_pagination(){
            
            $bk_ajax_pagination = '';
            $bk_ajax_pagination .= '<div class="bk-related-pagination">';
            $bk_ajax_pagination .= '<a href="#" class="related-pagination-btn prev disable-click" title="'.esc_attr__('Previous', 'rubik').'" rel="prev"><i class="fa fa-angle-left" aria-hidden="true"></i>'.esc_html__('Prev', 'rubik').'</a>';
            $bk_ajax_pagination .= '<a href="#" class="related-pagination-btn next" title="'.esc_attr__('Next', 'rubik').'" rel="next">'.esc_html__('Next', 'rubik').'<i class="fa fa-angle-right" aria-hidden="true"></i></a>';
            $bk_ajax_pagination .= '</div>';
           
            return $bk_ajax_pagination;
        }      
        static function bk_megamenu_ajax_pagination($bk_cat_id = ''){
            
            $bk_ajax_pagination = '';
            $bk_ajax_pagination .= '<div class="bk-megamenu-pagination">';
            $bk_ajax_pagination .= '<a href="#" class="megamenu-pagination-btn prev disable-click" title="'.esc_attr__('Previous', 'rubik').'" rel="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>';
            $bk_ajax_pagination .= '<a href="#" class="megamenu-pagination-btn next" title="'.esc_attr__('Next', 'rubik').'" rel="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>';
            $bk_ajax_pagination .= '<a href="'.get_category_link( $bk_cat_id ).'" class="bk-megamenu-category-link">'.esc_html__('View All', 'rubik').'</a>';
            $bk_ajax_pagination .= '</div>';
           
            return $bk_ajax_pagination;
        }          
        static function bk_get_block_title($page_info, $tabs = null){  
            $block_title = '';
            $i = 0;
            $title = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_title', true ); 
                        
            if(strlen($title)!=0) {
                $block_title .= '<div class="module-title hide">';
                //$block_title .= '<div class="module-title">';
                $block_title .= '<div class="main-title clearfix">';
                if(strlen($title)!=0) {
                    $block_title .= '<h2 class="bk-tab-original active"><a href="#">'.$title.'</a></h2>';
                }
                if($tabs != null) {
                    $tab_count = count($tabs);
                }else {
                    $tab_count = 0;
                }
                if(($tabs != null) && ($tab_count > 1)) {
                    $block_title .= '<div class="bk-tabs-wrapper bk-hide-tabs">';
                    $block_title .= '<div class="bk-module-tabs">';
                    for($i=0; $i<$tab_count; $i++) {
                        if($tabs[$i] != -1) {
                            $block_title .= '<div class="bk-tab-'.$i.' bk-tabs"><a class="term-'.$tabs[$i].'" href="#">'.get_cat_name($tabs[$i]).'</a></div>';
                        }
                    }
                    $block_title .= '</div>';
                    $block_title .= '</div><!-- Close module tabs -->';                
                }
                $block_title .= '</div>';
    			$block_title .= '</div>';
            }
            return $block_title;
        }
        static function get_feature_image($thumbSize, $clickable = '', $post_icon = ''){
            $feat_img = '';
            $postID = get_the_ID();
            $category = get_the_category($postID); 
            
            if(rubik_core::bk_check_has_post_thumbnail( $postID )) {            
                $feat_img .= '<div class="thumb hide-thumb term-'.$category[0]->term_id.'">';
                if($clickable == true) {
                    $feat_img .= '<a href="'.get_permalink($postID).'">';
                }
                
                $feat_img .= get_the_post_thumbnail($postID, $thumbSize);
                
                $review_checkbox = get_post_meta($postID, 'bk_review_checkbox', true );
                if(($post_icon != 'hard-disable') && (($post_icon != 'hide') || ($review_checkbox == 1))) {
                    $feat_img .= '<div class="rubik-post-icon-wrap">';
                    if ($post_icon == 'show') {
                        $feat_img .= rubik_core::bk_get_post_icon($postID);
                    }else if ($post_icon == 'video-only') {
                        $feat_img .= rubik_core::bk_get_post_icon_video_only($postID);
                    }
                    if($review_checkbox == 1) {
                        $bk_final_score = get_post_meta($postID, 'bk_final_score', true );
                        if($bk_final_score != 0) {
                            $feat_img .= rubik_core::bk_get_review_score($postID, $bk_final_score);
                        }
                    }
                    $feat_img .= '</div>';
                }
                if($clickable == true) {
                    $feat_img .= '</a> <!-- close a tag -->';
                }
                $feat_img .= '</div> <!-- close thumb -->';
            }
            return $feat_img;
        }
     
        static function bk_get_post_icon ($bkPostID) {
            $str_ret = '';
            $postformat = get_post_format( $bkPostID );
            switch($postformat) {
                case "video":
                    $str_ret = '<div class="post-icon-item"><i class="fa fa-play post-icon video-icon"></i></div>';
                    break;
                case "audio":
                    $str_ret = '<div class="post-icon-item"><i class="fa fa-volume-up post-icon audio-icon"></i></div>';
                    break;
                case "gallery":
                    $str_ret = '<div class="post-icon-item"><i class="fa fa-picture-o post-icon gallery-icon"></i></div>';
                    break;
                default:
                    $str_ret = '';
                    break;
            }
            return $str_ret;
        }
        static function bk_get_post_icon_video_only ($bkPostID) {
            $str_ret = '';
            $postformat = get_post_format( $bkPostID );
            switch($postformat) {
                case "video":
                    $str_ret = '<div class="post-icon-item"><i class="fa fa-play post-icon video-icon"></i></div>';
                    break;
                default:
                    $str_ret = '';
                    break;
            }
            return $str_ret;
        }
        static function bk_meta_cases( $meta_type ) {
            $bk_meta = $meta_type;
            $bkpostID = get_the_ID();            
            $bk_meta_str = '';
            switch ($bk_meta) {
                case "cat":
                    $bk_meta_str .= '<div class="post-category">';
                    $bk_meta_str .= rubik_get_category_link(get_the_ID());
                    $bk_meta_str .= '</div>';
                    break;
                case "date":
                    $bk_meta_str .= '<div class="post-date">';
                    $bk_meta_str .=  get_the_date();
                    $bk_meta_str .= '</div>';
                    break;
                case "author":
                    $bk_meta_str .=  '<div class="post-author">';
                    
                    if(is_single($bkpostID)) {
                        //function coauthors_posts_links( $between = null, $betweenLast = null, $before = null, $after = null, $echo = true )
                        if(function_exists('coauthors_posts_links')){
                              $bk_meta_str .= coauthors_posts_links(', ', esc_html__(' and ', 'rubik'), ' ', ' ', false);
                        }
                        else {
                              $bk_meta_str .= '<a href="'. get_author_posts_url(get_the_author_meta( 'ID' )).'">'. get_the_author() .'</a>';
                        }
                    }else {
                        $bk_meta_str .= '<a href="'. get_author_posts_url(get_the_author_meta( 'ID' )).'">'. get_the_author() .'</a>';
                    }
                             
                    $bk_meta_str .=  '</div>';
                    break;
                case "bg":
                    $thumb130 = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'rubik-90-65');
                    $bk_meta_str .=  '<div class="meta-bg" style="background-image:url('.$thumb130['0'].');background-size:cover;background-position:50% 50%;background-repeat:no-repeat;"></div>';
                    break;
                case "postview":
                    $bk_meta_str .=  '<div class="views"><i class="fa fa-eye"></i>'.self::bk_getPostViews($bkpostID).'</div>';
                    break;
                case "postcomment":
                    $bk_meta_str .=  '<div class="comments"><i class="fa fa-comment-o"></i>'.get_comments_number($bkpostID).'</div>';
                    break;
                default:
                    echo "No Case Matched!";
            }
            return $bk_meta_str;
        }
    
        static function bk_get_post_meta( $meta_arg ) {
            $bk_meta = '';
            $bk_meta .= '<div class="meta">';
            if ((isset($meta_arg[0])) && ($meta_arg[0] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[0]);
            }
            if ((isset($meta_arg[1])) && ($meta_arg[1] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[1]);
            }
            if ((isset($meta_arg[2])) && ($meta_arg[2] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[2]);
            }
            if ((isset($meta_arg[3])) && ($meta_arg[3] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[3]);
            }
            if ((isset($meta_arg[4])) && ($meta_arg[4] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[4]);
            }
            if ((isset($meta_arg[5])) && ($meta_arg[5] != null)) {
                $bk_meta .= self::bk_meta_cases($meta_arg[5]);
            }
            $bk_meta .= '</div>';
            return $bk_meta;
        }
        
        static function the_excerpt_limit_by_word($string, $word_limit){
            $words = explode(' ', $string, ($word_limit + 1));
            if(count($words) > $word_limit)
            array_pop($words);
            $strout = implode(' ', $words);
            if (strlen($strout) < strlen($string))
                $strout .=" ...";
            return $strout;
        }
        
        static function bk_get_post_title ($bkPostId, $length = NULL){
            $bk_title = '';
            $bk_title .= '<h4 class="title">';
            $bk_title .= self::bk_get_title_link($bkPostId, $length);
            $bk_title .= '</h4>';
            return $bk_title;
        }
        
        static function bk_get_title_link( $bkPostId, $length ) {
            $titleLink = '';
            $category = get_the_category($bkPostId); 
                if(!empty($category)) :
                $titleLink .= '<a class="term-'.$category[0]->term_id.'" href="'.get_permalink($bkPostId).'">';
                if($length != null) {
                    $titleLink .= self::the_excerpt_limit_by_word(get_the_title($bkPostId),$length);
                }else {
                    $titleLink .= get_the_title($bkPostId);
                }
                $titleLink .= '</a>';
            endif;
            return $titleLink;
        }
        
        static function bk_get_post_excerpt($length) {
            $bk_excerpt = '';
            $the_excerpt = get_the_excerpt();
            $bk_excerpt .= '<div class="excerpt">';
            $bk_excerpt .= self::the_excerpt_limit_by_word($the_excerpt, $length); 
            $bk_excerpt .= '</div>';
            return $bk_excerpt;
        }
        static function bk_readmore_btn($bkPostId, $icon = '') {
            $readmore = '';
            $readmore .= '<div class="readmore">';
            if($icon != null) {
                $readmore .= $icon;
            }
            $readmore .= '<a href="'.get_permalink($bkPostId).'">'.__("Read More", 'rubik').'</a>';
            $readmore .= '</div>';
            return $readmore;
        }
        
        static function bk_get_review_score($bkPostId, $bk_final_score) {           
            $bk_review = '';
            $reviewCatID = get_the_category($bkPostId); 
            if (isset($bk_final_score) && ($bk_final_score != null)){
                $arc_percent = $bk_final_score * 10;
                $bk_review .= '<div class="rating-wrap term-'.$reviewCatID[0]->term_id.'"><div class="rating-canvas-bg"></div><canvas class="rating-canvas" data-rating="'.$arc_percent.'"></canvas><span>'.$bk_final_score.'</span></div>';
            }
            return $bk_review;
        }
        static function bk_count_social( $user, $social_type ) {
    		//check options
    		if ( empty( $user ) ) {
    			return false;
    		}
    
    		$params = array(
    			'timeout'   => 120,
    			'sslverify' => false
    		);
            if($social_type == 'twitter') {
        		$filter   = array(
        			'start_1' => 'ProfileNav-item--followers',
        			'start_2' => 'title',
        			'end'     => '>'
        		);
        		$response = wp_remote_get( 'https://twitter.com/' . $user, $params );
            }else if($social_type == 'dribbble') {
                $filter   = array(
        			'start_1' => 'full-tabs-links',
        			'start_2' => 'envato/followers',
        			'end'     => '</a>'
        		);
        		$response = wp_remote_get( 'https://dribbble.com/' . $user, $params );   
            }else if($social_type == 'facebook') {
                $filter   = array(
        			'start_1' => 'PagesLikesCountDOMID',
        			'start_2' => 'span',
                    'start_3' => '>',
                    'end'     => 'span',
        		);
        		$response = wp_remote_get( 'https://facebook.com/' . $user, $params );
            }
    		//check & return
    		if ( is_wp_error( $response ) || empty( $response['response']['code'] ) || '200' != $response['response']['code'] ) {
    			return false;
    		}
    		//get content
    		$response = wp_remote_retrieve_body( $response );
    
    		if ( ! empty( $response ) && $response !== false ) {
    			foreach ( $filter as $key => $value ) {
    
    				$key = explode( '_', $key );
    				$key = $key[0];
    
    				if ( $key == 'start' ) {
    					$key = false;
    				} else if ( $value == 'end' ) {
    					$key = true;
    				}
    				$key = (bool) $key;
    
    				$index = strpos( $response, $value );
    				if ( $index === false ) {
    					return false;
    				}
    				if ( $key ) {
    					$response = substr( $response, 0, $index );
    				} else {
    					$response = substr( $response, $index + strlen( $value ) );
    				}
    			}
    
    			if ( strlen( $response ) > 100 ) {
    				return false;
    			}
    
    			$count = self::bk_extract_one_number( $response );
    
    			if ( ! is_numeric( $count ) || strlen( number_format( $count ) ) > 15 ) {
    				return false;
    			}
    
    			$count = intval( $count );
    
    			return $count;
    		} else {
    			return false;
    		}
    
    	}
        
        static function bk_widget_latest_layout($the_query, $widgetLayout){
            if($widgetLayout == 1) {
                echo '<ul class="list post-list bk-widget-content">';
				echo self::bk_widget_latest_post_1_render($the_query);
                echo '</ul>';
            }else if($widgetLayout == 2) {
                echo '<ul class="list post-list bk-widget-content">';
                echo rubik_core::bk_widget_latest_post_2_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 3) {
                echo '<ul class="row list post-list bk-widget-content">';
                echo rubik_core::bk_widget_latest_post_3_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 4) {
                echo '<ul class="row list post-list bk-widget-content">';
                echo rubik_core::bk_widget_latest_post_4_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 5) {
                echo '<ul class="row list post-list bk-widget-content">';
                echo rubik_core::bk_widget_latest_post_5_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 6) {
                echo '<ul class="list post-list bk-widget-content">';
                echo rubik_core::bk_widget_latest_post_6_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 7) {
                echo '<ul class="row list post-list bk-widget-content">';
                echo rubik_core::bk_widget_latest_post_7_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 8) {
                echo '<ul class="row list post-list bk-widget-content">';
                echo rubik_core::bk_widget_latest_post_8_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 9) {
                echo '<ul class="row list post-list bk-widget-content">';
                echo rubik_core::bk_widget_latest_post_9_render($the_query);
    			echo '</ul>';
            }else {
                echo '<ul class="list post-list bk-widget-content">';
				echo self::bk_widget_latest_post_1_render($the_query);
                echo '</ul>';
            }
            return '';
        }
        static function bk_widget_latest_post_1_render($the_query){
            
            $bk_contentout3 = new bk_contentout3;
            
            $custom_var_out3 = array (
                'thumbnail'    => 'rubik-90-65',
                'meta'         => array('date'),
            );
            
            $render_modules = '';
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="small-post content_out clearfix">';
            $render_modules .= $bk_contentout3->render($custom_var_out3);
            $render_modules .= '</li><!-- End post -->';      
           
            endwhile;
            
            return $render_modules;
        } 
        
        static function bk_widget_latest_post_2_render($the_query){
            
            $bk_contentout2 = new bk_contentout2;
            
            $bk_contentout3 = new bk_contentout3;
            
            $custom_var_out2 = array (
                'thumbnail'    => 'rubik-360-240',
                'post-icon'     => 'video-only',
                'except_length' => 12,
            );
            
            $custom_var_out3 = array (
                'thumbnail'    => 'rubik-90-65',
                'meta'         => array('date'),
            );
            
            $render_modules = '';
            
            if($the_query -> have_posts()) : 
                $the_query->the_post();
                $render_modules .= '<li class="large-post row-type content_out ">';
                $render_modules .= $bk_contentout2->render($custom_var_out2);               
                $render_modules .=  '</li>';
            endif;
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="small-post content_out clearfix">';
            $render_modules .= $bk_contentout3->render($custom_var_out3);
            $render_modules .= '</li><!-- End post -->';      
           
            endwhile;
            
            return $render_modules;
        } 
        static function bk_widget_latest_post_3_render($the_query){
            
            $bk_contentout8 = new bk_contentout8;
            
            $custom_var_out8 = array (
                'thumbnail'     => 'rubik-150-105',
                'post-icon'     => 'video-only',
                'meta'          => array('date'),
            );
            
            $render_modules = '';
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="col-xs-6 row-type content_out ">';
            $render_modules .= $bk_contentout8->render($custom_var_out8);         
            $render_modules .=  '</li>';      
           
            endwhile;
            
            
            
            return $render_modules;
        } 
        
        static function bk_widget_latest_post_4_render($the_query){
            
            $bk_contentin3 = new bk_contentin3;
            
            $custom_var_in3 = array (
                'cat'           => 'on',
                'thumbnail'    => 'rubik-360-240',
                'post-icon'     => 'video-only',
                'meta'          => array('author', 'date'),
            );
            
            $custom_var_out3 = array (
                'thumbnail'    => 'rubik-90-65',
                'meta'         => array('date'),
            );
            
            $bk_contentout3 = new bk_contentout3;
            
            $render_modules = '';
            
            if($the_query -> have_posts()) : 
                $the_query->the_post();
                $render_modules .= '<li class="item large-post square-grid-type content_in col-md-12">';
                $render_modules .= '<div class="content_in_wrapper">';
                $render_modules .= $bk_contentin3->render($custom_var_in3);
                $render_modules .= '</div></li><!-- end post item -->';
            endif;
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="col-md-12 small-post content_out clearfix">';
            $render_modules .= $bk_contentout3->render($custom_var_out3);
            $render_modules .= '</li><!-- End post -->';      
           
            endwhile;
            
            return $render_modules;
        } 
        static function bk_widget_latest_post_5_render($the_query){
            
            $bk_contentin3 = new bk_contentin3;
            
            $bk_contentout8 = new bk_contentout8;
            
            $custom_var_in3 = array (
                'cat'           => 'on',
                'thumbnail'    => 'rubik-360-240',
                'post-icon'     => 'video-only',
                'meta'          => array('author', 'date'),
            );
            
            $custom_var_out8 = array (
                'thumbnail'     => 'rubik-150-105',
                'post-icon'     => 'video-only',
                'meta'          => array('date'),
            );
            
            $render_modules = '';
            
            if($the_query -> have_posts()) : 
                $the_query->the_post();
                $render_modules .= '<li class="item large-post square-grid-type content_in col-md-12">';
                $render_modules .= '<div class="content_in_wrapper">';
                $render_modules .= $bk_contentin3->render($custom_var_in3);
                $render_modules .= '</div></li><!-- end post item -->';
            endif;
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="col-xs-6 row-type content_out ">';
            $render_modules .= $bk_contentout8->render($custom_var_out8);         
            $render_modules .=  '</li>';     
           
            endwhile;
            
            return $render_modules;
        } 
        
        static function bk_widget_latest_post_6_render($the_query){
            
            $bk_contentout3 = new bk_contentout3;
            
            $custom_var_out3 = array (
                'thumbnail'    => 'off',
                'cat'          => 'on',
                'meta'         => array('date'),
            );
            
            $render_modules = '';
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="small-post content_out clearfix">';
            $render_modules .= $bk_contentout3->render($custom_var_out3);
            $render_modules .= '</li><!-- End post -->';      
           
            endwhile;
            
            return $render_modules;
        } 
        
        static function bk_widget_latest_post_7_render($the_query){
            
            $bk_contentout2 = new bk_contentout2;
            
            $bk_contentout8 = new bk_contentout8;
            
            $custom_var_out2 = array (
                'thumbnail'    => 'rubik-360-240',
                'post-icon'     => 'video-only',
                'except_length' => 12,
            );
            
            $custom_var_out8 = array (
                'thumbnail'     => 'rubik-150-105',
                'post-icon'     => 'video-only',
                'meta'          => array('date'),
            );
            
            $render_modules = '';
            
            if($the_query -> have_posts()) : 
                $the_query->the_post();
                $render_modules .= '<li class="large-post row-type content_out col-md-12">';
                $render_modules .= $bk_contentout2->render($custom_var_out2);               
                $render_modules .=  '</li>';
            endif;
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="col-xs-6 row-type content_out ">';
            $render_modules .= $bk_contentout8->render($custom_var_out8);         
            $render_modules .=  '</li>';     
           
            endwhile;
            
            return $render_modules;
        } 
        static function bk_widget_latest_post_8_render($the_query){
            
            $bk_contentout2 = new bk_contentout2;
            
            $custom_var_out2 = array (
                'thumbnail'    => 'rubik-360-180',
                'post-icon'     => 'video-only',
                'except_length' => '',
                'meta_items'    => 'off',
            );
            
            $render_modules = '';
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="large-post row-type content_out col-md-12">';
            $render_modules .= $bk_contentout2->render($custom_var_out2);               
            $render_modules .=  '</li>';   
           
            endwhile;
            
            return $render_modules;
        } 
        static function bk_widget_latest_post_9_render($the_query){
            
            $bk_contentin3 = new bk_contentin3;
            
            $custom_var_in3 = array (
                'cat'           => 'on',
                'thumbnail'    => 'rubik-360-240',
                'post-icon'     => 'video-only',
                'except_length' => '',
                'meta'          => array('author', 'date'),
            );
            
            $render_modules = '';
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="item large-post square-grid-type content_in col-md-12">';
            $render_modules .= '<div class="content_in_wrapper">';
            $render_modules .= $bk_contentin3->render($custom_var_in3);
            $render_modules .= '</div></li><!-- end post item -->';
           
            endwhile;
            
            return $render_modules;
        } 
        
        static function bk_widget_index_layout($the_query, $widgetLayout){
            if($widgetLayout == 1) {
                echo '<ul class="list post-list bk-widget-content">';
				echo self::bk_widget_index_post_1_render($the_query);
                echo '</ul>';
            }else if($widgetLayout == 2) {
                echo '<ul class="list post-list bk-widget-content">';
                echo rubik_core::bk_widget_index_post_2_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 4) {
                echo '<ul class="row list post-list bk-widget-content">';
                echo rubik_core::bk_widget_index_post_4_render($the_query);
    			echo '</ul>';
            }else if($widgetLayout == 6) {
                echo '<ul class="list post-list bk-widget-content">';
                echo rubik_core::bk_widget_index_post_6_render($the_query);
    			echo '</ul>';
            }else {
                echo '<ul class="list post-list bk-widget-content">';
				echo self::bk_widget_index_post_1_render($the_query);
                echo '</ul>';
            }
            return '';
        }
        static function bk_widget_index_post_1_render($the_query){
            
            $bk_contentout3 = new bk_contentout3;
            
            $custom_var_out3 = array (
                'thumbnail'    => 'off',
                'meta'         => array('cat', 'date'),
            );
            
            $render_modules = '';
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="small-post content_out">';
            $render_modules .= $bk_contentout3->render($custom_var_out3);
            $render_modules .= '</li><!-- End post -->';      
           
            endwhile;
            
            return $render_modules;
        } 
        static function bk_widget_index_post_2_render($the_query){
            
            $bk_contentout2 = new bk_contentout2;
            
            $bk_contentout3 = new bk_contentout3;
            
            $custom_var_out2 = array (
                'thumbnail'    => 'rubik-360-240',
                'post-icon'     => 'video-only',
                'except_length' => 12,
            );
            
            $custom_var_out3 = array (
                'thumbnail'    => 'off',
                'meta'         => array('cat', 'date'),
            );
            
            $render_modules = '';
            
            if($the_query -> have_posts()) : 
                $the_query->the_post();
                $render_modules .= '<li class="large-post row-type content_out ">';
                $render_modules .= $bk_contentout2->render($custom_var_out2);               
                $render_modules .=  '</li>';
            endif;
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="small-post content_out">';
            $render_modules .= $bk_contentout3->render($custom_var_out3);
            $render_modules .= '</li><!-- End post -->';      
           
            endwhile;
            
            return $render_modules;
        } 
        static function bk_widget_index_post_4_render($the_query){
            
            $bk_contentin3 = new bk_contentin3;
            
            $custom_var_in3 = array (
                'cat'   => 'on',
                'meta'  => array('author', 'date'),
                'thumbnail' => 'rubik-360-240',
                'post-icon'     => 'video-only',
            );
            $custom_var_out3 = array (
                'thumbnail'    => 'off',
                'meta'         => array('cat', 'date'),
            );
            
            $bk_contentout3 = new bk_contentout3;
            
            $render_modules = '';
            
            if($the_query -> have_posts()) : 
                $the_query->the_post();
                $render_modules .= '<li class="item large-post square-grid-type content_in col-md-12">';
                $render_modules .= '<div class="content_in_wrapper">';
                $render_modules .= $bk_contentin3->render($custom_var_in3);
                $render_modules .= '</div></li><!-- end post item -->';
            endif;
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="col-md-12 small-post content_out">';
            $render_modules .= $bk_contentout3->render($custom_var_out3);
            $render_modules .= '</li><!-- End post -->';      
           
            endwhile;
            
            return $render_modules;
        } 
        static function bk_widget_index_post_6_render($the_query){
            
            $bk_contentout3 = new bk_contentout3;
            
            $custom_var_out3 = array (
                'thumbnail'    => 'off',
                'cat'          => 'on',
                'meta'         => array('date'),
            );
            
            $render_modules = '';
            
            while ( $the_query -> have_posts() ) : $the_query -> the_post();
            
            $render_modules .= '<li class="small-post content_out">';
            $render_modules .= $bk_contentout3->render($custom_var_out3);
            $render_modules .= '</li><!-- End post -->';      
           
            endwhile;
            
            return $render_modules;
        } 
    //Single Function
        
    /**
    * ************* Author box *****************
    *---------------------------------------------------
    */  
        static function bk_author_details($bk_author_id, $bk_desc = true) {
            
            $bk_author_email = get_the_author_meta('publicemail', $bk_author_id);
            $bk_author_name = get_the_author_meta('display_name', $bk_author_id);
            $bk_author_tw = get_the_author_meta('twitter', $bk_author_id);
            $bk_author_go = get_the_author_meta('googleplus', $bk_author_id);
            $bk_author_fb = get_the_author_meta('facebook', $bk_author_id);
            $bk_author_yo = get_the_author_meta('youtube', $bk_author_id);
            $bk_author_www = get_the_author_meta('url', $bk_author_id);
            $bk_author_desc = get_the_author_meta('description', $bk_author_id);
            $bk_author_posts = count_user_posts( $bk_author_id ); 
        
            $bk_author = NULL;
            $bk_author .= '<div class="bk-author-box clearfix"><div class="bk-author-avatar"><a href="'.get_author_posts_url($bk_author_id).'">'. get_avatar($bk_author_id, '75').'</a></div><div class="author-info" itemprop="author"><div class="author-title"><a href="'.get_author_posts_url($bk_author_id).'">'.$bk_author_name.'</a></div>';
                                
            if (($bk_author_desc != NULL) && ($bk_desc == true)) { $bk_author .= '<p class="bk-author-bio">'. $bk_author_desc .'</p>'; }                    
            if (($bk_author_email != NULL) || ($bk_author_www != NULL) || ($bk_author_tw != NULL) || ($bk_author_go != NULL) || ($bk_author_fb != NULL) ||($bk_author_yo != NULL)) {$bk_author .= '<div class="bk-author-page-contact">';}
            if ($bk_author_email != NULL) { $bk_author .= '<a class="bk-tipper-bottom" data-title="'.esc_attr__('Email', 'rubik').'" href="mailto:'. $bk_author_email.'"><i class="fa fa-envelope " title="'.esc_attr__('Email', 'rubik').'"></i></a>'; } 
            if ($bk_author_www != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="'.esc_attr__('Website', 'rubik').'" href="'. $bk_author_www .'" target="_blank"><i class="fa fa-globe " title="'.esc_attr__('Website', 'rubik').'"></i></a> '; } 
            if ($bk_author_tw != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="'.esc_attr__('Twitter', 'rubik').'" href="//www.twitter.com/'. $bk_author_tw.'" target="_blank" ><i class="fa fa-twitter " title="Twitter"></i></a>'; } 
            if ($bk_author_go != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="'.esc_attr__('Google Plus', 'rubik').'" href="'. $bk_author_go .'" rel="publisher" target="_blank"><i title="Google+" class="fa fa-google-plus " ></i></a>'; }
            if ($bk_author_fb != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="'.esc_attr__('Facebook', 'rubik').'" href="'.$bk_author_fb. '" target="_blank" ><i class="fa fa-facebook " title="Facebook"></i></a>'; }
            if ($bk_author_yo != NULL) { $bk_author .= ' <a class="bk-tipper-bottom" data-title="'.esc_attr__('Youtube', 'rubik').'" href="//www.youtube.com/user/'.$bk_author_yo. '" target="_blank" ><i class="fa fa-youtube " title="Youtube"></i></a>'; }
            if (($bk_author_email != NULL) || ($bk_author_www != NULL) || ($bk_author_go != NULL) || ($bk_author_tw != NULL) || ($bk_author_fb != NULL) ||($bk_author_yo != NULL)) {$bk_author .= '</div>';}                          
            
            $bk_author .= '</div></div><!-- close author-infor-->';
                 
            return $bk_author;
        }
    
        static function bk_get_thumbnail($bkPostId, $thumb_type) {
            $bk_thumb_output = '';
            if(rubik_core::bk_check_has_post_thumbnail( $bkPostId )) {
            $bk_thumb_output .= '<div class="thumb hide-thumb">';
            $bk_thumb_output .= '<a href="'.get_the_permalink().'">';
            
            $bk_thumb_output .=  get_the_post_thumbnail($bkPostId, $thumb_type);
            
            $bk_thumb_output .= '</a>';
            $bk_thumb_output .= '</div>';
            }
            return $bk_thumb_output;
        }
    /**
     *  BK Get Feature Image
     */
        static function bk_get_single_interface($bkPostId, $bk_post_layout) {
            $postFormat = rubik_core::bk_post_format_detect($bkPostId);
            $bkReviewSW = get_post_meta($bkPostId, 'bk_review_checkbox',true);
            if ( $bkReviewSW == '1' ) { $itemprop =  'itemprop="itemReviewed"'; } else { $itemprop = 'itemprop="headline"'; }
            $bk_feat_img = '';
            $meta_ar = array('cat', 'date', 'author');
            $bk_post_header = '';
            $bk_post_header .= '<div class="s_header_wraper"><div class="s-post-header container"><h1 '.$itemprop.'>'.get_the_title().'</h1>';
            $bk_post_header .= self::bk_get_post_meta($meta_ar);
            $bk_post_header .= '</div></div><!-- end single header -->';
            if (($bk_post_layout === 'bk-parallax-feat') && ($postFormat['format'] != 'gallery')) {
                $bk_feat_img .= '<header id="bk-parallax-feat" class="clearfix">'; 
            }else if (($bk_post_layout === 'bk-fw-feat') && ($postFormat['format'] != 'gallery')) {
                $bk_feat_img .= '<header id="bk-fw-feat" class="clearfix">'; 
            }else {
                if((rubik_core::bk_check_has_post_thumbnail($bkPostId)) || ($postFormat['format'] == 'video')) {
                    $bk_feat_img .= '<div id="bk-normal-feat" class="clearfix">'; 
                }else {
                    $bk_feat_img = null;
                }
            }
            if($postFormat['format'] != 'gallery') {
                if ($bk_feat_img != null) {
                    $bk_feat_img .= self::bk_post_format_display($bkPostId, $bk_post_layout);
                    if (($bk_post_layout === 'bk-parallax-feat') || ($bk_post_layout === 'bk-fw-feat')) {
                        $bk_feat_img .= $bk_post_header;
                        $bk_feat_img .= '</div>';
                    }else {
                        $bk_feat_img .= '</div>';
                        $bk_feat_img .= $bk_post_header;
                    }
                }else {
                    $bk_feat_img .= $bk_post_header;
                }
            }else {
                $bk_feat_img .= self::bk_post_format_display($bkPostId, $bk_post_layout);
                $bk_feat_img .= '</div>';
                $bk_feat_img .= $bk_post_header;
            }
            
            return $bk_feat_img;
        }
        
        static function bk_extract_one_number( $str ) {
        	return intval( preg_replace( '/[^0-9]+/', '', $str ), 10 );
        }
        static function bk_count_dribble( $user ) {
    		//check options
    		if ( empty( $user ) ) {
    			return false;
    		}
    
    		$params = array(
    			'timeout'   => 120,
    			'sslverify' => false
    		);
    
    		$filter   = array(
    			'start_1' => 'full-tabs-links',
    			'start_2' => 'envato/followers',
    			'end'     => '</a>'
    		);
    		$response = wp_remote_get( 'https://dribbble.com/' . $user, $params );
    		//check & return
    		if ( is_wp_error( $response ) || empty( $response['response']['code'] ) || '200' != $response['response']['code'] ) {
    			return false;
    		}
    		//get content
    		$response = wp_remote_retrieve_body( $response );
    
    		if ( ! empty( $response ) && $response !== false ) {
    			foreach ( $filter as $key => $value ) {
    
    				$key = explode( '_', $key );
    				$key = $key[0];
    
    				if ( $key == 'start' ) {
    					$key = false;
    				} else if ( $value == 'end' ) {
    					$key = true;
    				}
    				$key = (bool) $key;
    
    				$index = strpos( $response, $value );

    				if ( $index === false ) {
    					return false;
    				}
    				if ( $key ) {
    					$response = substr( $response, 0, $index );
    				} else {
    					$response = substr( $response, $index + strlen( $value ) );
    				}
    			}
    
    			if ( strlen( $response ) > 100 ) {
    				return false;
    			}
    			$count = self::bk_extract_one_number( $response );
    
    			if ( ! is_numeric( $count ) || strlen( number_format( $count ) ) > 15 ) {
    				return false;
    			}
    
    			$count = intval( $count );
    
    			return $count;
    		} else {
    			return false;
    		}
    
    	}
        /**
    * ************* Get youtube ID  *****************
    *---------------------------------------------------
    */ 
      
        static function parse_youtube($link){
         
            $regexstr = '~
                # Match Youtube link and embed code
                (?:                             # Group to match embed codes
                    (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
                    |(?:                        # Group to match if older embed
                        (?:<object .*>)?      # Match opening Object tag
                        (?:<param .*</param>)*  # Match all param tags
                        (?:<embed [^>]*src=")?  # Match embed tag to the first quote of src
                    )?                          # End older embed code group
                )?                              # End embed code groups
                (?:                             # Group youtube url
                    https?:\/\/                 # Either http or https
                    (?:[\w]+\.)*                # Optional subdomains
                    (?:                         # Group host alternatives.
                    youtu\.be/                  # Either youtu.be,
                    | youtube\.com              # or youtube.com
                    | youtube-nocookie\.com     # or youtube-nocookie.com
                    )                           # End Host Group
                    (?:\S*[^\w\-\s])?           # Extra stuff up to VIDEO_ID
                    ([\w\-]{11})                # $1: VIDEO_ID is numeric
                    [^\s]*                      # Not a space
                )                               # End group
                "?                              # Match end quote if part of src
                (?:[^>]*>)?                       # Match any extra stuff up to close brace
                (?:                             # Group to match last embed code
                    </iframe>                 # Match the end of the iframe
                    |</embed></object>          # or Match the end of the older embed
                )?                              # End Group of last bit of embed code
                ~ix';
        
            preg_match($regexstr, $link, $matches);
        
            return $matches[1];
        
        }
        
    /**
     * ************* Get vimeo ID *****************
     *---------------------------------------------------
     */  
        
        static function parse_vimeo($link){
         
            $regexstr = '~
                # Match Vimeo link and embed code
                (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
                (?:                         # Group vimeo url
                    https?:\/\/             # Either http or https
                    (?:[\w]+\.)*            # Optional subdomains
                    vimeo\.com              # Match vimeo.com
                    (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
                    \/                      # Slash before Id
                    ([0-9]+)                # $1: VIDEO_ID is numeric
                    [^\s]*                  # Not a space
                )                           # End group
                "?                          # Match end quote if part of src
                (?:[^>]*></iframe>)?        # Match the end of the iframe
                (?:<p>.*</p>)?              # Match any title information stuff
                ~ix';
        
            preg_match($regexstr, $link, $matches);
        
            return $matches[1];
        }
    /**
     * ************* Get Dailymotion ID *****************
     *---------------------------------------------------
     */  
        static function parse_dailymotion($link){
            preg_match('#<object[^>]+>.+?http://www.dailymotion.com/swf/video/([A-Za-z0-9]+).+?</object>#s', $link, $matches);
        
                // Dailymotion url
                if(!isset($matches[1])) {
                    preg_match('#http://www.dailymotion.com/video/([A-Za-z0-9]+)#s', $link, $matches);
                }
        
                // Dailymotion iframe
                if(!isset($matches[1])) {
                    preg_match('#http://www.dailymotion.com/embed/video/([A-Za-z0-9]+)#s', $link, $matches);
                }
            return $matches[1];
        }
        static function bk_module_pagenav_title($page_ID){
            $bk_buff = '';
            $heading_text = get_post_meta($page_ID,'bk_pagenav_heading',true);
            
            if($heading_text == '') {
                return '';
            }
            
            $bk_buff .= '<div class="module-title">';
            $bk_buff .= '<div class="main-title clearfix">';

            $bk_buff .= '<h2>'.$heading_text.'</h2>';
            
            $bk_buff .= '</div></div>';
            
            return $bk_buff;
        }
        static function bk_module_pagenav_classic_blog($page_ID) {
            $bk_contentout = new bk_contentout5;
            $bk_post_icon = get_post_meta($page_ID,'bk_pagenav_post_icon',true);
            $pagenav_excerpt = get_post_meta($page_ID,'bk_pagenav_excerpt',true);
            
            if($pagenav_excerpt == '') {
                $pagenav_excerpt = 15;
            }
            $custom_var = array (
                'thumbnail'     => 'rubik-310-280',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('author', 'date'),
                'except_length' => $pagenav_excerpt,
                'rm_btn'        => true,
            );
            $bk_no_thumb = '';
            $bk_buff = '';
            
            $bk_buff .= '<div class="bkmodule module-classic-blog module-blog">';
            
            $bk_buff .= '<div class="bk-module-inner row clearfix">';
            $bk_buff .= '<ul class="bk-blog-content clearfix">';
                while (have_posts()): the_post();
                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                        $bk_no_thumb = 'bk_no_thumb';
                    }else {
                        $bk_no_thumb = '';
                    }
                    $bk_buff .= '<li class="item col-md-12">';
                    $bk_buff .= '<div class="content_out clearfix '.$bk_no_thumb.'">';
                    $bk_buff .= $bk_contentout->render($custom_var);
                    $bk_buff .= '</div></li><!-- end post item -->';
                endwhile;
            $bk_buff .= '</ul>';

            $bk_buff .= '</div> <!-- Close Module Inner -->';
            
            $bk_buff .= '</div>';
            
            wp_reset_postdata();
            
            return $bk_buff;
        }
        static function bk_render_html_string($str = '') {
            return $str;
        }
        static function bk_render_google_adsense_code() {
            $rubik_option = self::bk_get_global_var('rubik_option'); 
            if((isset($rubik_option['bk-banner-script'])) && ($rubik_option['bk-banner-script'] != '')) :
                return $rubik_option['bk-banner-script'];
            else:
                return '';
            endif;
        }
        static function bk_module_pagenav_large_blog($page_ID) {
            $bk_contentout = new bk_contentout2;
            $bk_post_icon = get_post_meta($page_ID,'bk_pagenav_post_icon',true);
            $pagenav_excerpt = get_post_meta($page_ID,'bk_pagenav_excerpt',true);
            
            if($pagenav_excerpt == '') {
                $pagenav_excerpt = 25;
            }
            $custom_var = array (
                'thumbnail'     => 'rubik-730-400',
                'post-icon'     => $bk_post_icon,
                'except_length' => $pagenav_excerpt,
            );
            $bk_no_thumb = '';
            $bk_buff = '';
            
            $bk_buff .= '<div class="bkmodule module-large-blog module-blog">';
            
            $bk_buff .= '<div class="bk-module-inner row clearfix">';
            $bk_buff .= '<ul class="bk-blog-content clearfix">';
            
            while (have_posts()): the_post();
                $bk_buff .= '<li class="item col-md-12 row-type">';
                $bk_buff .= '<div class="content_out clearfix">';
                $bk_buff .= $bk_contentout->render($custom_var);
                $bk_buff .= '</div></li><!-- end post item -->';
            endwhile;
                
            $bk_buff .= '</ul></div>';
            
            $bk_buff .= '</div>';
            
            wp_reset_postdata();
            
            return $bk_buff;
        }
        static function bk_module_pagenav_large_blog_2($page_ID) {
            $bk_contentout = new bk_contentout10;
            $bk_post_icon = get_post_meta($page_ID,'bk_pagenav_post_icon',true);
            $pagenav_excerpt = get_post_meta($page_ID,'bk_pagenav_excerpt',true);
            
            if($pagenav_excerpt == '') {
                $pagenav_excerpt = 25;
            }
            $custom_var = array (
                'thumbnail'     => 'rubik-730-400',
                'cat'           => 'on',
                'post-icon'     => $bk_post_icon,
                'meta'          => array('author', 'date'),
                'except_length' => $pagenav_excerpt,
            );
            $bk_no_thumb = '';
            $bk_buff = '';
            
            $bk_buff .= '<div class="bkmodule module-large-blog layout-2 module-blog">';
            
            $bk_buff .= '<div class="bk-module-inner row clearfix">';
            $bk_buff .= '<ul class="bk-blog-content clearfix">';
            
            while (have_posts()): the_post();
                $bk_buff .= '<li class="item col-md-12 row-type">';
                $bk_buff .= '<div class="content_out clearfix">';
                $bk_buff .= $bk_contentout->render($custom_var);
                $bk_buff .= '</div></li><!-- end post item -->';
            endwhile;
                
            $bk_buff .= '</ul></div>';
            
            $bk_buff .= '</div>';
            
            wp_reset_postdata();
            
            return $bk_buff;
        }
        static function bk_module_pagenav_masonry($page_ID) {
            $bk_contentout = new bk_contentout2;
            $bk_post_icon = get_post_meta($page_ID,'bk_pagenav_post_icon',true);
            $pagenav_excerpt = get_post_meta($page_ID,'bk_pagenav_excerpt',true);
            
            if($pagenav_excerpt == '') {
                $pagenav_excerpt = 15;
            }
            
            $custom_var = array (
                'thumbnail'     => 'rubik-masonry-size',
                'post-icon'     => $bk_post_icon,
                'except_length' => $pagenav_excerpt,
            );
            $col_width = 'col-md-6 col-sm-6';
            $bk_buff = '';
            
            $bk_buff .= '<div class="bkmodule module-masonry clearfix">';
            
            $bk_buff .= '<div class="bk-masonry-wrap">';
            $bk_buff .= '<div class="row clearfix">';
            $bk_buff .= '<ul class="bk-masonry-content bk-module-inner clearfix">';
            while (have_posts()): the_post();
                $bk_buff .= '<li class="'.$col_width.' item row-type content_out">';
                $bk_buff .= $bk_contentout->render($custom_var);
                $bk_buff .= '</li><!-- end post item -->';
            endwhile;
            $bk_buff .= '</ul></div></div>';
            $bk_buff .= '</div>';
            
            wp_reset_postdata();
            
            return $bk_buff;
        }
        static function bk_module_pagenav_row($page_ID) {
            $bk_contentout = new bk_contentout2;
            $bk_post_icon = get_post_meta($page_ID,'bk_pagenav_post_icon',true);
            $pagenav_excerpt = get_post_meta($page_ID,'bk_pagenav_excerpt',true);
            
            if($pagenav_excerpt == '') {
                $pagenav_excerpt = 15;
            }
            
            $custom_var = array (
                'thumbnail'     => 'rubik-360-180',
                'post-icon'     => $bk_post_icon,
                'except_length' => $pagenav_excerpt,
            );
            $bk_buff = '';
            
            $bk_buff .= '<div class="bkmodule module-row wsb-module layout-1 clearfix">';
            
            $bk_buff .= '<ul class="bk-row-wrap bk-module-inner row clearfix">';
            while (have_posts()): the_post();
                $bk_buff .= '<li class="row-type content_out col-md-6 col-sm-6">';
                $bk_buff .= $bk_contentout->render($custom_var);
                $bk_buff .= '</li><!-- end post item -->';
            endwhile;
            $bk_buff .= '</ul>';
            $bk_buff .= '</div>';
            
            wp_reset_postdata();
            
            return $bk_buff;
        }
        static function bk_module_pagenav_square_grid($page_ID) {
            $bk_contentin3 = new bk_contentin3;
            $col_width = 'col-md-6 col-sm-6';
            $custom_var['cat'] = 'on';
            $custom_var['meta'] = array('author', 'date');
            $custom_var['thumbnail'] = 'rubik-620-420';
            $bk_buff = '';
            
            $bk_buff .= '<div class="bkmodule square-grid-2 module-square-grid">';
            
            $bk_buff .= '<div class="bk-square-grid-wrap">';
            $bk_buff .= '<div class="row clearfix">';
            $bk_buff .= '<ul class="bk-square-grid-content bk-module-inner clearfix">';
            while (have_posts()): the_post();
                $bk_buff .= '<li class="item content_in '.$col_width.'">';
                $bk_buff .= '<div class="content_in_wrapper">';
                $bk_buff .= $bk_contentin3->render($custom_var);
                $bk_buff .= '</div></li><!-- end post item -->';
            endwhile;
            $bk_buff .= '</ul></div></div>';
            $bk_buff .= '</div>';
            
            wp_reset_postdata();
            
            return $bk_buff;
        }
        
        static function bk_module_pagenav_layout($page_ID, $pagenav_layout) {
            $bk_buff = '';
            global $paged;
            
            $pagenav_large_1st = get_post_meta($page_ID,'bk_pagenav_large_1st',true);
            
            if (have_posts()):
                $bk_buff .= self::bk_module_pagenav_title($page_ID);
            endif;
            
            if(($pagenav_large_1st == 'enable') && (!empty($paged) && ($paged == 1)) && ($pagenav_layout != 'large_blog_2')) {
                if (have_posts()): the_post();
                    $bk_contentout = new bk_contentout2;
                    $custom_var = array (
                        'thumbnail'     => 'rubik-600-315',
                        'post-icon'     => '',
                        'meta'          => array('author', 'date'),
                        'except_length' => 35,
                    );
                    $bk_buff .= '<div class="module-large-blog module-blog rubik-pagenav-first-large">';
                    $bk_buff .= '<div class="content_out clearfix">';
                    $bk_buff .= $bk_contentout->render($custom_var);
                    $bk_buff .= '</div><!-- end post item -->';
                    $bk_buff .= '</div>';
                endif;
            }
            switch ($pagenav_layout) {
                case 'classic_blog':
                    $bk_buff .= self::bk_module_pagenav_classic_blog($page_ID);
                    break;
                case 'large_blog':
                    $bk_buff .= self::bk_module_pagenav_large_blog($page_ID);
                    break;
                case 'large_blog_2':
                    $bk_buff .= self::bk_module_pagenav_large_blog_2($page_ID);
                    break;
                case 'masonry':
                    $bk_buff .= self::bk_module_pagenav_masonry($page_ID);
                    break;
                case 'row':
                    $bk_buff .= self::bk_module_pagenav_row($page_ID);
                    break;
                case 'square_grid':
                    $bk_buff .= self::bk_module_pagenav_square_grid($page_ID);
                    break;
                default:
                    $bk_buff .= self::bk_module_pagenav_classic_blog($page_ID);
            }
            return $bk_buff;
            
        }
        
        //the default options
        static function pagenavi_init() {
            $pagenavi_options = array();
            $pagenavi_options['pages_text'] = esc_html__('Page %CURRENT_PAGE% of %TOTAL_PAGES%', 'rubik');
            $pagenavi_options['current_text'] = '%PAGE_NUMBER%';
            $pagenavi_options['page_text'] = '%PAGE_NUMBER%';
            $pagenavi_options['first_text'] = esc_html__('1', 'rubik');
            $pagenavi_options['last_text'] = esc_html__('%TOTAL_PAGES%', 'rubik');

            $pagenavi_options['next_text'] = '<i class="fa fa-long-arrow-right"></i>';
            $pagenavi_options['prev_text'] = '<i class="fa fa-long-arrow-left"></i>';

            $pagenavi_options['dotright_text'] = esc_html__('...', 'rubik');
            $pagenavi_options['dotleft_text'] = esc_html__('...', 'rubik');
    
            $pagenavi_options['num_pages'] = 3;
    
            $pagenavi_options['always_show'] = 0;
            $pagenavi_options['num_larger_page_numbers'] = 3;
            $pagenavi_options['larger_page_numbers_multiple'] = 1000;
    
            return $pagenavi_options;
        }
        
        static function rubik_round_number($num, $tonearest) {
            return floor($num/$tonearest)*$tonearest;
        }
        
        static function rubik_get_pagination() {
    
            if (is_singular()) {
                return; //no pagination on single pages
            }
    
            global $wpdb, $wp_query;
            $pagenavi_options = self::pagenavi_init();
        
            $pagination_ret = '';
            $request = $wp_query->request;
            $posts_per_page = intval(get_query_var('posts_per_page'));
            $paged = intval(get_query_var('paged'));
            $numposts = $wp_query->found_posts;
            $max_page = $wp_query->max_num_pages;

            if(empty($paged) || $paged == 0) {
                $paged = 1;
            }
            $pages_to_show = intval($pagenavi_options['num_pages']);
            $larger_page_to_show = intval($pagenavi_options['num_larger_page_numbers']);
            $larger_page_multiple = intval($pagenavi_options['larger_page_numbers_multiple']);
            $pages_to_show_minus_1 = $pages_to_show - 1;
            $half_page_start = floor($pages_to_show_minus_1/2);
            $half_page_end = ceil($pages_to_show_minus_1/2);
            $start_page = $paged - $half_page_start;
            if($start_page <= 0) {
                $start_page = 1;
            }
            $end_page = $paged + $half_page_end;
            if(($end_page - $start_page) != $pages_to_show_minus_1) {
                $end_page = $start_page + $pages_to_show_minus_1;
            }
            if($end_page > $max_page) {
                $start_page = $max_page - $pages_to_show_minus_1;
                $end_page = $max_page;
            }
            if($start_page <= 0) {
                $start_page = 1;
            }
            $larger_per_page = $larger_page_to_show*$larger_page_multiple;
            $larger_start_page_start = (self::rubik_round_number($start_page, 10) + $larger_page_multiple) - $larger_per_page;
            $larger_start_page_end = self::rubik_round_number($start_page, 10) + $larger_page_multiple;
            $larger_end_page_start = self::rubik_round_number($end_page, 10) + $larger_page_multiple;
            $larger_end_page_end = self::rubik_round_number($end_page, 10) + ($larger_per_page);
            if($larger_start_page_end - $larger_page_multiple == $start_page) {
                $larger_start_page_start = $larger_start_page_start - $larger_page_multiple;
                $larger_start_page_end = $larger_start_page_end - $larger_page_multiple;
            }
            if($larger_start_page_start <= 0) {
                $larger_start_page_start = $larger_page_multiple;
            }
            if($larger_start_page_end > $max_page) {
                $larger_start_page_end = $max_page;
            }
            if($larger_end_page_end > $max_page) {
                $larger_end_page_end = $max_page;
            }
    
            if($max_page > 1 || intval($pagenavi_options['always_show']) == 1) {
                $pages_text = str_replace("%CURRENT_PAGE%", number_format_i18n($paged), $pagenavi_options['pages_text']);
                $pages_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pages_text);
    
                $pagination_ret .= '<div class="page-nav">';
    			
    			$pagination_ret .= get_previous_posts_link($pagenavi_options['prev_text']);
                if ($start_page >= 2 && $pages_to_show < $max_page) {
                    $first_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['first_text']);
                    $pagination_ret .= '<a href="'.esc_url(get_pagenum_link()).'" class="first" title="'.$first_page_text.'">'.$first_page_text.'</a>';
                    if(!empty($pagenavi_options['dotleft_text'])) {
                        $pagination_ret .= '<span class="extend">'.$pagenavi_options['dotleft_text'].'</span>';
                    }
                }
                if($larger_page_to_show > 0 && $larger_start_page_start > 0 && $larger_start_page_end <= $max_page) {
                    for($i = $larger_start_page_start; $i < $larger_start_page_end; $i+=$larger_page_multiple) {
                        $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
                        $pagination_ret .= '<a href="'.esc_url(get_pagenum_link($i)).'" class="page" title="'.$page_text.'">'.$page_text.'</a>';
                    }
                }
                
                for($i = $start_page; $i  <= $end_page; $i++) {
                    if($i == $paged) {
                        $current_page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['current_text']);
                        $pagination_ret .= '<span class="current">'.$current_page_text.'</span>';
                    } else {
                        $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
                        $pagination_ret .= '<a href="'.esc_url(get_pagenum_link($i)).'" class="page" title="'.$page_text.'">'.$page_text.'</a>';
                    }
                }
                
                if($larger_page_to_show > 0 && $larger_end_page_start < $max_page) {
                    for($i = $larger_end_page_start; $i <= $larger_end_page_end; $i+=$larger_page_multiple) {
                        $page_text = str_replace("%PAGE_NUMBER%", number_format_i18n($i), $pagenavi_options['page_text']);
                        $pagination_ret .= '<a href="'.esc_url(get_pagenum_link($i)).'" class="page" title="'.$page_text.'">'.$page_text.'</a>';
                    }
                }
                if ($end_page < $max_page) {
                    if(!empty($pagenavi_options['dotright_text'])) {
                        $pagination_ret .= '<span class="extend">'.$pagenavi_options['dotright_text'].'</span>';
                    }
                    $last_page_text = str_replace("%TOTAL_PAGES%", number_format_i18n($max_page), $pagenavi_options['last_text']);
                    $pagination_ret .= '<a href="'.esc_url(get_pagenum_link($max_page)).'" class="last" title="'.$last_page_text.'">'.$last_page_text.'</a>';
                }
    			$pagination_ret .= get_next_posts_link($pagenavi_options['next_text'], $max_page);
    			if(!empty($pages_text)) {
                    $pagination_ret .= '<span class="pages">'.$pages_text.'</span>';
                }
    
                $pagination_ret .= '</div>';
    
            }
            
            return $pagination_ret;
        
        }
        static function rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid) {
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            
            $moduleTitleColor       = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_title_color', true ); 
            $moduleTabColor         = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_tab_color', true ); 
            $moduleTitleTextColor   = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_title_text_color', true ); 
            $moduleBackground       = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_background', true ); 
            $titleFontSize          = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_font_size', true );
            $titeFontWeight         = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_font_weight', true ); 
            $fontsizeSmallPost      = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_font_size_small_post', true ); 
            $fontweightSmallPost    = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_font_weight_small_post', true ); 
            $titleLetterSpacing     = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_letter_spacing', true );
            $moduleMargin           = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_margin', true );
                        
            if($moduleTitleTextColor == '') {
                $moduleTitleTextColor = $rubik_option['bk-module-title-text-color-default'];
            }
            if($moduleTitleColor == '') {
                $moduleTitleColor = $rubik_option['bk-module-title-color-default'];
            }
            if($moduleTabColor == '') {
                $moduleTabColor = $rubik_option['bk-module-tab-color-default'];
            }
            
            if ($moduleTitleColor != '') { 
                $rubik_dynamic_css[$uid]['module_title_color'] = $moduleTitleColor;
            }else {
                $rubik_dynamic_css[$uid]['module_title_color'] = '';
            }  
            if ($moduleTitleTextColor != '') { 
                $rubik_dynamic_css[$uid]['module_title_text_color'] = $moduleTitleTextColor;
            }else {
                $rubik_dynamic_css[$uid]['module_title_text_color'] = '';
            }
            if ($titleFontSize != '') { 
                $rubik_dynamic_css[$uid]['title_font_size'] = $titleFontSize;
            }else {
                $rubik_dynamic_css[$uid]['title_font_size'] = '';
            }
            if ($titeFontWeight != '') { 
                $rubik_dynamic_css[$uid]['title_font_weight'] = $titeFontWeight;
            }else {
                $rubik_dynamic_css[$uid]['title_font_weight'] = '';
            }
            if ($fontsizeSmallPost != '') { 
                $rubik_dynamic_css[$uid]['font_size_small_post'] = $fontsizeSmallPost;
            }else {
                $rubik_dynamic_css[$uid]['font_size_small_post'] = '';
            }
            if ($fontweightSmallPost != '') { 
                $rubik_dynamic_css[$uid]['font_weight_small_post'] = $fontweightSmallPost;
            }else {
                $rubik_dynamic_css[$uid]['font_weight_small_post'] = '';
            }
            if ($titleLetterSpacing != '') { 
                $rubik_dynamic_css[$uid]['title_letter_spacing'] = $titleLetterSpacing;
            }else {
                $rubik_dynamic_css[$uid]['title_letter_spacing'] = '';
            }
            if ($moduleMargin != '') { 
                $rubik_dynamic_css[$uid]['module_margin'] = $moduleMargin;
            }else {
                $rubik_dynamic_css[$uid]['module_margin'] = '';
            }
            if ($moduleTabColor != '') { 
                $rubik_dynamic_css[$uid]['module_tab_color'] = $moduleTabColor;
            }else {
                $rubik_dynamic_css[$uid]['module_tab_color'] = '';
            }                     
            if ($moduleBackground != '') { 
                $rubik_dynamic_css[$uid]['module_background'] = $moduleBackground;
                $rubik_dynamic_css[$uid]['moduleHasbg']       = 'moduleHasbg';
            }else {
                $rubik_dynamic_css[$uid]['moduleHasbg']       = '';
            }
            return $rubik_dynamic_css;
        }
        /**
         * ************* Get Images Instagram *****************
         *---------------------------------------------------
         */
        static function rubik_footer_instagram(){
            $rubik_option = rubik_core::bk_get_global_var('rubik_option');
            $photos_arr = array();
            
            $instagram_title = $rubik_option['bk-footer-instagram-title'];
    		$access_token = $rubik_option['bk-footer-instagram-access-token'];
            $amount = $rubik_option['bk-footer-instagram-image-count'];
        	
            $json_link = "https://api.instagram.com/v1/users/self/media/recent/?";
            $json_link .="access_token={$access_token}&count={$amount}";
            $json = file_get_contents($json_link);
            $obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);
            
            ?>
            <div class="rubik-instagram-footer">
                <h4 class="instagram-title">
                    <?php echo esc_attr($instagram_title);?>
                </h4>
                <div class="ri-grid ri-grid-size-2">
                	<ul>
                		<?php
                            foreach ($obj['data'] as $post){
                                $pic_link = $post['link'];
                                $pic_src=str_replace("http://", "https://", $post['images']['standard_resolution']['url']);
                                echo '<li>';
                                    echo "<a href='{$pic_link}' target='_blank'>";
                                      echo "<img src='{$pic_src}' alt='instagram-img'>";
                                    echo "</a>";
                                echo "</li>";
                            }
                        ?>
                	</ul>	
                </div>
            </div>
            <?php
        }
    } // Close rubik_core class
    
}
