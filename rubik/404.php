<?php
/**
 * The template for 404 page (Not Found).
 *
 */
?>
<?php 
    get_header();
    $rubik_option   = rubik_core::bk_get_global_var('rubik_option');
    $errorTitle     = $rubik_option['bk-404-title'];
    $errorDes       = $rubik_option['bk-404-description'];
    $errorSearch    = $rubik_option['bk-404-search'];
    $errorLatest    = $rubik_option['bk-404-latest'];
    if($errorLatest == 'enable') {
        $latestTitle    = $rubik_option['bk-404-latest-title'];
    }
?>
<div class="page-wrap bkwrapper container">
    <div class="row">
        <div class="bk-404-header">
            <section class="error-number">
                <h4><?php esc_html_e('404','rubik'); ?></h4>
            </section>
            <?php if($errorTitle != ''):?>              
            <h1 class="bk-error-title"><?php echo esc_attr($errorTitle); ?></h1>
            <?php endif;?>
        </div>
        
        <div id="bk-404-wrap">
            <?php if($errorDes != ''):?>       
        	<div class="entry-content">	
                <?php echo esc_attr($errorDes); ?>
        	</div>
            <?php endif;?>
            <?php if($errorSearch != 'disable'):?>       
        	<div class="search">
        	    <?php get_search_form(); ?>
        	</div>
        	<?php endif;?>
        </div> <!-- end #bk-404-wrap -->
        <?php if($errorLatest == 'enable') :?>
        <div class="error-404-page-posts-wrap bkmodule">
            <div class="page-title-wrapper">
        		<div class="module-title">
                    <h2 itemprop="name"><span><?php echo esc_attr($latestTitle);?></span></h2>
                </div>
            </div>
            <?php
                $args = array (
                    'post_type' => 'post',
        			'ignore_sticky_posts' => 1,
                    'post_status' => 'publish',
        			'posts_per_page' => 6,
                );
                $the_query = new WP_Query( $args );
                $bk_contentin3 = new bk_contentin3;
                $custom_var = array (
                    'cat'           => 'on',
                    'thumbnail'     => 'rubik-620-420',
                    'meta'          => array('author', 'date'),
                );
                
                echo '<div class="content-wrap square-grid-3 module-square-grid module_style1">';
                echo '<ul class="bk-blog-content row clearfix">';
                    while ( $the_query->have_posts() ): $the_query->the_post();
                        echo '<li class="content_in col-md-4 col-sm-6">';
                        echo '<div class="content_in_wrapper">';
                        echo rubik_core::bk_render_html_string($bk_contentin3->render($custom_var));
                        echo '</div>';
                        echo '</li>';
                    endwhile;
                echo '</ul>';
                echo '</div>';
            ?>
        </div>
        <?php endif;?>
    </div>
</div>
<?php get_footer(); ?>
