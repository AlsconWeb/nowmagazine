<?php
if ( ! function_exists( 'rubik_custom_css' ) ) {
    function rubik_custom_css() {
        $rubik_option = rubik_core::bk_get_global_var('rubik_option');
        $rubik_css_option = '';
        if ( isset($rubik_option)):
            $primary_color = isset($rubik_option['bk-primary-color']) ? $rubik_option['bk-primary-color'] : '#EA2323';
            $bg_switch = isset($rubik_option['bk-site-layout']) ? $rubik_option['bk-site-layout'] : 'wide';
            $cat_opt = get_option('bk_cat_opt');  
            $header_top = isset($rubik_option['bk-header-top-switch']) ? $rubik_option['bk-header-top-switch'] : 0;  
            $sb_responsive_sw = isset($rubik_option['bk-sb-responsive-sw']) ? $rubik_option['bk-sb-responsive-sw'] : 1;
            $main_nav_layout = isset($rubik_option['bk-main-nav-layout']) ? $rubik_option['bk-main-nav-layout'] : 'left';      
            $bkbreadcrumbs = isset($rubik_option['bk-breadcrumbs']) ? $rubik_option['bk-breadcrumbs'] : 'disable';     
            $bk_logo_margin_top = isset($rubik_option['bk-logo-margin-top']) ? $rubik_option['bk-logo-margin-top'] : '';     
            $bk_logo_margin_bottom = isset($rubik_option['bk-logo-margin-bottom']) ? $rubik_option['bk-logo-margin-bottom'] : '';     
            $bk_footer_padding_top = isset($rubik_option['bk-footer-padding-top']) ? $rubik_option['bk-footer-padding-top'] : '';     
            $bk_footer_padding_bottom = isset($rubik_option['bk-footer-padding-bottom']) ? $rubik_option['bk-footer-padding-bottom'] : '';    
            $sidebar_header_arrow = isset($rubik_option['bk-sidebar-header-arrow']) ? $rubik_option['bk-sidebar-header-arrow'] : '';
            $footer_header_arrow = isset($rubik_option['bk-footer-header-arrow']) ? $rubik_option['bk-footer-header-arrow'] : '';
            $bk_sidebar_header_border = isset($rubik_option['bk-sidebar-header-border-color']) ? $rubik_option['bk-sidebar-header-border-color'] : '';
            $bk_sidebar_header_border_background = isset($rubik_option['bk-sidebar-header-5-16-bg']) ? $rubik_option['bk-sidebar-header-5-16-bg'] : '';
            $bk_sidebar_footer_border_background = isset($rubik_option['bk-footer-widget-header-bg']) ? $rubik_option['bk-footer-widget-header-bg'] : '';
            $footer_force_link_hover_color = isset($rubik_option['bk-footer-a-hover-color']) ? $rubik_option['bk-footer-a-hover-color'] : '';
            
            $moduleHeaderStyle = 'module_header_1';
            if (isset($rubik_option['bk-module-header']) && ($rubik_option['bk-module-header'] != '')) {
                $moduleHeaderStyle = $rubik_option['bk-module-header'];
            }else {
                $moduleHeaderStyle = 'module_header_1';
            }
            
            if ( $header_top == 0 ) $rubik_css_option .= '.top-bar {display: none !important;}';
            if ( $bkbreadcrumbs == 'disable' ) $rubik_css_option .= '.bk-breadcrumbs-wrap{display: none;}';

        $rubik_css_option .= "::selection {color: #FFF; background: $primary_color;}";
        $rubik_css_option .= "::-webkit-selection {color: #FFF; background: $primary_color;}";
        if ( ($primary_color) != null) {
            $rubik_css_option .= ".content_out.small-post h4:hover,
            .bk-sub-posts .post-title a:hover,
            .bk-blog-content .meta-wrap .post-category, 
             .breadcrumbs .fa-home, .module-feature2 .meta-wrap .post-category, .module-tech-grid ul li .meta > div.post-category a, .module-pyramid-grid ul li .meta > div.post-category a,
             
             p > a, p > a:hover, .single-page .article-content a:hover, .single-page .article-content a:visited, .content_out.small-post .meta .post-category, .bk-sub-menu li > a:hover,
            #top-menu>ul>li > .sub-menu a:hover, .bk-dropdown-menu li > a:hover, .widget-twitter .fa-twitter,
            .widget-twitter ul.slides li .twitter-message a, .content_in .meta > div.post-category a,
            .row-type .meta-wrap .post-category, .article-content li a, .article-content p a,
            .s-post-header .meta > .post-category, .breadcrumbs .location,
            .s-post-nav .nav-title span, .error-number h4, .redirect-home, .module-breaking-carousel .flex-direction-nav .flex-next, .module-breaking-carousel:hover .flex-direction-nav .flex-prev,
            .bk-author-box .author-info .author-title:hover, .bk-author-box .author-info .bk-author-page-contact a:hover, .module-feature2 .meta .post-category, 
            .bk-blog-content .meta .post-category, .bk-forum-title:hover,
            .content_out .post-c-wrap .readmore a:hover, .module-blog ul li .readmore a:hover, .widget_latest_replies .details h4:hover,
            #pagination .page-numbers, .post-page-links a, .single-page .icon-play, #wp-calendar tbody td a, #wp-calendar tfoot #prev,
            .widget_comment .post-title:hover,
            .widget_latest_replies .details .comment-author, .widget_recent_topics .details .comment-author a,
             a.bk_u_login:hover, a.bk_u_logout:hover, .bk-back-login:hover, 
            .main-nav.bk-menu-light .bk-sub-menu li > a:hover, .main-nav.bk-menu-light .bk-sub-posts .post-title a:hover,
            .row-type h4:hover, .widget-social ul li a:hover, .module-large-blog .post-c-wrap h4:hover,
            .module-feature2 .content_out h4:hover, .module-classic-blog .bk-blog-content h4:hover, .content_out .meta > div.post-author a:hover,
            .s-post-header .meta > div.post-author a:hover, .s-post-header .post-category a, .module-title .bk-tabs.active a,
            .bk-tabs-dropdown:hover .bk-tabs-more, .header-5 .main-menu > ul > li.current-menu-item > a, .header-5 .main-menu > ul > li.current-menu-item > a:hover,
            .header-2 .main-menu > ul > li.current-menu-item > a, .header-2 .main-menu > ul > li.current-menu-item > a:hover,
            .nav-btn h3:hover, .title > a:hover, .module-grid-7 .grid7-col:first-child:hover .title,
            .widget a:hover, .ticker-content a:hover, .heading-related-posts h4:hover, .s-template-14 .heading-related-posts h4:hover,
            .s-template-16 .heading-related-posts h4:hover
            {color: $primary_color;}";
            
            $rubik_css_option .= "#top-menu>ul>li > .sub-menu, .bk-dropdown-menu, .s-post-nav .nav-title span,
            .s-post-nav .nav-title span, .bk-mega-menu, .bk-mega-column-menu, .search-loadding, #comment-submit:hover, .s-tags a:hover, input[type='submit']:hover,
            .ticker-controls li.jnt-prev:hover:before, .ticker-controls li.jnt-next:hover:after,
            #pagination .page-numbers, .post-page-links a, .post-page-links > span, .widget-twitter .flex-direction-nav li a:hover, .menu-location-title,
            .button:hover, .bk-lwa:hover > .bk-account-info, .bk-back-login:hover,
            .page-nav .current, .widget_tag_cloud .tagcloud a:hover
            {border-color: $primary_color;}";
            
            
            $rubik_css_option .= ".sidebar_header_24 .sidebar-wrap .widget-title
            {border-color: $bk_sidebar_header_border;}";
            
            $rubik_css_option .= ".sidebar_header_25 .sidebar-wrap .widget-title h3:after, .module_header_11 .module-title h2:after
            {border-left-color: {$bk_sidebar_header_border_background['background-color']};}";
            
            $rubik_css_option .= ".footer_header_15 .footer .widget-title h3:after
            {border-left-color: {$bk_sidebar_footer_border_background['background-color']};}";
            
            $rubik_css_option .= ".module_header_13 .module-title h2:before
            {border-color: $primary_color transparent transparent transparent;}";
            
            $rubik_css_option .= ".bk-preload-wrapper:after {border-top-color: $primary_color; border-bottom-color: $primary_color;}";
            
            
            $rubik_css_option .= ".s-tags a:hover, .s-post-nav .icon, #comment-submit:hover, .flex-direction-nav li a,
            .widget-twitter .flex-direction-nav li a:hover,.button:hover, .widget_most_commented .comments,
            .footer .widget-title h3, .post-c-wrap .readmore a:hover, .ticker-title span, .ticker-controls li.jnt-prev:hover:before, 
            .ticker-controls li.jnt-next:hover:after,
            .widget_tag_cloud .tagcloud a:hover, .footer .widget_tag_cloud .tagcloud a:hover, input[type='submit']:hover,
            
            .thumb, .module-fw-slider .flex-control-nav li a.flex-active, .module-breaking-carousel .content_out.small-post .meta:after,
            .footer .cm-flex .flex-control-paging li a.flex-active, .header-1 .main-nav .menu > li.current-menu-item, 
            .header-3 .main-nav .menu > li.current-menu-item,
            .module-feature2 .flex-direction-nav li a, 
            .bk-review-box .bk-overlay span, .bk-score-box, .share-total, #pagination .page-numbers.current, .post-page-links > span,
            .widget_display_stats dd strong, .widget_display_search .search-icon, .searchform-wrap .search-icon,
            #back-top, .module-square-grid .content_in_wrapper, .bk_tabs .ui-tabs-nav li.ui-tabs-active, #bk-gallery-carousel .bk-gallery-item:before,
            .page-nav .current, .bk-page-header.header-1 .main-menu > ul > li:hover > a:after, .bk-page-header.header-2 .main-menu > ul > li:hover > a:after,
            .module_header_13 .module-title h2, .bk-page-header.header-3 .main-menu > ul > li:hover > a:after, .loadmore span.ajaxtext,
            .heading-related-posts h4:hover:before, .s-template-14 .heading-related-posts h4:hover:before, .s-template-16 .heading-related-posts h4:hover:before
            {background-color: $primary_color;}";
            
            $rubik_css_option .= ".content_out .review-score, ::-webkit-scrollbar-thumb
            {background-color: ".rubik_hex2rgba (esc_attr($primary_color), 0.9)."}";
            
            $rubik_css_option .= ".footer .cm-flex .flex-control-paging li a
            {background-color: ".rubik_hex2rgba (esc_attr($primary_color), 0.3)."}";
            
            $rubik_css_option .= ".widget_most_commented .comments:after 
            {border-right-color: $primary_color;}";
            
            $rubik_css_option .= ".post-category a:after 
            {border-left-color: $primary_color;}";
        }

        if ( $bg_switch == 'wide') {
            $rubik_css_option .= "#page-wrap { width: auto; }";
        }else{
            $rubik_css_option .= "body { background-position: left; background-repeat: repeat; background-attachment: fixed;}";
        }  
        if ( $sb_responsive_sw == 0) {
            $rubik_css_option .= "@media (max-width: 991px){
                .sidebar {display: none !important}
            }";
        }
        if ( $main_nav_layout == 'center') {
            $rubik_css_option .= ".main-nav .main-menu{
                text-align: center !important;
            }";
            $rubik_css_option .= ".header-6 .header-wrap:first-child .main-nav .canvas-menu-wrap {margin-right: 0;}";
            $rubik_css_option .= ".header-2 .main-menu > ul > li:first-child > a {
                padding: 0 24px;
            }";
        }

        if ( $bk_logo_margin_top != '') {
            $rubik_css_option .= ".header-inner {margin-top: {$bk_logo_margin_top}px;}";
        }
        
        if ( $bk_logo_margin_bottom != '') {
            $rubik_css_option .= ".header-inner {margin-bottom: {$bk_logo_margin_bottom}px;}";
        }

        if ( $bk_footer_padding_top != '') {
            $rubik_css_option .= ".footer {padding-top: {$bk_footer_padding_top}px;}";
        }

        if ( $bk_footer_padding_bottom != '') {
            $rubik_css_option .= ".footer .footer-lower {margin-top: {$bk_footer_padding_bottom}px;}";
        }
        
        if ( $sidebar_header_arrow != '') {
            $rubik_css_option .= ".sidebar-wrap .widget-title h3:before{border-color: {$sidebar_header_arrow} transparent transparent transparent;}";
        }
        
        if ( $footer_header_arrow != '') {
            $rubik_css_option .= ".footer .widget-title h3:before{border-color: {$footer_header_arrow} transparent transparent transparent;}";
        }
        
        if ((is_array($cat_opt))) {
            
            foreach ($cat_opt as $key => $value) {
                if ((is_array($value))&&(count($value) > 0 )) {
                    if (array_key_exists('cat_color',$value)) {
                        $rubik_css_option .=  '.post-category a.term-'.$key.',  
                            .main-nav .main-menu .menu > li.menu-category-'.$key.':hover > a:after, .main-nav .menu > li.current-menu-item.menu-category-'.$key.' > a:after,
                            .content_out .thumb.term-'.$key.', .content_in .bk-thumb-wrap.term-'.$key.',
                            .header-1 .main-nav .menu > li.current-menu-item.menu-category-'.$key.',
                            .header-3 .main-nav .menu > li.current-menu-item.menu-category-'.$key.',
                            .header-4 .main-nav .menu > li.current-menu-item.menu-category-'.$key.',
                            .rating-wrap.term-'.$key.' .rating-canvas-bg
                            {background-color: '.$value['cat_color'].' !important;}'; 
                        
                        $rubik_css_option .= '.bk-preload-wrapper.term-'.$key.':after
                            {border-top-color: '.$value['cat_color'].' !important; border-bottom-color: '.$value['cat_color'].' !important;}';
                        
                        $rubik_css_option .= '.bk-tabs.active > a.term-'.$key.', .bk-tabs a.term-'.$key.':hover, .content_out h4:hover a.term-'.$key.',
                        .menu-category-'.$key.' .post-title a:hover,
                        .bk-mega-menu .bk-sub-menu .menu-category-'.$key.'.active > a, .bk-mega-menu .bk-sub-menu .menu-category-'.$key.':hover > a,
                        .menu-category-'.$key.' .all.active > a,
                        .main-nav .main-menu .bk-mega-menu .post-title a.term-'.$key.':hover,.bk-dropdown-menu li.menu-category-'.$key.' > a:hover,
                        .bk-page-header.header-5 .main-menu > ul > li.menu-category-'.$key.':hover > a,
                        .bk-page-header.header-5 .main-menu > ul > li.current-menu-item.menu-category-'.$key.' > a,
                        .bk-page-header.header-2 .main-menu > ul > li.menu-category-'.$key.':hover > a,
                        .bk-page-header.header-2 .main-menu > ul > li.current-menu-item.menu-category-'.$key.' > a
                            {color: '.$value['cat_color'].' !important;}'; 
                          
                        $rubik_css_option .= '.main-nav .menu > li.menu-category-'.$key.':hover > .bk-mega-menu, 
                            .main-nav .menu > li.menu-category-'.$key.':hover > .bk-mega-column-menu, .main-menu > ul > li.menu-category-'.$key.':hover .bk-dropdown-menu
                                {border-color: '.$value['cat_color'].' !important;}';  
                                
                        $rubik_css_option .= '.post-category a.term-'.$key.':after
                                {border-left-color: '.$value['cat_color'].' !important;}';                                                                                                
                        
                        // Header Type
                        //1
                        if ($moduleHeaderStyle == 'module_header_1') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.' .bk-tab-original, .module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;}';
                        }
                        //2
                        else if ($moduleHeaderStyle == 'module_header_2') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.' .bk-tab-original, .module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;}';
                        }
                        //3
                        else if ($moduleHeaderStyle == 'module_header_3') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;} .module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //4
                        else if ($moduleHeaderStyle == 'module_header_4') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;} .module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //5
                        else if ($moduleHeaderStyle == 'module_header_5') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;} .module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //6
                        else if ($moduleHeaderStyle == 'module_header_6') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;} .module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //7
                        else if ($moduleHeaderStyle == 'module_header_7') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;} .module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //8
                        else if ($moduleHeaderStyle == 'module_header_8') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;} .module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //9
                        else if ($moduleHeaderStyle == 'module_header_9') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //10
                        else if ($moduleHeaderStyle == 'module_header_10') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //11
                        else if ($moduleHeaderStyle == 'module_header_11') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.' h2, .module-title.module-term-'.$key.':before {background: '.$value['cat_color'].' !important;}';
                            $rubik_css_option .= '.module-title.module-term-'.$key.' h2:after, .module-title.module-term-'.$key.' .bk-tab-original:after
                                                    {border-left-color: '.$value['cat_color'].'!important;}';
                        }
                        //12
                        else if ($moduleHeaderStyle == 'module_header_12') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.':before {background-color: '.$value['cat_color'].' !important;} .module-title.module-term-'.$key.' .bk-tab-original {color: '.$value['cat_color'].' !important;}';
                        }
                        //13
                        else if ($moduleHeaderStyle == 'module_header_13') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.' h2:before {border-color: '.$value['cat_color'].' transparent transparent transparent !important;} .module-title.module-term-'.$key.' h2{background: '.$value['cat_color'].' !important;}';
                        }
                        //14
                        else if ($moduleHeaderStyle == 'module_header_14') {
                            $rubik_css_option .= '.module-title.module-term-'.$key.' h2:before {border-color: '.$value['cat_color'].' !important;}';
                        }
                    }           
                } 
                 
            }
        }
        $rubik_css_option .= '.bkmodule-dark .content_in .bk-thumb-wrap {background-color: #000 !important;}'; 
                
        $rubik_css_option .= ".post-category a{
            background-color: $primary_color; 
            color:  #fff !important;
            padding: 1px 5px 2px 5px;
        }";
        
        if($footer_force_link_hover_color != '') {
            $rubik_css_option .= ".footer a:hover, .footer h4:hover a, .footer .content_out h4:hover a {color: $footer_force_link_hover_color !important;}";
        }   
        
        if(isset($rubik_option['bk-mainnav-bg-style']) && ($rubik_option['bk-mainnav-bg-style'] == 'gradient')) {
            if(isset($rubik_option['bk-mainnav-bg-gradient']) && !empty($rubik_option['bk-mainnav-bg-gradient'])) {
                $rubik_gradient_bg = $rubik_option['bk-mainnav-bg-gradient'];
                $rubik_gradient_deg = $rubik_option['bk-mainnav-bg-gradient-direction'];
                if($rubik_gradient_deg == '') {
                    $rubik_gradient_deg = 90;
                }
                $rubik_css_option .= ".bk-page-header.header-1 .main-nav,
                                    .header-3 .main-nav, .bk-page-header.header-6 .main-nav-container, .main-nav .rubik-ajax-search-bg
                                    {background: ".$rubik_gradient_bg['from'].";
                                    background: -webkit-linear-gradient(".$rubik_gradient_deg."deg, ".$rubik_gradient_bg['from']." 0, ".$rubik_gradient_bg['to']." 100%);
                                    background: linear-gradient(".$rubik_gradient_deg."deg, ".$rubik_gradient_bg['from']." 0, ".$rubik_gradient_bg['to']." 100%);}";   
            }
        }
        if(isset($rubik_option['bk-topbar-bg-style']) && ($rubik_option['bk-topbar-bg-style'] == 'gradient')) {
            if(isset($rubik_option['bk-topbar-bg-gradient']) && !empty($rubik_option['bk-topbar-bg-gradient'])) {
                $rubik_gradient_bg = $rubik_option['bk-topbar-bg-gradient'];
                $rubik_gradient_deg = $rubik_option['bk-topbar-bg-gradient-direction'];
                if($rubik_gradient_deg == '') {
                    $rubik_gradient_deg = 90;
                }
                $rubik_css_option .= ".top-bar, .top-bar .rubik-ajax-search-bg
                                    {background: ".$rubik_gradient_bg['from'].";
                                    background: -webkit-linear-gradient(".$rubik_gradient_deg."deg, ".$rubik_gradient_bg['from']." 0, ".$rubik_gradient_bg['to']." 100%);
                                    background: linear-gradient(".$rubik_gradient_deg."deg, ".$rubik_gradient_bg['from']." 0, ".$rubik_gradient_bg['to']." 100%);}";   
            }
        }
        if(isset($rubik_option['bk-footer-bg-style']) && ($rubik_option['bk-footer-bg-style'] == 'gradient')) {
            if(isset($rubik_option['bk-footer-bg-gradient']) && !empty($rubik_option['bk-footer-bg-gradient'])) {
                $rubik_gradient_bg = $rubik_option['bk-footer-bg-gradient'];
                $rubik_gradient_deg = $rubik_option['bk-footer-bg-gradient-direction'];
                if($rubik_gradient_deg == '') {
                    $rubik_gradient_deg = 90;
                }
                $rubik_css_option .= ".footer, .footer:before
                                    {background: ".$rubik_gradient_bg['from'].";
                                    background: -webkit-linear-gradient(".$rubik_gradient_deg."deg, ".$rubik_gradient_bg['from']." 0, ".$rubik_gradient_bg['to']." 100%);
                                    background: linear-gradient(".$rubik_gradient_deg."deg, ".$rubik_gradient_bg['from']." 0, ".$rubik_gradient_bg['to']." 100%);}";   
            }
        }
        if(isset($rubik_option['bk-footer-lower-bg-style']) && ($rubik_option['bk-footer-lower-bg-style'] == 'gradient')) {
            if(isset($rubik_option['bk-footer-lower-bg-gradient']) && !empty($rubik_option['bk-footer-lower-bg-gradient'])) {
                $rubik_gradient_bg = $rubik_option['bk-footer-lower-bg-gradient'];
                $rubik_gradient_deg = $rubik_option['bk-footer-lower-bg-gradient-direction'];
                if($rubik_gradient_deg == '') {
                    $rubik_gradient_deg = 90;
                }
                $rubik_css_option .= ".footer .footer-lower
                                    {background: ".$rubik_gradient_bg['from'].";
                                    background: -webkit-linear-gradient(".$rubik_gradient_deg."deg, ".$rubik_gradient_bg['from']." 0, ".$rubik_gradient_bg['to']." 100%);
                                    background: linear-gradient(".$rubik_gradient_deg."deg, ".$rubik_gradient_bg['from']." 0, ".$rubik_gradient_bg['to']." 100%);}";   
            }
        }
        /*
            $rubik_css_option .= ".post-category a{
                padding: 0 !important;
                border-radius: 0 !important;
            }";
            foreach ($cat_opt as $key => $value) {
                if ((is_array($value))&&(count($value) > 0 )) {
                    if (array_key_exists('cat_color',$value)) {
                        $rubik_css_option .=  '.post-category a.term-'.$key.'
                            {background-color: transparent !important; color: '.$value['cat_color'].' !important;}'; 
                    }           
                } 
                 
            }
        */
    endif;
    wp_add_inline_style( 'rubik-style', $rubik_css_option );        
    }
    add_action( 'wp_enqueue_scripts', 'rubik_custom_css' );    
}