<?php
if (!class_exists('rubik_feature2')) {
    class rubik_feature2 extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('featured2_slider-');
            
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_feature2'], $page_info);    //get block config
            
            $the_query = bk_get_query::query($module_cfg);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-feature2 container bkwrapper clearfix">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
            endif;        
            $block_str .= $this->render_modules($the_query, $module_cfg, $bk_post_icon);            //render modules
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        public function render_modules ($the_query, $module_cfg, $bk_post_icon){
            $render_modules = '';
            $bk_contentout1 = new bk_contentout1;
            $custom_var = array (
                    'thumbnail'    => 'rubik-620-420',
                    'meta'           => array('author', 'date'),
                    'post-icon'     => $bk_post_icon,
                    'cat'            => 'on',
                    'except_length'     => 40,
                    'rm_btn'            => true,
                );
            if ( $the_query->have_posts() ) :            
                $render_modules .= '<div class="row">';
                $render_modules .= '<div class="flexslider">';
                $render_modules .= '<ul class="slides">';
                $bk_no_thumb = '';
                
                while ( $the_query->have_posts() ): $the_query->the_post();
                    if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                        $bk_no_thumb = 'bk_no_thumb';
                    }else {
                        $bk_no_thumb = '';
                    }
                    $render_modules .= '<li class="content_out '.$bk_no_thumb.'">';
                    $render_modules .= $bk_contentout1->render($custom_var);
                    $render_modules .= '</li>';
                endwhile;
                
                $render_modules .= '</ul></div></div> <!-- Close render modules -->';
                
            endif;
            return $render_modules;
        }
        
    }
}