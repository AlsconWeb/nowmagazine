<?php
if (!class_exists('rubik_row_tall_fw')) {
    class rubik_row_tall_fw extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('row_tall_fw-');
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            
            $bk_ajax_button = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_button', true );
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            $excerpt_length = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_excerpt_length', true );
            
            $module_cfg = bk_get_cfg::configs($cfg_ops['has_sb']['bk_row'], $page_info);    //get block config
            $module_cfg['limit'] = $module_cfg['limit']*3;
            
            $ajax_load_number = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_load_number', true );
            
            rubik_section_parent::$rubik_ajax_c[$uid]['entries'] = $ajax_load_number;
            
            rubik_section_parent::$rubik_ajax_c[$uid]['offset'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_offset', true );
            
            /** Tabs **/
            $rubik_tabs = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_categorytabs', true );
            
            $rubikTabsToArray = explode(",",$rubik_tabs);
            
            $tabs_amount = intval(count($rubikTabsToArray));
            
            rubik_section_parent::$rubik_ajax_c[$uid]['post_icon'] = $bk_post_icon;
            rubik_section_parent::$rubik_ajax_c[$uid]['excerpt_length'] = $excerpt_length;
            
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['content'] = '';
            
            if($tabs_amount > 0) {
                for ($i=0; $i< $tabs_amount; $i++) {
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['cat'] = $rubikTabsToArray[$i];
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['content'] = '';
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule container bkwrapper module-row fw-module tall layout-1 clearfix">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info, $rubikTabsToArray);  //render block title
            endif;
            
            $block_str .= '<ul class="bk-row-wrap bk-module-inner row clearfix">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon, $excerpt_length);            //render modules
            $block_str .= '</ul> <!-- Close bk-row-wrap -->';
            //Loadmore button 
            if($bk_ajax_button !== 'disable') {
                $block_str .= '<div class="row-ajax loadmore">';
                $block_str .= rubik_core::bk_get_load_more_buttom();
                $block_str .= '</div><!-- End Loadmore -->';
            }
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        public function render_modules ($the_query, $bk_post_icon, $excerpt_length){
            $render_modules = '';
            $bk_contentout2 = new bk_contentout2;
            $custom_var = array (
                'thumbnail'    => 'rubik-620-420',
                'post-icon'     => $bk_post_icon,
                'except_length' => $excerpt_length,
            );
            if ( $the_query->have_posts() ) :
                
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="row-type content_out col-md-4 col-sm-6">';
                    $render_modules .= $bk_contentout2->render($custom_var);
                    $render_modules .= '</li><!-- end post item -->';
                endwhile;
                
            endif;
            return $render_modules;
        }
        
    }
}