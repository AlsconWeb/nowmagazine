<?php
/**
 * The template for displaying Author archive pages
 *
 */
 ?> 
<?php get_header();?>
<?php
$cur_cat_id = $wp_query->get_queried_object_id();
$rubik_option = rubik_core::bk_get_global_var('rubik_option');
$bk_post_icon = 'hide';
$cat_opt = get_option('bk_cat_opt');
$cat_layout = 0;
$sidebarPosition = '';

if (isset($cat_opt[$cur_cat_id]) && is_array($cat_opt[$cur_cat_id]) && array_key_exists('archive-sidebar-position',$cat_opt[$cur_cat_id])) { $sidebarPosition = $cat_opt[$cur_cat_id]['archive-sidebar-position'];};
if($sidebarPosition == 'left'){
    $sidebar_option_class = 'has-left-sidebar';
}else {
    $sidebar_option_class = '';
}

if (isset($rubik_option) && ($rubik_option != '')): 
    $archiveLayout = $rubik_option['bk-category-layout'];                
    if(isset($rubik_option['category_post_icon'])) {
        $bk_post_icon= $rubik_option['category_post_icon'];
    }else {
        $bk_post_icon = 'hide';
    }  
    if($sidebarPosition == 'global') :
        $sidebarPosition = $rubik_option['category-page-sidebar-position'];
        if($sidebarPosition == 'left'){
            $sidebar_option_class = 'has-left-sidebar';
        }else {
            $sidebar_option_class = '';
        }
    endif;
endif;
if (isset($cat_opt[$cur_cat_id]) && is_array($cat_opt[$cur_cat_id]) && array_key_exists('cat_layout',$cat_opt[$cur_cat_id])) { $cat_layout = $cat_opt[$cur_cat_id]['cat_layout'];};
if ((strlen($cat_layout) != 0)&&($cat_layout != 'global')) { $archiveLayout = $cat_layout;};
?>
<div id="body-wrapper" class="wp-page bk-cat-arc-page-<?php echo esc_attr($cur_cat_id);?>">	
    <div class="bkwrapper container <?php if($sidebar_option_class != '') echo esc_attr($sidebar_option_class);?> <?php if(($archiveLayout == 'large-blog-nosb') || ($archiveLayout == 'large-blog-2-nosb')){echo ('page-large-blog-nosb');}?>">	
        <div class="row bksection">
            <div class="bk-category-content bk-cat-arc-content bkpage-content <?php if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3')) && (!($archiveLayout == 'large-blog-nosb')) && (!($archiveLayout == 'large-blog-2-nosb')) && (!($archiveLayout == 'row-nosb'))): echo 'col-md-8 has-sb'; else: echo 'col-md-12 fullwidth';  endif;?>">
                <div class="row">
                    <div id="main-content" class="clear-fix" role="main">
                  		<div class="page-title-wrapper col-md-12">
                    		<div class="module-title module-term-<?php echo esc_attr($cur_cat_id);?>">
                                <h2 class="heading bk-tab-original"><?php single_cat_title();?></h2>
                            </div>
                            <?php if ( category_description() ) : // Show an optional category description ?>
                    			<div class="sub-title"><?php echo category_description(); ?></div>
                    		<?php endif;?>
                        </div>
                    <?php
                        if (have_posts()) {
                            echo rubik_archive::archive_content_area($archiveLayout, $bk_post_icon);
                        }
                    ?>
            
    	            </div> <!-- end #main -->
                </div>
            </div> <!-- end #bk-content -->
            <?php
                if ((!($archiveLayout == 'masonry-nosb')) && (!($archiveLayout == 'square-grid-3-s1')) && (!($archiveLayout == 'square-grid-3-s2')) && (!($archiveLayout == 'square-grid-3-s3')) && (!($archiveLayout == 'large-blog-nosb')) && (!($archiveLayout == 'large-blog-2-nosb')) && (!($archiveLayout == 'row-nosb'))) {?>
                    <div class="sidebar col-md-4">
                        <div class="sidebar-wrap <?php if($rubik_option['category-stick-sidebar'] == 'enable') echo 'stick';?>" id="bk-category-sidebar">
                            <?php
                                if (isset($cat_opt[$cur_cat_id]) && is_array($cat_opt[$cur_cat_id]) && array_key_exists('sb_category',$cat_opt[$cur_cat_id])) { 
                                    $bkcatSidebar = $cat_opt[$cur_cat_id]['sb_category'];
                                    if ((strlen($bkcatSidebar) != 0)&&($bkcatSidebar != 'global')) { 
                                        dynamic_sidebar($bkcatSidebar);
                                    }else {
                                        get_sidebar();
                                    }
                                }else {
                                    get_sidebar();
                                }
                            ?>
                        </div>
                    </div>
                <?php }
            ?>
        </div>
    </div>
</div> 
<?php get_footer(); ?>