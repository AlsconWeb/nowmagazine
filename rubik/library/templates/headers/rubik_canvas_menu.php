<?php
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    $logo = null;
    if ((isset($rubik_option['bk-offcanvas-logo'])) && (($rubik_option['bk-offcanvas-logo']) != NULL)){ 
        $logo = $rubik_option['bk-offcanvas-logo'];
        if (($logo != null) && (array_key_exists('url',$logo))) {
            if ($logo['url'] == '') {
                if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
                    $logo = $rubik_option['bk-logo'];
                }
            }
        }
    }else {
        if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
            $logo = $rubik_option['bk-logo'];
        }
    }
    $socialSwitch = isset($rubik_option['bk-offcanvas-social-header-switch']) ? $rubik_option['bk-offcanvas-social-header-switch'] : 0;
    $canvasStyle = isset($rubik_option['bk-canvas-panel-style']) ? $rubik_option['bk-canvas-panel-style'] : 'white';
?>
<div id="main-canvas-menu" class="<?php echo esc_attr($canvasStyle);?>">
    <div class="canvas-panel-wrap block">
        <div class="rubik-offcanvas-top">
            <div id="canvas-inner-header">
                <div class="bk-offcanvas__title">
            		<h2 class="site-logo">
                        <a href="/nowmagazine/">
            				<!-- logo open -->
                            <?php if (($logo != null) && (array_key_exists('url',$logo))) {
                                    if ($logo['url'] != '') {
                                ?>
                                <img src="<?php echo esc_url($logo['url']);?>" alt="<?php esc_attr_e( 'Logo', 'rubik' ); ?>"/>
                			<!-- logo close -->
                            <?php } else {?>
                                <?php echo esc_attr(bloginfo( 'name' ));?>
                            <?php }
                            } else {?>
                                <?php echo esc_attr(bloginfo( 'name' ));?>
                            <?php } ?>
            			</a>
                    </h2>
            	</div>
                <a class="canvas-menu-close" href="#" title="<?php esc_attr_e('Close', 'rubik');?>">&#10005;</a>
            </div>
            <div class="rubik-canvas-menu">
            <?php if ( has_nav_menu( 'canvas-menu' ) ) { ?>
                <?php
                wp_nav_menu( array( 
                    'theme_location' => 'canvas-menu',
                    'depth' => '',
                    'container_id' => 'canvas-menu' ) );
                ?>
            <?php }else if ( has_nav_menu( 'main-menu' ) ) {?>
                    <?php
                    wp_nav_menu( array( 
                        'theme_location' => 'main-menu',
                        'depth' => '',
                        'container_id' => 'canvas-menu' ) );
                    ?>
            <?php }?>
            </div>
        </div>
        <div class="rubik-offcanvas-bottom">
            <?php 
            if($socialSwitch == 1) {
                get_template_part( 'library/templates/headers/rubik_header_social' );
            }
            ?>
            <?php if(isset($rubik_option['bk-offcanvas-copyright']) && ($rubik_option['bk-offcanvas-copyright'] != '')) {?>
            <div class="rubik-off-canvas-copyright">
                <?php 
                    $copyright_text = $rubik_option['bk-offcanvas-copyright'];
                    $bk_allow_html = array(
                        'a' => array(
                            'href' => array(),
                            'title' => array()
                        ),
                        'br' => array(),
                        'em' => array(),
                        'strong' => array(),
                        'p' => array(),
                    );
                    echo wp_kses($copyright_text, $bk_allow_html);
                ?>
            </div>    
            <?php }?>
        </div>
    </div>
    <div class="rubik-canvas-background-img"></div>
</div>