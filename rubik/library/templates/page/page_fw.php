<?php
/**
 * The Default Page Template -- With Sidebar page template
 *
 */
 ?>
<?php 
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    $pageID  = get_the_ID();
    $featuredImage  = $rubik_option['bk_page_feat_img'];    
?>
<div class="wp-page bkwrapper container">
    <div class="row">
        <div id="page-content" class="col-sm-12">
            <div class="page-title-wrapper">
        		<div class="module-title">
                    <h2 itemprop="name"><span><?php the_title(); ?></span></h2>
                </div>
            </div>
            <?php
                if ( ($featuredImage != 0) && rubik_core::bk_check_has_post_thumbnail($pageID)) {
                    echo '<div class="entry-thumb page-entry-thumb">';
                    echo get_the_post_thumbnail($pageID, 'rubik-1000-540');
    				echo '</div>';
                }
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
				<div class="post-content"><?php the_content(); ?></div>
			</article>
            <?php wp_link_pages( array(
					'before' => '<div class="post-page-links">',
					'pagelink' => '<span>%</span>',
					'after' => '</div>',
				)
			 ); 
            ?>
            <?php
                if (function_exists("rubik_paginate")) {
                    rubik_paginate();
                }

                if($rubik_option['bk-comment-sw']) {
                    comments_template();
                }
            ?>
        </div>
    </div>
</div>