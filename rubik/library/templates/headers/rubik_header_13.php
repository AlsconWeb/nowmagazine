<?php
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    $logo = array();
    if (isset($rubik_option)){  
        if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
            $logo = $rubik_option['bk-logo'];
        }
        $backtop = $rubik_option['bk-backtop-switch'];
        $canvasSwitch = isset($rubik_option['bk-canvas-desktop-switch']) ? $rubik_option['bk-canvas-desktop-switch'] : 0;
    }
    
    $mobilelogo = array();
    if ((isset($rubik_option['bk-sticky-nav-logo'])) && (($rubik_option['bk-sticky-nav-logo']) != NULL)){ 
        $mobilelogo = $rubik_option['bk-sticky-nav-logo'];
        if (($mobilelogo != null) && (array_key_exists('url',$mobilelogo))) {
            if ($mobilelogo['url'] == '') {
                if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
                    $mobilelogo = $rubik_option['bk-logo'];
                }
            }
        }
    }else {
        if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
            $mobilelogo = $rubik_option['bk-logo'];
        }
    }
    ?> 
    <?php get_template_part( 'library/templates/headers/rubik_canvas_menu' );?>
    <div id="page-inner-wrap">
        <div class="page-cover canvas-menu-close"></div>
        <div class="bk-page-header header-1 header-2 header-12 header-13">            
            <div class="header-wrap">
                <!-- ticker open -->
                <?php        
                    if (isset($rubik_option)){
                        if (isset($rubik_option['bk-header-ticker']) && ($rubik_option['bk-header-ticker'] == 1)) {?>
                            <div class="bk-ticker-module bk-white-bg">
                            <?php rubik_core::bk_get_ticker('header');?>
                            </div><!--end ticker-module-->
                        <?php }
                    }
                ?>
                <!-- ticker close -->
                <div class="top-bar">
                    <div class="rubik-ajax-search-bg"></div>
                    <div class="bkwrapper container">
                        <div class="top-nav clearfix">
                            <?php if($rubik_option['bk-top-date-position'] == 'left') {?>
                            <div class="rubik_data_time rubik-float-left">
                                <?php
                                    echo date_i18n(stripslashes('l, F j')); 
                                ?>
                            </div>
                            <?php }?>
                            <?php if ( has_nav_menu('menu-top') ) {?> 
                                <?php wp_nav_menu(array('theme_location' => 'menu-top','container_id' => 'top-menu'));?> 
                            <?php }?>
                            
                            <?php 
                                $moreSearchButtonClass = 'top-bar-ajax-search';
                                echo rubik_ajax_form_search($moreSearchButtonClass, 1);
                            ?> 
                            
                            <?php if($rubik_option['bk-top-date-position'] == 'right') {?>
                            <div class="rubik_data_time rubik-float-right">
                                <?php
                                    echo date_i18n(stripslashes('l, F j')); 
                                ?>
                            </div>
                            <?php }?>
                        </div><!--top-nav-->
                    </div>
                </div><!--top-bar-->
                
                <div class="header-logo-wrapper">
                    <div class="header container">
                        <div class="row">
                            <div class="col-md-12">
                    			<div class="header-inner">
                        			<div class="rubik-header12-col rubik-header12-left rubik-header12-grow">
                                        <div class="canvas-menu-wrap <?php if($canvasSwitch == 1) echo 'show-on-desktop';?>">
                                            <?php echo rubik_core::bk_canvas_button();?>
                                        </div>
                                    </div>
                                    <div class="rubik-header12-col rubik-header12-center rubik-header12-auto">
                                        <?php if(isset ($rubik_option['bk-logo-hide']) && ($rubik_option['bk-logo-hide'] != 0)) {?>
                                        <!-- logo open -->
                                            <?php if (($logo != null) && (array_key_exists('url',$logo))) {
                                                    if ($logo['url'] != '') {
                                                ?>
                                			<div class="logo">
                                                <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                    <img src="<?php echo esc_url($logo['url']);?>" alt="<?php esc_attr_e( 'Logo', 'rubik' ); ?>"/>
                                                </a>
                                			</div>
                                			<!-- logo close -->
                                            <?php } else {?> 
                                            <div class="logo logo-text">
                                                <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                    <?php echo esc_attr(bloginfo( 'name' ));?>
                                                </a>
                                			</div>
                                            <?php }
                                            } else {?> 
                                            <div class="logo logo-text">
                                                <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                    <?php echo esc_attr(bloginfo( 'name' ));?>
                                                </a>
                                			</div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                    <div class="rubik-header12-col rubik-header12-right rubik-header12-grow">
                                        <?php get_template_part( 'library/templates/headers/rubik_header_social' );?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
                
                <!-- nav open -->
                <?php if ( has_nav_menu( 'main-menu' ) || has_nav_menu( 'canvas-menu' )) {?>
        		<nav class="main-nav">
                    <div class="main-nav-inner bkwrapper container">
                        <div class="main-nav-container clearfix">
                            <div class="main-nav-wrap">
                                <div class="canvas-menu-wrap <?php if($canvasSwitch == 1) echo 'show-on-desktop';?>">
                                    <a class="canvas-nav-btn nav-open-btn">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>  
                                    <?php if(isset ($rubik_option['bk-logo-hide']) && ($rubik_option['bk-logo-hide'] != 0)) {?>
                                    <!-- logo open -->
                                        <?php if (($mobilelogo != null) && (array_key_exists('url',$mobilelogo))) {
                                                if ($mobilelogo['url'] != '') {
                                            ?>
                            			<div class="logo aaaaaaaaaaaaaaa">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <img src="<?php echo esc_url($mobilelogo['url']);?>" alt="<?php esc_attr_e( 'Logo', 'rubik' ); ?>"/>
                                            </a>
                            			</div>
                            			<!-- logo close -->
                                        <?php } else {?> 
                                        <div class="logo logo-text">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </a>
                            			</div>
                                        <?php }
                                        } else {?> 
                                        <div class="logo logo-text">
                                            <a href="<?php echo esc_url(get_home_url('/'));?>">
                                                <?php echo esc_attr(bloginfo( 'name' ));?>
                                            </a>
                            			</div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php if ( has_nav_menu( 'main-menu' ) ) { 
                                wp_nav_menu( array( 
                                    'theme_location' => 'main-menu',
                                    'container_class' => 'main-menu',
                                    'walker' => new BK_Walker,
                                    ) );}?>
                            </div>
                            <?php 
                                echo rubik_ajax_form_search();
                            ?> 
                        </div>    
                    </div><!-- main-nav-inner -->       
                    <div class="rubik-ajax-search-bg"></div>    
        		</nav>
                <?php }?>
                <!-- nav close -->
    		</div>
            <?php 
                if(isset($rubik_option['bk-sticky-nav-switch']) && ($rubik_option['bk-sticky-nav-switch'] == 2)) :
                    get_template_part( 'library/templates/headers/rubik_sticky_header' );
                endif;    
            ?>
        </div>                
        <?php if(function_exists('rubik_breadcrumbs')) :?>         
            <div class="bk-breadcrumbs-container">
            <?php rubik_breadcrumbs();?>
            </div>
        <?php endif;?>
        
        <!-- backtop open -->
		<?php if ($backtop) { ?>
            <div id="back-top"><i class="fa fa-long-arrow-up"></i></div>
        <?php } ?>
		<!-- backtop close -->