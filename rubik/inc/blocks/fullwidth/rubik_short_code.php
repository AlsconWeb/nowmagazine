<?php
if (!class_exists('rubik_shortcode')) {
    class rubik_shortcode extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('shortcode-');
            $block_str = '';
            $i=0;
            $shortcode_str = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_shortcode', true );
            $shortcodes = explode("[bkend]",$shortcode_str);
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $bkwrapper_container = '';                         
            }else {
                $bkwrapper_container = 'bkwrapper container';            
            }
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-shortcode '.$bkwrapper_container.' clearfix">';
            $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
            $block_str .= '<div class="bkshortcode-wrapper">';
            for ($i=0; $i< count($shortcodes); $i++) {
                $block_str .= do_shortcode($shortcodes[$i]);
            }
            $block_str .= '</div><!-- End bkshortcode-wrapper --></div><!-- End module-shortcode -->';
            wp_reset_postdata();
            return $block_str;
    	}
        
    }
}