<?php
if (!class_exists('rubik_hero')) {
    class rubik_hero extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('hero-');
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
                                    
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            $moduleLayout = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_layout', true );
            
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_hero'], $page_info);    //get block config
            
            $module_cfg['limit'] = 5;
            
            /** Tabs **/
            $rubik_tabs = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_categorytabs', true );
            
            $rubikTabsToArray = explode(",",$rubik_tabs);
            
            $tabs_amount = intval(count($rubikTabsToArray));
            
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['content'] = '';
            rubik_section_parent::$rubik_ajax_c[$uid]['post_icon'] = $bk_post_icon;
            
            if($tabs_amount > 0) {
                for ($i=0; $i< $tabs_amount; $i++) {
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['cat'] = $rubikTabsToArray[$i];
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['content'] = '';
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);           //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule container bkwrapper module-hero clearfix '.$moduleLayout.'">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info, $rubikTabsToArray);  //render block title
            endif;
            $block_str .= '<div class="bk-module-inner">';    
            $block_str .= $this->render_modules($the_query, $bk_post_icon);            //render modules
            $block_str .= '</div><!--end bk-module-inner -->';
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        public function render_modules ($the_query, $bk_post_icon){
            $render_modules = '';
            $bk_contentout2 = new bk_contentout2;
            $bk_contentin3 = new bk_contentin3;
            
            $custom_var_in3 = array(); $custom_var_out2 = array(); 
            
            $custom_var_in3 = array (
                    'cat'           => 'off',
                    'post-icon'     => $bk_post_icon,
                    'meta'          => 'off',
                    'thumbnail'     => 'rubik-620-420',
            );
            
            $custom_var_out2 = array (
                'thumbnail'    => 'rubik-450-380',
                'post-icon'     => $bk_post_icon,
                'except_length' => 0,
            );
            if ( $the_query->have_posts() ) :            
                $render_modules .= '<div class="row clearfix">';
                
                if ( $the_query->have_posts() ) :
                    $render_modules .= '<div class="small-posts-col col-md-3 col-sm-6 clearfix">';
                    $render_modules .= '<ul class="list-small-post row">';
                    foreach( range( 1, 2) as $i ):
                        if ( $the_query->have_posts() ) :
                            $the_query->the_post();
                            $render_modules .= '<li class="content_in col-md-12 col-sm-12 bk-post-title-small">';
                            $render_modules .= '<div class="post-wrap">';
                            $render_modules .= $bk_contentin3->render($custom_var_in3);
                            $render_modules .= '</div>';
                            $render_modules .= '</li><!-- End post -->';
                        else:
                            $render_modules .= '</ul> <!-- End list-post -->';
                            $render_modules .= '</div><!-- End Column -->';
                            goto BKENDMODULE;   
                        endif;     
                    endforeach;
                    $render_modules .= '</ul> <!-- End list-post -->';
                    $render_modules .= '</div><!-- End Column -->';
                else:
                    goto BKENDMODULE;
                endif;
                
                if ( $the_query->have_posts() ) :
                    foreach( range( 1, 1) as $i ):
                        $the_query->the_post();
                        $render_modules .= '<ul>';
                        $render_modules .= '<li><div class="large-post row-type content_out col-md-6 col-sm-6 ">';
                        $render_modules .= $bk_contentout2->render($custom_var_out2);               
                        $render_modules .=  '</div></li>';      
                        $render_modules .= '</ul>';
                    endforeach;
                else:
                    goto BKENDMODULE;
                endif;
                
                if ( $the_query->have_posts() ) :
                    $render_modules .= '<div class="small-posts-col col-md-3 col-sm-12 clearfix">';
                    $render_modules .= '<ul class="list-small-post row">';
                    foreach( range( 1, $the_query->post_count - 3) as $i ):
                        if ( $the_query->have_posts() ) :
                            $the_query->the_post();
                            $render_modules .= '<li class="content_in col-md-12 col-sm-6 bk-post-title-small">';
                            $render_modules .= '<div class="post-wrap">';
                            $render_modules .= $bk_contentin3->render($custom_var_in3);
                            $render_modules .= '</div>';
                            $render_modules .= '</li><!-- End post -->';  
                        else:
                            $render_modules .= '</ul> <!-- End list-post -->';
                            $render_modules .= '</div><!-- End Column -->';
                            goto BKENDMODULE;         
                        endif;
                    endforeach;
                    $render_modules .= '</ul> <!-- End list-post -->';
                    $render_modules .= '</div><!-- End Column -->';
                else:
                    goto BKENDMODULE;
                endif;
BKENDMODULE:
                $render_modules .= '</div><!-- Close render modules -->';
                
            endif;
            return $render_modules;
        }
        
    }
}