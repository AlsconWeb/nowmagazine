/* -----------------------------------------------------------------------------
 * Page Template Meta-box
 * -------------------------------------------------------------------------- */
;(function( $, window, document, undefined ){
	"use strict";
	
	$( document ).ready( function () {
        var container = $('#bk-container');
        $( '#page_template' ).change( function() {
			var template = $( '#page_template' ).val();

			// Page Composer Template
			if ( 'page_builder.php' == template ) {
				if(container.length) {  
                    container.show();
                }
				$.page_builder( 'show' );
				$( '#bk_page_options' ).hide();
                $( '#pagenav_pagebuilder').show();
                
                $('.bk-module-options').find('.bk-module-option-wrap').hide();
                $('.bk-module-options').find('.option-tab-1').show();

			} else {
				$.page_builder( 'hide' );
				$( '#bk_page_options' ).show();
                $('#pagenav_pagebuilder').hide();
			}
		} ).triggerHandler( 'change' );
        
	} );
})( jQuery, window , document );
