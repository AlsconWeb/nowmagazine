<?php
/*
Template Name: Page Builder
*/
get_header(); ?>

<?php
global $paged, $post;

$rubik_option = rubik_core::bk_get_global_var('rubik_option');

$bk_page = (get_query_var('page')) ? get_query_var('page') : 1; //rewrite the global var
$bk_paged = (get_query_var('paged')) ? get_query_var('paged') : 1; //rewrite the global var


//paged works on single pages, page - works on homepage
if ($bk_paged > $bk_page) {
    $paged = $bk_paged;
} else {
    $paged = $bk_page;
}
$page_ID = get_the_ID();

?>
	
<div id="page-content-wrap">

    <?php
    /*
        Show Pagebuilder on First Page
    */

    if (empty($paged) or $paged < 2) { //show this only on the first page
        bk_page_builder();
    }
    
    ?>
    
    <?php
    if(function_exists('rubik_meta_box_cb')) :
        $sidebar_prefix = 'bk_sidebar_6996';
    
        $pagenav_sidebar = get_post_meta($page_ID,'bk_pagenav_sb_select',true);
        
        $pagenav_layout = get_post_meta($page_ID,'bk_pagenav_layout',true);
     
        if ($pagenav_layout != 'disable') {
            query_posts('post_type=post&ignore_sticky_posts=true&post_status=publish&paged=' . $paged);
        ?>
            <div class="has-sb container bkwrapper bksection">
                <div class="row">
                    <div class="content-wrap col-md-8">
                        <?php
                            echo rubik_core::bk_module_pagenav_layout($page_ID, $pagenav_layout);
                        ?>
                        <div class="bk-latest-pagination row">
                            <div class="col-md-12">
                                <?php echo rubik_core::rubik_get_pagination();?>
                            </div>
                        </div>
                    </div>
                
                    <div class='sidebar col-md-4'>
                        <div class="sidebar-wrap <?php if($rubik_option['pagebuilder-sidebar'] == 'enable') echo 'stick';?>" id="<?php echo esc_attr($sidebar_prefix);?>">
                            <?php dynamic_sidebar( $pagenav_sidebar );?>
                        </div>
                    </div>
                </div>
            </div>
        <?php }?>
        <?php
    endif;
    ?>    
</div>

<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>