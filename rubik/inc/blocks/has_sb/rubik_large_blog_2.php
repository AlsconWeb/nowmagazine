<?php
if (!class_exists('rubik_large_blog_2')) {
    class rubik_large_blog_2 extends rubik_section_parent  {
    
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('large_blog_2-');
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $block_str = '';
            $cfg_ops = array();
            $bk_ajax_button = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_button', true );
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            $excerpt_length = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_excerpt_length', true );
            $cfg_ops = $this->cfg_options(); 
                
            $sec = '';
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $sec = 'has_sb';
                $col_width = '';
                parent::$rubik_ajax_c[$uid]['sec'] = 'has_sb';  
                $columns_number = 1;                       
            }else {
                $sec = 'fw';
                $col_width = 'container bkwrapper';
                parent::$rubik_ajax_c[$uid]['sec'] = 'fw';      
                $columns_number = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_columns_number', true );                   
            }
            
            if($columns_number == 2) {
                $columns_class = 'col-md-6';
                $columns_number_class = 'large-blog-2-columns';
            }else {
                $columns_class = 'col-md-12';
                $columns_number_class = 'large-blog-1-columns';
            }
            
    // prepare ajax vars 
            $ajax_load_number = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_load_number', true );
            
            parent::$rubik_ajax_c[$uid]['entries'] = $ajax_load_number;
            parent::$rubik_ajax_c[$uid]['sec'] = ''; 
            parent::$rubik_ajax_c[$uid]['offset'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_offset', true );
            parent::$rubik_ajax_c[$uid]['post_icon'] = $bk_post_icon;
            parent::$rubik_ajax_c[$uid]['excerpt_length'] = $excerpt_length;
            parent::$rubik_ajax_c[$uid]['columns_class'] = $columns_class;
    ///////////////////////
                    
            $module_cfg = bk_get_cfg::configs($cfg_ops['has_sb']['bk_large_blog'], $page_info);    //get block config
            
            /** Tabs **/
            $rubik_tabs = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_categorytabs', true );
            
            $rubikTabsToArray = explode(",",$rubik_tabs);
            
            if ($module_cfg['category_id'] != 0) {
                $catTabArray = explode(",",$module_cfg['category_id']);
            }else {
                $catTabArray = 0;
            }
            $tabs_amount = intval(count($rubikTabsToArray));
            
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['cat'] = $catTabArray;
                
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['content'] = '';
            
            if($tabs_amount > 0) {
                
                for ($i=0; $i< $tabs_amount; $i++) {
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['cat'] = $rubikTabsToArray[$i];
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['content'] = '';
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);           //get query
    
    // locallize ajax vars 
            //wp_localize_script( 'rubik-module-load-post', 'ajax_c', parent::$rubik_ajax_c );
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-large-blog layout-2 module-blog '.$col_width.' '.$columns_number_class.'">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info, $rubikTabsToArray);  //render block title
            endif;
            $block_str .= '<div class="bk-module-inner row clearfix">';
            $block_str .= '<ul class="bk-blog-content clearfix">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon, $excerpt_length, $columns_class);            //render modules
            $block_str .= '</ul></div>';
            //Loadmore button 
            if($bk_ajax_button !== 'disable') {
                $block_str .= '<div class="blog-ajax loadmore large-blog-2">';
                $block_str .= rubik_core::bk_get_load_more_buttom();
                $block_str .= '</div><!-- End Loadmore -->';
            }
            
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $bk_post_icon, $excerpt_length, $columns_class){
            $render_modules = '';
            $bk_contentout = new bk_contentout10;
            $custom_var = array (
                'cat'           => 'on',
                'thumbnail'     => 'rubik-730-400',
                'post-icon'     => $bk_post_icon,
                'except_length' => $excerpt_length,
            );
            if ( $the_query->have_posts() ) :            
              
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="item '.$columns_class.' row-type">';
                    $render_modules .= '<div class="content_out clearfix">';
                    $render_modules .= $bk_contentout->render($custom_var);
                    $render_modules .= '</div></li><!-- end post item -->';
                endwhile;
                
            endif;
            return $render_modules;
        }
    }
}