<?php
if (!class_exists('bk_contentout10')) {
    class bk_contentout10 {
        
        function render($custom_var) {
            ob_start();
            $meta_ar = array('author', 'date');
            $meta2_ar = array('postview', 'postcomment');
            ?>
            <?php if(rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {?>
            <div class="bk-mask">
                <?php echo rubik_core::get_feature_image($custom_var['thumbnail'], true, $custom_var['post-icon']);?>
            </div>
            <?php }?>
            <div class="post-c-wrap <?php if(! rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) { echo 'bk-post-no-thumb';}?>">
                <?php echo rubik_core::bk_meta_cases('cat');?>
                <?php echo rubik_core::bk_get_post_title(get_the_ID(), 15);?>
                <?php if ($custom_var['except_length'] != 0) {?>
                    <div class="meta-wrap">
                        <?php echo rubik_core::bk_get_post_meta($meta_ar);?>
                        <?php echo rubik_core::bk_get_post_meta($meta2_ar);?>
                    </div>
                <?php }else {?>
                    <div class="meta-wrap">
                        <?php echo rubik_core::bk_get_post_meta($meta_ar);?>
                    </div>
                <?php }?>
                <?php if ($custom_var['except_length'] != 0) echo rubik_core::bk_get_post_excerpt($custom_var['except_length']);?>                
            </div>     
            
            <?php return ob_get_clean();
        }
        
    }
}