<?php
if (!class_exists('rubik_carousel_type2')) {
    class rubik_carousel_type2 extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('carousel_type2-');
            
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_carousel_type2'], $page_info);    //get block config
            $bk_post_icon = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_post_icon', true );
            $moduleLayout = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_layout', true ); 
            $moduleAlign = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_align', true ); 
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $sec = '';
            $has_bkwrapper = '';
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $has_bkwrapper = 'carousel_2';                 
            }else {
                if($moduleLayout == 'bkmodule-fw') {
                    $has_bkwrapper = 'carousel_4';
                }else {
                    $has_bkwrapper = 'bkwrapper container carousel_3';   
                }
            }
            $the_query = bk_get_query::query($module_cfg);//get query
            
            if($the_query->post_count > 3) {
                $flexClass = ' flexslider';
            }else {
                $flexClass = '';
            }
            
            $block_str .= '<div id="'.$uid.'" class="bkmodule '.$has_bkwrapper.' '.$moduleAlign.' module-carousel module-carousel-type2 hide">';
            if ( $the_query->have_posts() ) :
                if($moduleLayout == 'bkmodule-fw') {
                    $block_str .= '<div class="container bkwrapper">';
                    $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
                    $block_str .= '</div>';
                }else {
                    $block_str .= rubik_core::bk_get_block_title($page_info);  //render block title
                }
            endif;
            $block_str .= '<div class="row"><div class="bk-carousel-wrap'.$flexClass.'">';
            $block_str .= $this->render_modules($the_query, $bk_post_icon);
            $block_str .= '</div></div></div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query, $bk_post_icon){
            $render_modules = '';
            $bk_contentout10 = new bk_contentout10;
            $custom_var = array (
                    'thumbnail'    => 'rubik-620-420',
                    'cat'           => 'on',
                    'post-icon'     => $bk_post_icon,
                    'except_length' => 0,
                    'meta'          => array('date')
                );
            if ( $the_query->have_posts() ) :
                $render_modules .= '<ul class="slides clearfix">';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="row-type content_out col-md-4 col-sm-6">';
                    $render_modules .= $bk_contentout10->render($custom_var);    
                    $render_modules .= '</li>';
                endwhile;
                $render_modules .= '</ul> <!-- Close render slider -->';
            endif;
            return $render_modules;
        }
        
    }
}