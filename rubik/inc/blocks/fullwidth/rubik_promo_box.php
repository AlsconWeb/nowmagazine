<?php
if (!class_exists('rubik_promo_box')) {
    class rubik_promo_box extends rubik_section_parent  {
        public function render( $page_info ) {
            $pm_title_1 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_title_1', true );
            $pm_image_1 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_img_1', true );
            $pm_url_1 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_url_1', true );
            
            $pm_title_2 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_title_2', true );
            $pm_image_2 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_img_2', true );
            $pm_url_2 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_url_2', true );
            
            $pm_title_3 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_title_3', true );
            $pm_image_3 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_img_3', true );
            $pm_url_3 = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_promo_box_url_3', true );
            ?>
            <div class="bkmodule bkwrapper container module-promo-box">
            <?php if (strlen($pm_image_1) > 0) {?>
                <div class="rubik-promo-item" style="background-image:url(<?php echo esc_url($pm_image_1);?>)">
               	    <a class="rubik-promo-link" target="_blank" href="<?php echo esc_url($pm_url_1);?>"></a>
                    <div class="rubik-promo-content">
            			<?php if($pm_title_1) : ?>
            				<h4><?php echo esc_html($pm_title_1); ?></h4>
            			<?php endif; ?>
            		</div>
                </div>
            <?php }?>  
            
            <?php if (strlen($pm_image_2) > 0) {?>
                <div class="rubik-promo-item" style="background-image:url(<?php echo esc_url($pm_image_2);?>)">
               	    <a class="rubik-promo-link" target="_blank" href="<?php echo esc_url($pm_url_2);?>"></a>
                    <div class="rubik-promo-content">
            			<?php if($pm_title_2) : ?>
            				<h4><?php echo esc_html($pm_title_2); ?></h4>
            			<?php endif; ?>
            		</div>
                </div>
            <?php }?>  
            
            <?php if (strlen($pm_image_3) > 0) {?>
                <div class="rubik-promo-item" style="background-image:url(<?php echo esc_url($pm_image_3);?>)">
               	    <a class="rubik-promo-link" target="_blank" href="<?php echo esc_url($pm_url_3);?>"></a>
                    <div class="rubik-promo-content">
            			<?php if($pm_title_3) : ?>
            				<h4><?php echo esc_html($pm_title_3); ?></h4>
            			<?php endif; ?>
            		</div>
                </div>
            <?php }?>                   
            </div><!-- Close ads module -->
            <?php
    	}    
    }
}