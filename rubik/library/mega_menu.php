<?php

/**
* CUSTOM WALKER
*---------------------------------------------------
*/ 


/*--- Frontend Walker ---*/
class BK_Walker extends Walker_Nav_Menu {
    
    function start_el( &$output, $object, $depth = 0, $args = array(), $id = 0) {
        parent::start_el( $output, $object, $depth, $args );
        
        global $bk_megamenu_carousel_el;
        
        rubik_section_parent::$rubik_ajax_c['mega-'.$object->object_id]['offset'] = 0;
        
        $bk_mega_menu = $object->bkmegamenu;
        
        if ( $bk_mega_menu == NULL ) {
             $bk_mega_menu = '0'; 
        }
        
        rubik_section_parent::$rubik_ajax_c['megamenu'][$object->object_id]['content'] = '';
        rubik_section_parent::$rubik_ajax_c['megamenu'][$object->object_id]['cat-link'] = get_category_link( $object->object_id );
        
        $rubik_option = rubik_core::bk_get_global_var('rubik_option');
        $bk_output = $bk_posts = $bk_menu_featured = $bk_has_children = $bk_carousel_item_num = NULL;
        $bk_current_type = $object->object;
        $bk_current_classes = $object->classes;
        if ( in_array('menu-item-has-children', $bk_current_classes) ) { $bk_has_children = ' bk-with-sub'; }
        
        if (($object->menu_item_parent == '0')&($object->bkmegamenu == '1')) {
            $bk_mega_id = "bk-mega-".$object->ID;
            if ($bk_has_children == ' bk-with-sub') { 
                $bk_qry_amount = 4;
            } else { 
                $bk_qry_amount = 4;
            }
        }else {
            $bk_qry_amount = 0;
        }
        
        if ( $bk_mega_menu == 1 ) { 
            if ($object->object == "category") {
                
                $output .= '<div class="bk-mega-menu ">'; 
                $bk_cat_id = $object->object_id; 
                
                $bk_args = array( 'cat' => $bk_cat_id,  'post_type' => 'post',  'post_status' => 'publish', 'ignore_sticky_posts' => 1,  'posts_per_page' => $bk_qry_amount);
                $bk_qry_latest = $bk_img = $bk_cat_link = NULL;
                $bk_qry_latest = new WP_Query($bk_args);
            
                $bk_posts = rubik_core::bk_get_megamenu_posts($bk_qry_latest, $bk_qry_amount); 
                
                wp_reset_postdata(); 
                
            }else if ($object->object == "custom") {  
                $output .= '<div class="bk-mega-column-menu">'; 
            }
        }       
        
        if ( ( $bk_mega_menu == 0 )  && ( $object->menu_item_parent == '0')&& ( in_array('menu-item-has-children', $bk_current_classes) ) ) { 
            $output .= '<div class="bk-dropdown-menu">';
        }

        
        if ( $bk_posts != NULL ) {
            $output .= '<div title="'.$bk_mega_id.'" class="bk-sub-posts'.$bk_has_children.' clearfix">
                        <ul title="mega-'.$bk_cat_id.'" class="mega-row clearfix">'. $bk_posts .'</ul>
                        '.rubik_core::bk_megamenu_ajax_pagination($bk_cat_id).'
                     </div>'; 
        } 
        if ( ($bk_has_children == NULL) && ($object->bkmegamenu == '1') ) {
                $bk_closer = '</div>';
            } else {
                $bk_closer = NULL;
            }
        $output .= $bk_closer;

    
    }
    
    //start of the sub menu wrap
    function start_lvl( &$output, $depth=0, $args = array() ) {
        if ( $depth >= 3 )  { $output .= '<ul class="bk-sub-sub-menu">'; }
        if ( $depth == 2 )  { $output .= '<ul class="bk-sub-sub-menu">'; }
        if ( $depth == 1 )  { $output .= '<ul class="bk-sub-sub-menu bk-sub-dropdown-menu">'; }
        if ( $depth == 0 )  { $output .= '<div class="bk-sub-menu-wrap"><ul class="bk-sub-menu clearfix">'; }
    }
 
    //end of the sub menu wrap
    function end_lvl( &$output, $depth=0, $args = array() ) {
        if ( $depth == 0 ) { $output .= '</ul></div></div>'; }
        if ( $depth == 1 ) { $output .= '</ul>'; }
        if ( $depth == 2 ) { $output .= '</ul>'; }
        if ( $depth >= 3 ) { $output .= '</ul>'; }
        
    }    
}

/*--- Backend Walker ---*/
class bk_walker_backend extends Walker_Nav_Menu {
    function start_lvl( &$output, $depth = 0, $args = array() ) {}
    function end_lvl( &$output, $depth = 0, $args = array() ) {}
    
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $_wp_nav_menu_max_depth;
        $_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        ob_start();
        $item_id = esc_attr( $item->ID );
        if (empty($item->bkmegamenu[0])) {
            $bk_item_megamenu = NULL;
        } else {
            $bk_item_megamenu = esc_attr ($item->bkmegamenu[0]);
        }
        $removed_args = array( 'action','customlink-tab', 'edit-menu-item', 'menu-item', 'page-tab',  '_wpnonce', );

        $original_title = '';
        if ( 'taxonomy' == $item->type ) {
            $original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
            if ( is_wp_error( $original_title ) )
                $original_title = false;
        } elseif ( 'post_type' == $item->type ) {
            $original_object = get_post( $item->object_id );
            $original_title = $original_object->post_title;
        }

        $classes = array(
            'menu-item menu-item-depth-' . $depth,
            'menu-item-' . esc_attr( $item->object ),
            'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
        );

        $title = $item->title;

        if ( ! empty( $item->_invalid ) ) {
            $classes[] = 'menu-item-invalid';
            /* translators: %s: title of menu item which is invalid */
            $title = sprintf( esc_html__( '%s (Invalid)' , 'rubik'), $item->title );
        } elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
            $classes[] = 'pending';
            /* translators: %s: title of menu item in draft status */
            $title = sprintf( esc_html__('%s (Pending)' , 'rubik'), $item->title);
        }

        $title = ( ! isset( $item->label ) || '' == $item->label ) ? $title : $item->label;

        $submenu_text = '';
        if ( 0 == $depth )
            $submenu_text = 'style="display: none;"';

        ?>
        <li id="menu-item-<?php echo esc_attr($item_id); ?>" class="<?php echo implode(' ', $classes ); ?>">
            <dl class="menu-item-bar">
                <dt class="menu-item-handle">
                    <span class="item-title"><span class="menu-item-title"><?php echo esc_html( $title ); ?></span> <?php if ($depth != 0) {?><span class="is-submenu" <?php echo esc_attr($submenu_text); ?>><?php esc_html_e( 'sub item' , 'rubik'); ?></span><?php }?></span>
                    <span class="item-controls">
                        <span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
                        <span class="item-order hide-if-js">
                            <a href="<?php
                                echo wp_nonce_url(
                                    add_query_arg(
                                        array(
                                            'action' => 'move-up-menu-item',
                                            'menu-item' => $item_id,
                                        ),
                                        esc_url(remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) ))
                                    ),
                                    'move-menu_item'
                                );
                            ?>" class="item-move-up"><abbr title="<?php esc_attr_e('Move up', 'rubik'); ?>">&#8593;</abbr></a>
                            |
                            <a href="<?php
                                echo wp_nonce_url(
                                    add_query_arg(
                                        array(
                                            'action' => 'move-down-menu-item',
                                            'menu-item' => $item_id,
                                        ),
                                        esc_url(remove_query_arg($removed_args, admin_url( 'nav-menus.php' )) )
                                    ),
                                    'move-menu_item'
                                );
                            ?>" class="item-move-down"><abbr title="<?php esc_attr_e('Move down', 'rubik'); ?>">&#8595;</abbr></a>
                        </span>
                        <a class="item-edit" id="edit-<?php echo esc_attr($item_id); ?>" title="<?php esc_attr_e('Edit Menu Item', 'rubik'); ?>" href="<?php
                            echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : esc_url(add_query_arg( 'edit-menu-item', $item_id, esc_url(remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) )) ));
                        ?>"><?php esc_html_e( 'Edit Menu Item' , 'rubik'); ?></a>
                    </span>
                </dt>
            </dl>

            <div class="menu-item-settings" id="menu-item-settings-<?php echo esc_attr($item_id); ?>">
                <?php if( 'custom' == $item->type ) : ?>
                    <p class="field-url description description-wide">
                        <label for="edit-menu-item-url-<?php echo esc_attr($item_id); ?>">
                            <?php esc_html_e( 'URL' , 'rubik'); ?><br />
                            <input type="text" id="edit-menu-item-url-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
                        </label>
                    </p>
                <?php endif; ?>
                <p class="description description-thin">
                    <label for="edit-menu-item-title-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Navigation Label' , 'rubik'); ?><br />
                        <input type="text" id="edit-menu-item-title-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
                    </label>
                </p>
                <p class="description description-thin">
                    <label for="edit-menu-item-attr-title-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Title Attribute' , 'rubik' ); ?><br />
                        <input type="text" id="edit-menu-item-attr-title-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
                    </label>
                </p>
                <p class="field-link-target description">
                    <label for="edit-menu-item-target-<?php echo esc_attr($item_id); ?>">
                        <input type="checkbox" id="edit-menu-item-target-<?php echo esc_attr($item_id); ?>" value="_blank" name="menu-item-target[<?php echo esc_attr($item_id); ?>]"<?php checked( $item->target, '_blank' ); ?> />
                        <?php esc_html_e( 'Open link in a new window/tab' , 'rubik'); ?>
                    </label>
                </p>
                <p class="field-css-classes description description-thin">
                    <label for="edit-menu-item-classes-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'CSS Classes (optional)' , 'rubik'); ?><br />
                        <input type="text" id="edit-menu-item-classes-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
                    </label>
                </p>
                <p class="field-xfn description description-thin">
                    <label for="edit-menu-item-xfn-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Link Relationship (XFN)' , 'rubik'); ?><br />
                        <input type="text" id="edit-menu-item-xfn-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
                    </label>
                </p>
                <p class="field-bkmegamenu description">
                    <?php if ($depth == 0 && (($item->object == 'category') || ($item->object == 'custom'))) { ?>
                    <label for="edit-menu-item-bkmegamenu-<?php echo esc_attr($item_id); ?>">Megamenu</label>
                    <input type="checkbox" id="edit-menu-item-bkmegamenu-<?php echo esc_attr($item_id); ?>" name="menu-item-bkmegamenu[<?php echo esc_attr($item_id); ?>]" value="1" <?php checked( $bk_item_megamenu,1 ); ?> />
                    <?php } ?>
                </p>
                <p class="field-description description description-wide">
                    <label for="edit-menu-item-description-<?php echo esc_attr($item_id); ?>">
                        <?php esc_html_e( 'Description' , 'rubik'); ?><br />
                        <textarea id="edit-menu-item-description-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo esc_attr($item_id); ?>]">
                            <?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
                        <span class="description"><?php esc_html_e('The description will be displayed in the menu if the current theme supports it.' , 'rubik'); ?></span>
                    </label>
                </p>  
                <p class="field-move hide-if-no-js description description-wide">
                    <label>
                        <span><?php esc_html_e( 'Move' , 'rubik'); ?></span>
                        <a href="#" class="menus-move-up"><?php esc_html_e( 'Up one' , 'rubik'); ?></a>
                        <a href="#" class="menus-move-down"><?php esc_html_e( 'Down one' , 'rubik'); ?></a>
                        <a href="#" class="menus-move-left"></a>
                        <a href="#" class="menus-move-right"></a>
                        <a href="#" class="menus-move-top"><?php esc_html_e( 'To the top' , 'rubik'); ?></a>
                    </label>
                </p>

                <div class="menu-item-actions description-wide submitbox">
                    <?php if( 'custom' != $item->type && $original_title !== false ) : ?>
                        <p class="link-to-original">
                            <?php printf( esc_html__('Original: %s' , 'rubik'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
                        </p>
                    <?php endif; ?>
                    <a class="item-delete submitdelete deletion" id="delete-<?php echo esc_attr($item_id); ?>" href="<?php
                    echo wp_nonce_url(
                        add_query_arg(
                            array(
                                'action' => 'delete-menu-item',
                                'menu-item' => $item_id,
                            ),
                            admin_url( 'nav-menus.php' )
                        ),
                        'delete-menu_item_' . $item_id
                    ); ?>"><?php esc_html_e( 'Remove' , 'rubik'); ?></a> <span class="meta-sep hide-if-no-js"> | </span> <a class="item-cancel submitcancel hide-if-no-js" id="cancel-<?php echo esc_attr($item_id); ?>" href="<?php echo esc_url( add_query_arg( array( 'edit-menu-item' => $item_id, 'cancel' => time() ), admin_url( 'nav-menus.php' ) ) );
                        ?>#menu-item-settings-<?php echo esc_attr($item_id); ?>"><?php esc_html_e('Cancel' , 'rubik'); ?></a>
                </div>

                <input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr($item_id); ?>" />
                <input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
                <input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
                <input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
                <input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
                <input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
            </div><!-- .menu-item-settings-->
            <ul class="menu-item-transport"></ul>
        <?php
        $output .= ob_get_clean();
    }
}

if ( ! function_exists( 'bk_megamenu_walker' ) ) { 
    function bk_megamenu_walker($walker) {
            if ( $walker === 'Walker_Nav_Menu_Edit' ) {
                        $walker = 'bk_walker_backend';
                  }
           return $walker;
        }
}
add_filter( 'wp_edit_nav_menu_walker', 'bk_megamenu_walker');  

if ( ! function_exists( 'bk_megamenu_walker_save' ) ) { 
    function bk_megamenu_walker_save($menu_id, $menu_item_db_id) {

        if  (isset($_POST['menu-item-bkmegamenu'][$menu_item_db_id])) {
                update_post_meta( $menu_item_db_id, '_menu_item_bkmegamenu', $_POST['menu-item-bkmegamenu'][$menu_item_db_id]);
        } else {
            update_post_meta( $menu_item_db_id, '_menu_item_bkmegamenu', '0');
        }
    }
}
add_action( 'wp_update_nav_menu_item', 'bk_megamenu_walker_save', 10, 2 );

if ( ! function_exists( 'bk_megamenu_walker_loader' ) ) { 
    function bk_megamenu_walker_loader($menu_item) {
            $menu_item->bkmegamenu = get_post_meta($menu_item->ID, '_menu_item_bkmegamenu', true);
            return $menu_item;
     }
}
add_filter( 'wp_setup_nav_menu_item', 'bk_megamenu_walker_loader' );