<?php
/**
 * The Default Page Template
 *
 */
 ?>
<?php
    get_header();
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');        
    if ( have_posts() ) : while ( have_posts() ) : the_post();
    $pageLayout     = $rubik_option['bk_page_layout'];    
    if ( is_page() && ! is_front_page() ) : 
        if($pageLayout == 'has_sidebar') {
            get_template_part( '/library/templates/page/page_with__sidebar');
        }else if($pageLayout == 'no_sidebar') {
            get_template_part( '/library/templates/page/page_fw' ); //with-sidebar
        }
    endif; 
    endwhile; endif;
    get_footer();
?>