<?php
if (!class_exists('rubik_fw_slider')) {
    class rubik_fw_slider extends rubik_section_parent  {
        
        static $pageInfo = '';
        
        public function render( $page_info ) {
            self::$pageInfo = $page_info;
            
            global $rubik_dynamic_css;
            $uid = uniqid('fw_slider_');
            
            $moduleAlign = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_align', true ); 
            $moduleStyle = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_style', true );
            
            $rubik_dynamic_css[$uid]['title_font_size'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_font_size', true ); 
            $rubik_dynamic_css[$uid]['title_font_weight'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_font_weight', true );
            
            $rubik_dynamic_css[$uid]['title_letter_spacing'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_letter_spacing', true );
            $rubik_dynamic_css[$uid]['module_margin'] = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_margin', true );
            
            
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_fw_slider'], $page_info);    //get block config
            
            $the_query = bk_get_query::query($module_cfg);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-fw-slider bk-slider-module '.$moduleAlign.' '.$moduleStyle.'">';
            $block_str .= $this->render_modules($the_query);            //render modules
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentin4 = new bk_contentin4;
            $custom_var['cat'] = 'on';
            $custom_var['meta'] = array('author', 'date');
            $custom_var['thumbnail'] = 'rubik-900-613';
            
            $meta_items = get_post_meta( self::$pageInfo['page_id'], self::$pageInfo['block_prefix'].'_meta_items', true ); 
            if($meta_items == 'hide-cat-meta') {
                $custom_var['meta'] = 'off';
                $custom_var['cat'] = 'off';
            }else if($meta_items == 'hide-cat') {
                $custom_var['cat'] = 'off';
            }else if($meta_items == 'hide-meta') {
                $custom_var['meta'] = 'off';
            }
            
            if ( $the_query->have_posts() ) :            
                $render_modules .= '<div class="flexslider">';
                $render_modules .= '<ul class="col-md-12 slides">';
                
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="item content_in">';
                    $render_modules .= $bk_contentin4->render($custom_var);
                    $render_modules .= '</li>';
                endwhile;
                
                $render_modules .= '</ul></div> <!-- Close render modules -->';
                
            endif;
            return $render_modules;
        }
        
    }
}