<?php
if (!class_exists('rubik_feature3')) {
    class rubik_feature3 extends rubik_section_parent  {
        
        static $pageInfo = '';
        
        public function render( $page_info ) {
            self::$pageInfo = $page_info;
            
            global $rubik_dynamic_css;
            $uid = uniqid('featured3-');
                      
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $block_str = '';
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['fullwidth']['bk_feature3'], $page_info);    //get block config
            $module_cfg['limit'] = 5;
            
            $the_query = bk_get_query::query($module_cfg);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-feature3-block">';
            $block_str .= $this->render_modules($the_query);            //render modules
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        static function render_modules ($the_query){
            $render_modules = '';
            $bk_contentin4 = new bk_contentin4;
            $bk_contentout3 = new bk_contentout3;
            $render_modules_tmp_open = '';
            $render_modules_tmp = '';
            $render_modules_tmp_close = '';
            
            $custom_var['cat'] = 'on';
            $custom_var['meta'] = array('author', 'date');
            $custom_var['thumbnail'] = 'rubik-900-613';
            
            $meta_items = get_post_meta( self::$pageInfo['page_id'], self::$pageInfo['block_prefix'].'_meta_items', true ); 
            if($meta_items == 'hide-cat-meta') {
                $custom_var['meta'] = 'off';
                $custom_var['cat'] = 'off';
            }else if($meta_items == 'hide-cat') {
                $custom_var['cat'] = 'off';
            }else if($meta_items == 'hide-meta') {
                $custom_var['meta'] = 'off';
            }
            
            $custom_var_out3 = array (
                'meta'         => array('date'),
            );
            if ( $the_query->have_posts() ) : $the_query->the_post();
                $postID = get_the_ID();
                $category = get_the_category($postID); 
                $bkThumbId = get_post_thumbnail_id( $postID );
                $bkThumbUrl = wp_get_attachment_image_src( $bkThumbId, $custom_var['thumbnail'] );

                $render_modules .= '<div class="rubik-featured3-main">';
                $render_modules .= '<div class="bk-thumb-wrap term-'.$category[0]->term_id.'"><a class="link-overlap" href="'.esc_url(get_permalink($postID)).'"></a><div class="thumb hide-thumb" data-type="background" style="background-image: url('.$bkThumbUrl[0].')"></div></div>';
                $render_modules .= '<div class="post-list-wrap">';
                $render_modules .= '<div class="post-list-wrap-inner container bkwrapper">';
                $render_modules .= '<div class="large-post-content post-c-wrap">';                                
                    if (isset($custom_var['cat']) && ($custom_var['cat'] != 'off')) :
                        $render_modules .= rubik_core::bk_meta_cases('cat');
                    endif;
                    $render_modules .= rubik_core::bk_get_post_title($postID);
                    if (isset($custom_var['meta']) && ($custom_var['meta'] != 'off')) :
                        $render_modules .= rubik_core::bk_get_post_meta($custom_var['meta']);
                    endif;     
                $render_modules .= '</div>';   
                if ( $the_query->have_posts() ) :            
                    $render_modules_tmp_open .= '<ul class="list-small-post row rubik-hidden-991">';
                    while ( $the_query->have_posts() ): $the_query->the_post();
                        $postID = get_the_ID();
                        $render_modules_tmp .= '<li class="small-post col-md-3 col-sm-6 bk-post-title-small clearfix">';
                        $render_modules_tmp .= '<div class="small-post-inner post-c-wrap">';
                        $render_modules_tmp .= rubik_core::bk_get_post_title($postID);
                        $render_modules_tmp .= rubik_core::bk_get_post_meta($custom_var_out3['meta']);
                        $render_modules_tmp .= '</div>';
                        $render_modules_tmp .= '</li><!-- End post -->';        
                    endwhile;
                    $render_modules_tmp_close .= '</ul> <!-- End list-post -->';
                else:
                    $render_modules_tmp_open = '';
                    $render_modules_tmp = '';
                    $render_modules_tmp_close = '';
                endif;    
                $render_modules .=  $render_modules_tmp_open . $render_modules_tmp . $render_modules_tmp_close;
                $render_modules .= '</div></div>';
                $render_modules .= '</div><!-- Close Main Section -->';
                $render_modules .= '<div class="container bkwrapper rubik-hidden-desktop rubik-show-991"><ul class="list-small-post row clearfix">';
                $render_modules .= $render_modules_tmp;
                $render_modules .= '</ul></div>';
            endif;       
            
            return $render_modules;
        }
        
    }
}