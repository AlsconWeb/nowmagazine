/* -----------------------------------------------------------------------------
 * Page Template Meta-box
 * -------------------------------------------------------------------------- */
;(function( $, window, document, undefined ){
	"use strict";
	
	$( document ).ready( function () {
        function selectAndScrollToOption(select) {
            var $select;
            var $selectedOptions
            $select = $(select);
            $select.each(function(index,el){
                // Store the currently selected options
                $selectedOptions = $(el).find("option[selected]");
                       
                // Re-select the original options
                $selectedOptions.prop("selected", true);
            });
        }
        var select = $('.bk-category-field');
        var selecttabs = $('.bk-categorytabs-field');

        selectAndScrollToOption(selecttabs);
        selectAndScrollToOption(select);
        
        /** Option Tabs **/
        $('.bk-module-tabs a').on('click', function(e) {
            e.preventDefault();
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            if($(this).hasClass('bk-tab-1')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-1').show();
            }else if($(this).hasClass('bk-tab-2')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-2').show();
            }else if($(this).hasClass('bk-tab-3')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-3').show();
            }else if($(this).hasClass('bk-tab-4')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-4').show();
            }else if($(this).hasClass('bk-tab-5')) {
                $(this).parent().siblings('.bk-module-option-wrap').hide();
                $(this).parent().siblings('.option-tab-5').show();
            }
            
        });
        $("body").on('DOMSubtreeModified', "#bk-container", function() {
            $('.bk-module-tabs a').on('click', function(e) {
                e.preventDefault();
                $(this).addClass('active');
                $(this).siblings().removeClass('active');
                if($(this).hasClass('bk-tab-1')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-1').show();
                }else if($(this).hasClass('bk-tab-2')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-2').show();
                }else if($(this).hasClass('bk-tab-3')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-3').show();
                }else if($(this).hasClass('bk-tab-4')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-4').show();
                }else if($(this).hasClass('bk-tab-5')) {
                    $(this).parent().siblings('.bk-module-option-wrap').hide();
                    $(this).parent().siblings('.option-tab-5').show();
                }
                
            });
        });

        // Pagebuilder Tabs
        
	} );
})( jQuery, window , document );
/* -----------------------------------------------------------------------------
 * UUID
 * https://github.com/eburtsev/jquery-uuid/blob/master/jquery-uuid.js
 * -------------------------------------------------------------------------- */
;(function( $, window, document, undefined ){
	$.uuid = function() {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
			return v.toString(16);
		});
	};
})( jQuery, window , document );
