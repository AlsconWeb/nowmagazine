<?php
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    $logo = array();
    $bkHeaderType = '';
    if (isset($rubik_option)){  
        if ((isset($rubik_option['bk-logo'])) && (($rubik_option['bk-logo']) != NULL)){ 
            $logo = $rubik_option['bk-logo'];
        }
        if ((isset($rubik_option['bk-header-type'])) && (($rubik_option['bk-header-type']) != NULL)){ 
            $bkHeaderType = $rubik_option['bk-header-type'];
        }
        $header_banner = $rubik_option['bk-header-banner-switch'];
        $ga_script = $rubik_option['bk-banner-script'];
        $backtop = $rubik_option['bk-backtop-switch'];
        if ($header_banner){ 
            $imgurl = $rubik_option['bk-header-banner']['imgurl'];
            $linkurl = $rubik_option['bk-header-banner']['linkurl'];
        }
        $canvasSwitch = isset($rubik_option['bk-canvas-desktop-switch']) ? $rubik_option['bk-canvas-desktop-switch'] : 0;
        $canvasPosition = isset($rubik_option['bk-canvas-button-position']) ? $rubik_option['bk-canvas-button-position'] : 'left';
    }
    ?>
    <?php get_template_part( 'library/templates/headers/rubik_canvas_menu' );?>
    <div id="page-inner-wrap">
        <div class="page-cover canvas-menu-close"></div>
        <div class="bk-page-header header-3">            
            <div class="header-wrap">
                <!-- ticker open -->
                <?php        
                    if (isset($rubik_option)){
                        if (isset($rubik_option['bk-header-ticker']) && ($rubik_option['bk-header-ticker'] == 1)) {?>
                            <div class="bk-ticker-module bk-white-bg">
                            <?php rubik_core::bk_get_ticker('header');?>
                            </div><!--end ticker-module-->
                        <?php }
                    }
                ?>
                <!-- ticker close -->
                
                <?php echo rubik_core::rubik_top_bar();?>
                
                <!-- nav open -->
        		<nav class="main-nav">
                    <div class="main-nav-inner bkwrapper container">
                        <div class="main-nav-container clearfix">
                            <div class="main-nav-wrap">
                                <div class="canvas-menu-wrap <?php if($canvasSwitch == 1) echo 'show-on-desktop';?> <?php if($canvasPosition != 'right') echo 'left'; else echo 'right';?>">
                                    <?php echo rubik_core::bk_canvas_button();?>
                                </div>
                                
                                <div class="header-logo-wrapper">
                                    
                                    <!-- logo open -->
                                    <?php if (($logo != null) && (array_key_exists('url',$logo))) {
                                            if ($logo['url'] != '') {
                                        ?>
                        			<div class="logo">
                                        <a href="<?php echo esc_url(get_home_url('/'));?>">
                                            <img src="<?php echo esc_url($logo['url']);?>" alt="<?php esc_attr_e( 'Logo', 'rubik' ); ?>"/>
                                        </a>
                        			</div>
                        			<!-- logo close -->
                                    <?php } else {?> 
                                    <div class="logo logo-text">
                                        <a href="<?php echo esc_url(get_home_url('/'));?>">
                                            <?php echo esc_attr(bloginfo( 'name' ));?>
                                        </a>
                        			</div>
                                    <?php }
                                    } else {?> 
                                    <div class="logo logo-text">
                                        <a href="<?php echo esc_url(get_home_url('/'));?>">
                                            <?php echo esc_attr(bloginfo( 'name' ));?>
                                        </a>
                        			</div>
                                    <?php } ?>  
                                </div>  
                                
                                <?php if ( has_nav_menu( 'main-menu' ) ) { 
                                    wp_nav_menu( array( 
                                        'theme_location' => 'main-menu',
                                        'container_class' => 'main-menu',
                                        'walker' => new BK_Walker,
                                        ) );}?>
                            </div>
                            <?php 
                                echo rubik_ajax_form_search();
                            ?> 
                        </div>    
                    </div><!-- main-nav-inner -->    
                    <div class="rubik-ajax-search-bg"></div>      
        		</nav>
                <!-- nav close -->
    		</div>
            <?php 
                if(isset($rubik_option['bk-sticky-nav-switch']) && ($rubik_option['bk-sticky-nav-switch'] == 2)) :
                    get_template_part( 'library/templates/headers/rubik_sticky_header' );
                endif;    
            ?>
        </div>                
        <?php if(function_exists('rubik_breadcrumbs')) :?>         
            <div class="bk-breadcrumbs-container">
            <?php rubik_breadcrumbs();?>
            </div>
        <?php endif;?>
        
        <!-- backtop open -->
		<?php if ($backtop) { ?>
            <div id="back-top"><i class="fa fa-long-arrow-up"></i></div>
        <?php } ?>
		<!-- backtop close -->