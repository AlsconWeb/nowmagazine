<?php 
/**
 * Random Posts Recommend Box. Appears in single.php
**/
$rubik_option = rubik_core::bk_get_global_var('rubik_option');
if(isset($rubik_option['recommend-number']) && ($rubik_option['recommend-number']) != null) {$entries = $rubik_option['recommend-number'];}else {$entries = 3;};
if(isset($rubik_option['recommend-categories']) && ($rubik_option['recommend-categories'] != null)){ $cat_id = $rubik_option['recommend-categories'];} else {$cat_id = 0;};
?>
<?php 
    $bk_contentout3 = new bk_contentout3;
       
   $custom_var_out3 = array (
        'thumbnail'    => 'rubik-90-65',
        'meta'         => array('date'),
    );
    
   $arg =  array(
        'post_type' => 'post',
        'post__not_in' => array( $post->ID ),
        'category__in' => $cat_id,
        'orderby' => 'rand',
        'ignore_sticky_posts' => 1,
        'posts_per_page' => $entries
    );
    $bk_random_post = new WP_Query($arg);
    ?>
    <div class="widget recommend-box">
        <a class="close" href="#" title="<?php esc_attr_e('Close', 'rubik');?>"><i class="fa fa-long-arrow-right"></i></a>
        <h3><?php echo esc_attr($rubik_option['recommend-box-title']);?></h3>
        
        <div class="entries">
            <ul class="list-small-post">
            <?php while ( $bk_random_post->have_posts() ) : $bk_random_post->the_post();?>
                <li class="small-post content_out clearfix">
                    <?php echo rubik_core::bk_render_html_string($bk_contentout3->render($custom_var_out3));?>
                </li><!-- End post -->      
            <?php endwhile;?>
            </ul> <!-- End list-post -->
        </div>
    </div><!--recommend-box -->
    
    <?php
    wp_reset_query();
?>