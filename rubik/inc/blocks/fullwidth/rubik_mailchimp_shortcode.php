<?php
if (!class_exists('rubik_mailchimp_shortcode')) {
    class rubik_mailchimp_shortcode extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('mailchimp_shortcode-');
            $block_str = '';
            $i=0;
            $shortcode_str = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_shortcode', true );
            $layout = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_layout', true );
            if (substr( $page_info['block_prefix'], 0, 10 ) == 'bk_has_rsb') {
                $containerClass = '';                         
            }else {
                $containerClass = 'bkwrapper container';            
            }
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
                        
            $moduleHasbg = '';                                    
            $moduleHasbg = $rubik_dynamic_css[$uid]['moduleHasbg'];
            
            if($layout != 'boxed') {
                $block_str .= '<div id="'.$uid.'" class="bkmodule module_mailchimp_shortcode '.$moduleHasbg.' clearfix"><div class="'.$containerClass.'">';
            }else {
                $block_str .= '<div class="bkmodule module_mailchimp_shortcode '.$containerClass.'"><div id="'.$uid.'" class="rubik-boxed-layout '.$moduleHasbg.' clearfix">';
            }
            
            $block_str .= '<div class="rubik-inline-subscribeform">';
            $block_str .= do_shortcode($shortcode_str);
            $block_str .= '</div><!-- End rubik-inline-subscribeform -->';
            $block_str .= '</div></div><!-- End module -->';
            wp_reset_postdata();
            return $block_str;
    	}
        
    }
}