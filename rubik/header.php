<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    
    <link rel="profile" href="https://gmpg.org/xfn/11" />

	<?php wp_head(); ?>
</head>
<?php
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');
    
    $moduleHeaderStyle = 'module_header_1';
    if (isset($rubik_option['bk-module-header']) && ($rubik_option['bk-module-header'] != '')) {
        $moduleHeaderStyle = $rubik_option['bk-module-header'];
    }else {
        $moduleHeaderStyle = 'module_header_1';
    }
    
    $footerHeaderStyle = 'footer_header_1';
    if (isset($rubik_option['bk-footer-header']) && ($rubik_option['bk-footer-header'] != '')) {
        $footerHeaderStyle = $rubik_option['bk-footer-header'];
    }else {
        $footerHeaderStyle = 'footer_header_1';
    }
    
    $sidebarHeaderStyle = 'sidebar_header_1';
    if (isset($rubik_option['bk-sidebar-header']) && ($rubik_option['bk-sidebar-header'] != '')) {
        $sidebarHeaderStyle = $rubik_option['bk-sidebar-header'];
    }else {
        $sidebarHeaderStyle = 'sidebar_header_1';
    }
    
    $header_class_defined = $moduleHeaderStyle.' '.$sidebarHeaderStyle.' '.$footerHeaderStyle;
    
    $bkSiteLayout = $rubik_option['bk-site-layout'];
    
?>
<body <?php body_class($header_class_defined); ?>>
    <?php
        $bkSingleTemplate = '';
        if(is_single()) {
            $bkPostLayout = get_post_meta(get_the_ID(),'bk_post_layout',true);
            if($bkPostLayout == 'single_template_17') {
                $bkSiteLayout = 'boxed';
                echo '<div class="bk-backstretch">'.get_the_post_thumbnail(get_the_ID(), 'full').'</div>';
                $bkSingleTemplate = 'single_template_17';
            }
        }
    ?>
    <div id="page-wrap" <?php if($bkSiteLayout == 'wide') {echo 'class="wide"';}else {echo 'class="boxed-layout"';}?>>
        <?php
            rubik_get_header();
        ?>
        <div class="rubik-page-content-wrapper clearfix <?php echo esc_attr($bkSingleTemplate);?>">    