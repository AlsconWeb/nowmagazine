<?php
if (!class_exists('rubik_youtube_playlist_fw_2')) {
    class rubik_youtube_playlist_fw_2 extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            $uid = uniqid('youtube_playlist_2-');
            
            $block_str = '';
            
            $YTplaylist_URL = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_youtube_playlist_url', true );
            
            if ($YTplaylist_URL == '') {
                $block_str .= '<div class="bkwrapper container">';
                $block_str .= '<div class="bk-notice">Playlist URL is empty. Please insert the necessary data to the setting fields.</div>';
                $block_str .= '</div>';
                return $block_str;
            }
            
            $YTplaylist = rubik_youtube::rubik_get_playlist($YTplaylist_URL);
            if (!( $YTplaylist && ! empty( $YTplaylist['videos'] ) )) :
                $block_str .= '<div class="bkwrapper container">';
                $block_str .= '<div class="bk-notice">Cannot fetch URL, please check and correct it.</div>';
                $block_str .= '</div>';
                return $block_str;
            endif;
            
            $module_bg = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_module_bg', true );
            $playing_title_bg = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_playing_title_bg', true );
            $playing_title_color = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_playing_title_color', true );
            
            $module_title = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_title', true );
            
            $rubik_dynamic_css[$uid]['module_bg'] = $module_bg;
            $rubik_dynamic_css[$uid]['playing_title_bg'] = $playing_title_bg;
            $rubik_dynamic_css[$uid]['playing_title_color'] = $playing_title_color;
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $bk_color_opt = array();
            
            $bk_color_opt['module_bg_class'] = '';
            $bk_color_opt['title_bg_class'] = '';
            $bk_color_opt['title_color_class'] = '';
            
            if($module_bg != '') {
                $bk_color_opt['module_bg_class'] = 'bk-module-inline-bg';
            }
            if($playing_title_bg != '') {
                $bk_color_opt['title_bg_class'] = 'bk-title-inline-bg';
            }
            if($playing_title_color != '') {
                $bk_color_opt['title_color_class'] = 'bk-title-color-bg';
            }
            
            $block_str .= '<div id='.$uid.' class="bkmodule module-youtube-playlist playlist-layout2 '.$bk_color_opt['module_bg_class'].'">';
            $block_str .= '<div class="bkwrapper container">';
            
            $block_str .= '<div class="video_playlist_module_title">';
            $block_str .= '<span class="video_title_text">';
            $block_str .= $module_title;
            $block_str .= '</span>';
            $block_str .= '</div>';
            
            $block_str .= rubik_youtube::rubik_youtube_playlist_render($YTplaylist, $bk_color_opt, 'layout-2');
          
            $block_str .= '</div><!-- Close container bkwrapper -->';
            $block_str .= '</div><!-- Close module -->';
            return $block_str;
    	}    
    }
}