<?php
define('MAX_COUNT', 100);
/* -----------------------------------------------------------------------------
 * Render Sections
 * -------------------------------------------------------------------------- */
if ( ! function_exists( 'bk_page_builder' ) ) {
	function bk_page_builder( $args=array() ) {
		$page_info['page_id'] = get_queried_object_id();
        global $rubik_ajax_btnstr, $rubik_option;
        wp_localize_script( 'rubik-module-load-post', 'ajax_btn_str', $rubik_ajax_btnstr );
		for ( $counter=1; $counter < MAX_COUNT; $counter++ ) { 
			$field_prefix = 'bk_section_'.$counter;
			$section_type = get_post_meta( $page_info['page_id'], $field_prefix, true );
			if ( ! $section_type ) continue;

            if ($section_type == 'fullwidth'){?>
                <div class="fullwidth bksection">
                    <div class="sec-content">
                        <?php
                        for ($mcount=1; $mcount < MAX_COUNT; $mcount ++) {
                            $page_info['block_prefix'] = 'bk_fullwidth_module_'.$counter.'_'.$mcount;
                            $block_type = get_post_meta( $page_info['page_id'], $page_info['block_prefix'], true );
                            if ( ! $block_type ) continue;
                            $class = "rubik_".$block_type;
                            $section_render = new $class();
                            echo rubik_core::bk_render_html_string($section_render->render($page_info));
                        }?>
                    </div>
                </div>
            <?php
            }else if($section_type == 'has-rsb') {
                $sidebar_prefix = 'bk_sidebar_'.$counter;
                $sidebar = get_post_meta( $page_info['page_id'], $sidebar_prefix, true );
                $sidebarPos_key = 'bk_sidebarpos_'.$counter;
                $sidebarPos = get_post_meta( $page_info['page_id'], $sidebarPos_key, true );
                ?>
                
                <div class="has-sb container bkwrapper bksection <?php if($sidebarPos == 'left') echo('has-left-sidebar');?>">
                    <div class="row">
                        <div class="content-wrap col-md-8">
                        <?php
                            for ($mcount=1; $mcount < MAX_COUNT; $mcount ++) {
                                $page_info['block_prefix'] = 'bk_has_rsb_module_'.$counter.'_'.$mcount;
                                $block_type = get_post_meta( $page_info['page_id'], $page_info['block_prefix'], true );
                                if ( ! $block_type ) continue;
                                $class = "rubik_".$block_type;
                                $section_render = new $class();
                                echo rubik_core::bk_render_html_string($section_render->render($page_info));
                            }?>
                        </div>
                    
                        <div class='sidebar col-md-4'>
                            <div class="sidebar-wrap <?php if($rubik_option['pagebuilder-sidebar'] == 'enable') echo 'stick';?>" id="<?php echo esc_attr($sidebar_prefix);?>">
                                <?php dynamic_sidebar( $sidebar );?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
		}
	}
}
