<?php if (is_active_sidebar('footer_d3_1') 
        || is_active_sidebar('footer_d3_2')
        || is_active_sidebar('footer_d3_3')) { ?>
<div class="footer-content footer-d3 bkwrapper clearfix container">
    <div class="row">
        <div id="footer_d3_1" class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_d3_1' ); ?>
        </div>
        <div id="footer_d3_2" class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_d3_2' ); ?>
        </div>
        <div id="footer_d3_3" class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_d3_3' ); ?>
        </div>
    </div>
</div>
<?php } ?>