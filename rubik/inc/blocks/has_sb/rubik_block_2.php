<?php
if (!class_exists('rubik_block_2')) {
    class rubik_block_2 extends rubik_section_parent  {
        
        public function render( $page_info ) {
            global $rubik_dynamic_css;
            
            $block_str = '';
            $uid = uniqid('block_2-');
            
            $bk_ajax_button = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_ajax_button', true );
            
            $rubik_dynamic_css = rubik_core::rubik_set_dynamic_css($page_info, $rubik_dynamic_css, $uid);
            
            $cfg_ops = array();
            $cfg_ops = $this->cfg_options(); 
            $module_cfg = bk_get_cfg::configs($cfg_ops['has_sb']['bk_block_2'], $page_info);    //get block config
            
            /** Tabs **/
            $rubik_tabs = get_post_meta( $page_info['page_id'], $page_info['block_prefix'].'_categorytabs', true );
            
            $rubikTabsToArray = explode(",",$rubik_tabs);
            
            $tabs_amount = intval(count($rubikTabsToArray));
            
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['content'] = '';
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['offset'] = $module_cfg['offset'];
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['entries'] = $module_cfg['limit'];
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['prev'] = '';
            rubik_section_parent::$rubik_ajax_c[$uid]['tabfirst']['next'] = '';
            
            if($tabs_amount > 0) {
                for ($i=0; $i< $tabs_amount; $i++) {
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['cat'] = $rubikTabsToArray[$i];
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['content'] = '';
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['offset'] = 0;
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['entries'] = $module_cfg['limit'];
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['prev'] = '';
                    rubik_section_parent::$rubik_ajax_c[$uid]['tab'.$i]['next'] = '';
                }
            }
            
            $the_query = bk_get_query::query($module_cfg, $uid);              //get query
    
            $block_str .= '<div id="'.$uid.'" class="bkmodule module-block-2 clearfix">';
            if ( $the_query->have_posts() ) :
                $block_str .= rubik_core::bk_get_block_title($page_info, $rubikTabsToArray);  //render block title
            endif;
            
            $block_str .= '<div class="bk-block-2-wrap bk-module-inner">';  
            $block_str .= $this->render_modules($the_query);            //render modules
            $block_str .= '</div> <!-- bk-block-2-wrap -->';
            
            if($bk_ajax_button !== 'disable') {
                $block_str .= rubik_core::bk_module_ajax_pagination();
            }
            
            $block_str .= '</div>';
            
            unset($cfg_ops); unset($module_cfg); unset($the_query);     //free
            wp_reset_postdata();
            return $block_str;
    	}
        public function render_modules ($the_query){
            $render_modules = '';
            $bk_contentout3 = new bk_contentout3;
            $custom_var_out3 = array (
                'thumbnail'    => 'rubik-90-65',
                'meta'         => array('date'),
            );
            if ( $the_query->have_posts() ) :           
                $render_modules .= '<ul class="list-small-post row">';
                while ( $the_query->have_posts() ): $the_query->the_post();
                    $render_modules .= '<li class="small-post content_out col-md-6 col-sm-6 clearfix">';
                    $render_modules .= $bk_contentout3->render($custom_var_out3);
                    $render_modules .= '</li><!-- End post -->';        
                endwhile;
                $render_modules .= '</ul> <!-- End list-post -->';
            endif;
            return $render_modules;
        }
        
    }
}