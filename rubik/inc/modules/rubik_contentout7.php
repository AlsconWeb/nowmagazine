<?php
if (!class_exists('bk_contentout7')) {
    class bk_contentout7 {
        
        function render($title_length = 15) {
            ob_start();
            $meta_ar = array('cat', 'date');
            if(rubik_core::bk_check_has_post_thumbnail( get_the_ID() )) {
                echo rubik_core::get_feature_image('rubik-620-420', true);
            }
            ?>
            <div class="post-c-wrap">
                <?php echo rubik_core::bk_get_post_title(get_the_ID(), $title_length);?>
                <?php echo rubik_core::bk_get_post_meta($meta_ar);?> 
            </div>
            <?php return ob_get_clean();
        }
        
    }
}