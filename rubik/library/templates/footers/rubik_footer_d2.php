<?php if (is_active_sidebar('footer_d2_1') 
        || is_active_sidebar('footer_d2_2')) { ?>
<div class="footer-content footer-d2 bkwrapper clearfix container">
    <div class="row">
        <div id="footer_d2_1" class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_d2_1' ); ?>
        </div>
        <div id="footer_d2_2" class="footer-sidebar">
            <?php dynamic_sidebar( 'footer_d2_2' ); ?>
        </div>
    </div>
</div>
<?php } ?>