<?php
/**
 * Registering meta sections for taxonomies
 *
 * All the definitions of meta sections are listed below with comments, please read them carefully.
 * Note that each validation method of the Validation Class MUST return value.
 *
 * You also should read the changelog to know what has been changed
 *
 */

// Hook to 'admin_init' to make sure the class is loaded before
// (in case using the class in another plugin)
add_action( 'admin_init', 'rubik_register_taxonomy_meta_boxes' );

/**
 * Register meta boxes
 *
 * @return void
 */
function rubik_register_taxonomy_meta_boxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Taxonomy_Meta' ) )
		return;
        
    $rubik_option = rubik_core::bk_get_global_var('rubik_option');

    if ( isset($rubik_option) && isset($rubik_option['bk-primary-color'])) {
        $primary_color = $rubik_option['bk-primary-color'];
    }else {
        $primary_color = '';
    }
    
	$bk_sidebar = array();
    foreach ( $GLOBALS['wp_registered_sidebars'] as $value => $label ) {
        $bk_sidebar[$value] = ucwords( $label['name'] );
    }
    $bk_sidebar['global'] = esc_html__( 'From Theme Options', 'rubik' );
    
	$meta_sections = array();

	// First meta section
	$meta_sections[] = array(
		'title'      => esc_html__('BK Category / Tag Options','rubik'),             // section title
		'taxonomies' => array('category', 'post_tag'), // list of taxonomies. Default is array('category', 'post_tag'). Optional
		'id'         => 'bk_cat_opt',                 // ID of each section, will be the option name

		'fields' => array(                             // List of meta fields
			// SELECT
			array(
				'name'    => esc_html__('Category / Tag layouts','rubik'),
				'id'      => 'cat_layout',
				'type'    => 'select',
				'options' => array(
                                'global' => esc_html__('From Theme Options', 'rubik'),
                                'classic-blog' => esc_html__('Classic Blog', 'rubik'), 
                                'large-blog' => esc_html__('Large Blog with Sidebar', 'rubik'),
                                'large-blog-nosb' => esc_html__('Large Blog No Sidebar', 'rubik'),
                                'large-blog-2' => esc_html__('Large Blog 2 with Sidebar', 'rubik'),
                                'large-blog-2-nosb' => esc_html__('Large Blog 2 No Sidebar', 'rubik'),
                                'row'=>esc_html__('Row with Sidebar', 'rubik'),
                                'row-nosb' => esc_html__('Row no Sidebar', 'rubik'),
                                'masonry'=>esc_html__('Masonry with Sidebar', 'rubik'),
                                'masonry-nosb' => esc_html__('Masonry no Sidebar', 'rubik'),
                                'square-grid-3-s1' => esc_html__('Square Grid no Sidebar', 'rubik'),                                                              
                                'square-grid-2-s1' => esc_html__('Square Grid with Sidebar', 'rubik'),                                                             
                            ),
                'std' => 'global',
                'desc' => esc_html__('Global setting option is set in Theme Option panel','rubik')
			),
            // SELECT SIDEBAR
    		array(
    			'name'      => esc_html__('Select a sidebar for this page','rubik'),
    			'id'        => 'sb_category',
                'type'      => 'select',
    			'options'    => $bk_sidebar, 
                'std'       => 'global',
                'desc'      => esc_html__('Global setting option is set in Theme Option panel','rubik')
    		),
            array(
                'id' => 'archive-sidebar-position',  
                'type' => 'select',
                'name' => esc_html__('Sidebar Position', 'rubik'),
                'desc' => esc_html__('Support right and left sidebar', 'rubik'),
                'options' => array('right' => esc_html__('Right', 'rubik'), 
                                    'left' => esc_html__('Left', 'rubik'),
                                    'global' => esc_html__('From Theme Options', 'rubik'),
                                    ),
                'std' => 'global',
            ),
            array(
				'name'  => esc_html__('Category Color','rubik'),
				'id'    => 'cat_color',
				'type'  => 'color',
                'std'   => $primary_color,
                'desc'  => esc_html__('Category Color setting - Only for category','rubik')
			),
            array(
				'name'  => esc_html__('Post Title Font Size Number','rubik'),
				'id'    => 'font_size',
				'type'  => 'text',
                'std'   => '',
                'desc'  => esc_html__('Leave Blank to use the default parameter (in px)','rubik')
			),
            array(
				'name'  => esc_html__('Post Title Letter Spacing Number','rubik'),
				'id'    => 'letter_spacing',
				'type'  => 'text',
                'std'   => '',
                'desc'  => esc_html__('Leave Blank to use the default parameter (in px)','rubik')
			),
		),
	);

	foreach ( $meta_sections as $meta_section )
	{
		new RW_Taxonomy_Meta( $meta_section );
	}
}
